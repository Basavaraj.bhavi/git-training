1
00:00:02,140 --> 00:00:06,430
The pipeline for top environment is ready, but before pushing the branch to rappel.

2
00:00:06,790 --> 00:00:10,000
One more thing what we have to do is and protect the future branches.

3
00:00:10,750 --> 00:00:10,960
Why?

4
00:00:10,990 --> 00:00:16,420
Because the stop environment thing, what we have written will only trigger once you check this option

5
00:00:16,420 --> 00:00:23,230
while merging, which is to delete the feature branch automatically upon merging and as a default behavior.

6
00:00:23,500 --> 00:00:28,030
This delete on merge option on the Merge page won't come for protected branches.

7
00:00:28,360 --> 00:00:31,090
That is why we have to protect the feature branches.

8
00:00:31,510 --> 00:00:32,020
Let's do it.

9
00:00:33,670 --> 00:00:35,620
What was that an apple?

10
00:00:38,330 --> 00:00:39,230
Protected branches.

11
00:00:42,460 --> 00:00:43,090
And correct.

12
00:00:48,220 --> 00:00:54,430
So all the branches starting with feature hyphen keyboard are now unprotected on protecting the branches

13
00:00:54,430 --> 00:00:55,930
has one small ripple effect.

14
00:00:56,290 --> 00:00:57,610
Any guesses what would be that?

15
00:01:00,070 --> 00:01:06,210
Remember, with unprotected brunches, the protected variables are not available in pipelines, so we

16
00:01:06,210 --> 00:01:08,340
need to protect those variables as well.

17
00:01:16,650 --> 00:01:21,750
I know it's a little weird condition to protect the branches and variables for stopping vitamin functioning.

18
00:01:22,110 --> 00:01:23,490
But this is how it is.

19
00:01:25,020 --> 00:01:28,200
Everything is done, let's not push the branch to remote repository.

20
00:01:30,480 --> 00:01:33,720
Actually, I did created a new branch for a start up environment, so.

21
00:01:55,140 --> 00:01:57,270
The pipeline for new feature branch has started.

22
00:02:00,770 --> 00:02:05,690
As you can see, unlike other jobs that I can forestall, future job is a little different.

23
00:02:06,140 --> 00:02:10,370
And hovering over it since when condition was had to manually in the pipeline.

24
00:02:10,520 --> 00:02:13,130
So it's saying storage requires manual action.

25
00:02:14,070 --> 00:02:21,540
And also, as we had the action stop for this job, so we gotta stop environment, but here, let this

26
00:02:21,540 --> 00:02:22,260
pipeline pass.

27
00:02:26,510 --> 00:02:33,050
See the pipeline for featured branches past GitLab has executed all the jobs, except this dogfighter

28
00:02:33,050 --> 00:02:39,200
job, which requires manual action, no guys to stop this job, I mean to stop the running environment.

29
00:02:39,560 --> 00:02:41,390
You can simply click on the stop button.

30
00:02:41,840 --> 00:02:46,820
But doing this, as we learned, will destroy the running application environment, and the maintainer

31
00:02:46,850 --> 00:02:49,280
will not be able to see and review your application.

32
00:02:50,390 --> 00:02:56,120
So we will not drop environments like this, and we will create a budget request and let maintainer

33
00:02:56,180 --> 00:02:59,810
review the app and then take the decision, which we will see in a moment.

34
00:03:00,320 --> 00:03:05,570
Meanwhile, let's check that labor application to get access to running environment from the deployment

35
00:03:05,570 --> 00:03:05,990
section.

36
00:03:07,980 --> 00:03:08,820
What do environment?

37
00:03:12,920 --> 00:03:15,110
Under the review group, here it is.

38
00:03:17,060 --> 00:03:18,200
Open that live environment.

39
00:03:21,340 --> 00:03:23,890
Our application is running at this dynamic address.

40
00:03:24,640 --> 00:03:26,020
Also, if we cross-check.

41
00:03:30,040 --> 00:03:32,380
Its entry is in Hiroko Dashboard as well.

42
00:03:33,350 --> 00:03:37,690
Nice, once you find your application code, it will be working as expected.

43
00:03:38,230 --> 00:03:39,490
You will create a major tourist.

44
00:03:59,400 --> 00:04:04,890
Remember to keep this delete branch option checked, checking this box only will make desktop environment

45
00:04:04,890 --> 00:04:07,440
job good run when your budget request is approved.

46
00:04:12,040 --> 00:04:13,330
Much request is greater.

47
00:04:15,860 --> 00:04:21,770
Since I'm the maintainer as well, so we got this decision page again, GitLab is showing maintainer

48
00:04:21,770 --> 00:04:23,930
the results from previous pipeline run.

49
00:04:24,970 --> 00:04:27,700
If you want, you can click here to see that on pipeline logs.

50
00:04:28,480 --> 00:04:33,580
And guys, since that dynamic environment is still running, so maintainer can click here to see the

51
00:04:33,580 --> 00:04:39,430
running environment with life application and then further take decision whether to accept this merger

52
00:04:39,430 --> 00:04:40,360
request or not.

53
00:04:41,080 --> 00:04:42,700
Anyways, I'm happy with the application.

54
00:04:42,970 --> 00:04:47,530
I will accept this merger request, so approve and merge.

55
00:04:49,930 --> 00:04:52,510
And don't forget to select this delete option.

56
00:04:58,830 --> 00:05:02,940
The branches moist and see the magic of stopping a minute.

57
00:05:03,390 --> 00:05:09,210
Once the maintainer clicked on the merge, this time along with the pipeline for Main Branch, the stop

58
00:05:09,210 --> 00:05:12,180
beta job from the feature branch also started running.

59
00:05:12,990 --> 00:05:15,330
If we open the logs, let it complete.

60
00:05:15,840 --> 00:05:20,130
While it is running, meanwhile, the main branch run would also have started.

61
00:05:22,490 --> 00:05:23,720
See, it is running as well.

62
00:05:31,630 --> 00:05:33,880
In this job, we have nothing but two tasks.

63
00:05:34,600 --> 00:05:39,370
First, it destroying that dynamic application running on Hirokazu US and last.

64
00:05:39,820 --> 00:05:41,890
He called out the application destroyed message.

65
00:05:42,910 --> 00:05:46,390
Let's refresh the application page and see if it is still running or not.

66
00:05:50,810 --> 00:05:56,990
Great edit there is nothing here yet means an application no longer exist here, and we can confirm

67
00:05:56,990 --> 00:05:58,280
the same from Heroku dashboard.

68
00:06:02,280 --> 00:06:06,060
The application is gone from here as went back to get a.

69
00:06:07,140 --> 00:06:08,910
If we go back to environments.

70
00:06:14,580 --> 00:06:20,490
The featured branch environment no longer exists in that available environmentalist because it has move

71
00:06:20,490 --> 00:06:22,050
to stop environmentalist.

72
00:06:22,440 --> 00:06:22,980
Wonderful.

73
00:06:24,050 --> 00:06:27,720
So that was how you can add stop environmental scripts in your pipeline.

74
00:06:28,380 --> 00:06:33,840
Now, with a new pipeline configuration, we need not to worry about any resource misuse, age or extra

75
00:06:33,840 --> 00:06:39,860
costings because now stop environment being added once the feature branch is merged into main GitLab

76
00:06:39,870 --> 00:06:45,000
will automatically destroy the temporary creator environment and will free down all the resources occupied

77
00:06:45,000 --> 00:06:46,170
by that environment.

78
00:06:46,650 --> 00:06:51,870
And with this lecture comes the end of this section and project, so you guys in next section with some

79
00:06:51,870 --> 00:06:52,440
new topics.
