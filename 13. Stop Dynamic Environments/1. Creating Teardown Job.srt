1
00:00:00,650 --> 00:00:04,530
Welcome to this lecture, one last improvisation in the project code.

2
00:00:04,550 --> 00:00:05,540
Before we wrap it up.

3
00:00:06,230 --> 00:00:11,210
So guys, the scenario till now is that whenever a new feature is to be added in, the application,

4
00:00:11,600 --> 00:00:13,670
developer will write its code for that.

5
00:00:14,030 --> 00:00:19,700
And as part of deployment, the application will be deployed to an independent, dynamic environment.

6
00:00:20,210 --> 00:00:25,100
It will be tested there and upon successful testing and subsequent steps of merging.

7
00:00:25,100 --> 00:00:28,790
Two main staging deployment and production deployment will happen, right?

8
00:00:29,240 --> 00:00:29,870
That's OK.

9
00:00:30,320 --> 00:00:35,390
But now, in a large organization, there could be several teams with number of developers working on

10
00:00:35,390 --> 00:00:35,900
a project.

11
00:00:36,320 --> 00:00:42,320
So for each new feature or code development, you can see a separate dynamic environment would be created

12
00:00:42,320 --> 00:00:43,130
automatically.

13
00:00:43,850 --> 00:00:48,440
Now, though, this dynamic environment creation part has sold problems, as we've discussed.

14
00:00:49,100 --> 00:00:55,130
But if you notice, it has also led us into another problem which you would have already guessed seeing

15
00:00:55,130 --> 00:00:56,150
from this presentation.

16
00:00:56,990 --> 00:01:03,020
Problem is a huge pile of unused and idle dynamic environments for each new feature branch.

17
00:01:03,320 --> 00:01:09,200
The pipeline is creating a separate dynamic environment, but once that feature branch that created

18
00:01:09,230 --> 00:01:14,090
a dynamic environment is most into the main branch aspirant current configuration.

19
00:01:14,210 --> 00:01:16,280
We are doing nothing with that environment.

20
00:01:16,820 --> 00:01:22,640
After the application is tested in its dynamic environment and approved, we merge the branch into main

21
00:01:23,000 --> 00:01:25,820
and probably the feature branch can be deleted too.

22
00:01:26,240 --> 00:01:28,310
But what about this dynamic environment?

23
00:01:28,760 --> 00:01:33,620
We are still keeping that dynamic environment life which could lead us to resource crunching problem.

24
00:01:34,670 --> 00:01:40,310
There could be tens or even hundreds of featured branches getting created and each of it generating

25
00:01:40,310 --> 00:01:47,480
its own environment, consuming a whole load of resources like disk storage, CPU computing power and

26
00:01:47,480 --> 00:01:51,200
numerous other resources, which of course, would need to compensate with money.

27
00:01:51,860 --> 00:01:55,920
And in our particular case, it would be the resources at Iroko Cloud.

28
00:01:55,940 --> 00:01:57,020
We would be paying for it.

29
00:01:57,710 --> 00:01:59,960
This is an unwanted cost, which would be handled.

30
00:02:01,390 --> 00:02:07,300
Now, the first solution to this resource crunch problem one would think of is once the application

31
00:02:07,300 --> 00:02:12,550
in dynamic environment is tested successfully and its corresponding future branch is much into meet,

32
00:02:13,000 --> 00:02:18,940
the developer or some other person would log in to the Heroku dashboard and manually delete the dynamic

33
00:02:18,940 --> 00:02:21,400
application, either from Clia or from you.

34
00:02:22,240 --> 00:02:23,320
Yes, you can do that.

35
00:02:23,620 --> 00:02:27,700
But this seems to be a little off the track as we are talking about automation here.

36
00:02:27,940 --> 00:02:29,740
So this would not be a valid approach.

37
00:02:30,430 --> 00:02:35,920
Now, the other automated way would be to leverage some of the Getler features and handle it in the

38
00:02:35,920 --> 00:02:36,700
pipeline itself.

39
00:02:37,360 --> 00:02:38,980
And how exactly we will achieve this.

40
00:02:39,370 --> 00:02:45,370
It is by adding a new concept into our pipeline called stop environment, by adding some additional

41
00:02:45,370 --> 00:02:48,660
piece of code in the pipeline and doing some settings and get like you.

42
00:02:49,150 --> 00:02:52,120
We're going to achieve the automatic deletion of dynamic environments.

43
00:02:53,940 --> 00:02:59,790
All right, now, coming back to the good guys, the basic idea for adding stop environment code would

44
00:02:59,790 --> 00:03:05,820
be first, we will create a new job that will help us to bring down the resources occupied by dynamic

45
00:03:05,820 --> 00:03:06,300
environment.

46
00:03:07,140 --> 00:03:10,600
Leading down in our particular case would be to destroy the application.

47
00:03:10,620 --> 00:03:17,580
We'll start at the Hiroko dynamic address and once that job is created, we will modify it and club

48
00:03:17,580 --> 00:03:22,440
it with desktop environment, but which will also stop the running environment from GitLab Environment

49
00:03:22,440 --> 00:03:22,860
Speech.

50
00:03:23,640 --> 00:03:25,290
So let's create that job.

51
00:03:31,920 --> 00:03:37,350
Now, to destroy Heroku application, we would be again using the Alpine, Heroku, UCLA Docker image.

52
00:03:46,130 --> 00:03:47,600
Now under the script part.

53
00:03:53,840 --> 00:03:59,180
Our first command would be a Heroku UCLA command who destroyed the Heroku application created by the

54
00:03:59,180 --> 00:04:00,740
previous deploy fridtjof.

55
00:04:05,480 --> 00:04:11,570
So I have copied the same command from the Blyvoor job now as part of writing the Heroku command to

56
00:04:11,570 --> 00:04:12,560
delete the application.

57
00:04:12,800 --> 00:04:13,520
The command is.

58
00:04:14,460 --> 00:04:14,970
Apps.

59
00:04:17,960 --> 00:04:22,760
Destroy and then pass the application name, which is stored in variable feature up.

60
00:04:29,590 --> 00:04:31,030
And then confirm.

61
00:04:33,610 --> 00:04:35,850
And again, the application name.

62
00:04:45,430 --> 00:04:49,650
One side note, nice for clarification on any Hiroko sales command.

63
00:04:49,720 --> 00:04:55,390
You can visit the Heroku official documentation they have documented all the available Hillock UCLA

64
00:04:55,390 --> 00:04:56,800
commands in a very simple way.

65
00:04:57,490 --> 00:05:00,970
So what this command will do is upon execution.

66
00:05:01,810 --> 00:05:07,240
It will find and delete the specified application from hello because of us and will free all the resources

67
00:05:07,360 --> 00:05:13,450
like storage application data, the URL from which that application is accessible, etc. Everything

68
00:05:13,450 --> 00:05:14,950
will be freed from HIROKO servers.

69
00:05:15,460 --> 00:05:21,340
And yes, keep in mind, deleting an application is irreversible and lost an optional.

70
00:05:22,950 --> 00:05:23,640
Equal message.

71
00:05:42,550 --> 00:05:43,030
And.

72
00:05:44,400 --> 00:05:49,150
And for this feature, a variable seemed like we didn't deploy a feature job in here.

73
00:05:49,170 --> 00:05:51,270
Also, we need to export it in before script.

74
00:05:52,080 --> 00:05:52,950
So it.

75
00:06:00,740 --> 00:06:06,530
And since this environment also won't be available in this job, so for this job, also, we need to

76
00:06:06,770 --> 00:06:08,300
specify the same environment name.

77
00:06:20,900 --> 00:06:26,220
Now, guys, we have this job to destroy a dynamic environment that any business do not completely ready,

78
00:06:26,230 --> 00:06:28,150
but yeah, in some way it is now.

79
00:06:28,150 --> 00:06:33,970
The question of interest is when to run this job, after which job or in which stage this jobs children.

80
00:06:34,630 --> 00:06:37,300
There could be few possible options that may come into your mind.

81
00:06:37,750 --> 00:06:42,430
I'm wrapping this lecture here and asking you to give it a thought on when to run this job.
