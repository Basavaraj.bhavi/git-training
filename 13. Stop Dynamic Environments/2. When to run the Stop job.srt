1
00:00:00,770 --> 00:00:06,440
First option, which comes to your mind is we can put this job into some new stage ceased all feature

2
00:00:06,770 --> 00:00:11,180
and place that stage just after that deploy if we just stage in stages action.

3
00:00:11,720 --> 00:00:18,100
But if we do so, then in that case, just after the new feature up is deployed with some dynamic environment.

4
00:00:18,430 --> 00:00:23,990
Then right after the deployment staff which are stage run, which will delete that newly created environment,

5
00:00:24,530 --> 00:00:28,460
causing to make that deployment unavailable for the next nesting stage.

6
00:00:28,820 --> 00:00:32,870
And as a result of this configuration, best feature job will always be.

7
00:00:33,230 --> 00:00:37,070
So putting the staff with the job right after that deploy feature will be of no use.

8
00:00:37,940 --> 00:00:43,010
The second option one might think of is putting it next to this automated feature testing stage.

9
00:00:43,400 --> 00:00:48,380
I mean, it would be practical to destroy the application after the successful testing of the review

10
00:00:48,380 --> 00:00:48,950
application.

11
00:00:49,400 --> 00:00:55,400
But no, we can't put it here as well, because if we do so, then the under review application will

12
00:00:55,400 --> 00:01:01,130
not be available even for the developer himself or for the reviewer who would want to manually see the

13
00:01:01,130 --> 00:01:03,140
live application before approving it.

14
00:01:03,470 --> 00:01:06,170
So in short, this option would again become wide for us.

15
00:01:06,770 --> 00:01:08,870
So when and where to run this job?

16
00:01:09,530 --> 00:01:15,260
The ideal answer would be what if we run this job after the feature branch is merged into Main Branch

17
00:01:15,380 --> 00:01:21,800
and gets deleted once the developer makes a major request to main branch, dynamic application would

18
00:01:21,800 --> 00:01:22,490
still be life.

19
00:01:22,850 --> 00:01:28,380
Hence, Mandana can see that application and test it after it maintainer reviews that application.

20
00:01:28,400 --> 00:01:34,520
He can go ahead and most up branch into mean, and this is the time when we can delete the feature branch,

21
00:01:34,760 --> 00:01:37,100
as well as a dynamic environment created by it.

22
00:01:37,550 --> 00:01:43,490
So in short, does Profiter job children when the merged two main branch happens and how exactly we

23
00:01:43,490 --> 00:01:46,640
do this, it's by adding desktop environment configuration.

24
00:01:47,270 --> 00:01:52,580
By adding desktop environment configuration, we basically instruct GitLab what to do when a branch

25
00:01:52,580 --> 00:01:53,330
is to be deleted.

26
00:01:54,540 --> 00:01:58,950
Stopping environments can be achieved with the on stop giver defined under environment.

27
00:02:02,810 --> 00:02:08,870
And as we would be calling desktop environment job that is double feature from this deploy feature job.

28
00:02:09,230 --> 00:02:14,390
So under the environment tag of deploy feature, add a new keyboard on Stop.

29
00:02:17,730 --> 00:02:22,260
And Pastor, job name that will do the cleanup, which is stop feature.

30
00:02:23,730 --> 00:02:28,490
Actually, dynamic environments stop automatically when their associated branches deleted.

31
00:02:28,950 --> 00:02:34,230
Also, when an environment is daubed, this on stop action is executed.

32
00:02:34,800 --> 00:02:40,860
So if you understand the flaw, if this option is checked while merging, then the feature branch gets

33
00:02:40,860 --> 00:02:50,790
deleted and this nonstop action is executed, which in turn is calling the stop feature job that contains

34
00:02:50,790 --> 00:02:54,630
the Heroku app Destroy Command, eventually cleaning up everything.

35
00:02:54,990 --> 00:02:55,800
I hope you got this.

36
00:02:57,080 --> 00:03:04,400
And next, you make a link between deploy feature and stop feature job plus who indicate that dogfighter

37
00:03:04,400 --> 00:03:08,000
job stops deployment inside its environment bag.

38
00:03:09,890 --> 00:03:13,340
Basically, what action with value stop?

39
00:03:15,330 --> 00:03:21,480
Adding this quote indicates that this job stops deployment, and GitLab won't run this job as any other

40
00:03:21,480 --> 00:03:21,780
job.

41
00:03:22,200 --> 00:03:25,410
We will see in the pipeline on how this job is different from other jobs.

42
00:03:26,040 --> 00:03:29,210
And yes, the job with action stop might not run.

43
00:03:29,320 --> 00:03:33,030
It's in a later stage than the job that started that environment.

44
00:03:33,480 --> 00:03:35,040
So to prevent this thing to happen.

45
00:03:35,340 --> 00:03:41,580
We need to keep Starfighter Job in the same stage, which started the environment that is the play feature.

46
00:03:44,420 --> 00:03:46,400
Stage deploy feature.

47
00:03:48,220 --> 00:03:52,030
Now, one extra thing we need to set here would be good strategy to not.

48
00:03:54,210 --> 00:03:55,050
It is a variable.

49
00:04:03,150 --> 00:04:08,940
Setting the good strategy to none is necessary on the profitable job so that the get LeBron won't try

50
00:04:08,940 --> 00:04:11,220
to check out the code after the branches deleted.

51
00:04:12,570 --> 00:04:17,520
And this double feature job should run only after the is approved.

52
00:04:19,490 --> 00:04:24,530
Because, you know, if we leave it like this, then stop your job would probably start with the deploy

53
00:04:24,530 --> 00:04:30,170
feature job, but ideally this job should run only after some person approves the merger request from

54
00:04:30,220 --> 00:04:30,530
GitLab.

55
00:04:31,010 --> 00:04:32,810
And feature branches deleted.

56
00:04:33,200 --> 00:04:35,510
So it means it is a kind of manual trigger off job.

57
00:04:35,930 --> 00:04:40,520
That is why we have to add a one condition install feature job like this.

58
00:04:43,200 --> 00:04:45,510
Now, this job will only run after a manual action.

59
00:04:46,650 --> 00:04:50,850
So, yeah, finally, with this, the coding part of stop environment is done.

60
00:04:51,210 --> 00:04:56,160
There are few other settings that we have to do in GitLab UA for the proper functioning of stop environment,

61
00:04:56,430 --> 00:04:58,080
which I will discuss in the next lecture.

62
00:04:58,260 --> 00:04:58,650
Thank you.
