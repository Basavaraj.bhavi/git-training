1
00:00:00,760 --> 00:00:03,100
I hope you would have accomplished the assignment task.

2
00:00:03,460 --> 00:00:07,390
But on your back for that, if not, let me explain in here in this lecture.

3
00:00:07,810 --> 00:00:08,560
It's very simple.

4
00:00:09,010 --> 00:00:10,840
We have to add one another job.

5
00:00:17,740 --> 00:00:22,510
Seemed like would flicker to hear as well, it will require to use some Python Docker image.

6
00:00:30,720 --> 00:00:33,270
Then, right, the prerequisites under before the script.

7
00:00:39,870 --> 00:00:42,180
And what are the prerequisites are there for this stage?

8
00:00:42,840 --> 00:00:46,740
The first obvious is install the protest framework with the estiment plugin.

9
00:00:47,280 --> 00:00:48,300
So the first command is.

10
00:00:57,820 --> 00:01:01,720
And then it also need some basic packages for the Python methods to run.

11
00:01:02,510 --> 00:01:08,410
See, with flick it, we were just dusting the code for any stylistic or any programmatic errors, not

12
00:01:08,410 --> 00:01:14,200
any of our code work getting done anywhere and flick it so it did not require any environment, packages

13
00:01:14,200 --> 00:01:14,680
or so.

14
00:01:15,130 --> 00:01:20,890
But in this case, with bite test, since python methods of abducting a file are going to get run.

15
00:01:21,250 --> 00:01:27,250
That is why for this job, before we can run the test, we need to provide it the basic packages that

16
00:01:27,250 --> 00:01:33,730
this application requires to execute and where those package requirements is listed inside the requirements

17
00:01:33,730 --> 00:01:34,560
not be certified.

18
00:01:35,020 --> 00:01:40,270
So accordingly, the next line of command would be to install the Python packages listed in requirements.

19
00:01:40,270 --> 00:01:41,680
Fight for that.

20
00:01:51,160 --> 00:01:54,580
Now with this, an appropriate environment would be their current best.

21
00:01:55,580 --> 00:01:57,500
And finally, under the script tag.

22
00:02:02,030 --> 00:02:03,680
Right document to run by test.

23
00:02:07,530 --> 00:02:10,710
And the test report would be artifacts for later use.

24
00:02:11,250 --> 00:02:13,110
So to find artifacts for this job as well.

25
00:02:15,020 --> 00:02:15,530
Copied.

26
00:02:28,650 --> 00:02:35,100
See here also the when property is said to always because we want the artifacts to be saved no matter

27
00:02:35,100 --> 00:02:36,330
this job fails or pass.

28
00:02:37,510 --> 00:02:44,320
Now coming to the stage for this job, you have the freedom to put it under a separate stage, but as

29
00:02:44,320 --> 00:02:49,420
we have seen in stages lecture, it's a good approach to keep all the tests under the same stage to

30
00:02:49,420 --> 00:02:56,710
see if the overall execution time for this pipeline as these two tests by Test and Linde test are independent

31
00:02:56,710 --> 00:02:59,650
of each other, so we can definitely allow them to run in parallel.

32
00:03:00,970 --> 00:03:01,570
That is way.

33
00:03:06,530 --> 00:03:09,130
I'm keeping this stage as best only.

34
00:03:11,050 --> 00:03:16,260
So with this, despite the job in the pipeline, got to find time to commit and push the dangers to

35
00:03:16,270 --> 00:03:16,600
remote.

36
00:03:32,460 --> 00:03:36,990
It got pushed in the pipeline for this feature branch should have started itself.

37
00:03:43,010 --> 00:03:43,820
It's already running.

38
00:03:49,610 --> 00:03:51,080
See, both are running in parallel.

39
00:03:54,150 --> 00:03:54,630
Bust.

40
00:03:55,700 --> 00:03:56,810
Open by test.

41
00:04:00,860 --> 00:04:04,430
All the five days passed and here we got the.

42
00:04:06,500 --> 00:04:07,640
Job artifacts to.

43
00:04:08,560 --> 00:04:13,030
OK, so with this, we are done with adding that testing part of application and pipeline.

44
00:04:13,330 --> 00:04:14,230
So your next lecture?
