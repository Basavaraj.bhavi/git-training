1
00:00:00,280 --> 00:00:03,660
Surveys are first goal to develop, the application is achieved.

2
00:00:04,140 --> 00:00:10,290
Let's not put our efforts to build a GitLab pipeline to give our client a frequent and seamless deployment

3
00:00:10,290 --> 00:00:10,890
experience.

4
00:00:11,370 --> 00:00:16,710
Having said that, I will not create to get lapses by email, file and start defining the sequence of

5
00:00:16,710 --> 00:00:17,070
tasks.

6
00:00:18,180 --> 00:00:22,920
You can create a new branch for this as well, but to say over time, I will continue with the current

7
00:00:22,920 --> 00:00:23,490
branch only.

8
00:00:24,750 --> 00:00:26,250
So in the project route.

9
00:00:29,730 --> 00:00:30,960
Create a new pipeline fight.

10
00:00:35,270 --> 00:00:41,450
And if I recall the workflow, the first task by Blanchard to perform is to test the code for any defects

11
00:00:41,450 --> 00:00:42,800
to surface at the earliest.

12
00:00:43,280 --> 00:00:48,440
And as part of testing, I'm going to test it from various aspects where the Lynch test, smoke test

13
00:00:48,440 --> 00:00:49,190
and unit test.

14
00:00:50,150 --> 00:00:54,650
OK, so starting with our first testing, the Lynch testing create a new job.

15
00:00:57,210 --> 00:01:04,590
Linda underscored Test and what is this, Linda lending is a technique of automated checking off your

16
00:01:04,590 --> 00:01:09,660
code for any programmatic and stylistic errors where programming errors can be like.

17
00:01:09,840 --> 00:01:15,600
You could have import some library, but it is nowhere used in the code than stylistic tricks include

18
00:01:15,870 --> 00:01:17,310
checking the coding style standards.

19
00:01:17,640 --> 00:01:22,380
It may be any wrong indentation, unnecessary spaces, empty lines in the code or so.

20
00:01:22,650 --> 00:01:28,200
You know, basically it checks the quality of the code and the tools that perform such type of tests

21
00:01:28,200 --> 00:01:29,230
are called Linda.

22
00:01:29,730 --> 00:01:32,340
In our case, as the application is based on Python.

23
00:01:32,640 --> 00:01:39,840
So the tool that can be used to perform Lynch Test is Flick It, which is a Python library before I

24
00:01:40,110 --> 00:01:42,870
flick it into Yemen for your better understanding.

25
00:01:43,140 --> 00:01:44,730
Let me show you how does it work?

26
00:01:45,180 --> 00:01:48,810
I will run the Lynch test in my local system and show it to you.

27
00:01:50,710 --> 00:01:52,560
Put on flicker test first.

28
00:01:52,570 --> 00:01:55,770
Obviously, you will need a flicker tool installed on your system.

29
00:01:56,400 --> 00:01:58,680
The easiest way to install it is with PIP.

30
00:01:59,820 --> 00:02:12,240
So in the terminal type three, install flickered estimable guys rather than installing the default

31
00:02:12,240 --> 00:02:18,920
flickered here I'm installing flickered with that flick it SDL plugin that will generate the flicker

32
00:02:18,930 --> 00:02:23,220
test result reports into some nice and fancy looking at steam and web pages.

33
00:02:23,820 --> 00:02:28,380
And yes, if you wish to learn more about Flick It, then you can visit to the official website.

34
00:02:28,740 --> 00:02:31,080
The link is there in the Resources tab of this lecture.

35
00:02:40,600 --> 00:02:44,520
It is installed now, let's run the it rest.

36
00:02:47,070 --> 00:02:57,420
Iran flickered and then passed the option format former defines the format of the test results, you

37
00:02:57,420 --> 00:03:00,600
want the output and I want it to be an HDMI.

38
00:03:02,740 --> 00:03:06,340
And last, specify the location where you want to store the results.

39
00:03:07,860 --> 00:03:17,070
The option is a directory, and I will shorten, Flake underscored reports.

40
00:03:18,630 --> 00:03:22,740
Currently, there is no such flag underscore reports, the rectory in our project.

41
00:03:23,070 --> 00:03:25,410
So upon running, it will be automatically created.

42
00:03:28,120 --> 00:03:28,660
It's done.

43
00:03:29,200 --> 00:03:30,370
Let's see the results we got.

44
00:03:35,450 --> 00:03:38,950
We have this as video files, CSIS and the index, not Yemen.

45
00:03:39,110 --> 00:03:39,590
Open it.

46
00:03:42,620 --> 00:03:44,870
Excellent result says it's all good.

47
00:03:44,960 --> 00:03:51,500
No error is found to check if liquor is working properly and to show you how does it highlight the errors?

48
00:03:51,890 --> 00:03:53,330
Let me intentionally add another.

49
00:03:58,720 --> 00:04:00,820
Let's remove these empty spaces.

50
00:04:02,910 --> 00:04:03,720
Empty lines.

51
00:04:05,050 --> 00:04:05,740
Save it.

52
00:04:09,750 --> 00:04:10,680
Rinderpest again.

53
00:04:13,130 --> 00:04:15,110
It is showing here as well, and if we go.

54
00:04:20,170 --> 00:04:25,540
See, Iraq got caught, it says flick violations, and there is some issue in the app not be verified

55
00:04:26,050 --> 00:04:26,890
if I open.

56
00:04:29,120 --> 00:04:33,290
We got the error code with a message thing expected to blank lines, phone zero.

57
00:04:34,030 --> 00:04:35,990
Look at this edit, you can click on the code.

58
00:04:37,300 --> 00:04:40,000
And it will take you to that particular line in the cold.

59
00:04:40,570 --> 00:04:41,410
I hope you'll get it.

60
00:04:41,920 --> 00:04:46,210
So in this way with flick it, you can find any basic stylistic errors in your core.

61
00:04:47,110 --> 00:04:49,210
OK, so let me revert back the genius.

62
00:04:52,870 --> 00:04:55,900
Not been demonstrated, let's add flick it in a regional fight.

63
00:04:58,360 --> 00:05:07,210
Another job at a script where as a first task, we need to install flickered as HTML with PIP.

64
00:05:07,750 --> 00:05:08,470
So right?

65
00:05:14,890 --> 00:05:18,610
And then the second step would be to run the test test that we just saw.

66
00:05:19,960 --> 00:05:21,100
I will pass the whole command.

67
00:05:29,820 --> 00:05:36,540
And as by default, the LeBron will use Docker image, which doesn't know about it, so to work with

68
00:05:36,540 --> 00:05:42,270
PIP, we need to explicitly change the default Docker image to a Docker image built for Python.

69
00:05:43,860 --> 00:05:44,640
So here.

70
00:05:51,810 --> 00:05:52,720
It's almost done.

71
00:05:53,400 --> 00:05:58,740
But wait, there is one important thing pending, we should be alert to this job before jumping to the

72
00:05:58,740 --> 00:05:59,250
next lecture.

73
00:05:59,430 --> 00:06:00,570
Just give a thought over it.

74
00:06:00,900 --> 00:06:05,430
You have already gone through that thing in the previous lectures, and I will tell you what is missing

75
00:06:05,430 --> 00:06:06,180
in the next lecture.
