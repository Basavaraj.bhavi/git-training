1
00:00:00,970 --> 00:00:06,790
You're testing you all we know is a type of software testing where individual units or components of

2
00:00:06,790 --> 00:00:12,190
a software are testing components like functions, procedures or interfaces are tested.

3
00:00:12,640 --> 00:00:18,130
And in our case, we will test for each of the methods defined in the app, not be by fight methods,

4
00:00:18,160 --> 00:00:20,740
the add, delete and edit methods.

5
00:00:21,940 --> 00:00:24,490
I will based remaining code in this file.

6
00:00:26,470 --> 00:00:27,820
First, add some imports.

7
00:00:29,640 --> 00:00:30,450
With Debbie.

8
00:00:35,510 --> 00:00:36,980
Then add fixture functions.

9
00:00:50,570 --> 00:00:56,750
Now, if I talk about the additions, which I just made at first, I have important despite desk for

10
00:00:56,750 --> 00:00:57,260
later use.

11
00:00:58,320 --> 00:01:03,000
And since now, we'll be working with that database, so models are also important.

12
00:01:04,870 --> 00:01:06,850
Then we have this by dust fixture.

13
00:01:07,540 --> 00:01:14,110
This is new gas by dust fixtures, other functions attached to test and runs before each test function,

14
00:01:14,110 --> 00:01:14,830
which it is up.

15
00:01:14,830 --> 00:01:20,680
Light fixtures are generally used to feed some data to the test methods, such as database connections

16
00:01:20,950 --> 00:01:23,370
or anything that you think your test would require.

17
00:01:23,980 --> 00:01:29,050
Since our test started later to ground operations, so before each test, we would need to create a

18
00:01:29,050 --> 00:01:29,980
database connection.

19
00:01:30,070 --> 00:01:36,490
Right now, what you can do is either within each dust method, you write the same code to establish

20
00:01:36,500 --> 00:01:37,420
database connection.

21
00:01:37,780 --> 00:01:39,250
But this would be repetitive.

22
00:01:39,760 --> 00:01:45,610
So what we can do is instead of writing the same code for every test, we can write a separate fixture

23
00:01:45,610 --> 00:01:49,630
function and then attach that fixture function to all the test methods.

24
00:01:50,500 --> 00:01:56,070
And like I said, it is a property of fixture function that they run before each test function, which

25
00:01:56,110 --> 00:01:56,680
is applied.

26
00:01:57,190 --> 00:02:02,350
So the database connection would be automatically set up whenever a test methods start execution.

27
00:02:03,540 --> 00:02:09,450
And this marker, this is used to specify that this below function is a fixture function.

28
00:02:10,540 --> 00:02:12,070
This plant is a fixture function.

29
00:02:13,120 --> 00:02:19,600
And inside this function first initiative is invoked that will create a database connection to this

30
00:02:19,600 --> 00:02:21,360
test, debate or debate.

31
00:02:22,000 --> 00:02:28,150
If you notice this is the same file as we have created in the previous Abbott record, but this one

32
00:02:28,150 --> 00:02:29,530
is specific to testing only.

33
00:02:30,130 --> 00:02:36,370
I'm creating a separate database file because while testing rules would get added, deleted and ed in

34
00:02:36,370 --> 00:02:39,400
the table, which would make the original table inconsistent.

35
00:02:39,730 --> 00:02:45,760
So to keep production and testing separate, I have created a separate fight after the database is established.

36
00:02:47,210 --> 00:02:51,140
The yield fixture is used in which the APS does decline discreetly.

37
00:02:51,740 --> 00:02:57,770
And finally, a function truncated is called, which will truncate every test data from the database

38
00:02:58,040 --> 00:03:00,920
so as to provide an empty database to the next method.

39
00:03:02,180 --> 00:03:03,380
Now, if I scroll down.

40
00:03:06,910 --> 00:03:08,950
Coming to this method, we have already done it.

41
00:03:09,700 --> 00:03:15,100
But now that we are also interacting with database as well, we can add one more check in this best

42
00:03:15,130 --> 00:03:16,210
index response function.

43
00:03:16,750 --> 00:03:21,220
We can write a check to ensure that there should be no implied record in the database.

44
00:03:21,970 --> 00:03:22,390
So.

45
00:03:41,570 --> 00:03:47,360
This line instructs to fetch the count of Rose from employed table and pass if the result count is equal

46
00:03:47,360 --> 00:03:47,810
to zero.

47
00:03:48,020 --> 00:03:49,280
Otherwise, the test will fit.

48
00:03:49,910 --> 00:03:55,460
Basically by writing this, I'm making sure that the database file is there and a successful connection

49
00:03:55,460 --> 00:03:56,390
has been made with it.

50
00:03:57,470 --> 00:04:03,320
And now that I have added an association on database as well in this method, so it would definitely

51
00:04:03,320 --> 00:04:09,980
need that fixture function to create connections and and to call the Python picture function just part

52
00:04:09,980 --> 00:04:11,270
of function as argument.

53
00:04:14,240 --> 00:04:14,600
Don't.

54
00:04:16,150 --> 00:04:16,990
And delete this.

55
00:04:19,100 --> 00:04:22,190
OK, now coming to the concerned test methods for unit testing.

56
00:04:25,160 --> 00:04:31,290
Three test methods would be there to test, add, edit and delete functionality, first test it.

57
00:04:31,910 --> 00:04:34,910
I believe you all would know the fundamentals of unit testing.

58
00:04:35,600 --> 00:04:41,780
The idea here is to test that when a post request is made to applications at page with some dummy data,

59
00:04:42,380 --> 00:04:45,590
whether it would add the employed data into a database or not.

60
00:04:46,750 --> 00:04:52,180
This is the dummy data to be added in database with all the columns, name, gender address and their

61
00:04:52,180 --> 00:04:53,050
corresponding values.

62
00:04:54,590 --> 00:05:02,240
A post request is made to our page, along with the desktop now after the post request is made to add

63
00:05:02,240 --> 00:05:02,600
page.

64
00:05:07,030 --> 00:05:12,940
This ad function would come into action, and if everything to here is correct, then hopefully it will

65
00:05:12,940 --> 00:05:15,490
add or test employee data into a test database.

66
00:05:17,540 --> 00:05:23,740
Which is then is uttered in the next line here while asserting, I'm just checking that account of database

67
00:05:23,750 --> 00:05:25,130
will be one simple.

68
00:05:26,510 --> 00:05:33,140
Afterpay just start moving next, we have the added test because remember from the previous lecture,

69
00:05:33,590 --> 00:05:37,930
I told that Applications and It page is based on the employed Boston.

70
00:05:37,940 --> 00:05:38,390
They got it.

71
00:05:38,960 --> 00:05:44,570
For example, if you wish to edit an employee with I.D. two, then you will be redirected to this unit.

72
00:05:45,170 --> 00:05:50,200
Or if you wish to edit an employee with I.D. zero, then you will be taken to this unit.

73
00:05:51,350 --> 00:05:56,900
In case if there is no employer that I.D., then it will throw us another Saudi, that blood does not

74
00:05:56,900 --> 00:05:57,200
exist.

75
00:05:58,760 --> 00:06:02,130
Now, the test case for this functionality is defined in the three minute.

76
00:06:03,200 --> 00:06:09,470
Where I'm trying to edit and employ with it zero, which I know would not be there in the database.

77
00:06:10,860 --> 00:06:13,650
Hence, the assertion should be a storyline in this monster.

78
00:06:14,520 --> 00:06:18,240
And along with that, I'm also testing for the status quo 200.

79
00:06:19,600 --> 00:06:23,950
Guys, you would be thinking that this is a very naive test case, reject the added functionality.

80
00:06:24,250 --> 00:06:24,880
Yes, it is.

81
00:06:25,210 --> 00:06:30,580
The added functionality can be checked by first adding a dummy oral and hitting it and then cross-checking

82
00:06:30,580 --> 00:06:31,060
the results.

83
00:06:31,540 --> 00:06:34,420
But I am trying to not complicate the off-topic things here.

84
00:06:34,600 --> 00:06:40,030
So keeping it simple, if you are an experienced tester, you can write any complex test cases for this

85
00:06:40,030 --> 00:06:40,600
application.

86
00:06:41,680 --> 00:06:49,010
Anyhow, finally, the last US function for delayed functionality here also do the same thing, but

87
00:06:49,300 --> 00:06:54,870
I could lead and employ the I.D. zero while as I think, the audio message in response data.

88
00:06:55,950 --> 00:06:58,590
OK, so guys, with this, our test file is complete.

89
00:06:59,520 --> 00:07:01,980
Just we have to add the database file.

90
00:07:07,740 --> 00:07:10,970
You already know how to create this using libido to create a.

91
00:07:12,820 --> 00:07:14,470
Let's run this test in a local system.

92
00:07:22,470 --> 00:07:25,890
So by debt collected, five items and five tests were passed.

93
00:07:26,970 --> 00:07:27,390
Great.

94
00:07:27,540 --> 00:07:28,980
Our application is looking fine.

95
00:07:29,730 --> 00:07:30,690
Everything looks good.

96
00:07:31,080 --> 00:07:31,740
What's next?

97
00:07:32,340 --> 00:07:34,050
You guessed it time for automation.

98
00:07:34,590 --> 00:07:39,930
Let's take every piece of knowledge we gain from these manual steps and write them in the GitLab a pipeline.

99
00:07:41,010 --> 00:07:46,020
And I guess we have come a good distance in the course, and I believe you have all the knowledge to

100
00:07:46,020 --> 00:07:48,090
include a test stage in the pipeline.

101
00:07:48,510 --> 00:07:54,420
So I'm giving you to add, despite the stage by yourself as an assignment and I will meet you with the

102
00:07:54,420 --> 00:07:56,580
assignment solution in the next video lecture.

103
00:07:56,760 --> 00:07:57,180
Thank you.
