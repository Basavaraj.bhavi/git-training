1
00:00:03,340 --> 00:00:10,720
Create a new job build, and since the nature of this job is to build and push a Docker image, so the

2
00:00:10,750 --> 00:00:14,470
base image that we will need for this job would be a Docker image itself.

3
00:00:19,870 --> 00:00:20,440
Dr.

4
00:00:21,500 --> 00:00:27,710
Latest, you'll be able to run all the docket commands like Docker, build Docker push within this Docker

5
00:00:27,710 --> 00:00:28,040
image.

6
00:00:29,710 --> 00:00:31,180
Then starred the script tag.

7
00:00:35,240 --> 00:00:42,200
Inside it, the first command would be, of course, to build the image for that type Docker.

8
00:00:44,420 --> 00:00:51,810
Build B and then the image came before I start writing the image name a not nice.

9
00:00:51,920 --> 00:00:57,050
When images are built to be shared between developers and machine, they need to be stored somewhere

10
00:00:57,350 --> 00:01:00,140
on some container registry container registry.

11
00:01:00,440 --> 00:01:06,740
It is a repository for our team to store, duck and manage Docker images for later use and to store

12
00:01:06,740 --> 00:01:07,520
your Docker images.

13
00:01:07,790 --> 00:01:12,950
You can choose from different options available out there, like Docker Hub Repository or any third

14
00:01:12,950 --> 00:01:14,420
party cloud container registries.

15
00:01:15,140 --> 00:01:19,490
But GitLab is all about having a single integrated experience under one roof.

16
00:01:20,120 --> 00:01:22,220
So why choose any other country noted history?

17
00:01:22,340 --> 00:01:28,340
When GitLab itself provides the GitLab container registry telling you a little about GitLab container

18
00:01:28,340 --> 00:01:35,420
registry introduced in GitLab version 8.8 built on open source software, GitLab Container Registry

19
00:01:35,630 --> 00:01:39,440
is a secure and private registry for storing your Docker images.

20
00:01:40,250 --> 00:01:42,530
It's not just a standalone registry service.

21
00:01:42,980 --> 00:01:45,410
Rather, it is completely integrated with GitLab.

22
00:01:46,280 --> 00:01:51,380
As it comes out of the box with GitLab 8.8 or higher, so no additional installation is required.

23
00:01:52,010 --> 00:01:54,020
And more importantly, it's free to use.

24
00:01:55,470 --> 00:02:01,080
Now, starting a Docker image into GitLab containers, history has a small role, it's that you need

25
00:02:01,080 --> 00:02:06,960
to follow the GitLab container registries naming convention, which says that your name should follow

26
00:02:06,960 --> 00:02:07,530
this pattern.

27
00:02:09,450 --> 00:02:14,730
So let's create the image team following this convention, starting with we have a registry, y'all,

28
00:02:15,030 --> 00:02:25,710
which is registry dot com slash namespace, which is my GitHub account username.

29
00:02:29,350 --> 00:02:31,810
Slash project name, which is Imply Portal.

30
00:02:35,170 --> 00:02:41,350
Slash at last, you can have an additional names for the end of any major name up to three levels deep.

31
00:02:41,920 --> 00:02:47,290
For example, these all are valid image names for images within this Imply Portal project.

32
00:02:48,620 --> 00:02:50,180
I will keep it up to two levels.

33
00:02:51,930 --> 00:03:00,360
So employ image, then you specify the image tag separated by golden hair.

34
00:03:00,390 --> 00:03:04,070
I will use a built in Sicily environment variable S.A. commit light.

35
00:03:12,510 --> 00:03:18,240
This variable resolves to branch name, and in this case, it will resolve to AB logic since I'm in

36
00:03:18,240 --> 00:03:18,810
that branch.

37
00:03:19,350 --> 00:03:24,110
But when you will run the same pipeline in a massive branch, then this image tag would be start.

38
00:03:25,140 --> 00:03:30,570
And in case if you don't want to tag it with branch name, then you can also choose from other available

39
00:03:30,570 --> 00:03:36,210
environmental variables like see a comet, etc. that will target with the comet revision number.

40
00:03:36,750 --> 00:03:43,020
Or you can also use a comet short SMG, which will target with the first eight characters of comet revision

41
00:03:43,020 --> 00:03:44,580
number seem like this.

42
00:03:44,730 --> 00:03:49,890
There may be some other predefined CCD environment variables by which you would like to tag the image

43
00:03:49,890 --> 00:03:54,480
name with the link of all those variables was already given to you in the previous section.

44
00:03:55,490 --> 00:03:56,150
And also.

45
00:03:57,290 --> 00:04:01,010
This long registry address can be replaced by one more variable.

46
00:04:07,380 --> 00:04:09,120
See a registry image.

47
00:04:09,750 --> 00:04:13,830
This variable contains the address of a project's container registry, which is set.

48
00:04:14,910 --> 00:04:21,030
And I think since we would be reusing the image name again and again further in the pipeline, so to

49
00:04:21,030 --> 00:04:27,840
minimize any type of errors, I should replace this whole limb with one variable only for that.

50
00:04:29,570 --> 00:04:33,950
After the stages cleared, a new section of global variables.

51
00:04:39,610 --> 00:04:42,400
Not the variable name, be image tag.

52
00:04:44,640 --> 00:04:45,930
And its value would be this.

53
00:04:52,380 --> 00:04:53,160
Replace it.

54
00:04:57,770 --> 00:05:00,020
Good image, your name being defined.

55
00:05:00,410 --> 00:05:03,680
The one lasting bending of this command is Docker file location.

56
00:05:04,670 --> 00:05:06,890
It's in the group, so not only.

57
00:05:08,080 --> 00:05:13,600
Well, because of this, the Dzokhar command to build an image is completed in the next line.

58
00:05:16,920 --> 00:05:21,450
Just for logging purposes, list images, using Docker images, come on.

59
00:05:22,140 --> 00:05:25,110
Which is if everything went right would list the newly built image.

60
00:05:25,350 --> 00:05:26,010
This is optional.

61
00:05:27,000 --> 00:05:27,750
Now what's next?

62
00:05:28,380 --> 00:05:33,960
The next step would be to push this image to GitLab container registry so that we can later use it when

63
00:05:33,960 --> 00:05:36,900
we need to deploy it to any staging or production environment.

64
00:05:37,590 --> 00:05:39,780
We don't store the images as artifacts.

65
00:05:40,110 --> 00:05:45,970
Rather, a concrete container registry is used which stores it permanently and which can be shared across

66
00:05:45,970 --> 00:05:49,050
their teams, irrespective of the projects or event organizations.

67
00:05:49,590 --> 00:05:51,960
Let's continue with the Docker Push Command and next lecture.
