1
00:00:00,360 --> 00:00:00,920
Nobody's.

2
00:00:00,960 --> 00:00:04,470
We cannot rely on just checking the code for the coding standards alone.

3
00:00:04,980 --> 00:00:08,610
As with the lint test, we are no, we are testing the application logics.

4
00:00:09,000 --> 00:00:11,430
We are not checking that one application runs.

5
00:00:11,670 --> 00:00:13,110
What will be its status quo?

6
00:00:13,710 --> 00:00:18,060
Will it be sending us back the expected response or not a specific case?

7
00:00:18,360 --> 00:00:19,380
Will the ad function?

8
00:00:19,590 --> 00:00:25,290
What actually ad the employee time do database will delete button, remove the employer from database

9
00:00:25,770 --> 00:00:28,980
like this testing for the basic functionality of our application.

10
00:00:29,550 --> 00:00:34,860
So welcome to this lecture, where we will move one step ahead in testing and will add a few more tests

11
00:00:34,860 --> 00:00:39,360
in the pipeline using by test, giving you a short and drove by test.

12
00:00:40,080 --> 00:00:46,530
My test is a popular open source Python testing framework that allows users to write test code using

13
00:00:46,530 --> 00:00:47,760
Python programming language.

14
00:00:48,210 --> 00:00:54,420
It is primarily used for unit testing and helps you to write simple and scalable test cases for databases.

15
00:00:54,450 --> 00:00:55,530
APIs are huge.

16
00:00:56,430 --> 00:00:59,340
Again, I will first demonstrate that by test on local.

17
00:01:02,150 --> 00:01:06,080
First things first, to be able to use by test, you need to install it.

18
00:01:06,620 --> 00:01:13,340
So in the terminal around three, this is just for the local branch I'm installing.

19
00:01:17,190 --> 00:01:19,690
Same like we did with flaky tool here.

20
00:01:19,800 --> 00:01:26,070
Also, I'm not using the basic binders tool and either leveraging the biased HDMI, which is a plug

21
00:01:26,070 --> 00:01:30,690
in for my test that generates some fancy nice looking HDMI reports for test results.

22
00:01:37,860 --> 00:01:41,250
Installation is done now moving towards testing.

23
00:01:42,700 --> 00:01:50,380
Guys, how by just works is by default, by test expects your test to be located in files whose names

24
00:01:50,560 --> 00:01:55,510
either begin with test underscore or ends with underscore test, don't be white.

25
00:01:56,020 --> 00:02:02,080
If these type of files with matching names are present, then by test automatically identify those files

26
00:02:02,260 --> 00:02:04,180
as test files and execute them.

27
00:02:04,870 --> 00:02:07,080
So let's create a test flight like that.

28
00:02:12,740 --> 00:02:15,710
Following the naming conventions destined to go to be white.

29
00:02:17,510 --> 00:02:21,980
Starting with a basic test, which is sort of a smoke test and paste the code.

30
00:02:24,230 --> 00:02:30,350
OK, so first, the app is imported from Apple TV, and we have two different methods.

31
00:02:30,890 --> 00:02:33,620
Best Index and Best Index response.

32
00:02:34,830 --> 00:02:41,310
Guys seem like the filing by dust also expects the best function names to be prefixed with Destiny Score.

33
00:02:41,670 --> 00:02:45,420
That is why test score, index and test and index response.

34
00:02:46,910 --> 00:02:53,330
Coming to the first best index function in one line, if I would say this best method is to test the

35
00:02:53,330 --> 00:02:58,040
response to the score of Applications Index page in the method definition.

36
00:02:59,470 --> 00:03:05,050
The first line of code creates a test claims for this application that will help us to make a request

37
00:03:05,050 --> 00:03:06,460
to employ a portal application.

38
00:03:07,810 --> 00:03:14,800
Then the request is made at Splash Page, which is nothing but the index page, and the response from

39
00:03:14,800 --> 00:03:17,020
the page is stored in this response.

40
00:03:18,070 --> 00:03:21,850
Then, at the last line of code, this is important.

41
00:03:22,480 --> 00:03:28,780
The Biotest assertions by test assertions are the checks that return done boolean either true or false

42
00:03:28,780 --> 00:03:32,580
status through means testing is just false means it failed.

43
00:03:33,220 --> 00:03:38,830
Upon testing a method, if an assertion fails, then that method execution is stopped there, and the

44
00:03:38,830 --> 00:03:44,380
remaining code in that particular test method is not executable and the control moves to the next test

45
00:03:44,380 --> 00:03:44,740
method.

46
00:03:45,520 --> 00:03:51,100
And here, as per our application, I'm asserting that this monster does code 200 to be true.

47
00:03:51,610 --> 00:03:57,520
Therefore, after making the request to application's main page, if it returns, the status quo 200

48
00:03:57,760 --> 00:04:01,120
that's orchestrators means the target page is working fine.

49
00:04:01,540 --> 00:04:04,570
Otherwise, the test will fail and you need to give up the error.

50
00:04:05,170 --> 00:04:11,690
OK, so this was just indexed method to check the status code in the next method.

51
00:04:11,710 --> 00:04:17,770
We have a similar method test index, a response that will check the actual response message, the absence

52
00:04:17,770 --> 00:04:20,230
to us see in the previous method.

53
00:04:20,530 --> 00:04:27,100
We were only asserting the return status code to be 200, but in this test method, I'm going to check

54
00:04:27,100 --> 00:04:33,520
the actual data that is returned and the response, which means this whole content of the index fight.

55
00:04:36,450 --> 00:04:41,940
Now that all the content from index page is being written in the response, so as part of testing,

56
00:04:42,150 --> 00:04:48,750
what we can check is that the written response should have this implied that given present in its response,

57
00:04:49,440 --> 00:04:53,580
if by test finds this particular quiver in the response, the test will pass.

58
00:04:53,790 --> 00:04:54,780
Otherwise, it will fit.

59
00:04:56,090 --> 00:04:57,000
Nice in here.

60
00:04:57,020 --> 00:05:00,770
I'm doing a type of string matching test case, unemployed Iraqi voter.

61
00:05:01,070 --> 00:05:06,740
But if you want, you can check it for any other keyword or can think of some new test cases as well.

62
00:05:06,980 --> 00:05:12,080
Other than this string matching, you know, like the return response, should have these many characters

63
00:05:12,440 --> 00:05:17,540
or fixed number of votes should be present like this that can be unique best guesses, according to

64
00:05:17,540 --> 00:05:18,710
the nature of your application.

65
00:05:20,470 --> 00:05:22,750
OK, so tests being defined, it's time for.

66
00:05:24,290 --> 00:05:24,910
Isn't it?

67
00:05:29,180 --> 00:05:30,860
To run the test in terminal.

68
00:05:31,280 --> 00:05:34,640
Either way, you can simply run my test command here.

69
00:05:35,600 --> 00:05:40,370
It will run the test and display the test results in terminal itself or the other way.

70
00:05:40,640 --> 00:05:47,000
Since we have installed by test with its achievable plug in so we can instructed to store the test results

71
00:05:47,000 --> 00:05:52,970
into some nicely formatted achievement webpages for that had on this command by test.

72
00:05:53,330 --> 00:05:56,090
And then we have an option at DML.

73
00:05:56,750 --> 00:06:00,170
This option is to specify the location where to store the test results.

74
00:06:07,040 --> 00:06:11,780
So it's in the Directory Business Report with business report A. M. Fight.

75
00:06:13,330 --> 00:06:13,960
And last.

76
00:06:14,680 --> 00:06:20,140
This thing is optional, self-contained SDM an option.

77
00:06:21,710 --> 00:06:28,070
With this self-contained, stable option, we are instructing it to create a self-contained report means

78
00:06:28,070 --> 00:06:34,250
all the assets like CIUSSS images, will be stored in one file only and not in separate folders here

79
00:06:34,340 --> 00:06:34,690
dental.

80
00:06:38,130 --> 00:06:44,850
OK, so results says to passed and one warning and the warning message cycle alchemy track modifications,

81
00:06:45,150 --> 00:06:49,280
add significant overhead and will be disabled in the application.

82
00:06:49,290 --> 00:06:52,590
You're just add to this property to true or false to suppress this funding.

83
00:06:52,980 --> 00:06:53,460
Let's do it.

84
00:07:06,580 --> 00:07:08,140
Close to false.

85
00:07:11,340 --> 00:07:11,970
So what?

86
00:07:13,500 --> 00:07:14,840
And now again, I don't.

87
00:07:17,980 --> 00:07:21,910
No warnings to test must now open the test results.

88
00:07:37,540 --> 00:07:39,910
This is how the former drug test results look like.

89
00:07:40,540 --> 00:07:43,420
These are the environment details that this test ran.

90
00:07:44,410 --> 00:07:44,940
Somebody.

91
00:07:45,820 --> 00:07:48,010
And these are the test results in tabular form.

92
00:07:48,580 --> 00:07:53,680
So we have the test results as past and the duration time in seconds.

93
00:07:54,610 --> 00:07:55,020
All right.

94
00:07:55,030 --> 00:08:00,670
So guys, these were very simple test methods which just text the response to decode and actually response

95
00:08:00,670 --> 00:08:00,960
data.

96
00:08:01,420 --> 00:08:06,310
Now it's time to increase the complexity of tests by adding unit tests in it, which are covered in

97
00:08:06,310 --> 00:08:06,940
the next lecture.

98
00:08:07,240 --> 00:08:07,600
Thanks.
