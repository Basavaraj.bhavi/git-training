1
00:00:00,910 --> 00:00:02,800
I hope you found out what is missing.

2
00:00:03,280 --> 00:00:10,090
It is the artifacts see as of now when this dust will run, the output directory flag underscore report

3
00:00:10,420 --> 00:00:14,140
by default is not going to get stored anywhere after the job is complete.

4
00:00:14,680 --> 00:00:17,080
You remember the default behavior enough jobs in GitLab.

5
00:00:17,530 --> 00:00:19,630
Everything is cleaned up up on job completion.

6
00:00:20,080 --> 00:00:21,870
So how will you see this report?

7
00:00:21,880 --> 00:00:27,250
If by chance the job is filled after a test is filled, you would definitely want to visit the reports

8
00:00:27,490 --> 00:00:29,560
to debug what went wrong in there, right?

9
00:00:30,040 --> 00:00:34,120
It means you need to store the output of this test somewhere for later use.

10
00:00:34,690 --> 00:00:39,460
And the answer is that artifacts stored this output directly as artifacts.

11
00:00:40,000 --> 00:00:41,290
So let's add artifacts.

12
00:00:43,930 --> 00:00:45,010
By artifacts.

13
00:00:49,250 --> 00:00:50,570
And the parts would be simple.

14
00:00:56,180 --> 00:00:56,870
Flick reports.

15
00:00:58,000 --> 00:01:00,940
Artifacts monitored, but still, there is one small problem.

16
00:01:02,030 --> 00:01:07,520
Actually, guys, with the default behavior of artifacts, the artifacts are not stored if the job fails.

17
00:01:07,880 --> 00:01:09,770
They are only stored if the job succeeds.

18
00:01:10,160 --> 00:01:16,130
And if you notice this is just the opposite of what we want to achieve with lint test, we want to detect

19
00:01:16,130 --> 00:01:17,240
any bugs in the code.

20
00:01:17,570 --> 00:01:21,920
And if at all there are any bugs detected, then this job is definitely going to fail.

21
00:01:22,430 --> 00:01:27,890
But with the current artifacts default behavior, it is never going to see if the test result for different

22
00:01:27,900 --> 00:01:28,280
jobs.

23
00:01:28,730 --> 00:01:31,760
So it means the code as of now is not going to help us.

24
00:01:32,360 --> 00:01:37,070
Now the question is how do we instruct GitLab to store artifacts even if the job is filled?

25
00:01:37,550 --> 00:01:44,510
It's by adding a keyword when in this job, when given as a name suggests, is used to tell when to

26
00:01:44,510 --> 00:01:45,470
run a particular task.

27
00:01:46,370 --> 00:01:47,660
So under the artifacts?

28
00:01:50,890 --> 00:01:52,150
Babe, the key word when.

29
00:01:53,290 --> 00:01:59,770
And Pastor Value, as always, always conditioned states to execute this task regardless of the status

30
00:01:59,770 --> 00:02:00,370
of this job.

31
00:02:00,730 --> 00:02:05,500
So no matter this job fails or succeeds, the artifacts are always going to get saved.

32
00:02:06,340 --> 00:02:09,070
This condition will make the artifacts available to us.

33
00:02:09,070 --> 00:02:14,260
In any case, with this necessary part of this lynch test is complete.

34
00:02:14,920 --> 00:02:19,210
The next complimentary change we can make is we can remove this.

35
00:02:20,980 --> 00:02:23,580
Flick it installation part from the script tag.

36
00:02:25,490 --> 00:02:29,030
And can edit inside the quiver before script.

37
00:02:32,450 --> 00:02:32,960
Like this?

38
00:02:34,050 --> 00:02:39,750
Before script is one more keyword that is used to define the area of commands that you think should

39
00:02:39,750 --> 00:02:40,830
run before each job.

40
00:02:41,370 --> 00:02:48,210
So in this case, before the commands listed under the script tag that is this command starts executing.

41
00:02:48,660 --> 00:02:55,100
First, the command inside that before script will run and then only the control will pass to script.

42
00:02:56,460 --> 00:02:59,670
Now you may ask what's the difference of being this way and that way?

43
00:03:00,240 --> 00:03:01,230
Well, nothing much.

44
00:03:01,500 --> 00:03:06,360
You can also write the whole set of commands within the script tag itself, like we were doing since

45
00:03:06,360 --> 00:03:06,840
the beginning.

46
00:03:07,410 --> 00:03:12,480
But generally, it's a good practice to keep this installation kind of commands, logging in commands

47
00:03:12,720 --> 00:03:18,310
or whatever other commands you think are prerequisites for the main commands should be kept under the

48
00:03:18,420 --> 00:03:19,110
before script.

49
00:03:19,110 --> 00:03:19,480
Give it.

50
00:03:19,860 --> 00:03:20,160
OK.

51
00:03:21,490 --> 00:03:26,590
After everything finally, to make this job part of my plane, I will add it to a stage.

52
00:03:31,910 --> 00:03:36,650
Let's say next stage and also initiate the stages order as well.

53
00:03:45,080 --> 00:03:51,400
So this way, the tests stage would be our first stage for the pipeline, save it and I will commit

54
00:03:51,410 --> 00:03:53,120
the changes macro terminal.

55
00:03:54,840 --> 00:03:56,370
Let me check the branch, which I'm in.

56
00:04:00,070 --> 00:04:01,570
App logic, not an issue.

57
00:04:05,500 --> 00:04:11,460
OK, so in good status, we are seeing two entities get lapsed, the 8.00 a.m. and flag reports directly.

58
00:04:12,280 --> 00:04:16,990
Actually, this report directly is the result of the lint test, which I just performed locally.

59
00:04:17,560 --> 00:04:19,900
But this directory should not be part of our committee.

60
00:04:20,380 --> 00:04:26,260
So either way, you can stage only the YAML file individually or seems to be no longer needed, Flake

61
00:04:26,260 --> 00:04:28,570
reports directly, so we can simply delete it.

62
00:04:38,820 --> 00:04:40,090
I can tell you this as well.

63
00:04:40,110 --> 00:04:40,770
By gosh.

64
00:04:47,140 --> 00:04:49,690
These files are also the part of the run, which would.

65
00:04:51,800 --> 00:04:52,070
Yeah.

66
00:05:00,780 --> 00:05:01,920
Upstream is already set.

67
00:05:01,950 --> 00:05:02,820
So when you get push.

68
00:05:07,900 --> 00:05:09,010
Go back to the.

69
00:05:11,790 --> 00:05:14,460
And switched to the logic branch.

70
00:05:19,400 --> 00:05:20,810
We got a YAML file here.

71
00:05:21,470 --> 00:05:24,620
And upon getting Yamal the issue, the pipeline must have started.

72
00:05:28,520 --> 00:05:28,980
Great.

73
00:05:29,000 --> 00:05:29,630
It's bust.

74
00:05:30,380 --> 00:05:32,360
And if we see the execution details of it.

75
00:05:37,750 --> 00:05:44,110
I just wanted to show you that before script commands see before the execution of commands on the script

76
00:05:44,110 --> 00:05:47,650
tag, before script commands and forced to install flickered.

77
00:05:48,430 --> 00:05:50,080
And if I go to artifacts.

78
00:06:04,470 --> 00:06:05,550
All good, no worries.

79
00:06:06,150 --> 00:06:08,700
Wonderful testing stages coded correctly.

80
00:06:08,910 --> 00:06:11,380
It's time to most attendees into the main must report.

81
00:06:11,880 --> 00:06:12,360
Let's do it.

82
00:06:26,530 --> 00:06:30,760
It is merged nice now again upon the receival of Yemen file.

83
00:06:31,050 --> 00:06:36,190
That's the pipeline for Main Branch would also have run too, since I have not done it off.

84
00:06:41,770 --> 00:06:43,000
See, it is also running.

85
00:06:43,450 --> 00:06:46,690
I will exclude the automated run in monster branch later in this course.

86
00:06:47,440 --> 00:06:48,520
This is also past.

87
00:06:57,510 --> 00:06:59,790
And we have got artifacts for this pipeline as well.

88
00:07:00,800 --> 00:07:06,200
Well, so far, so we have successfully entered the first job lynch test in the pipeline now, next

89
00:07:06,200 --> 00:07:06,560
lecture.

90
00:07:06,770 --> 00:07:10,520
We will continue with the same YAML file and will add the remaining tests in it.

91
00:07:10,670 --> 00:07:11,630
See your next glasses.
