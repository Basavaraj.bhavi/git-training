1
00:00:00,750 --> 00:00:06,270
Once the code is dusted from all possible angles, it gives you the green flag to go ahead and take

2
00:00:06,270 --> 00:00:11,230
the pipeline to the next stage, which is to build stage since it's a Python application.

3
00:00:11,250 --> 00:00:16,680
So we need to build some Python environment with all the required external libraries so as to deploy

4
00:00:16,680 --> 00:00:19,590
the application and make it accessible by the end user.

5
00:00:20,220 --> 00:00:22,770
And today's vote is a vote of containerisation.

6
00:00:23,130 --> 00:00:28,260
Gone are the days when you would deploy it on some Linux based server by doing all the installations

7
00:00:28,410 --> 00:00:29,970
manually on server one by one.

8
00:00:30,390 --> 00:00:35,460
And whenever there are some updates in software revisions, you would have to go to the same loop again.

9
00:00:36,160 --> 00:00:36,690
Yes, it was.

10
00:00:37,170 --> 00:00:42,780
So that is why we will deploy it using some container based technology, which is none other than Docker.

11
00:00:43,380 --> 00:00:49,020
The main component of a Docker based workflow is an image which contains everything needed to run an

12
00:00:49,020 --> 00:00:49,620
application.

13
00:00:50,400 --> 00:00:55,890
Having said that, in the next stage of our continuous integration, we would build a Docker image for

14
00:00:55,890 --> 00:00:57,000
the project application.

15
00:00:58,090 --> 00:01:02,320
To create a custom Docker image, the key file it needs is the Docker fight.

16
00:01:03,070 --> 00:01:07,390
I've already shown you the glimpse of how a Docker file looks like in the previous lecture.

17
00:01:08,080 --> 00:01:09,850
Let's create a new one for this project.

18
00:01:10,810 --> 00:01:11,800
In the project route.

19
00:01:13,400 --> 00:01:14,210
Great, no file.

20
00:01:15,590 --> 00:01:16,180
Mm.

21
00:01:17,500 --> 00:01:18,170
No coffee.

22
00:01:24,160 --> 00:01:27,400
Unless you have already seen how to write file previously.

23
00:01:27,790 --> 00:01:33,190
So if possible, I would suggest by seeing the requirements of this application, try to create it on

24
00:01:33,190 --> 00:01:35,590
your own because this video and do some hands on.

25
00:01:36,620 --> 00:01:37,550
I will start writing it.

26
00:01:41,620 --> 00:01:44,020
Britain would be the best image for obvious reasons.

27
00:01:50,070 --> 00:01:51,570
And then create a Vogue directory.

28
00:01:56,010 --> 00:01:59,430
Then inside this woke directory and everything president on the road.

29
00:02:06,420 --> 00:02:10,700
After that, we need to install the packages defined in requirements, not the extra fight.

30
00:02:11,650 --> 00:02:17,440
But before we install the packages for their foresight, let's add a command to upgrade the pipe itself.

31
00:02:27,040 --> 00:02:29,810
Now command to install packages listed under requirements.

32
00:02:41,710 --> 00:02:44,010
The last command to run in Docker file would be.

33
00:02:44,740 --> 00:02:45,790
Me first type.

34
00:02:47,230 --> 00:02:48,610
It's a unicorn command.

35
00:03:00,980 --> 00:03:03,740
This on at this moment comes out of no context.

36
00:03:04,010 --> 00:03:08,840
It can be understood better in the coming lectures, actually as part of application deployment.

37
00:03:09,200 --> 00:03:14,510
I would be deploying it on Heroku, which is a container based cloud platform, as a service to deploy,

38
00:03:14,660 --> 00:03:16,340
manage and scale modern apps.

39
00:03:16,970 --> 00:03:22,970
That is why, rather than running the Python application, as is, I will run it with the unicorn,

40
00:03:23,420 --> 00:03:24,710
which is pure python.

41
00:03:24,860 --> 00:03:31,670
Actually Pixelbook for WSJ applications that allows you to run any Python application concurrently by

42
00:03:31,670 --> 00:03:34,180
running multiple Python processes within a single day.

43
00:03:34,970 --> 00:03:36,830
Just don't get into too much details of it.

44
00:03:37,160 --> 00:03:43,790
Focus on the command CMD stands for which command to run and the command is due unicorn.

45
00:03:45,960 --> 00:03:50,730
Then this app is telling which application to run in from app not be verified.

46
00:03:52,280 --> 00:03:56,460
Then the option bind with its value in the form of host Colin board.

47
00:03:57,290 --> 00:04:04,790
It specifies a server socket to bind 0.2 0.0 is the host and port is defined as an environmental variable.

48
00:04:05,180 --> 00:04:08,720
By default, it's 8000 and last reload.

49
00:04:09,050 --> 00:04:12,560
It will cause workers to be restarted whenever application content is.

50
00:04:13,950 --> 00:04:19,980
And guys, since you unicorn does not come by default, so you need to add your unicorn entry into Goldman's

51
00:04:19,980 --> 00:04:20,520
file as well.

52
00:04:21,910 --> 00:04:22,810
Let me save this.

53
00:04:33,840 --> 00:04:36,840
OK, so with this Addo-Carr fine definition is complete.

54
00:04:37,950 --> 00:04:43,140
Just one additional step since we would be deploying this application on Heroku with the unicorn.

55
00:04:43,950 --> 00:04:47,670
The step is that you have to sort of inform Heroku of unicorn.

56
00:04:48,330 --> 00:04:50,340
This information is based in the profile.

57
00:04:56,100 --> 00:05:02,550
BlockFi creators in the Project Room directory is always a simple text file with the names, but without

58
00:05:02,550 --> 00:05:07,200
any file extension who doesn't know about Brockway and Hiroko apps?

59
00:05:07,530 --> 00:05:09,990
BlockFi lists the processed types in an application.

60
00:05:10,500 --> 00:05:16,140
Each process type is a declaration of command that is executed when a container of that processed type

61
00:05:16,140 --> 00:05:16,680
is started.

62
00:05:17,970 --> 00:05:20,760
The command in Brockville starts with the processed type.

63
00:05:24,040 --> 00:05:29,380
That can be a number of process types like Web worker John Walker, Clock and so on.

64
00:05:29,950 --> 00:05:33,580
According to our application, the process type would be a web process type.

65
00:05:34,270 --> 00:05:41,260
The web command tells Oracle to start a web server for the application using Do Unicorn.

66
00:05:42,900 --> 00:05:48,180
Said the app name who I brought Billy and last is very well put.

67
00:05:52,750 --> 00:05:53,100
Good.

68
00:05:53,680 --> 00:05:58,390
Now, everything being said, I guess we are ready to define build stage in the capital abroad fireman

69
00:05:58,390 --> 00:06:02,080
file that will build a Docker image using this Docker file.
