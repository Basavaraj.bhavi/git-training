1
00:00:00,240 --> 00:00:04,680
Continuing the better job, we are left with pushing the image to GitHub repository.

2
00:00:05,280 --> 00:00:10,740
But before you can actually push to container registry first, you have to authenticate with the container

3
00:00:10,740 --> 00:00:17,460
registry and to authenticate with Cassilly, you can either use a built in take the variable state registry

4
00:00:17,460 --> 00:00:23,820
user that has a read write access to the container registry, or you can use your personal access token

5
00:00:23,820 --> 00:00:27,180
from your profile while using Personal Access Token.

6
00:00:27,390 --> 00:00:32,430
You have to make sure that token should have a minimum scope of registry for it.

7
00:00:32,760 --> 00:00:37,050
That is the full access and scope of right registry for right.

8
00:00:37,260 --> 00:00:38,550
That is the bush access.

9
00:00:39,240 --> 00:00:43,260
Well, I will go with the first option using the registry user variable.

10
00:00:44,670 --> 00:00:50,190
And since the logging is an indispensable part, so it is kind of prerequisite to authenticate to get

11
00:00:50,200 --> 00:00:51,210
lab container history.

12
00:00:51,690 --> 00:00:56,610
So rather than writing the log in command and script tag, I would place it in before script.

13
00:00:59,170 --> 00:00:59,590
So.

14
00:01:05,920 --> 00:01:13,030
Before script and then to log in, we have darker command Docker logging.

15
00:01:14,770 --> 00:01:18,370
U u is for use it a variable.

16
00:01:22,930 --> 00:01:24,100
B is for password.

17
00:01:24,910 --> 00:01:30,490
Its password is also created automatically and is assigned to a variable C registry password.

18
00:01:40,820 --> 00:01:46,420
And last past the address of GitLab container industry, which is stored in environment variable S.A.

19
00:01:46,430 --> 00:01:46,940
registry.

20
00:01:54,050 --> 00:01:55,340
And now the push.

21
00:01:58,480 --> 00:02:00,070
Simply type Dr. Push.

22
00:02:03,820 --> 00:02:07,840
Now, corporate and image tag, which is stored in image tag media, but.

23
00:02:12,540 --> 00:02:12,900
Done.

24
00:02:14,130 --> 00:02:19,800
But guys, with this set of build job, if you commit, unfortunately, you won't be able to build a

25
00:02:19,800 --> 00:02:21,720
Docker image, let alone the pushback.

26
00:02:22,470 --> 00:02:25,740
See, the code we have written so far is 100 percent correct.

27
00:02:26,220 --> 00:02:32,460
But as we are in GitLab City, so to create images from Docker would require us to do a little more

28
00:02:32,460 --> 00:02:38,220
effort and would need to enable the Docker commands in our case of the pipeline by using the Docker

29
00:02:38,250 --> 00:02:39,370
with a Docker service.

30
00:02:39,390 --> 00:02:42,000
Did we do short form of Docker in Dakar?

31
00:02:42,870 --> 00:02:43,530
Actually, guys.

32
00:02:43,650 --> 00:02:46,710
Docker at group level uses a client server.

33
00:02:46,710 --> 00:02:50,100
Architecture means manual and Docker on command line.

34
00:02:50,430 --> 00:02:56,850
You use a Docker blind, which in further connects to Docker Daemon that listens to Docker API request

35
00:02:57,090 --> 00:03:02,070
and manages the Docker objects like Docker images, containers, volumes, etc. things.

36
00:03:02,580 --> 00:03:07,890
And for that, Docker images also have two flavors that simple Docker image.

37
00:03:08,280 --> 00:03:09,900
And second, the Docker did.

38
00:03:10,500 --> 00:03:16,290
The stable flavor is intended to be used as a Docker blind, and it contains everything required to

39
00:03:16,290 --> 00:03:23,400
connect with Docker Daemon, whereas the flavor is intended to be used as Docker daemon as its entry

40
00:03:23,400 --> 00:03:23,760
point.

41
00:03:24,510 --> 00:03:30,240
So in short, we enable the Docker commands to build or push Docker image from Kayseri jobs.

42
00:03:30,540 --> 00:03:35,830
We need to use this daemon, although running Docker inside Docker is generally not recommended.

43
00:03:36,180 --> 00:03:41,820
But for these such legitimate use cases, like development of Docker image, we can use Docker in Docker.

44
00:03:42,760 --> 00:03:51,190
So let's add this didn't intend to build job after images, Doug, at a new keyboard services.

45
00:03:56,190 --> 00:03:58,860
And then defined the decade in service like this.

46
00:04:04,380 --> 00:04:10,800
This services is a new keyword, defining a Docker image that runs in a minute with the parent Docker

47
00:04:10,800 --> 00:04:13,230
image specified in that image given.

48
00:04:14,130 --> 00:04:17,710
This feature allows you to access the service image during building.

49
00:04:18,510 --> 00:04:24,090
So in our case, then it's sort of child image that will run in a linked manner.

50
00:04:24,150 --> 00:04:26,430
With this Docker image?

51
00:04:28,310 --> 00:04:34,730
And using Docker service, deal certificates for communication will be automatically generated and we

52
00:04:34,730 --> 00:04:38,360
will be allowed to run the full Docker toolset within a Docker container.

53
00:04:39,860 --> 00:04:40,280
All right.

54
00:04:40,400 --> 00:04:46,310
So with this addition of service in this job, finally, we will be able to build a custom Docker image

55
00:04:46,700 --> 00:04:50,870
and after building, it will be automatically pushed to GitLab container registry.

56
00:04:52,230 --> 00:04:53,970
Lasting pending is stage.

57
00:05:08,820 --> 00:05:11,220
Everything is done, nothing for the moment of truth.

58
00:05:11,610 --> 00:05:13,680
Save the code and commit genius to remote.

59
00:05:39,600 --> 00:05:44,820
It would take some extra time, as it has to first do the court and amnesty, and then it will build

60
00:05:45,990 --> 00:05:46,620
its completion.

61
00:05:49,230 --> 00:05:53,220
Pipeline got completed going through the build job logs.

62
00:06:00,950 --> 00:06:06,890
At first, the command inside before the script again, which is to log in and authenticate with GitLab

63
00:06:06,890 --> 00:06:09,710
container registry after the logging succeeded.

64
00:06:11,590 --> 00:06:15,040
And here it started building the image out of our application.

65
00:06:15,790 --> 00:06:19,090
The building task is of six steps defined in Docker file.

66
00:06:22,200 --> 00:06:22,950
Step one.

67
00:06:24,230 --> 00:06:26,180
Step two, three four.

68
00:06:27,900 --> 00:06:28,650
And so on.

69
00:06:31,000 --> 00:06:32,200
After the images built.

70
00:06:38,000 --> 00:06:41,630
After the images built, remember, we have listed the Docker images.

71
00:06:42,170 --> 00:06:43,700
This is the image it created.

72
00:06:44,610 --> 00:06:50,700
The repository name came from the variable serious three image, and then we have that image tag app

73
00:06:50,700 --> 00:06:56,310
logic, which is the branch name for which, despite being around for a game, it came from the environment

74
00:06:56,310 --> 00:06:58,320
variable ref, slug.

75
00:06:58,800 --> 00:07:07,470
And then we have some more details of this image, like the image ID creation time and sizing images.

76
00:07:08,490 --> 00:07:12,570
And this second image is a python that we specified in a dogfight.

77
00:07:14,330 --> 00:07:20,210
After this, the next task was to push this Docker image to GitLab container district, and it is doing

78
00:07:20,210 --> 00:07:20,570
the same.

79
00:07:22,520 --> 00:07:29,540
The push refers to Repository District or GitLab dot com slash attachment to slash employer portal slash

80
00:07:29,540 --> 00:07:31,220
employer image as specified.

81
00:07:32,540 --> 00:07:35,420
After this, the jobs magnet succeeded.

82
00:07:36,260 --> 00:07:38,390
Good after this job.

83
00:07:38,680 --> 00:07:43,160
Bad image would be stored in my GitLab container registry and where to view it?

84
00:07:43,820 --> 00:07:47,480
Well, it's under packages and repositories containing registry.

85
00:07:51,670 --> 00:08:00,070
Here it is a newly built image with one back image name is a dog's mental slash imply portal slash image

86
00:08:00,070 --> 00:08:00,280
name.

87
00:08:01,700 --> 00:08:02,210
Open it.

88
00:08:04,420 --> 00:08:05,180
Image name.

89
00:08:05,710 --> 00:08:07,060
These are the tag list.

90
00:08:07,240 --> 00:08:11,950
Currently, we have only one the app logic tag image size.

91
00:08:13,120 --> 00:08:18,730
You can click on this to expand for more details and to delete image with specific bag.

92
00:08:19,360 --> 00:08:25,930
You can select from here and delete selected tags, or if you want to delete the complete image repository,

93
00:08:26,110 --> 00:08:27,970
then you'll do it from this delete button.

94
00:08:29,350 --> 00:08:31,720
OK, so with this comes the end of this built job.

95
00:08:32,020 --> 00:08:33,130
So you next class.
