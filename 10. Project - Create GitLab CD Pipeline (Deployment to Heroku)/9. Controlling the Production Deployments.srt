1
00:00:00,660 --> 00:00:05,250
I hope you would have figured out the problem, if not, let me quickly go to the pipeline.

2
00:00:07,410 --> 00:00:10,410
So let's say you're asked to work on some applications feature.

3
00:00:10,920 --> 00:00:16,080
What we will do is you will check out a branch from us to make some changes in the app, not be white.

4
00:00:16,620 --> 00:00:22,140
Then as soon as you push the feature branch into GitLab Ripple, the pipeline would automatically trigger

5
00:00:22,590 --> 00:00:29,280
all the stages defined here will run in the specified on the test build, deploy staging, automated

6
00:00:29,280 --> 00:00:31,230
testing and deploy production.

7
00:00:32,040 --> 00:00:35,160
But guys, the problem here is in the production deployment stage.

8
00:00:35,760 --> 00:00:41,820
Asper current pipeline After the acceptance testing stage, your future branch pipeline will also deploy

9
00:00:41,820 --> 00:00:46,350
the application to production environment as well without even asking featured branch.

10
00:00:46,470 --> 00:00:48,430
Really, you can't let it do so.

11
00:00:48,870 --> 00:00:51,690
Doesn't matter how many tests this application would have passed.

12
00:00:52,020 --> 00:00:56,700
We can't let the pipeline to deploy to production from this branch, which is meant for development

13
00:00:56,700 --> 00:00:57,420
purposes only.

14
00:00:57,960 --> 00:01:01,470
Only a dedicated master branch should be allowed to deploy in production.

15
00:01:02,160 --> 00:01:04,770
So that's a problem currently, and you have to rectify it.

16
00:01:05,550 --> 00:01:11,100
And if we think about the solution, somehow, if we can instruct GitLab to run the last stage of the

17
00:01:11,350 --> 00:01:15,450
production only for the master branch, then our problem is solved, right?

18
00:01:15,930 --> 00:01:23,130
And this is exactly I will be doing by using a GitLab given only in one line if I say that given only

19
00:01:23,280 --> 00:01:25,770
controls when to add a job in your pipeline.

20
00:01:26,310 --> 00:01:30,990
It is used to define a condition when a job should run with only given.

21
00:01:31,200 --> 00:01:37,110
You can specify conditions in form of variable branch names, pipeline types, bags or you can even

22
00:01:37,110 --> 00:01:39,270
use neighborhood expressions and other options.

23
00:01:40,260 --> 00:01:45,930
Actually, guys, when you write a GitLab viral file for a project and feed it to GitLab, GitLab does

24
00:01:45,930 --> 00:01:48,950
not just blindly run all the jobs defined in that fight.

25
00:01:49,400 --> 00:01:53,310
Read behind the scenes GitLab first great execution plan.

26
00:01:53,820 --> 00:01:58,890
As part of execution plan, it first checks for some logic that could be defined in the pipeline.

27
00:01:59,220 --> 00:02:05,820
In this case, for example, we will use job keywords to define the job behavior as per the Logic Execution

28
00:02:05,820 --> 00:02:10,950
Plan is created, which tells the machine that which jobs to run for this branch, etc. things.

29
00:02:11,190 --> 00:02:14,610
And then only the actual run happens as part of the execution plan.

30
00:02:15,610 --> 00:02:21,610
Now, in our case, we want the production job to run for the main branch only, so I will only give

31
00:02:21,610 --> 00:02:24,820
it and deploy a production stage with main branch has value.

32
00:02:31,890 --> 00:02:32,370
Only.

33
00:02:35,520 --> 00:02:36,070
I mean.

34
00:02:38,600 --> 00:02:43,280
With this setting, Gettler will not run that deploy production job for this staging ground.

35
00:02:43,830 --> 00:02:49,250
But yeah, when you would most this pipeline to the main branch, then of course this job will run and

36
00:02:49,250 --> 00:02:51,620
deploy the new application to production environment.

37
00:02:52,010 --> 00:02:53,210
This is what we wanted, right?

38
00:02:54,140 --> 00:02:54,630
All right.

39
00:02:54,650 --> 00:02:57,680
So with this given are completed, the pipeline is ready.

40
00:02:58,160 --> 00:03:00,560
I can accommodate changes and push it to the depot.

41
00:03:01,460 --> 00:03:06,530
If after the runoff, this branch, everything goes OK, we can confidently mortgages grants to master

42
00:03:06,530 --> 00:03:06,860
branch.

43
00:03:32,160 --> 00:03:38,430
You can see this time by using only archival or piping looks a little short, as we are in Amlogic branch.

44
00:03:38,710 --> 00:03:44,160
So GitLab has not allowed the job in production environment for this pipeline, but I will let this

45
00:03:44,160 --> 00:03:45,930
pipeline done and see you after a few minutes.

46
00:03:47,780 --> 00:03:49,070
The pipeline was a success.

47
00:03:49,430 --> 00:03:53,360
Now we can confidently create a new multi request to merge this into monster.

48
00:03:53,810 --> 00:03:54,560
Let's do it quickly.

49
00:04:20,110 --> 00:04:25,750
See, it is showing the last pipeline results for approval as I am the owner of this project, so I

50
00:04:25,750 --> 00:04:27,730
myself will approve and most this request.

51
00:04:34,780 --> 00:04:38,080
Upon minding the pipeline, the main branch would have also got started.

52
00:04:46,130 --> 00:04:49,700
Great, since this run is being executed for the main branch.

53
00:04:50,000 --> 00:04:52,460
So this time deployed production stage is hit.

54
00:04:54,560 --> 00:04:55,780
Let's wait for its success.

55
00:04:58,430 --> 00:05:00,470
Excellent pipeline is a success.

56
00:05:01,130 --> 00:05:03,470
And if I hop up to the production website.

57
00:05:16,080 --> 00:05:20,730
Congratulations are application passing from various stages is final life of the public.
