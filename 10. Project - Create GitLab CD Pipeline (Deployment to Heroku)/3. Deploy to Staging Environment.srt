1
00:00:00,630 --> 00:00:02,100
So let's add a new job here.

2
00:00:09,510 --> 00:00:10,260
Our job.

3
00:00:13,000 --> 00:00:13,990
Deploy stage.

4
00:00:19,170 --> 00:00:22,170
The image this job values would be, again, the darker.

5
00:00:26,340 --> 00:00:32,160
And since we would be logging in and pulling the Docker image from GitLab container district, so to

6
00:00:32,160 --> 00:00:37,500
enable that Docker commands in this case, every job we have to use Docker in here as well.

7
00:00:38,370 --> 00:00:40,440
I will copy the dinner services.

8
00:00:42,840 --> 00:00:43,560
And based.

9
00:00:45,430 --> 00:00:48,400
Next, add the stage.

10
00:00:52,900 --> 00:00:55,990
Say Deploy deployer staging.

11
00:00:57,900 --> 00:01:03,480
And as this state should execute after the bill is complete, so added accordingly in the stages of

12
00:01:03,480 --> 00:01:03,660
the.

13
00:01:16,160 --> 00:01:21,620
The basic code is done now, start with the argument that this job should run to upload the Docker image

14
00:01:21,620 --> 00:01:24,920
to Hiroko Cloud to push the Docker image.

15
00:01:25,190 --> 00:01:30,920
The first obvious step we need to do is we need to pull the Docker image from GitLab container registry

16
00:01:31,190 --> 00:01:37,850
before it can be pushed to Heroku Container District for pulling again, just like Build Job.

17
00:01:38,060 --> 00:01:40,460
We need to authenticate with the registry first.

18
00:01:42,410 --> 00:01:46,190
So copy and paste the whole before script thing.

19
00:01:51,520 --> 00:01:55,180
After the logging is successful, we can move on to the script attack.

20
00:02:02,110 --> 00:02:05,470
In script, first command should be to pull the image.

21
00:02:05,560 --> 00:02:11,530
So Dr. Cole, and then that image and name of tag.

22
00:02:15,780 --> 00:02:21,150
They mentioned a missile in variable image deck was the image police successful, it can be no push

23
00:02:21,150 --> 00:02:26,190
to Hiroko and to push a Docker image to Heroku container industry.

24
00:02:26,610 --> 00:02:29,670
Likewise for GitLab Container District for Heroku.

25
00:02:29,670 --> 00:02:35,880
Also, we first need to tag the Docker image with the Heroku naming conventions, according to which

26
00:02:35,880 --> 00:02:38,130
your image should follow this naming template.

27
00:02:39,300 --> 00:02:44,160
So it should be a registry dot Heroku dot com slash your application name.

28
00:02:44,740 --> 00:02:47,700
Notice the application name is not imply portal.

29
00:02:48,000 --> 00:02:54,000
It's the app that we created in Heroku dashboard in the last lecture that is this important staging.

30
00:02:54,750 --> 00:02:59,460
And lastly, the process type, which would be so after pulling.

31
00:02:59,460 --> 00:03:03,270
Let's add a command to tag the pooled image according to this naming template.

32
00:03:05,930 --> 00:03:09,650
Dr Tag, the source image name, which is.

33
00:03:15,520 --> 00:03:25,420
Image, Doug, then the new tag, which would be a registry door, Heroku dot com.

34
00:03:27,220 --> 00:03:27,880
Slash.

35
00:03:28,890 --> 00:03:30,870
And then the application name, which is.

36
00:03:32,780 --> 00:03:33,170
AMP.

37
00:03:34,420 --> 00:03:35,020
Portal.

38
00:03:38,790 --> 00:03:39,390
Staging.

39
00:03:40,640 --> 00:03:42,130
Last is a process type from.

40
00:03:44,210 --> 00:03:47,450
Now, will this new image name we would require again and again?

41
00:03:47,600 --> 00:03:49,430
So let's create a variable for it.

42
00:03:55,400 --> 00:03:56,120
Go above.

43
00:04:07,600 --> 00:04:13,690
So, Hiroko, staging is a new variable, and its value is this, again, this variable can be variables

44
00:04:13,690 --> 00:04:14,230
used more.

45
00:04:14,950 --> 00:04:20,470
Don't know if minimalized is actually word or not, but you just want my attention right before this

46
00:04:20,470 --> 00:04:20,950
variable.

47
00:04:22,390 --> 00:04:24,790
What I can do is create a new variable.

48
00:04:30,220 --> 00:04:31,120
Staging app.

49
00:04:49,550 --> 00:04:53,120
Now, after this images, Doug put the variable in the.

50
00:05:01,130 --> 00:05:05,150
After the images, we can push it to the Bush.

51
00:05:05,300 --> 00:05:06,560
It's simple Bush command.

52
00:05:12,860 --> 00:05:21,040
Dr. Bush and then the Hiroko Container Registry, which is stored in Hiroko staging video, but that's

53
00:05:21,050 --> 00:05:21,350
OK.

54
00:05:21,680 --> 00:05:26,810
But how would Heroku know that we are the right person and have the privileges to push to this registry?

55
00:05:27,320 --> 00:05:33,140
That is why authentication before push is required, just like we did with the GitLab container district

56
00:05:34,040 --> 00:05:35,270
for GitLab registry.

57
00:05:35,300 --> 00:05:37,940
We have these environment variables to authenticate.

58
00:05:38,330 --> 00:05:45,250
But for Heroku, Hiroko provides us an API key for authentication to get those API keys go back to her

59
00:05:45,260 --> 00:05:45,920
local dashboard.

60
00:05:47,550 --> 00:05:51,060
Click on your profile icon, go to account settings.

61
00:05:53,600 --> 00:05:56,840
Scroll down to the EPA section.

62
00:05:59,400 --> 00:06:00,300
Click to reveal.

63
00:06:01,470 --> 00:06:02,070
Here we go.

64
00:06:02,460 --> 00:06:05,520
We will use this long key to authenticate from the L.A..

65
00:06:06,900 --> 00:06:07,440
Copied.

66
00:06:10,400 --> 00:06:15,950
And guys, in case if you found that your keys got compromised, then you can create a new set of keys

67
00:06:15,950 --> 00:06:17,750
by clicking on this regenerate button.

68
00:06:18,780 --> 00:06:19,500
Back to God.

69
00:06:20,940 --> 00:06:26,610
All these guys are confidential, so we can't feed them directly in this pipeline and either create

70
00:06:26,610 --> 00:06:27,600
a new secret variable.

71
00:06:28,440 --> 00:06:29,910
So go to the variables page.

72
00:06:31,230 --> 00:06:31,850
In Gettler.

73
00:06:43,180 --> 00:06:44,860
And create a new secret variable.

74
00:06:48,370 --> 00:07:00,310
The key variable would be Heroku staging API key and valuable based.

75
00:07:02,870 --> 00:07:09,280
Variable type is variable only environmental scope, as we haven't created an environment, so set it

76
00:07:09,280 --> 00:07:10,970
to default all environments.

77
00:07:12,260 --> 00:07:14,450
Now to protect this variable or not.

78
00:07:15,620 --> 00:07:17,480
Guys remember from the Rebels lecture.

79
00:07:17,870 --> 00:07:23,850
If we check this option and protect this variable, then you won't be able to use Heroku staging API

80
00:07:23,900 --> 00:07:27,050
key value in any of the unprotected feature branch.

81
00:07:27,500 --> 00:07:29,170
So do use this variable.

82
00:07:29,180 --> 00:07:30,140
You have two options.

83
00:07:30,590 --> 00:07:37,040
Either way, you can uncheck this option to protect this variable, making the Heroku staging API available

84
00:07:37,040 --> 00:07:41,630
for every protected and unprotected branches and banks or the other way.

85
00:07:41,930 --> 00:07:48,170
You can protect this variable by checking it and can add your feature branch into the protected branches

86
00:07:48,170 --> 00:07:48,440
list.

87
00:07:48,620 --> 00:07:49,220
Like this?

88
00:07:49,700 --> 00:07:54,050
So I will keep it protected variable and make my branch a protected branch.

89
00:07:58,080 --> 00:08:04,740
Variable is created now, let's protect the app logic branch to configure protected branches from the

90
00:08:04,740 --> 00:08:05,580
project settings.

91
00:08:06,330 --> 00:08:07,620
Go to a repository.

92
00:08:12,210 --> 00:08:15,210
Here we have the protected branches section expanded.

93
00:08:19,300 --> 00:08:21,820
See, the main branch is protected by default.

94
00:08:22,600 --> 00:08:24,730
Now let's protect our app logic branch.

95
00:08:27,410 --> 00:08:31,150
A bookmaker branch protected, you again have two options first.

96
00:08:32,340 --> 00:08:38,100
You can only select from the list of available branches and protect it, or in case you have a long

97
00:08:38,100 --> 00:08:41,970
list of branches, then you can use white cards like shown here.

98
00:08:43,410 --> 00:08:50,010
To protect every branch whose name matches this criteria, like given stable, would protect every branch

99
00:08:50,160 --> 00:08:56,250
whose name ends with stable world names like production, stable, staging, stable will be protected

100
00:08:57,120 --> 00:08:57,500
anyway.

101
00:08:57,510 --> 00:09:02,550
For example, I will go simple and would protect the branch with the ideologic name.

102
00:09:04,450 --> 00:09:10,570
The second option allowed too much from here you can select a rule or group that can merge into this

103
00:09:10,570 --> 00:09:10,930
branch.

104
00:09:11,770 --> 00:09:14,530
I will go for developers and maintainers to merge.

105
00:09:16,450 --> 00:09:23,830
Then allowed to push and also select the roll group or user that can push to this branch again, developers

106
00:09:23,830 --> 00:09:24,490
and maintainers.

107
00:09:29,260 --> 00:09:30,910
For Bush, no.

108
00:09:31,960 --> 00:09:33,250
Finally, click on Protect.

109
00:09:44,680 --> 00:09:45,100
Great.

110
00:09:45,730 --> 00:09:49,780
You can see the branch app logic is now protected and it's listed here.

111
00:09:50,980 --> 00:09:56,470
Another variable being created and the branches protected, let's put them into some good use of authentication.

112
00:09:59,590 --> 00:10:07,450
So before you push out authentication, Docker logging.

113
00:10:09,960 --> 00:10:15,020
When you know, to specify any Hiroko username, Heroku is able to authenticate us from the epic.

114
00:10:15,450 --> 00:10:16,740
It has got us all covered.

115
00:10:16,950 --> 00:10:24,330
So for username, simply add an underscore and then for password field Bosner APK, which is available

116
00:10:24,330 --> 00:10:26,160
in the form of variable we just created.

117
00:10:36,760 --> 00:10:38,830
At last pastor registry address.

118
00:10:45,730 --> 00:10:51,640
OK, so finally, with this set of job defined, we would be able to push this Docker image out from

119
00:10:51,640 --> 00:10:58,660
the GitLab vicinity to Heroku Cloud, save it and let's push that in this remote to see the pipeline

120
00:10:58,660 --> 00:10:58,960
running.

121
00:11:04,640 --> 00:11:05,570
Good at.

122
00:11:16,010 --> 00:11:16,590
Pipeline.

123
00:11:32,570 --> 00:11:33,000
Great.

124
00:11:33,110 --> 00:11:37,220
The pipeline was a success briefing that deployed job logs.

125
00:11:39,460 --> 00:11:41,440
After that, Dr. Executor is prepared.

126
00:11:43,120 --> 00:11:44,020
The first step.

127
00:11:49,560 --> 00:11:51,960
It logged into the GitLab container registry.

128
00:11:53,840 --> 00:11:58,910
After logging succeeded, it pulled the Docker image from GitLab registry.

129
00:12:02,420 --> 00:12:06,980
Then the image was tagged with a new name defined in Heroku staging video but.

130
00:12:08,040 --> 00:12:12,630
And that here, not integration with a local registry was done by using the API key.

131
00:12:13,560 --> 00:12:18,450
And finally, when the log in was succeeded, the image was pushed to Heroku registry.

132
00:12:19,970 --> 00:12:23,930
Now, with the image being pushed to Hiroko, if I visited this application, you are.

133
00:12:27,970 --> 00:12:33,420
It is still showing the default Heroku welcome page, and the application is not yet live in this staging

134
00:12:33,430 --> 00:12:34,000
environment.

135
00:12:34,690 --> 00:12:40,240
Actually, guys pushing the Docker image to Hiroko Registry does not really mean that the images released

136
00:12:40,240 --> 00:12:46,390
to go life with that push, the images just loaded onto the Heroku container registry and it does not

137
00:12:46,390 --> 00:12:50,770
like and it has not run to run and release image or application.

138
00:12:51,070 --> 00:12:53,830
You have to run a specific Heroku Clia command.

139
00:12:54,070 --> 00:12:58,810
That is the container release command to release the image or application, but.

140
00:13:00,950 --> 00:13:04,460
Can we simply write that, Hiroko, come on as a next task in this job?

141
00:13:05,100 --> 00:13:07,580
I'm leaving this video here and you please give it a thought.
