1
00:00:00,950 --> 00:00:07,370
Before I give the answer to White, let me lay down its formal definition, Heroku is a container based

2
00:00:07,470 --> 00:00:14,330
global service provider, a platform as a service solution that enables developers to build, run and

3
00:00:14,330 --> 00:00:16,760
operate applications entirely in the cloud.

4
00:00:17,600 --> 00:00:22,910
Now that's the reason I chose Oracle over other cloud service providers for this particular use case

5
00:00:23,330 --> 00:00:31,640
is its ease of use as working with the cloud like a WAC or GCP requires the one time but manual configurations

6
00:00:31,640 --> 00:00:32,330
of the server.

7
00:00:32,660 --> 00:00:34,670
Its storage system and other things.

8
00:00:35,120 --> 00:00:40,880
On the other hand, when it use Heroku, then you're choosing platform, a service that provides an

9
00:00:41,240 --> 00:00:46,190
tools that you can use do not manage for hosting and deployment activities.

10
00:00:46,790 --> 00:00:52,580
So if you are a small business, then the best option for deployment will be platform as a service solution.

11
00:00:53,060 --> 00:00:56,150
All you have to do is just choose two settings and rest.

12
00:00:56,150 --> 00:01:01,220
Every other thing which can be destructive sometimes for developers is taken care by Hiroko.

13
00:01:01,850 --> 00:01:04,990
Basically in Heroku, deployment is just a plug and play thing.

14
00:01:05,870 --> 00:01:10,250
It also follows the thing that the best pipelines do not require teams to think about.

15
00:01:10,250 --> 00:01:15,740
Deploying your pipeline should be that much complete and automated that the development teams should

16
00:01:15,740 --> 00:01:21,270
only worry about the code that is, if it is ready to commit or not, and the rest must be taken care

17
00:01:21,290 --> 00:01:22,250
by the pipeline itself.

18
00:01:22,880 --> 00:01:26,600
Now that being said, let's visit Heracles website and create an account.

19
00:01:28,200 --> 00:01:30,270
In the browser, go to Heroku dot com.

20
00:01:30,660 --> 00:01:34,800
The link is attached in the Resources tab of this lecture to sign up.

21
00:01:34,890 --> 00:01:36,030
Click on the Sign Up button.

22
00:01:38,750 --> 00:01:40,280
We ask you to fill few details.

23
00:01:40,730 --> 00:01:41,690
Let's fill them quickly.

24
00:01:48,610 --> 00:01:49,780
Company name is optional.

25
00:01:50,170 --> 00:01:53,350
Then we have roll roll, let it be student.

26
00:01:54,320 --> 00:01:54,800
And the.

27
00:01:56,570 --> 00:02:00,590
Countries India primary developing language would be Britain.

28
00:02:07,280 --> 00:02:07,950
Fredricka?

29
00:02:10,370 --> 00:02:12,230
Hiroko will send you a confirmation email.

30
00:02:12,860 --> 00:02:13,820
You have to confirm it.

31
00:02:14,770 --> 00:02:15,760
I look from my phone.

32
00:02:17,560 --> 00:02:18,430
Then it for my phone.

33
00:02:18,580 --> 00:02:19,210
Let me logon.

34
00:02:50,520 --> 00:02:56,160
This is the Heroku dashboard to deploy the application to local force unit to create an application

35
00:02:56,160 --> 00:02:56,550
from you.

36
00:02:57,510 --> 00:02:59,970
So click on this create new app button.

37
00:03:03,350 --> 00:03:04,940
Give your application a unique name.

38
00:03:05,360 --> 00:03:06,170
Let the name be.

39
00:03:08,800 --> 00:03:09,490
Employ.

40
00:03:11,570 --> 00:03:11,980
Portal.

41
00:03:13,680 --> 00:03:20,130
OK, employee portal is not available by another one since I would be first deploying it to a staging

42
00:03:20,130 --> 00:03:24,210
environment, so I will take the uranium accordingly, so let it be.

43
00:03:28,800 --> 00:03:32,310
An important staging.

44
00:03:34,580 --> 00:03:36,710
It's available to the region.

45
00:03:36,950 --> 00:03:38,930
I will keep it U.S. and that.

46
00:03:44,030 --> 00:03:44,730
Congratulations.

47
00:03:44,960 --> 00:03:48,080
Your first Heroku application is created next visited.

48
00:03:49,380 --> 00:03:49,920
Open up.

49
00:03:52,640 --> 00:03:58,910
Are staging application is available at extra tips, important staging about Hiroko ABC.com.

50
00:03:59,420 --> 00:04:02,390
And currently it's showing us the default Iroko welcome message.

51
00:04:02,690 --> 00:04:03,200
No worries.

52
00:04:03,320 --> 00:04:05,150
It will be soon filled with employee data.

53
00:04:05,840 --> 00:04:10,670
So with the Hiroko application being created, let's start writing that YAML pipeline code that will

54
00:04:10,670 --> 00:04:15,470
help us to automatically deploy the application to this Heroku platform we do in the next class.
