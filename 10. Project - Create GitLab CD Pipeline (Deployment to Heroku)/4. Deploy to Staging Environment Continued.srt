1
00:00:00,710 --> 00:00:01,670
The answer is no.

2
00:00:02,150 --> 00:00:05,300
You cannot write the hero current command directly in this job.

3
00:00:05,870 --> 00:00:06,110
Why?

4
00:00:06,110 --> 00:00:08,690
Because see this deploy stage?

5
00:00:09,590 --> 00:00:11,780
It uses the base Docker image.

6
00:00:12,230 --> 00:00:15,950
So this instance of job will only understand the Docker commands.

7
00:00:16,190 --> 00:00:21,940
You know, the commands we were running all the way like Docker pull, Docker push and not but to release

8
00:00:21,950 --> 00:00:22,940
a hinako container.

9
00:00:23,150 --> 00:00:27,890
We need to run a Hilco specific command, which again since we are using a Docker image.

10
00:00:28,190 --> 00:00:31,280
So the instance of this job won't understand the Heroku command.

11
00:00:31,640 --> 00:00:35,120
And if you do so, the job will get filled once it reaches that point.

12
00:00:35,930 --> 00:00:36,530
That's OK.

13
00:00:36,740 --> 00:00:37,790
We know the problem now.

14
00:00:37,970 --> 00:00:38,960
But what's the solution?

15
00:00:39,870 --> 00:00:42,680
That's fortunately, there is an easy solution to this problem.

16
00:00:42,920 --> 00:00:44,360
In fact, there are two solutions.

17
00:00:44,840 --> 00:00:48,620
One is you can simply leave this deploy stage job here only.

18
00:00:48,860 --> 00:00:52,730
And just after this job, we can start a new job release date.

19
00:00:53,060 --> 00:00:58,850
As shown in the screenshot, in this job, you can make it able to understand Hiroko specific commands

20
00:00:59,210 --> 00:01:03,440
by first using the base image as a node image and on to that image.

21
00:01:03,690 --> 00:01:08,330
You can add a task to install Utako on it, as shown by doing this.

22
00:01:08,660 --> 00:01:14,390
This job will be able to understand the Heroku commands, and you can easily run the Heroku container

23
00:01:14,390 --> 00:01:15,320
at command on it.

24
00:01:16,250 --> 00:01:22,270
Now, this solution is a valid choice, but it comes with an overhead as just to release the image,

25
00:01:22,280 --> 00:01:24,620
we have to create a new job in our pipeline.

26
00:01:25,280 --> 00:01:28,160
That's why comes the second solution, which we will follow.

27
00:01:29,570 --> 00:01:34,760
Now what if by doing some tricks, we can run the same Heroku command in this job itself?

28
00:01:35,540 --> 00:01:37,430
Indeed, it will make it an easy solution.

29
00:01:37,790 --> 00:01:41,480
And this is what we are going to do for this second approach.

30
00:01:41,810 --> 00:01:47,540
We will leverage from a community contributor Docker image that will help us to accept and run Iroko

31
00:01:47,540 --> 00:01:48,560
commands on the fly.

32
00:01:49,280 --> 00:01:55,040
The image I'm talking about is Alpine, Heroku, UCLA contributed by Wendy Tanaka.

33
00:01:55,040 --> 00:01:56,900
And even let me show you.

34
00:01:58,880 --> 00:01:59,930
Open up the Docker hub.

35
00:02:07,670 --> 00:02:08,810
And search for.

36
00:02:11,990 --> 00:02:13,250
Alpine, Hiroko?

37
00:02:15,080 --> 00:02:15,680
The first one.

38
00:02:19,990 --> 00:02:20,560
Here it is.

39
00:02:21,050 --> 00:02:28,120
See, it has got 100000 downloads, this image packages the Hirokazu UCLA inside an Alpine based Docker

40
00:02:28,120 --> 00:02:28,540
container.

41
00:02:29,020 --> 00:02:32,200
The link to this Docker image is attached in the Resources tab of this lecture.

42
00:02:32,770 --> 00:02:34,330
Let's use it in our pipeline.

43
00:02:36,710 --> 00:02:40,550
Now, how do we use it with the dogged on command which goes like this?

44
00:02:41,030 --> 00:02:45,440
The doctrine command is the command used to build and launch Docker containers.

45
00:02:46,830 --> 00:02:49,770
On command was like this first Docker run.

46
00:02:50,280 --> 00:02:55,530
Then the name of the image to be used for this container and last is a command to overwrite.

47
00:02:56,190 --> 00:03:02,160
Basically, this command first creates a right table container layer over the specified image and then

48
00:03:02,160 --> 00:03:04,290
starts it using the specified command.

49
00:03:05,310 --> 00:03:08,700
Let me show you the practical way of it for our application,

50
00:03:12,390 --> 00:03:13,650
right Docker run.

51
00:03:14,730 --> 00:03:16,710
Then the image name, which is.

52
00:03:18,720 --> 00:03:19,200
When?

53
00:03:21,350 --> 00:03:22,280
R21.

54
00:03:24,580 --> 00:03:25,990
Slash wine.

55
00:03:32,560 --> 00:03:33,130
See a light.

56
00:03:34,300 --> 00:03:40,540
So with this command, Docker will find this image Alpine Hiroko clearly loads up the container and

57
00:03:40,540 --> 00:03:45,970
then around the specified command in that container, and the command is the Heroku command to release

58
00:03:45,970 --> 00:03:47,140
the image, which is.

59
00:03:56,550 --> 00:03:56,970
App.

60
00:04:03,250 --> 00:04:09,010
So basically, when this line of code will run Docker, it behind the scenes will first load up and

61
00:04:09,010 --> 00:04:15,580
launch a container of this Alpine Heroku Docker image and inside that container, it will run this helical

62
00:04:15,580 --> 00:04:22,300
command, which as of now is going to fail because we haven't authenticated ever since.

63
00:04:22,870 --> 00:04:24,220
Let's do that integration as well.

64
00:04:26,810 --> 00:04:30,290
To authenticate, we will pass the option environment variable like this.

65
00:04:33,870 --> 00:04:34,260
E!

66
00:04:50,660 --> 00:04:58,250
Also, I will pass an additional option item that will automatically remove the container when it exits.

67
00:04:59,600 --> 00:05:03,740
So this way, we will be able to release our Heroku application from this deploy stage.

68
00:05:04,860 --> 00:05:11,460
At last, I will add a complementary message that will let us know that our application is deployed

69
00:05:11,490 --> 00:05:12,210
to what address.

70
00:05:13,590 --> 00:05:15,000
It's a simple command.

71
00:05:29,070 --> 00:05:29,430
Great.

72
00:05:30,180 --> 00:05:35,370
So from now on, as soon as they're deployed, job push is done, you're Docker image to Heroku registry.

73
00:05:35,750 --> 00:05:40,530
This release command will run, creating a new release of our application and staging environment.

74
00:05:41,250 --> 00:05:42,450
Everything seems to be good.

75
00:05:43,230 --> 00:05:47,010
Save the pipeline and I will commit it and see you over the pipeline results.

76
00:05:49,130 --> 00:05:49,640
Excellent.

77
00:05:49,790 --> 00:05:51,110
The pipeline was a success.

78
00:05:51,710 --> 00:05:53,510
Let me directly go to the logs.

79
00:06:01,480 --> 00:06:05,440
We have already seen the logs tell this part and last.

80
00:06:06,700 --> 00:06:12,250
After this command, the release was also a success, with the message saying app deployed to staging

81
00:06:12,250 --> 00:06:12,570
server.

82
00:06:13,850 --> 00:06:14,930
That's open the application.

83
00:06:20,010 --> 00:06:20,610
Here we go.

84
00:06:21,420 --> 00:06:24,570
The employer portal application is live in the test environment.

85
00:06:24,840 --> 00:06:25,350
Wonderful.

86
00:06:26,190 --> 00:06:27,720
That's just application being life.

87
00:06:27,840 --> 00:06:33,540
Now you can perform the manual acceptance test to you or the other automated way you can handle this.

88
00:06:33,570 --> 00:06:35,700
Acceptance testing with the pipeline as well.

89
00:06:36,680 --> 00:06:39,320
Anyway, let's try to explore all the application features.

90
00:06:40,380 --> 00:06:41,960
So currently, there is one in play.

91
00:06:42,240 --> 00:06:43,500
Let's try to add one more.

92
00:07:06,760 --> 00:07:07,810
This ad is looking.

93
00:07:08,920 --> 00:07:09,790
Added feature.

94
00:07:19,570 --> 00:07:23,920
It is also looking to delete delete this.

95
00:07:28,530 --> 00:07:30,850
Yes, it is also looking cool.

96
00:07:31,200 --> 00:07:36,390
We have reviewed the application and every feature working correctly, so you can consider that the

97
00:07:36,390 --> 00:07:38,910
manual acceptance testing of the application is passed.

98
00:07:39,630 --> 00:07:43,740
Well, no, I know acceptance testing is not limited to just running the website.

99
00:07:43,980 --> 00:07:50,370
It's a lot more than that, like it can include various factors data integrity, data conversion, performance,

100
00:07:50,370 --> 00:07:53,400
usability, scalability and dozens of other criteria.

101
00:07:53,730 --> 00:07:59,400
But at this stage, I guess this manual of app in staging environment can be considered as that manual

102
00:07:59,400 --> 00:08:02,580
acceptance testing, and we are good to go for the next stage.
