1
00:00:00,670 --> 00:00:05,350
So, guys, we have successfully deployed application in production environment for the first time.

2
00:00:05,890 --> 00:00:09,430
The previous lecture was kind of first live for the application.

3
00:00:10,150 --> 00:00:13,120
Now let us see the magic for what Cassilly is built for.

4
00:00:13,630 --> 00:00:14,470
Don't get excited.

5
00:00:14,740 --> 00:00:15,810
It is not something new.

6
00:00:15,820 --> 00:00:21,490
I'm going to show you have been witnessing this magic all along the course master to complete the project

7
00:00:21,490 --> 00:00:22,090
lifecycle.

8
00:00:22,360 --> 00:00:25,330
I will go for one round of feature additions in the application.

9
00:00:26,140 --> 00:00:29,680
So without wasting any time, let's add a new feature in our application.

10
00:00:32,380 --> 00:00:38,110
Let's say Clint asked us to add a welcome note on the index page of application, what activities need

11
00:00:38,110 --> 00:00:41,440
to be carried out and in what order to add this feature in application.

12
00:00:41,920 --> 00:00:44,070
It is not new for you, obviously.

13
00:00:44,300 --> 00:00:46,630
First, you will check out to a new branch from Master.

14
00:00:56,200 --> 00:01:02,380
Unless you all project files out there now, take this project to the studio.

15
00:01:14,980 --> 00:01:16,060
In the index file.

16
00:01:27,440 --> 00:01:28,580
At a subheading here.

17
00:01:34,470 --> 00:01:36,210
And I welcome to imply portal.

18
00:01:44,580 --> 00:01:48,030
Save the file and commit the changes to remote.

19
00:01:58,630 --> 00:02:01,000
Get Bush while setting the upstream.

20
00:02:23,520 --> 00:02:27,870
A pipeline protest built and deploy the application on staging environment is running.

21
00:02:28,760 --> 00:02:30,960
See, we have it for new feature branch.

22
00:02:32,160 --> 00:02:33,420
Meanwhile, the pipeline is running.

23
00:02:33,810 --> 00:02:37,290
And if you don't want to wait, you can politely create a merger request.

24
00:02:37,770 --> 00:02:41,070
Don't worry, the changes won't get motion muster brought instantly.

25
00:02:41,550 --> 00:02:44,970
Approval will get an option to merge only if the pipeline succeeds.

26
00:02:45,120 --> 00:02:47,790
It will seek to create a merger request for this feature.

27
00:03:10,360 --> 00:03:15,760
After the merger request is created on this approved page, see, while the pipeline is still running,

28
00:03:16,060 --> 00:03:20,380
GitLab is giving us an option to merge this request after the pipeline succeeds.

29
00:03:20,920 --> 00:03:24,560
If you click on this option, it will only do the merge into main branch.

30
00:03:24,790 --> 00:03:28,630
If the running pipeline on feature branch is a success in case it fails.

31
00:03:28,870 --> 00:03:33,790
And don't worry, that change won't be merged into main and your main branch would remain intact in

32
00:03:33,790 --> 00:03:34,450
the same state.

33
00:03:38,790 --> 00:03:44,790
And guys, it depends on majority of the pipeline team to team and use gas to use case on how you want

34
00:03:44,790 --> 00:03:49,640
the mud and production deployment to happen, like in our case for the new feature, which we had.

35
00:03:50,100 --> 00:03:53,670
I have not written any test case to test a new feature in here.

36
00:03:53,910 --> 00:03:58,530
I have very easily allowed too much stuff Richard Branch to main drive if the job succeeds.

37
00:03:58,890 --> 00:04:03,150
But in real world, the approval cannot just go ahead and approve this much request.

38
00:04:03,630 --> 00:04:08,970
He or she would need concrete proofs of this new feature working fine before clicking on this button.

39
00:04:09,780 --> 00:04:14,220
For that, you would definitely need a proper testing team to test out the new feature, along with

40
00:04:14,220 --> 00:04:19,020
a number of other things, like whether or not this new feature broke any existing functionality.

41
00:04:19,410 --> 00:04:21,150
Did it affect the page loading speed?

42
00:04:21,510 --> 00:04:23,370
Is the whole application still intact?

43
00:04:23,820 --> 00:04:24,330
Like this?

44
00:04:24,330 --> 00:04:27,450
A number of other checks to ensure the integrity of application.

45
00:04:28,300 --> 00:04:32,970
Now, if you would attempt to automate this whole testing, then you should be fully confident about

46
00:04:32,970 --> 00:04:33,600
what you are doing.

47
00:04:34,110 --> 00:04:38,130
A pipeline with automated acceptance testing is a highly matured pipeline.

48
00:04:38,160 --> 00:04:39,210
Having its own risks.

49
00:04:39,630 --> 00:04:43,260
A fully automated pipeline means one click, and that changes will be production.

50
00:04:44,070 --> 00:04:46,320
That is why I was mentioning it multiple times.

51
00:04:46,530 --> 00:04:48,870
That acceptance testing is not some trivial thing.

52
00:04:49,290 --> 00:04:53,730
It is a most important part of the whole CASELY workflow going to same risks.

53
00:04:54,060 --> 00:04:57,960
Usually in realtime project deployments don't happen automatically.

54
00:04:58,230 --> 00:04:59,670
Rather, it is triggered manually.

55
00:05:00,090 --> 00:05:03,780
And yes, manually doesn't mean that you have to run some complex commands also.

56
00:05:04,170 --> 00:05:08,880
It's just a click that you have to do after you are fully confident on the testing of a new feature.

57
00:05:09,330 --> 00:05:13,410
I will tell you further in the course how production deployments can be mainly done with a click.

58
00:05:14,720 --> 00:05:16,490
Anyways, coming back to the pipeline.

59
00:05:18,670 --> 00:05:24,730
The pipeline for frigid branch was a success, and as soon as it was a success, the branch was merged

60
00:05:24,730 --> 00:05:30,370
into main branch and pipeline for the main branch also started running out like this mean pipeline to

61
00:05:30,370 --> 00:05:32,830
complete and show you the results later.

62
00:05:34,920 --> 00:05:36,150
Yep, it's complete.

63
00:05:36,750 --> 00:05:38,430
And if I visit the production, you'll.

64
00:05:44,470 --> 00:05:45,280
Congratulations.

65
00:05:45,580 --> 00:05:47,110
We got the new feature in production.

66
00:05:48,520 --> 00:05:53,890
Now again, I would like to add a piece of information here just to let you know the actual way of how

67
00:05:53,890 --> 00:05:55,960
deployments are carried out in real projects.

68
00:05:56,800 --> 00:06:00,880
See in here, you just kind of replaced the whole employee portal in one shot.

69
00:06:01,390 --> 00:06:03,010
But this is not recommended.

70
00:06:03,400 --> 00:06:06,670
Doesn't matter how stronger testing was, it is still not recommended.

71
00:06:07,150 --> 00:06:12,700
With sites like Amazon having millions of customers, you do not know how public is going to react to

72
00:06:12,700 --> 00:06:13,360
this new feature.

73
00:06:13,600 --> 00:06:15,070
Will it be positive or negative?

74
00:06:15,280 --> 00:06:17,710
What consequences can be there with this new feature?

75
00:06:18,010 --> 00:06:24,190
And if you always have that one percent of doubt on testing, therefore, apart from a single shot deployment,

76
00:06:24,490 --> 00:06:25,930
you do deployment in chunks.

77
00:06:26,230 --> 00:06:31,060
You know, like let's test out this feature and roll out only 10 percent of our customer base.

78
00:06:31,480 --> 00:06:36,460
If you get a positive feedback from that 10 percent, then you gradually start rolling out to full audience.

79
00:06:37,060 --> 00:06:42,280
Or you can initially let your company's employees use it and they decide whether to roll it for public

80
00:06:42,280 --> 00:06:42,670
or not.

81
00:06:43,390 --> 00:06:48,010
Or you can deploy it region wise, anything that sort of you know based on this.

82
00:06:48,190 --> 00:06:53,860
There are few deployment strategies defined by community like country deployment, Blue-Green deployment,

83
00:06:54,040 --> 00:06:54,790
AB testing.

84
00:06:55,270 --> 00:06:57,610
How about a lecture on the details of these strategies?

85
00:06:57,610 --> 00:06:59,110
In the bonus section of this course?

86
00:06:59,530 --> 00:07:02,050
You can watch that lecture after this lecture completes.

87
00:07:02,990 --> 00:07:08,750
Anyways, so, guys, you notice the power of Sicily with just one comment of developer, we were able

88
00:07:08,750 --> 00:07:12,740
to bring this change into production without near to zero human intervention.

89
00:07:13,400 --> 00:07:15,410
And with this comes the end of this project.

90
00:07:16,010 --> 00:07:21,680
After this long journey, I believe now you have gained a great knowledge of how quickly preferably

91
00:07:21,680 --> 00:07:24,150
get lapsed easily books wrapping up.

92
00:07:24,170 --> 00:07:29,840
I would again say that this pipeline was just an idea of continuous integration, delivery and deployment.

93
00:07:30,320 --> 00:07:34,280
But depending upon the project needs, this pipeline gets much more complex.

94
00:07:34,590 --> 00:07:37,550
You can add a lot more jobs and stages into a pipeline.

95
00:07:38,060 --> 00:07:43,580
Yet, the idea to initiate the pipeline would remain the same and would follow the same architecture

96
00:07:43,670 --> 00:07:44,780
as we discussed so far.

97
00:07:45,020 --> 00:07:45,470
Thank you.
