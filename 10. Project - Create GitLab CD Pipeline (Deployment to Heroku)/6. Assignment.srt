1
00:00:01,160 --> 00:00:03,110
Here is your next assignment for this course.

2
00:00:03,770 --> 00:00:08,690
So, you know, we are left with the last stage of the pipeline, but just to deploy the application

3
00:00:08,690 --> 00:00:13,040
to production environment, I give you this final task as an assignment.

4
00:00:13,490 --> 00:00:15,470
So get back to what we have done so far.

5
00:00:15,890 --> 00:00:20,250
Revise the previous lectures on how we deployed the application on a staging environment.

6
00:00:20,270 --> 00:00:24,920
We'll start on here out and then try to do the same for production environment as well.

7
00:00:25,520 --> 00:00:31,010
I would recommend not to skip this assignment as this assignment would give you hands on everything

8
00:00:31,010 --> 00:00:36,380
starting from pulling d'Image variables, handling secrets and GitLab and other major topics.

9
00:00:37,100 --> 00:00:42,740
One more thing say please don't copy paste the previous jobs, right to create a job for this task on

10
00:00:42,740 --> 00:00:44,660
your own and come up with their pipeline.

11
00:00:45,440 --> 00:00:49,110
Anyhow, we'll be discussing the solution to this assignment in the next video lecture.

12
00:00:49,670 --> 00:00:50,090
Good luck.
