1
00:00:00,950 --> 00:00:06,800
Now, the current situation of our pipeline so far is that we have managed to build an executable package

2
00:00:07,040 --> 00:00:08,990
that Docker image for our application.

3
00:00:09,560 --> 00:00:15,740
And if I recall this workflow with that built image, we have already completed a continuous integration

4
00:00:15,740 --> 00:00:23,150
part of the pipeline and are ready to transit and start journey into the continuous delivery and deployment

5
00:00:23,150 --> 00:00:23,750
part of it.

6
00:00:24,500 --> 00:00:25,280
So welcome, guys.

7
00:00:25,580 --> 00:00:29,570
Starting from this lecture, we will start our journey with the key part of a pipeline.

8
00:00:30,050 --> 00:00:35,870
And within this city, we will first start with continuous delivery, part in where I will write some

9
00:00:35,870 --> 00:00:40,820
commands or a script in the pipeline to deploy an application was a test environment.

10
00:00:41,480 --> 00:00:45,140
Desert environment would be a perfect place to preview the work in progress.

11
00:00:46,080 --> 00:00:51,930
In this environment, Automator, acceptance testing will be done, thus making us to have a production

12
00:00:51,930 --> 00:00:54,450
ready software available with us every night.

13
00:00:55,800 --> 00:00:58,230
Not before I directly jump into writing the pipeline.

14
00:00:58,620 --> 00:01:05,190
Let me keep my point here for any realtime applications when it comes to deployment, deciding what

15
00:01:05,190 --> 00:01:11,790
service to use for hosting and deployment is very crucial as it is a hosting service that will make

16
00:01:11,790 --> 00:01:14,430
or break the release of new features in your application.

17
00:01:15,030 --> 00:01:20,670
That's why, before directly deploying your application to any random host service, it's a crucial

18
00:01:20,670 --> 00:01:24,330
step to understand the purpose of the application you are going to deploy.

19
00:01:25,370 --> 00:01:30,710
Nice, though, for application deployment in this digital world, there are a lot of deployment options

20
00:01:30,710 --> 00:01:31,400
to choose from.

21
00:01:31,880 --> 00:01:38,930
All options differ from each other, with differentials like costings skills required to deploy infrastructure

22
00:01:38,930 --> 00:01:45,110
requirement, ease of deployment, then type of deployment that is automatic or manual deployment.

23
00:01:45,410 --> 00:01:49,670
And there may be other factors, too taking these factors in mind.

24
00:01:50,030 --> 00:01:53,270
Let us see what are the available options out there for us.

25
00:01:53,870 --> 00:01:56,270
I will only discuss the industrial plan options.

26
00:01:56,930 --> 00:02:02,480
OK, so starting with the first deployment option is to deploy your application on premise environment.

27
00:02:03,460 --> 00:02:10,180
In the previous environment, your company hosts everything in-house means from keeping the I.T. infrastructure

28
00:02:10,210 --> 00:02:12,070
on site to running the solution.

29
00:02:12,310 --> 00:02:17,920
Everything is done internally when compared to other deployment options like cloud service providers.

30
00:02:18,310 --> 00:02:23,860
The on premise will always offer you more flexibility and reliability, and most importantly, is a

31
00:02:23,860 --> 00:02:28,150
security, as every server used is confined within your own walls.

32
00:02:29,020 --> 00:02:32,710
But in here, since you will be the one who is managing the infra.

33
00:02:33,100 --> 00:02:39,040
So the downside of on premise environment is that you have to be at the extra cost associated with it.

34
00:02:39,160 --> 00:02:41,920
For info, management, updates and maintenance.

35
00:02:42,990 --> 00:02:48,750
That is why it is suitable for companies who have reached a certain level of maturity with large capital

36
00:02:48,750 --> 00:02:51,030
expenditure and needs extra security.

37
00:02:51,690 --> 00:02:54,630
This is on premise in general for you now.

38
00:02:54,660 --> 00:03:00,480
If I try to explore the option of on premise environment with context or application, remember we have

39
00:03:00,480 --> 00:03:01,920
a Docker image for our app.

40
00:03:02,340 --> 00:03:08,610
So before we could deploy that Docker image first, we have to do tons of inevitable jobs, like creating

41
00:03:08,610 --> 00:03:13,050
an appropriate environment so that the server could deploy and run Docker Python app.

42
00:03:13,650 --> 00:03:19,170
Also, you need to do a lot of networking, regular backups, software updates and the list will continue.

43
00:03:19,740 --> 00:03:24,060
It clearly indicates that on premise environment would not be the best option for this.

44
00:03:24,930 --> 00:03:26,760
Let's move on to the second deployment option.

45
00:03:27,570 --> 00:03:33,870
The second option you would be happy to know that GitLab out of the box allows you to deploy your application

46
00:03:34,080 --> 00:03:38,040
directly from GitHub repository with that service called GitLab pages.

47
00:03:38,640 --> 00:03:44,940
But there is a catch actually with GitLab pages, it only allows you to publish static websites.

48
00:03:45,360 --> 00:03:49,050
The website with code is compiled before you add it to the repository.

49
00:03:49,500 --> 00:03:56,310
So if you have a static website written directly in place at DML, CSS and JavaScript, then you can

50
00:03:56,310 --> 00:04:00,570
easily host it on your GitLab instance or on GitLab dot com for free.

51
00:04:01,440 --> 00:04:05,620
But GitLab Pages does not support dynamic server side processing.

52
00:04:06,060 --> 00:04:14,270
For instance, what this dot BHP Dot SB requires hence, and also for Python Flask application GitLab

53
00:04:14,280 --> 00:04:15,870
pages is not a valid choice.

54
00:04:17,460 --> 00:04:21,180
The third option is to deploy your application on some cloud service provider.

55
00:04:21,970 --> 00:04:25,590
That's the critical difference between cloud computing and on premise.

56
00:04:25,590 --> 00:04:30,450
Environment is where the data and code resides in on premise environment.

57
00:04:30,780 --> 00:04:36,360
Your company hosts everything in-house on your own servers while in cloud environment.

58
00:04:36,870 --> 00:04:43,860
A third party provider hosts all that for you, and everything it hosted is accessed via a web browser

59
00:04:43,860 --> 00:04:44,610
or APIs.

60
00:04:45,950 --> 00:04:51,680
Cloud computing is the On-Demand availability of computer system resources, especially data storage

61
00:04:51,680 --> 00:04:56,120
and computing power, without direct active management by the user.

62
00:04:56,960 --> 00:05:02,690
With cloud computing subscription model, there is no need to purchase any additional licenses or infra.

63
00:05:03,140 --> 00:05:08,540
Instead, everything is already configured for you and you're able to deploy replication on the cloud

64
00:05:08,540 --> 00:05:09,080
right away.

65
00:05:10,080 --> 00:05:15,840
For example, in our case, if you use any cloud provider, you don't have to worry about installing

66
00:05:15,840 --> 00:05:21,600
Docker, Python or whatever this application will require, the cloud provider has it covered for you,

67
00:05:22,320 --> 00:05:28,800
so you can see it is the most easy to use managed, scalable and cost effective deployment solution.

68
00:05:29,040 --> 00:05:30,840
And I'm also going to use the same.

69
00:05:31,440 --> 00:05:34,650
But again, the question is which cloud platform to choose?

70
00:05:35,790 --> 00:05:41,670
There are options in the market, with famous ones being the Amazon Web Services, NWS, Google Cloud

71
00:05:41,670 --> 00:05:48,990
Platform, GCP, Microsoft Azure, Iroko, Digital Ocean and others for this application deployment.

72
00:05:49,350 --> 00:05:54,090
I have chosen Heroku as a cloud service provider for a few reasons why.

73
00:05:54,300 --> 00:05:55,470
And what are those reasons?

74
00:05:55,890 --> 00:05:57,390
Let's discuss it in the next lecture.
