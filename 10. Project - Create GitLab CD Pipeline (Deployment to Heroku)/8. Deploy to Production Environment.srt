1
00:00:00,750 --> 00:00:05,640
I hope you all have prepared the pipeline and were successful in deploying the application to production.

2
00:00:06,060 --> 00:00:07,260
If not, let's do it.

3
00:00:08,160 --> 00:00:11,190
It will be more or less same as staging environment deployment only.

4
00:00:11,400 --> 00:00:14,280
It's just a few variables and environment will get change.

5
00:00:15,150 --> 00:00:21,120
OK, so before we start writing the job, at present, we have only one environment available with us

6
00:00:21,120 --> 00:00:21,760
in how to go.

7
00:00:23,220 --> 00:00:26,880
That is this important staging application for production.

8
00:00:27,090 --> 00:00:28,770
We would need a separate application.

9
00:00:29,130 --> 00:00:33,030
So let's create that first in Heroku dashboard.

10
00:00:33,210 --> 00:00:33,960
Create a new app.

11
00:00:37,840 --> 00:00:41,740
APT is a potent.

12
00:00:45,420 --> 00:00:45,930
Production.

13
00:00:47,080 --> 00:00:48,040
It's available.

14
00:00:54,370 --> 00:00:56,620
Apple's creator, it's as fresh as a Daisy.

15
00:00:57,790 --> 00:01:01,980
Now head back to webmail file to save us a little time.

16
00:01:01,990 --> 00:01:04,360
I would copy this job.

17
00:01:06,130 --> 00:01:07,390
And basic to production.

18
00:01:14,530 --> 00:01:16,150
Now, make the relevant edits in it.

19
00:01:17,020 --> 00:01:19,780
The first change would obviously be the job name.

20
00:01:23,250 --> 00:01:26,140
It could be deploy production.

21
00:01:27,210 --> 00:01:33,240
This job would require a Docker image, so no change in image Docker then services remains the same.

22
00:01:34,920 --> 00:01:38,070
The stage I will give it deploy production.

23
00:01:44,920 --> 00:01:46,240
Also added in the stages.

24
00:02:03,190 --> 00:02:08,470
Now for production as well, we need to first pull the image from get registry and then upload it to

25
00:02:08,680 --> 00:02:11,650
a local container district by tagging it with naming convention.

26
00:02:12,400 --> 00:02:13,030
So here.

27
00:02:14,140 --> 00:02:14,890
Police seem.

28
00:02:16,300 --> 00:02:17,590
Target with a different name.

29
00:02:20,750 --> 00:02:22,270
You know, coal production.

30
00:02:26,290 --> 00:02:27,400
Create a variable of a.

31
00:02:45,020 --> 00:02:45,730
Production at.

32
00:03:04,020 --> 00:03:05,970
The whole application in for production.

33
00:03:13,110 --> 00:03:18,000
Now, this time with the Heroku production key, its creator Newquay, in Enjoy.

34
00:04:05,510 --> 00:04:07,070
This game, ABC Paramount.

35
00:04:07,280 --> 00:04:09,350
But still to stick with the standard approach.

36
00:04:09,560 --> 00:04:13,190
I have to create a different variable for it and as will be seen.

37
00:04:19,450 --> 00:04:24,940
Dr push to production and also change this around command accordingly.

38
00:04:39,270 --> 00:04:40,710
Change the Eagle commander as well.

39
00:04:48,650 --> 00:04:48,960
Done.

40
00:04:49,670 --> 00:04:55,370
Now there's no doubt with this our pipeline is defined, and it was always our purpose to automatically

41
00:04:55,370 --> 00:04:57,440
build, test and deploy the application.

42
00:04:58,040 --> 00:05:00,260
But still, there is one problem in this pipeline.

43
00:05:00,890 --> 00:05:05,240
Think over it and I will introduce you with the problem and its solution in the next lesson.

44
00:05:06,110 --> 00:05:06,800
Just a hint.

45
00:05:07,340 --> 00:05:12,470
Problem lies in how the pipeline is deploying our application and production environment with the current

46
00:05:12,470 --> 00:05:13,340
version of this quarter.
