1
00:00:00,480 --> 00:00:06,330
In previous video, what we did was manual testing of application, but the other way, you can also

2
00:00:06,330 --> 00:00:11,670
induce automation for acceptance testing by writing some test scripts in the pipeline itself.

3
00:00:11,970 --> 00:00:14,580
And that is exactly what I will be doing in this lesson.

4
00:00:15,840 --> 00:00:19,350
That's just an affront, not this automatic testing job.

5
00:00:19,440 --> 00:00:21,510
I'm including for learning purposes only.

6
00:00:21,720 --> 00:00:26,910
You know, just for the sake of including it to mimic all the steps in a real world pipeline.

7
00:00:27,450 --> 00:00:32,190
That is why I would be including a very simple test case as part of automated testing.

8
00:00:33,060 --> 00:00:37,800
So create a job name best underscored stage.

9
00:00:39,450 --> 00:00:42,300
The idea of this acceptance testing would be very simple.

10
00:00:42,900 --> 00:00:48,690
Basically, from this job, we will send a request to the staging you all and will search for the string

11
00:00:48,870 --> 00:00:50,610
employed data in that response.

12
00:00:51,360 --> 00:00:53,130
Well, this best suits our need.

13
00:00:53,370 --> 00:00:58,530
As if there is no bug in the application, then we should definitely get the string employed later in

14
00:00:58,530 --> 00:00:59,520
the response, right?

15
00:01:00,270 --> 00:01:04,770
Again, this is a very simple test case, I know, but for learning purposes, this would suffice.

16
00:01:08,260 --> 00:01:09,670
The image it uses.

17
00:01:12,500 --> 00:01:18,310
Alpine, which is a minimal Docker image based on Alpine Linux, but size of only five.

18
00:01:19,970 --> 00:01:21,440
Next comes the stage.

19
00:01:25,000 --> 00:01:28,630
Stage would be automated.

20
00:01:30,070 --> 00:01:35,440
Automated testing also added above in stages.

21
00:01:47,300 --> 00:01:49,180
Now comes the script tag.

22
00:01:52,260 --> 00:01:58,260
Under the script, right document that would hit the staging Ural and check if the string employed here

23
00:01:58,410 --> 00:02:04,500
is present or not, for that, I will use Girl Command Girl, you all may know is a command line tool

24
00:02:04,860 --> 00:02:07,620
to transfer data to or from a server.

25
00:02:09,550 --> 00:02:13,480
So call and then pass the Web address to hit.

26
00:02:15,000 --> 00:02:15,660
Look, Poppy, this.

27
00:02:20,680 --> 00:02:28,720
And then pipe pipe lets you use two or more commands in a manner that output of one command serves as

28
00:02:28,720 --> 00:02:29,620
input to the next.

29
00:02:30,310 --> 00:02:34,990
So here the output of this call command would become the input to the grab command.

30
00:02:35,020 --> 00:02:38,200
We are going to right next to so I will use grep.

31
00:02:41,890 --> 00:02:47,560
The grep filter is a command line utility for searching particular patterns of correctness and displays

32
00:02:47,680 --> 00:02:49,540
all lines that contain that pattern.

33
00:02:51,640 --> 00:02:54,760
Grape and pasta string to be matched.

34
00:02:57,030 --> 00:02:58,710
Employ data.

35
00:03:00,780 --> 00:03:06,120
So with this command, if there is a string employed, it's in the response data grab would be a success.

36
00:03:06,270 --> 00:03:07,350
Otherwise, it would treat.

37
00:03:09,000 --> 00:03:12,960
And as this cold command will not be recognized in Alpine.

38
00:03:13,380 --> 00:03:16,620
So we need to explicitly added with this command.

39
00:03:18,150 --> 00:03:19,560
I will, I didn't before script.

40
00:03:31,150 --> 00:03:32,230
He began cash.

41
00:03:34,370 --> 00:03:40,580
Article so with this small piece of code, our automated acceptance testing is defined.

42
00:03:41,240 --> 00:03:44,450
Let's save it, commit and push the changes to see the results.

43
00:04:17,310 --> 00:04:18,270
My plan is success.

44
00:04:19,890 --> 00:04:20,880
Going through the logs.

45
00:04:26,440 --> 00:04:28,390
First thing, it installed the coal.

46
00:04:29,620 --> 00:04:31,990
And then the test command is run.

47
00:04:33,700 --> 00:04:34,270
So.

48
00:04:35,490 --> 00:04:40,950
So I had this address, and so it's for string embroidered on it, as does was present in there.

49
00:04:41,130 --> 00:04:45,090
So it returned the matching string and this job was marked as success.

50
00:04:45,810 --> 00:04:50,790
But in case there would have been some problem in Europe and this employed it, a string would not be

51
00:04:50,790 --> 00:04:55,140
there, then this job would have failed, resulting to hold application release to production.

52
00:04:55,800 --> 00:04:57,930
So yeah, we are going good with the project.

53
00:04:58,290 --> 00:05:03,540
But this acceptance testing done, I guess we are all set to move into the next and the final stage

54
00:05:03,540 --> 00:05:04,620
of the pipeline.

55
00:05:05,100 --> 00:05:10,380
The continuous deployment, but where we will write the jobs to automatically deploy the application

56
00:05:10,380 --> 00:05:10,890
for production.

57
00:05:11,550 --> 00:05:11,940
Thank you.
