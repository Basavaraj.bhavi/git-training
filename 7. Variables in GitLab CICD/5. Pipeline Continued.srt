1
00:00:01,000 --> 00:00:02,230
Home up to the YAML file.

2
00:00:03,580 --> 00:00:04,960
And use variables here.

3
00:00:17,150 --> 00:00:21,620
This way, when this line of code will run, it will log us in to the Docker hub account.

4
00:00:22,430 --> 00:00:26,870
Once we are logged in, the next task we will do is push it up by AMP image to Docker hub.

5
00:00:27,620 --> 00:00:31,760
But to push an image to Docker hub first, we have to tag the image.

6
00:00:32,360 --> 00:00:38,120
Docker follows a naming convention who identify unofficial images as we are creating our own image.

7
00:00:38,390 --> 00:00:40,940
So we need to rename it by following this index.

8
00:00:42,240 --> 00:00:47,400
So our next command to run should be Docker tag.

9
00:00:48,780 --> 00:00:52,230
And then the existing name Bieb.

10
00:00:53,590 --> 00:00:55,630
It's D&amp;G latest.

11
00:00:57,070 --> 00:01:00,970
Then specify that the username, which we already stored as a variable.

12
00:01:03,280 --> 00:01:07,150
Username slash rapper name.

13
00:01:08,650 --> 00:01:10,540
And the Togbe latest.

14
00:01:11,910 --> 00:01:14,930
If the dog is not specified, it defaults to latest only.

15
00:01:16,140 --> 00:01:20,730
OK, so once the images bagged, we can push it to Docker hub with Docker push command.

16
00:01:21,390 --> 00:01:21,750
So.

17
00:01:24,820 --> 00:01:27,820
Dr. Push, but the new technique.

18
00:01:32,300 --> 00:01:32,810
That's it.

19
00:01:32,990 --> 00:01:36,830
But this my pipe image will be pushed to my local repository.

20
00:01:37,980 --> 00:01:41,880
And yeah, at last, add the tags that are tagged with the local unit.

21
00:01:50,480 --> 00:01:51,560
Now before I commit.

22
00:01:51,740 --> 00:01:53,480
Let me check for you pre-requisites.

23
00:01:56,030 --> 00:01:58,370
First, let's check if our local runner is running.

24
00:02:08,170 --> 00:02:09,130
Yeah, it's running.

25
00:02:09,700 --> 00:02:11,590
And then check if Docker is running.

26
00:02:18,260 --> 00:02:20,230
It is also running now, go back to.

27
00:02:22,580 --> 00:02:23,720
And commit 13 years.

28
00:02:36,750 --> 00:02:37,650
The pipeline bust.

29
00:02:45,140 --> 00:02:48,410
See it, try to log in, and it succeeded.

30
00:02:49,610 --> 00:02:50,450
And in here.

31
00:02:51,380 --> 00:02:53,750
It pushed our image to become.

32
00:02:55,790 --> 00:02:57,440
And the final job was a success.

33
00:02:58,400 --> 00:03:00,140
Now if I go to Docker hub.

34
00:03:01,850 --> 00:03:02,720
And refresh.

35
00:03:09,610 --> 00:03:11,210
Excellent image.

36
00:03:11,230 --> 00:03:12,100
My app is hit.

37
00:03:13,330 --> 00:03:13,780
All right.

38
00:03:14,170 --> 00:03:17,800
So, guys, that was how you can create and use variables in GitLab.

39
00:03:18,250 --> 00:03:19,300
That's it for this lecture.

40
00:03:19,540 --> 00:03:20,830
See you guys in the next class.

41
00:03:21,010 --> 00:03:21,570
Keep learning.
