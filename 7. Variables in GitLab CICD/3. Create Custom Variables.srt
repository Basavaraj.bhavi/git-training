1
00:00:01,070 --> 00:00:07,580
For specific needs, we have custom Kayseri variables to set up custom variables, noise again, like

2
00:00:07,580 --> 00:00:09,380
I said, you have multiple options.

3
00:00:09,830 --> 00:00:17,030
Either way, you can directly set them up and get Ml fight for general reusability purposes or for any

4
00:00:17,030 --> 00:00:21,530
sensitive information like passwords that you don't want to expose in a real fight.

5
00:00:21,950 --> 00:00:25,070
Then you can also set them in project settings from Get You.

6
00:00:25,970 --> 00:00:29,450
First, let's see how we can declare them directly in Yemen to fight.

7
00:00:31,130 --> 00:00:32,310
I will add it to.

8
00:00:35,420 --> 00:00:36,200
Removed this.

9
00:00:37,920 --> 00:00:41,250
To create variables, type variables.

10
00:00:42,860 --> 00:00:48,260
And then in the next line, defining or variable as development, let's define two variables here.

11
00:00:49,250 --> 00:00:52,230
First is the name and the value it would hold me.

12
00:00:53,210 --> 00:00:53,660
John.

13
00:00:55,710 --> 00:00:59,460
And a new variable message.

14
00:01:03,260 --> 00:01:04,940
But these are variables are defined.

15
00:01:05,420 --> 00:01:08,630
And to use them create a new job.

16
00:01:13,910 --> 00:01:15,140
Under another script tag.

17
00:01:16,310 --> 00:01:17,960
I will simply echo the message.

18
00:01:26,050 --> 00:01:27,790
Broader message.

19
00:01:28,900 --> 00:01:31,720
So an execution this job will print out a message.

20
00:01:32,020 --> 00:01:32,750
Hello, John.

21
00:01:32,770 --> 00:01:35,140
How are you commit the changes?

22
00:01:54,180 --> 00:01:54,750
Completed.

23
00:01:59,410 --> 00:01:59,980
Excellent.

24
00:02:00,850 --> 00:02:05,230
It has printers theme expected message, adding a little more into this.

25
00:02:06,970 --> 00:02:12,970
Guys, these variables are defined globally at pipeline level, which means all the number of jobs you

26
00:02:12,970 --> 00:02:13,960
write in this pipeline.

27
00:02:14,350 --> 00:02:15,310
These two variables.

28
00:02:15,550 --> 00:02:19,960
Name and message will hold the value, John and how are you respectively?

29
00:02:20,800 --> 00:02:21,340
And no.

30
00:02:25,820 --> 00:02:30,080
If I create a new variable name here inside the job itself.

31
00:02:37,000 --> 00:02:37,990
With the same name.

32
00:02:38,860 --> 00:02:39,850
What would you expect?

33
00:02:40,660 --> 00:02:44,560
Well, this new variable will override the value, John with value mark.

34
00:02:45,190 --> 00:02:48,010
This is just as same as with any programming language.

35
00:02:48,040 --> 00:02:54,130
Nothing new variables defined within a particular job will override the values of the theme globally

36
00:02:54,130 --> 00:02:54,880
defined variables.

37
00:02:55,450 --> 00:03:01,240
So for this display method job, the name will get resolved to mark, but for the rest of the jobs in

38
00:03:01,240 --> 00:03:01,830
this pipeline.

39
00:03:02,200 --> 00:03:04,150
Name variable will result to John.

40
00:03:05,140 --> 00:03:05,920
Climate change is.

41
00:03:22,630 --> 00:03:23,020
Great.

42
00:03:23,710 --> 00:03:25,000
This time it it are done.

43
00:03:25,120 --> 00:03:26,260
Hello, Mark, how are you?

44
00:03:26,740 --> 00:03:27,430
Easy, right?

45
00:03:28,560 --> 00:03:33,170
So this was how you can create and define custom variables inside Yemen fight.

46
00:03:34,580 --> 00:03:40,310
While exposing its value right off in the fight, the use case for storing such variables is when you

47
00:03:40,310 --> 00:03:42,250
need to define something in a pipeline.

48
00:03:42,300 --> 00:03:43,610
He repeatedly, again and again.

49
00:03:43,940 --> 00:03:49,130
For example, if you have some yourself that you need to use multiple times in the whole pipeline,

50
00:03:49,640 --> 00:03:55,550
then apart from writing the theme Your value again and again, what you can do is just create a variable

51
00:03:55,550 --> 00:03:58,910
for that URL and use the variables where it is needed.

52
00:03:59,570 --> 00:04:02,600
This way, it also helps to minimize any typo in its.

53
00:04:03,900 --> 00:04:04,590
This is OK.

54
00:04:04,830 --> 00:04:10,860
But in case if you have any sensitive information like passwords, some tokens, then you will hesitate

55
00:04:10,860 --> 00:04:17,160
defining those variables in this file as anyone having access to this file can see that sensitive information.

56
00:04:17,790 --> 00:04:23,310
So for those kind of sensitive information, GitLab offers us another way of storing variables, which

57
00:04:23,310 --> 00:04:25,680
is inside the product settings of a project.

58
00:04:26,220 --> 00:04:28,620
Let's see how exactly to do so in next lecture.
