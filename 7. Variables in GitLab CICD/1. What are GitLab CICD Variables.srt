1
00:00:00,550 --> 00:00:07,690
Let us begin in U.S. variables and get lapses in this action, we will learn what target lapses variables,

2
00:00:08,050 --> 00:00:10,150
how to create and use them in a pipeline.

3
00:00:10,690 --> 00:00:15,730
I'm sure most of you must have some sort of programming background and you all would be aware of what

4
00:00:15,730 --> 00:00:16,420
variables are.

5
00:00:16,690 --> 00:00:18,340
So it doesn't require much explanation.

6
00:00:19,030 --> 00:00:25,510
Variables is a value that can change depending on conditions or an information that is passed in one

7
00:00:25,510 --> 00:00:26,010
gender line.

8
00:00:26,020 --> 00:00:31,990
If I have to say variables or information that can be retrieved anywhere in the code where the information

9
00:00:31,990 --> 00:00:34,780
could be anything a string, a number or whatever.

10
00:00:35,840 --> 00:00:41,720
Usually in projects, you use variables to store secured entities like passwords, broken keys, secret

11
00:00:41,720 --> 00:00:46,150
keys that you would use in a job script but don't want to expose in the code.

12
00:00:47,490 --> 00:00:54,000
Or you also use variables for reusability, whether entities like database connections, your Alpert's

13
00:00:54,300 --> 00:00:57,060
or any long strings are defined in some variables.

14
00:00:57,450 --> 00:01:03,240
And then instead of riding those long strings again and again, you just specify that variable defined

15
00:01:03,240 --> 00:01:07,770
flooded variables help in maintaining the consistency of the code.

16
00:01:08,290 --> 00:01:14,550
How, let's say, in the whole code, you have used a URL 100 times and now there is a need to change

17
00:01:14,550 --> 00:01:15,060
the URL.

18
00:01:15,600 --> 00:01:21,900
In this case, if you were to have hardcoded a URL at 100 places, then you have to visit those places

19
00:01:22,110 --> 00:01:23,850
and change the URL one by one.

20
00:01:24,510 --> 00:01:27,960
And what if you miss to update the URL at one or two places?

21
00:01:28,590 --> 00:01:32,400
Also, there can be typos while you make changes at those 100 places.

22
00:01:32,880 --> 00:01:35,670
This all can lead to an inconsistent code, right?

23
00:01:36,850 --> 00:01:43,210
But if the same URL would have been defined inside a variable, then all you need to do is just change.

24
00:01:43,210 --> 00:01:49,900
The variable value and arbitrary URL will be automatically reflected to those places, and chances of

25
00:01:49,900 --> 00:01:51,700
typo errors are also minimized.

26
00:01:53,590 --> 00:02:01,060
As the variables are part of the environment in which pipeline and job runs, so 63 variables very useful

27
00:02:01,060 --> 00:02:03,910
to customize pipelines based on their environment.

28
00:02:05,380 --> 00:02:10,960
Next, if I talk specifically to GitLab to define and use variables and GitLab pipeline, there are

29
00:02:10,960 --> 00:02:11,480
two ways.

30
00:02:12,070 --> 00:02:18,070
Either way, you can use from the predefined set of available GitLab zero variables or the other way

31
00:02:18,430 --> 00:02:19,690
for your specific needs.

32
00:02:19,870 --> 00:02:22,180
You can also custom create your own variables.

33
00:02:22,930 --> 00:02:29,380
Again, the custom variables can be defined in many ways, like in a project get lepeska via mail file

34
00:02:29,620 --> 00:02:31,960
in project settings or with API.

35
00:02:32,680 --> 00:02:38,320
Some of them can be used for all GitLab Kayseri features, but some of them are more or less limited

36
00:02:38,890 --> 00:02:40,180
further in the coming lectures.

37
00:02:40,240 --> 00:02:42,520
I'm going to demonstrate most of these variables.

38
00:02:43,480 --> 00:02:46,620
Last variables in GitLab are defined as value.

39
00:02:47,290 --> 00:02:52,120
For example, this token is a variable holding one two three value in it.

40
00:02:53,320 --> 00:02:56,800
So, guys, these are a few important points for the variables.

41
00:02:57,120 --> 00:02:58,630
Now let's create three of these.
