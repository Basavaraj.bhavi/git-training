1
00:00:00,420 --> 00:00:05,460
To understand those secret variables, I will leverage that Docker image that we created in that in

2
00:00:05,460 --> 00:00:05,970
this lecture.

3
00:00:06,390 --> 00:00:12,870
Remember, in that lecture, we had created this Bieb Docker image from Docker fight and also stored

4
00:00:12,870 --> 00:00:13,890
it and that local system.

5
00:00:15,000 --> 00:00:21,060
Now what I will do is in this lecture, I will push this by image to Docker hub and make it available

6
00:00:21,060 --> 00:00:21,990
for use to public.

7
00:00:23,120 --> 00:00:24,810
Now to upload image to Docker hub.

8
00:00:25,040 --> 00:00:27,590
Obviously, we would need an account that Docker hub.

9
00:00:32,470 --> 00:00:33,280
I already have.

10
00:00:33,430 --> 00:00:36,820
And if you don't, then sign up for the up is pretty simple.

11
00:00:37,300 --> 00:00:39,790
Just fill these details and your account will be created.

12
00:00:40,920 --> 00:00:41,580
Let me logon.

13
00:00:56,540 --> 00:00:57,950
If I go to the repositories.

14
00:00:59,280 --> 00:01:01,230
Currently, I have no repository available.

15
00:01:01,680 --> 00:01:03,240
Well, we will have one in a moment.

16
00:01:03,540 --> 00:01:04,170
Let's start.

17
00:01:07,960 --> 00:01:09,160
And the same project.

18
00:01:10,160 --> 00:01:11,120
Let me clear this.

19
00:01:13,160 --> 00:01:21,530
I don't want to create one on one project, so I have this digital Yemen now create a job push image.

20
00:01:22,040 --> 00:01:28,620
Now if you see it from a very high level, what steps could be there to push an image to a first step?

21
00:01:28,640 --> 00:01:34,220
You have to log into Docker hub and then a second step, some sort of upload or push command could be

22
00:01:34,220 --> 00:01:34,580
there.

23
00:01:34,610 --> 00:01:34,940
Right?

24
00:01:35,390 --> 00:01:39,170
I'm going to write something like that in this job script.

25
00:01:41,380 --> 00:01:45,370
First of all, logging into Docker from like we have a Docker command.

26
00:01:47,020 --> 00:01:47,890
Local logging.

27
00:01:50,570 --> 00:01:57,680
You option to provide DR account username and B option is to provide the password for that user account.

28
00:01:58,310 --> 00:02:04,670
And how to provide the values to both these options is exactly where variables come into picture writing

29
00:02:04,670 --> 00:02:10,760
and exposing the sensitive information of username and password in the pipeline itself could be disastrous

30
00:02:10,880 --> 00:02:12,500
for the obvious security reasons.

31
00:02:13,010 --> 00:02:16,190
That is why I will take advantage of variables to specify them.

32
00:02:16,970 --> 00:02:22,820
GitLab offers us a convenient way to create and define sensitive variables in project settings.

33
00:02:23,980 --> 00:02:24,150
Of.

34
00:02:29,490 --> 00:02:31,890
Here we have this variable section expand.

35
00:02:34,350 --> 00:02:35,880
To create a new custom variable.

36
00:02:36,420 --> 00:02:38,010
Click on Add Variable Button.

37
00:02:39,360 --> 00:02:45,380
In this dialogue box, we will create a new variable, a coconut based only project members with them

38
00:02:45,410 --> 00:02:48,390
in role can add or update products, basically variables.

39
00:02:49,600 --> 00:02:52,390
Now, every variable would be of key value pair type.

40
00:02:52,990 --> 00:02:54,910
Let's first grade for Dzokhar username.

41
00:02:55,960 --> 00:02:57,920
Let that Gib username.

42
00:02:59,970 --> 00:03:06,060
Note that a key must be one line with no spaces using only letters, numbers or underscored.

43
00:03:07,580 --> 00:03:12,530
And for the value, there are no limitations, its value would be my actual username on Docker.

44
00:03:16,500 --> 00:03:18,300
Next comes the type of variables.

45
00:03:19,570 --> 00:03:27,160
Guys in GitLab, there are two types of variables fight or variable variables of type variable are simply

46
00:03:27,160 --> 00:03:27,940
give Alipius.

47
00:03:29,050 --> 00:03:37,270
A typical variable of variable type consist of two things a key and its value all the predefined variables

48
00:03:37,270 --> 00:03:38,620
that I showed you previously.

49
00:03:39,070 --> 00:03:43,090
And the variables defined in GitLab got variable VI are of variable type.

50
00:03:43,600 --> 00:03:50,770
But on the other hand, the second type file type variables are used for tools that need a file as input.

51
00:03:51,370 --> 00:03:58,150
And these type of variables consist of a key value and a fight in file type variables.

52
00:03:58,450 --> 00:04:02,380
You don't really pass the actual value of the key in the value section.

53
00:04:02,740 --> 00:04:08,920
Rather, you store that value in a fight and then pass the location of that file in the value column.

54
00:04:09,840 --> 00:04:14,310
You know, by doing this file type variable gives you an extra layer of security.

55
00:04:15,000 --> 00:04:17,160
This vital variable is not some new feature.

56
00:04:17,490 --> 00:04:23,430
There are other tools like UCLA groups ideal that use fighter variables for configuration.

57
00:04:24,490 --> 00:04:28,030
For this use, the main variable, I'm going to keep it as variable only.

58
00:04:28,720 --> 00:04:34,570
Then this optional environmental scoop, I will keep it to default all this environment thing we will

59
00:04:34,570 --> 00:04:35,110
see later.

60
00:04:36,890 --> 00:04:42,590
Product variable, though, this is an optional field, but it is quite important, nice by checking

61
00:04:42,590 --> 00:04:46,670
this checkbox, you're applying a condition on the variable that is being created.

62
00:04:47,150 --> 00:04:53,260
The condition is that the variable should be only available in pipelines that run on protected branches.

63
00:04:53,270 --> 00:04:59,150
Ortex protected branches like the Mean or master branch of a project which is protected by default.

64
00:04:59,780 --> 00:05:05,240
Like in this case, if I check this option, then by default using this variable, I could only login

65
00:05:05,240 --> 00:05:06,860
into Docker from the main branch.

66
00:05:07,280 --> 00:05:12,950
But if I create some feature branch and then try to run a pipeline for it, this variable will become

67
00:05:12,950 --> 00:05:15,890
ineffective and the log in to Docker would be unsuccessful.

68
00:05:16,190 --> 00:05:19,580
As a future branch pipeline won't be doing my Docker username.

69
00:05:21,220 --> 00:05:26,890
The lastest mosque variable is masking a variable heights of variable value in job logs.

70
00:05:27,430 --> 00:05:30,100
This is also a good option who covers from all sides.

71
00:05:30,640 --> 00:05:37,450
If I don't mask this variable user name and in the pipeline, I wrote an ego command like equal username.

72
00:05:37,930 --> 00:05:41,950
Then, after execution, the value of this username will be shown in the logs.

73
00:05:42,340 --> 00:05:48,010
But by masking, the value won't get displayed in logs, thus giving the variable another level of security.

74
00:05:48,820 --> 00:05:54,430
But yeah, to Moscow variable, the variable value should be a single line, eight or more correct long.

75
00:05:55,090 --> 00:05:56,440
Now I will add this variable.

76
00:06:00,150 --> 00:06:03,270
So the variable user name is created and is available to use.

77
00:06:03,900 --> 00:06:09,360
Similarly, let's create another variable password to store the password associated with account.

78
00:06:22,030 --> 00:06:22,350
Good.

79
00:06:22,660 --> 00:06:25,750
We have our username and password stored in variables.

80
00:06:26,140 --> 00:06:28,540
Let's continue writing the YAML file and next lecture.
