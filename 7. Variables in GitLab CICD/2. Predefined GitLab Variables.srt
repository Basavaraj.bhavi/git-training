1
00:00:00,840 --> 00:00:03,560
I will first start with a predefined zero variables.

2
00:00:04,230 --> 00:00:10,260
Get Lepeska City offers you a default set of predefined category variables that you can use without

3
00:00:10,260 --> 00:00:11,640
any additional specification.

4
00:00:12,210 --> 00:00:17,250
The predefined variables are available in every GitLab pipeline you have.

5
00:00:17,760 --> 00:00:23,610
You can simply call those variables by their names and can get the useful GitLab backend details like

6
00:00:23,610 --> 00:00:30,210
usernames, branch names, pipeline communities and much more to get the complete list of available

7
00:00:30,210 --> 00:00:31,200
predefined variables.

8
00:00:31,350 --> 00:00:32,820
We have this link from GitLab.

9
00:00:33,630 --> 00:00:39,510
This link is also attached in the Resources tab of this lecture, so you can see we have a long list

10
00:00:39,510 --> 00:00:46,500
of available variables like we have a variable K commit message that stores the commit message you would

11
00:00:46,500 --> 00:00:48,240
have passed while committing a branch.

12
00:00:48,660 --> 00:00:54,240
And on the right side, if you see is the minimum GitLab and get LeBron Norwegians that this variable

13
00:00:54,240 --> 00:00:56,610
is compatible with next.

14
00:00:57,270 --> 00:01:02,190
Is this a commit underscore that it gives you the long commit revision number.

15
00:01:02,940 --> 00:01:09,250
Then is CAA commit shorter sedghi, which gives you the first eight characters of CAA, commit, etc.

16
00:01:09,690 --> 00:01:14,370
That is, the first eight characters of commit revision number seem like this.

17
00:01:14,370 --> 00:01:19,140
We have other variables, which comes handy while you are writing in a pipeline in your project.

18
00:01:19,860 --> 00:01:25,860
For example, this CAA commit message variable can be used as a tag for a Docker image to build.

19
00:01:26,610 --> 00:01:33,510
Caches can be defined with CAA commit flag so that the jobs of each branch always use the same cache.

20
00:01:33,900 --> 00:01:34,860
And what is this cache?

21
00:01:35,070 --> 00:01:37,830
It is explained further in the course anyways.

22
00:01:38,340 --> 00:01:41,040
Now let us open or get loveseat or YAML file.

23
00:01:44,300 --> 00:01:46,430
And we will see how to use them in a biplane.

24
00:01:53,190 --> 00:01:58,560
Create a new job, let's say, no job, and under the script tag.

25
00:02:01,150 --> 00:02:03,700
I will point to things using predefined variables.

26
00:02:04,450 --> 00:02:08,530
First, let's print out the commit message that I'm going to provide for this comment.

27
00:02:09,820 --> 00:02:13,070
So equal dollar.

28
00:02:13,840 --> 00:02:19,090
Don't forget to use this dollar sign before you go to variable name and then the variable name, which

29
00:02:19,090 --> 00:02:24,610
is see a commit message.

30
00:02:26,230 --> 00:02:31,150
And the next thing I would like to go out is the job name under which this variable is getting used.

31
00:02:34,100 --> 00:02:37,190
For that, we have a variable S.A. underscore the job name.

32
00:02:39,840 --> 00:02:43,740
This variable value, the output demo job as it is under demo job.

33
00:02:44,740 --> 00:02:44,990
Done.

34
00:02:45,790 --> 00:02:46,330
Committed.

35
00:02:49,130 --> 00:02:50,390
Let the comment be.

36
00:03:03,210 --> 00:03:04,380
What was seized by plane?

37
00:03:12,780 --> 00:03:13,590
Let it complete.

38
00:03:16,220 --> 00:03:16,970
What logs?

39
00:03:19,820 --> 00:03:25,250
In the terminal, as you can see now, variables they commit, message has output, the commit message

40
00:03:25,280 --> 00:03:26,780
that we parsed for this comment.

41
00:03:27,230 --> 00:03:30,380
And the variable K job name has output the job name.

42
00:03:30,810 --> 00:03:32,150
That's good job perfect.

43
00:03:32,900 --> 00:03:38,900
Now what would have happened behind the scene was as variables are part of GitLab and not their counterpart

44
00:03:38,900 --> 00:03:39,440
are not part.

45
00:03:39,890 --> 00:03:43,310
So GitLab, read the YAML file and send that information to.

46
00:03:43,310 --> 00:03:48,350
They don't know where the variables were exposed, and then the runner ran the script commands, but

47
00:03:48,350 --> 00:03:49,610
actual value of variables.

48
00:03:50,510 --> 00:03:56,330
And guys, this was just a dummy example to print a predefined query variables coming ahead when we

49
00:03:56,330 --> 00:03:57,890
will create a realtime application.

50
00:03:58,250 --> 00:04:01,610
There you would get to know the real implementation of these variables.

51
00:04:02,090 --> 00:04:06,920
All of the learnings and concept understandings we are doing now is to build that application.

52
00:04:07,870 --> 00:04:11,490
Anyhow, so this was what predefined variables are in GitLab.

53
00:04:12,460 --> 00:04:16,960
And I would definitely recommend you to visit that link, which I provided in the Resources tab.

54
00:04:17,410 --> 00:04:23,830
Have a look on all the available pre-defined variables as they come to be very helpful at times and

55
00:04:23,830 --> 00:04:24,940
moving next intersection.

56
00:04:25,120 --> 00:04:28,240
We have custom variables that we'll see in the next class.

57
00:04:28,450 --> 00:04:28,960
See you there.
