1
00:00:00,360 --> 00:00:04,260
Nobody's just knowing about Sicily does not make any effective change.

2
00:00:04,770 --> 00:00:10,860
What makes a difference would be by knowing how how to bring in Sicily methodology into practice so

3
00:00:10,860 --> 00:00:12,850
that we can leverage the benefits we saw.

4
00:00:13,560 --> 00:00:14,910
So welcome to this new section.

5
00:00:15,210 --> 00:00:19,650
From this section, we will start learning GitLab or more specifically, GitLab the Sicily.

6
00:00:20,250 --> 00:00:25,590
In this lecture, I will introduce you with GitLab and then we progressively move towards the practicals

7
00:00:25,590 --> 00:00:26,460
of get lifecycle.

8
00:00:27,590 --> 00:00:29,690
Let's start by discussing a few points around it.

9
00:00:30,290 --> 00:00:31,190
So what is GitLab?

10
00:00:31,910 --> 00:00:38,810
GitLab is a web based complete Davos platform, not that complete DevOps platform delivered as a single

11
00:00:38,810 --> 00:00:44,810
application that spans the entire software development lifecycle and helps teams who accelerate DevOps

12
00:00:44,810 --> 00:00:45,320
adoption.

13
00:00:46,550 --> 00:00:52,940
GitLab is a DevOps platform that enables professionals to perform all the tasks within a project starting

14
00:00:52,940 --> 00:00:58,400
from project planning to source code management to monitoring and security when used wisely.

15
00:00:58,550 --> 00:01:03,410
It provides end to end DevOps capabilities for each stage of the software development lifecycle.

16
00:01:04,700 --> 00:01:06,830
Guys get lab is not a monolithic application.

17
00:01:07,100 --> 00:01:12,620
Instead, it tries to follow that Unix philosophy, which states that a software module should do only

18
00:01:12,620 --> 00:01:14,360
one particular thing and do it well.

19
00:01:15,690 --> 00:01:19,020
Good offers a one stop location for online cold storage.

20
00:01:19,650 --> 00:01:25,350
It provides a great repository manager for issue tracking, continuous integration and deployment pipeline,

21
00:01:25,560 --> 00:01:28,560
which is the pipeline features and wikis.

22
00:01:29,280 --> 00:01:32,220
And yes, people generally confuse GitLab with GitHub.

23
00:01:32,580 --> 00:01:37,170
But please keep in mind GitLab and GitHub are competitors to each other and not the same thing.

24
00:01:38,830 --> 00:01:44,200
GitLab supports both public and private development branches and offers you both free and paid services

25
00:01:44,200 --> 00:01:46,270
to choose from for loan purposes.

26
00:01:46,540 --> 00:01:48,280
I will recommend you to go with the free version.

27
00:01:49,610 --> 00:01:51,670
We're talking about some of the non-technical points.

28
00:01:52,450 --> 00:01:57,100
GitLab was created by Dmitry IBEC, programmer from Ukraine in 2011.

29
00:01:58,330 --> 00:02:02,080
GitLab software is updated every 20 second day of the month since then.

30
00:02:03,850 --> 00:02:05,590
It is not 100 percent open source.

31
00:02:05,950 --> 00:02:11,560
Instead, it's an open court project under which they have released their core software system as an

32
00:02:11,560 --> 00:02:12,670
open source license.

33
00:02:13,030 --> 00:02:18,280
But they have various beta versions as well, which contains additional enhanced features from the code

34
00:02:18,280 --> 00:02:18,760
software.

35
00:02:19,240 --> 00:02:21,820
So, you know, it's a combination of both free and paid.

36
00:02:22,930 --> 00:02:23,280
All right.

37
00:02:23,400 --> 00:02:28,290
So, guys, this was a little introduction to GitLab, if you guys wish to explore more about it, then

38
00:02:28,290 --> 00:02:29,880
you can visit their official website.

39
00:02:29,910 --> 00:02:31,410
Link is given in the Resources tab.

40
00:02:32,040 --> 00:02:33,120
That's it for this lecture.

41
00:02:33,210 --> 00:02:37,110
In the next class, I will take you to the GitLab website and we'll start working on it.

42
00:02:37,290 --> 00:02:37,680
Thank you.
