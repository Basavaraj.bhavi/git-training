1
00:00:00,390 --> 00:00:02,010
Dashboard two will be a quick one.

2
00:00:03,330 --> 00:00:07,110
From here, you can navigate to any of your projects from this number.

3
00:00:08,210 --> 00:00:09,110
Your projects?

4
00:00:11,510 --> 00:00:17,090
Your projects page, it will list on all the projects you have starred, projects are your favorite

5
00:00:17,090 --> 00:00:19,010
project and last.

6
00:00:20,820 --> 00:00:24,530
You can explore other contributors projects from this explored Project Spartan.

7
00:00:25,680 --> 00:00:27,390
Let's go back to Project Dashboard.

8
00:00:31,040 --> 00:00:31,910
Go to our project.

9
00:00:34,460 --> 00:00:39,650
Now, giving you a short tour of this project dashboard on the left behind, we have options like Project

10
00:00:39,650 --> 00:00:44,930
Overview giving us good details about this particular project in this detail page.

11
00:00:45,380 --> 00:00:50,840
In fact, this is the details page only this page is important as it gives us most of the information

12
00:00:50,840 --> 00:00:51,380
at a glance.

13
00:00:51,950 --> 00:00:56,840
We have the Project I.D. number of commitment to this project.

14
00:00:57,260 --> 00:00:58,460
Gordon Lambert of Branches.

15
00:00:58,460 --> 00:01:04,220
This project have project size, then options to start or for the project.

16
00:01:04,820 --> 00:01:10,670
And well, as we are in it, it is asking us to enable auto DevOps in project settings to automatically

17
00:01:10,670 --> 00:01:14,720
build, test and deploy our application based on spatially configurations.

18
00:01:15,110 --> 00:01:16,280
We will skip this for now.

19
00:01:19,170 --> 00:01:20,370
Next from here.

20
00:01:21,940 --> 00:01:26,660
You can switch to any of your projects, branches, as of now, we have only one main.

21
00:01:26,740 --> 00:01:33,670
The Master Branch, then from here you can create a new file directory, create new branches or add

22
00:01:33,670 --> 00:01:34,660
back to this project.

23
00:01:35,200 --> 00:01:39,340
These are the buttons that we will frequently used while building a project at this moment.

24
00:01:39,550 --> 00:01:41,230
I'm just trying to make you familiar with that.

25
00:01:41,230 --> 00:01:41,950
Get away.

26
00:01:43,330 --> 00:01:49,870
Then an important link is this clone button from here, you can clone this project to other locations,

27
00:01:50,020 --> 00:01:53,830
like in your local system and to clone you have two options.

28
00:01:54,130 --> 00:01:58,570
Either way, you can clone it as such or with this as deepest link.

29
00:02:01,270 --> 00:02:07,120
This whole table is where you will get to see your project structure, all your project files and directories

30
00:02:07,120 --> 00:02:07,930
will be listed here.

31
00:02:08,630 --> 00:02:13,360
And remember, while initiating this project, we have checked on a button to create a file.

32
00:02:13,810 --> 00:02:15,040
This is that it be fight.

33
00:02:16,060 --> 00:02:19,900
And this is the only and last comment we have made to this project.

34
00:02:21,250 --> 00:02:24,610
This section here, it just displays the contents of Read Me Fight.

35
00:02:26,200 --> 00:02:27,970
Then we have activity page.

36
00:02:30,420 --> 00:02:35,130
This page will show you all the events that had happened in this project in a chronological order.

37
00:02:35,700 --> 00:02:41,040
Events like which you were pushed to which branch and at what time, which branch was marched or which

38
00:02:41,040 --> 00:02:45,720
branch and when all this list of activities will be shown here in chronological order.

39
00:02:46,260 --> 00:02:49,890
And you can check any particular type of events from these steps.

40
00:02:50,160 --> 00:02:51,940
All the bush events will be present here.

41
00:02:52,050 --> 00:02:59,130
All the monuments will be presented here to recover a few of the links inside the repository.

42
00:03:01,250 --> 00:03:07,490
We have seen pages related to this Project Files page containing list of files from its branches and

43
00:03:07,490 --> 00:03:08,030
other things.

44
00:03:08,060 --> 00:03:10,450
The purpose of every page is clear from the name only.

45
00:03:12,710 --> 00:03:13,790
Then we have issues.

46
00:03:16,540 --> 00:03:20,440
Here you can create any new issue or import from already existing issues.

47
00:03:20,980 --> 00:03:25,120
Issues are a great way to keep track of task enhancements and bugs for your project.

48
00:03:26,880 --> 00:03:27,780
Much request.

49
00:03:28,530 --> 00:03:29,350
This is too much.

50
00:03:29,370 --> 00:03:33,570
One branch into another will see this much option in detail while doing practical.

51
00:03:34,470 --> 00:03:36,570
Then we have this CAC section.

52
00:03:38,430 --> 00:03:43,770
The section, which is relevant to the schools going ahead, we and our course will be revolving around

53
00:03:43,770 --> 00:03:45,030
this the report only.

54
00:03:45,750 --> 00:03:49,920
This is from where we will handle our pipelines and their underlying jobs.

55
00:03:50,730 --> 00:03:54,910
Ideally, this year's U.S. should be clear and empty as currently in our project.

56
00:03:54,930 --> 00:03:56,370
We haven't defined any pipeline.

57
00:03:56,760 --> 00:03:59,480
But at present it is showing us filled pipeline at it.

58
00:04:00,180 --> 00:04:00,870
Let's open it.

59
00:04:01,470 --> 00:04:02,310
Click on this field.

60
00:04:07,010 --> 00:04:13,130
The aerospace user validation required, actually, guys, as we have chosen the GitLab free date account

61
00:04:13,490 --> 00:04:19,520
under that we are given some 400 minutes of GitLab shareowners, so in order to be eligible to use those

62
00:04:19,520 --> 00:04:20,210
shared on us.

63
00:04:20,490 --> 00:04:22,760
So what are you going to say, GitLab resources for a moment?

64
00:04:23,120 --> 00:04:26,360
We need to validate that account with a credit card or debit card.

65
00:04:26,930 --> 00:04:31,040
They do so so as to discourage and reduce abuse on GitLab infrastructure.

66
00:04:31,520 --> 00:04:36,740
And yes, don't worry, as it is right in here, GitLab will not judge anything or store your card details

67
00:04:36,740 --> 00:04:39,890
for billing this thing that only for the validation purposes.

68
00:04:44,460 --> 00:04:49,380
You can also skip this validation, but in case if you don't want to use that shareowners and would

69
00:04:49,380 --> 00:04:50,950
like to use your own local donors.

70
00:04:51,510 --> 00:04:55,980
But since they are not charging anything and in this course, I would be leveraging from their shared

71
00:04:55,980 --> 00:04:56,340
donors.

72
00:04:56,610 --> 00:04:58,140
So I'm going to validate my account.

73
00:04:58,830 --> 00:04:59,880
Click on valid account.

74
00:05:03,980 --> 00:05:05,540
And Alan Domagoj details.

75
00:05:29,830 --> 00:05:33,370
See, noticing users successfully validated, great.

76
00:05:34,370 --> 00:05:38,320
Last important page is this settings.

77
00:05:39,080 --> 00:05:42,020
This page will control the project at administrative level.

78
00:05:42,500 --> 00:05:45,800
From here, you can perform operations like changing the project name.

79
00:05:46,130 --> 00:05:50,120
It's visibility runners delete project archive project and not.

80
00:05:51,070 --> 00:05:51,460
All right.

81
00:05:51,700 --> 00:05:56,040
So, guys, that was a quick tour of some important pages of GitLab and upcoming lectures.

82
00:05:56,050 --> 00:05:59,440
We will definitely visit these pages again, who have hands on on them.

83
00:06:00,070 --> 00:06:01,120
That's it for this class.

84
00:06:01,240 --> 00:06:02,230
See you in the next lecture!
