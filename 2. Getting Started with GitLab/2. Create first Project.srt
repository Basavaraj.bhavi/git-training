1
00:00:00,940 --> 00:00:01,630
Welcome, guys.

2
00:00:02,080 --> 00:00:06,700
Starting from this lecture, we will concentrate on GitLab, Cassidy and its components only.

3
00:00:07,180 --> 00:00:11,050
And as a first move, I'm going to explain the user interface of GitLab.

4
00:00:11,740 --> 00:00:14,080
The built in UAE is a notable part of GitLab.

5
00:00:14,500 --> 00:00:20,230
Its rich user interface makes it easy to visualize running pipelines and production, monitor their

6
00:00:20,230 --> 00:00:21,850
progress and troubleshoot issues.

7
00:00:22,330 --> 00:00:27,760
It allows you to see the status of your jobs, their underlying tasks, and furthermore, you can perform

8
00:00:27,760 --> 00:00:29,170
different actions on that job.

9
00:00:29,920 --> 00:00:30,700
So get labs.

10
00:00:30,700 --> 00:00:34,870
Your dashboard is a thing which can help you managing your pipelines very easy.

11
00:00:35,620 --> 00:00:37,930
Having said that, let's start exploiting it.

12
00:00:39,290 --> 00:00:43,160
GitLab is available on GitLab dot com.

13
00:00:45,400 --> 00:00:49,300
They're not logged in it, redirect you to about dot dot com.

14
00:00:49,600 --> 00:00:52,130
And this is how it looks from the nav bot.

15
00:00:52,150 --> 00:00:56,350
You can navigate to different web pages like products pricing blog.

16
00:00:56,390 --> 00:01:00,520
Other pages the one we are concerned with is this Simon?

17
00:01:00,880 --> 00:01:01,330
Open it.

18
00:01:08,080 --> 00:01:09,840
As I do not have lab account it.

19
00:01:10,850 --> 00:01:12,020
So let's create one.

20
00:01:30,030 --> 00:01:31,590
Click on this registry button.

21
00:01:34,170 --> 00:01:36,390
We're at a moment while I can my e-mail address.

22
00:01:37,770 --> 00:01:39,000
I have confirmed my e-mail.

23
00:01:39,300 --> 00:01:40,050
Let me sign again.

24
00:01:49,500 --> 00:01:49,890
OK.

25
00:01:49,920 --> 00:01:54,800
Next, it will ask you certain questions like who you are, your role, with whom you will be using

26
00:01:54,810 --> 00:01:55,620
it, all this.

27
00:01:55,950 --> 00:01:57,120
Just answer them accordingly.

28
00:02:05,130 --> 00:02:06,210
I'm skipping this part.

29
00:02:09,410 --> 00:02:09,980
Here you go.

30
00:02:10,400 --> 00:02:13,190
Forget Live Oak Island has been created and we are logged into it.

31
00:02:14,330 --> 00:02:15,090
Is a fresh account.

32
00:02:15,110 --> 00:02:16,460
So we do not have any projects.

33
00:02:16,460 --> 00:02:17,450
It looks great.

34
00:02:17,450 --> 00:02:21,650
Our first project to create a new project from this button.

35
00:02:22,520 --> 00:02:23,690
Click on New Project.

36
00:02:25,490 --> 00:02:30,260
GitLab offers you four different ways to create a new project with this first one.

37
00:02:30,290 --> 00:02:33,160
You create a blank project from scratch after creation.

38
00:02:33,170 --> 00:02:34,700
There will be no pre-built files.

39
00:02:34,910 --> 00:02:36,530
Your project directory will be empty.

40
00:02:37,280 --> 00:02:40,800
Next, we have cleared from template from this option.

41
00:02:40,820 --> 00:02:44,030
You can start the project by choosing from already built templates.

42
00:02:44,360 --> 00:02:49,420
Like, if you're developing iOS app, then you can choose iOS template for Android application.

43
00:02:49,430 --> 00:02:56,690
You use Android template and similarly, they have around 27 building templates after project creation.

44
00:02:56,780 --> 00:02:57,950
Get level automatically.

45
00:02:57,950 --> 00:03:01,370
Create the necessary requisite files for you to get you started quickly.

46
00:03:02,000 --> 00:03:05,270
For example, if you create from Android Project Template.

47
00:03:06,690 --> 00:03:11,720
Upon your project creation, GitLab will create these files and folders into your project automatically

48
00:03:11,960 --> 00:03:13,340
so that you get going fast.

49
00:03:15,970 --> 00:03:21,820
Told me, in case if you already have your project created and it is hosted in some different external

50
00:03:21,820 --> 00:03:27,850
source like GitHub, Bitbucket, then you can migrate your data from that external source into GitLab

51
00:03:28,120 --> 00:03:28,960
from this option.

52
00:03:30,190 --> 00:03:34,180
All you have to do is just click on the appropriate tab where your project is located.

53
00:03:34,720 --> 00:03:36,400
Let it be GitHub.

54
00:03:38,590 --> 00:03:43,030
Then you sign into your GitHub account and all your data projects will get listed here.

55
00:03:44,000 --> 00:03:46,160
And they all can be bothered with just one click.

56
00:03:48,720 --> 00:03:55,080
Last, we have a record for external repository, but this option, instead of moving your entire project

57
00:03:55,080 --> 00:03:59,820
to get lab, you can connect your external opposite to get the benefits of telepsychiatry.

58
00:04:00,480 --> 00:04:03,490
So guys, these are a few ways how you can create a project and get lab.

59
00:04:04,080 --> 00:04:05,370
I will create a blank project.

60
00:04:07,610 --> 00:04:14,900
It will ask you to and a few details about your project, let the project name be Vice Project.

61
00:04:16,030 --> 00:04:17,080
Any descriptions?

62
00:04:18,930 --> 00:04:19,350
This is.

63
00:04:21,900 --> 00:04:31,110
My first project visibility level, whether you want to make this project private or public, I'll keep

64
00:04:31,110 --> 00:04:31,650
it private.

65
00:04:32,340 --> 00:04:35,550
Last, if you wish to create a read me file inside your project.

66
00:04:36,300 --> 00:04:38,610
Yes, I will create the project.

67
00:04:41,530 --> 00:04:42,250
Congratulations.

68
00:04:42,460 --> 00:04:47,620
Our first little project has been successfully created, and we are already into our project dashboard

69
00:04:47,620 --> 00:04:49,630
page at top.

70
00:04:49,660 --> 00:04:53,440
You can see it is giving us a warning to as a search gain to your profile.

71
00:04:53,890 --> 00:04:55,780
I will discard this warning as of now.

72
00:04:57,650 --> 00:04:59,450
So an empty project is created.

73
00:04:59,600 --> 00:05:00,830
And let me give you a shot.

74
00:05:00,860 --> 00:05:03,140
You might be put off this dashboard in the next lecture.
