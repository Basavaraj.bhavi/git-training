1
00:00:00,930 --> 00:00:06,660
Welcome to this lecture, where I will demonstrate how to install Ubuntu in VirtualBox in Windows operating

2
00:00:06,660 --> 00:00:07,020
system.

3
00:00:07,770 --> 00:00:08,120
Nice.

4
00:00:08,160 --> 00:00:11,290
If you are already on Ubuntu, then you can skip this lecture.

5
00:00:11,640 --> 00:00:14,580
For other students who install Ubuntu in VirtualBox.

6
00:00:14,880 --> 00:00:17,790
Just follow these steps, which I am going to perform in this lecture.

7
00:00:18,890 --> 00:00:22,520
OK, so first things first to install Ubuntu, what's really on your system?

8
00:00:22,850 --> 00:00:27,920
We will require the virtual box, which is an application that will help you install another operating

9
00:00:27,920 --> 00:00:28,310
system.

10
00:00:28,550 --> 00:00:35,030
What you see on your host So to download and install Oracle VirtualBox in your system, simply Google.

11
00:00:40,840 --> 00:00:42,160
Oracle VirtualBox.

12
00:00:43,630 --> 00:00:45,250
Click on the first official link.

13
00:00:46,620 --> 00:00:48,780
Hell, you will get a big download button.

14
00:00:49,080 --> 00:00:56,010
It's a little box, six point one, click on it on the next page, it will ask you to select the host

15
00:00:56,010 --> 00:00:58,590
on which you are on on select Windows host.

16
00:01:00,600 --> 00:01:04,980
I understand that downloading mine is already downloaded, so I'll cancel this.

17
00:01:06,230 --> 00:01:11,480
Now, the second requirement is you will need the ISO image of the operating system you are going to

18
00:01:11,480 --> 00:01:13,580
install for Ubuntu.

19
00:01:19,330 --> 00:01:20,560
Click on the first link.

20
00:01:23,330 --> 00:01:26,150
So you just have to click this button and it will start to download.

21
00:01:26,750 --> 00:01:27,560
I already have it.

22
00:01:28,190 --> 00:01:34,310
It's ISO image size of around two jobs, so it may take some time to download after you have bought

23
00:01:34,310 --> 00:01:36,140
VirtualBox and Ubuntu ISO image.

24
00:01:36,560 --> 00:01:39,260
First, you will need to install VirtualBox on your system.

25
00:01:40,340 --> 00:01:41,840
Installation is very simple.

26
00:01:43,130 --> 00:01:45,500
You just have to make a few clicks on the next buttons.

27
00:01:45,920 --> 00:01:47,660
Next Next.

28
00:01:48,620 --> 00:01:52,310
Next proceeded with the installation you click, Yes.

29
00:01:52,640 --> 00:01:56,030
Since I already have the virtual box installed with me, so I'll stop here.

30
00:02:02,470 --> 00:02:05,020
After the metal box is installed, you will get this shortcut.

31
00:02:06,310 --> 00:02:11,320
Now comes the next step install Ubuntu on the virtual box, so open this virtual box.

32
00:02:14,000 --> 00:02:15,020
Create a new machine.

33
00:02:21,360 --> 00:02:26,320
Let's give it a name, my one to buy Linux.

34
00:02:26,400 --> 00:02:28,410
And what do you need someone to 64bit?

35
00:02:28,920 --> 00:02:33,840
Click on next, then here you need to allow some memory to this virtual machine.

36
00:02:34,560 --> 00:02:39,060
Nice, though it depends upon your system configurations, but I will suggest you to give at least four

37
00:02:39,060 --> 00:02:40,950
gigs of memory for the smooth run of.

38
00:02:42,030 --> 00:02:42,990
I will give it around.

39
00:02:45,280 --> 00:02:45,940
It gives.

40
00:02:47,770 --> 00:02:48,280
Next.

41
00:02:49,340 --> 00:02:51,800
It will ask you to provide hard disk to this one, too.

42
00:02:52,190 --> 00:02:54,350
I will keep it default to create a virtual hard disk.

43
00:02:54,350 --> 00:02:54,620
No.

44
00:02:56,170 --> 00:02:56,490
Great.

45
00:02:57,460 --> 00:03:00,970
This is the hard disk file type, again, keep it default affiliate.

46
00:03:02,290 --> 00:03:03,660
Storage on physical hard drive.

47
00:03:03,790 --> 00:03:05,440
I will keep it dynamically allocated.

48
00:03:05,620 --> 00:03:06,310
Click next.

49
00:03:07,180 --> 00:03:10,180
Then add this window you have to allocate the virtual hard disk space.

50
00:03:11,050 --> 00:03:11,890
Let it be.

51
00:03:14,030 --> 00:03:18,260
Some 40 degrees, you can increase or decrease the size depending upon your usage.

52
00:03:18,950 --> 00:03:20,390
At last, click on this great button.

53
00:03:23,140 --> 00:03:28,360
OK, now before we start this machine, we need to provide it with the downloaded ISO image to load.

54
00:03:29,080 --> 00:03:32,200
So my open to being selected, click on Settings.

55
00:03:34,450 --> 00:03:35,410
Go to storage.

56
00:03:36,870 --> 00:03:43,380
Under the control, it currently it's empty at here, we need to provide it the image we have downloaded.

57
00:03:44,280 --> 00:03:45,810
Click on this disk icon.

58
00:03:47,450 --> 00:03:49,970
Choose what your optical disc.

59
00:03:51,100 --> 00:03:55,300
And browsed with the ISO file we downloaded, click on Add.

60
00:03:57,310 --> 00:04:01,270
I have this one two eight eight zero four version selected open.

61
00:04:02,280 --> 00:04:02,850
She was.

62
00:04:04,510 --> 00:04:05,320
Click OK.

63
00:04:06,880 --> 00:04:10,300
Everything is done now, choose your virtual machine and click on Stop.

64
00:04:14,790 --> 00:04:15,200
So.

65
00:04:18,820 --> 00:04:21,430
So you can see insulation steps have started.

66
00:04:23,690 --> 00:04:28,580
And this page from left side, select the language and click on Install Ubuntu.

67
00:04:32,580 --> 00:04:34,890
At next page, you need to select the keyboard layout.

68
00:04:35,280 --> 00:04:38,940
I will keep it to English, English continue.

69
00:04:41,790 --> 00:04:44,730
Then at last, what apps would you like to install at the start?

70
00:04:45,090 --> 00:04:51,780
I will do the normal installation only, but I will do this install third party software for some graphics.

71
00:04:53,140 --> 00:04:53,950
Click on Continue.

72
00:04:55,740 --> 00:04:59,850
And this step, it is disc and install Ubuntu, you have to check this.

73
00:05:00,210 --> 00:05:03,900
Don't worry, it is in disk is not going to erase anything from your host machine.

74
00:05:04,230 --> 00:05:08,190
All your data will be intact, so selected without worry and click on install.

75
00:05:08,190 --> 00:05:08,420
Not.

76
00:05:11,870 --> 00:05:12,440
Continue.

77
00:05:15,030 --> 00:05:16,090
Select your country.

78
00:05:19,660 --> 00:05:24,580
And this page, you need to fill some details like your name, your computer name and log in password.

79
00:05:37,310 --> 00:05:37,820
Continue.

80
00:05:39,910 --> 00:05:42,790
So as you can see, the installation of Ubuntu has been started.

81
00:05:43,270 --> 00:05:45,580
This installation process can take some time.

82
00:05:46,030 --> 00:05:48,700
So just relax and wait for the installation to complete.

83
00:05:50,510 --> 00:05:54,560
And after the installation is complete, you may have to restart it again from the virtual box.

84
00:05:54,770 --> 00:05:56,600
So if asked, then please do that.

85
00:05:58,620 --> 00:06:00,450
So after the restart, I was started again.

86
00:06:03,150 --> 00:06:06,720
Well, installation is complete, and not one to us is ready to use.
