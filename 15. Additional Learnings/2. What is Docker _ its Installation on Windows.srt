1
00:00:00,550 --> 00:00:01,450
What is this, Dr?

2
00:00:01,780 --> 00:00:06,520
I believe most of you would know, but still, I will give you a brief knowledge of it.

3
00:00:07,480 --> 00:00:13,240
Docker is a containerization platform and a kind of virtualization technology for building applications

4
00:00:13,270 --> 00:00:14,350
based on containers.

5
00:00:15,370 --> 00:00:21,970
It is designed to make it easy to create, deploy and run applications as portable and self-sufficient

6
00:00:21,970 --> 00:00:22,540
containers.

7
00:00:23,740 --> 00:00:29,650
Using Docker, all the application packages, libraries and dependencies are backed up as one package

8
00:00:29,650 --> 00:00:35,590
in the form of Docker container, which ensures that our application works seamlessly in any environment,

9
00:00:35,890 --> 00:00:38,110
no matter on which machine they are running on.

10
00:00:39,310 --> 00:00:44,620
Because of this containerization technique, Docker containers are sometimes that effort towards lightweight

11
00:00:44,620 --> 00:00:45,010
VMS.

12
00:00:45,700 --> 00:00:51,250
In fact, if you don't want to go into any technicalities of Docker, you just assume it has a compressed

13
00:00:51,250 --> 00:00:55,570
version of a virtual machine, which is small in size and very easy to use.

14
00:00:56,410 --> 00:01:02,440
Having this in mind, let's install Docker because you can install Docker on most of the operating systems

15
00:01:02,470 --> 00:01:06,160
like Ubuntu Windows Automatic Docker supports all of them.

16
00:01:06,700 --> 00:01:11,410
But before you install, I will recommend you to please check the system requirements for it.

17
00:01:12,040 --> 00:01:14,470
This check is necessary as to install Docker.

18
00:01:14,500 --> 00:01:16,450
There can be some issues for conversions.

19
00:01:16,990 --> 00:01:22,120
I'm giving you the link of it in the Resources tab of this lecture, and I would also recommend you

20
00:01:22,120 --> 00:01:27,580
to serve the provided link more as it contains more info on installation for various OS.

21
00:01:28,600 --> 00:01:32,890
So you can pause this video and check what Docker package foods for your system.

22
00:01:37,150 --> 00:01:37,640
All right.

23
00:01:38,050 --> 00:01:42,310
So if you have stuff through the link, you would have got to know that there are two different set

24
00:01:42,310 --> 00:01:42,550
ups.

25
00:01:42,850 --> 00:01:48,520
One is Dr. Desktop for the latest versions of Mac or Windows 10 Pro and its enterprise solution.

26
00:01:48,880 --> 00:01:54,400
And the second is Blocker Toolbox for the older versions of Mac and Windows 10 home or less.

27
00:01:54,910 --> 00:01:58,570
I have Windows Pro on my system, so I'll be installing Docker desktop.

28
00:01:59,230 --> 00:02:02,050
Actually, a Docker toolbox is a legacy desktop solution.

29
00:02:02,320 --> 00:02:08,410
It is created just to support the systems that do not meet minimum system requirements for the Docker

30
00:02:08,410 --> 00:02:09,130
desktop app.

31
00:02:09,670 --> 00:02:15,370
In fact, their official documentation itself recommends to switch to a Windows Pro for better and less

32
00:02:15,370 --> 00:02:15,880
functioning.

33
00:02:16,570 --> 00:02:20,470
So I would say that if you have Windows Pro, then it's the best anyways.

34
00:02:20,650 --> 00:02:23,520
Let us install Docker on Windows 10 Pro Edition.

35
00:02:24,370 --> 00:02:27,520
Open up the browser and go to dockless official website.

36
00:02:37,290 --> 00:02:39,000
This is how the website looks.

37
00:02:39,360 --> 00:02:45,960
Here you can find all the information and resources regarding Dzokhar Dzokhar products, solutions,

38
00:02:45,960 --> 00:02:47,010
customer resources.

39
00:02:47,220 --> 00:02:53,340
You can browse if you want to download or go to products and click on desktop.

40
00:02:55,800 --> 00:02:58,050
Hit download desktop for Mac and Windows.

41
00:03:03,680 --> 00:03:08,720
In order to start download, you will first need to log in to your local account so you can either sign

42
00:03:08,720 --> 00:03:14,630
in from here or in case if you don't have Dzokhar account, then click on the sign up button and create

43
00:03:14,630 --> 00:03:20,060
your account by entering your username, which they're going to scan it and your email and password.

44
00:03:21,290 --> 00:03:22,280
I will sign in my.

45
00:03:29,260 --> 00:03:35,490
After signing, click on this download button again, and it will start the download process nice,

46
00:03:35,500 --> 00:03:41,620
the installation for Mac is more or less same as of Windows, but you can definitely take reference

47
00:03:41,620 --> 00:03:43,600
from their official link with just this one.

48
00:03:44,170 --> 00:03:46,480
I'm sharing it in the resources that you can go through it.

49
00:03:48,070 --> 00:03:53,860
I will cancel this because I've already downloaded in my system and directly go to Downloads folder.

50
00:03:55,220 --> 00:03:57,110
An open door car installer.

51
00:04:07,130 --> 00:04:08,690
It is downloading some packages.

52
00:04:10,650 --> 00:04:13,890
Under the configuration, it will ask you to a shortcut.

53
00:04:14,220 --> 00:04:15,510
Check it and click next.

54
00:04:17,390 --> 00:04:20,510
Then it will start unpacking the files and will install them.

55
00:04:21,740 --> 00:04:23,030
This step may take some time.

56
00:04:23,150 --> 00:04:24,200
So you have to wait.

57
00:04:25,010 --> 00:04:27,980
I'm skipping this video to the point where installation is complete.

58
00:04:30,120 --> 00:04:31,650
OK, installation succeeded.

59
00:04:33,200 --> 00:04:38,570
OK, so once the installation is finished to get things ready, it may ask you to log on to your system

60
00:04:38,780 --> 00:04:42,680
so you have to do it after you log into your windows again.

61
00:04:43,100 --> 00:04:44,300
Open the desktop.

62
00:04:46,180 --> 00:04:51,610
Now, since you would be opening Dr. Indoor System for the first time, it may ask the permission to

63
00:04:51,610 --> 00:04:55,360
enable Hyper-V and container free agents so you have to click OK.

64
00:04:56,020 --> 00:04:59,410
After that, your computer will restart automatically to make the changes.

65
00:05:01,410 --> 00:05:04,710
So after the restart, when you click on this or desktop.

66
00:05:06,350 --> 00:05:09,200
Here it will say Docker desktop is starting.

67
00:05:11,260 --> 00:05:14,680
This may take a few minutes to come up, so we have to wait.

68
00:05:17,750 --> 00:05:23,690
Grows your desktop is now up and running, you have successfully installed Docker on a system.

69
00:05:27,460 --> 00:05:30,400
That's doctor has very less thing to do with it, right?

70
00:05:30,670 --> 00:05:36,460
So anything we will do with Doctor will be using Docker commands, so we'll run a simple Docker command

71
00:05:36,460 --> 00:05:37,570
to check its volume.

72
00:05:38,650 --> 00:05:39,700
Open up, PowerShell.

73
00:05:48,190 --> 00:05:50,110
And type Dr.

74
00:05:52,740 --> 00:05:53,280
Virgin.

75
00:05:55,970 --> 00:05:58,850
It will output you the Docker version installed on your system.

76
00:05:59,360 --> 00:06:01,820
In my case, it is 19 three foot.
