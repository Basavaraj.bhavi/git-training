1
00:00:00,490 --> 00:00:05,920
As part of application to run on this local runner, I will create a very simple Python Flask application.

2
00:00:14,370 --> 00:00:16,260
Let me paste the application code.

3
00:00:18,170 --> 00:00:22,970
This application does nothing complex, but simply wishes a person with the message, Happy Birthday,

4
00:00:23,390 --> 00:00:27,560
and then the person the person name it picks from the environment variable.

5
00:00:28,440 --> 00:00:30,500
Now, giving you a brief idea of this Python code.

6
00:00:31,820 --> 00:00:37,580
At start, I have imported this flask module where a flask, as you all might know, is a lightweight

7
00:00:37,580 --> 00:00:42,470
WSJ web application framework that allows us to build web applications.

8
00:00:43,400 --> 00:00:48,860
Then important was module, which provides US functions for interacting with operating system.

9
00:00:50,420 --> 00:00:54,660
Flask Constructor takes the name of current module name as argument.

10
00:00:55,520 --> 00:01:02,470
Then this root function of the flask class is a decorator, which does the application, which you all

11
00:01:02,480 --> 00:01:04,100
should call the associated function.

12
00:01:04,790 --> 00:01:08,390
It's simply a slash means the app indexed page.

13
00:01:09,930 --> 00:01:13,560
Next, we have a method vision that returns this message.

14
00:01:14,490 --> 00:01:15,270
Happy Birthday.

15
00:01:15,300 --> 00:01:22,800
And notice the name isn't curly braces and its value come from the environment variable name that we

16
00:01:22,800 --> 00:01:23,700
will define shortly.

17
00:01:24,240 --> 00:01:31,140
But in case, if there is no environment variable name, then this let's not get environment would return

18
00:01:31,140 --> 00:01:34,200
the default value of variable names, John.

19
00:01:34,950 --> 00:01:39,690
So basically, the default message that this application would return is Happy birthday, John.

20
00:01:39,900 --> 00:01:45,900
If there is no value specified in the environment and it's nice, please don't get confused with this

21
00:01:45,900 --> 00:01:51,720
name variable with the variables that we are going to learn in this environment.

22
00:01:51,720 --> 00:01:57,390
Variable name is just a part of this Python application, and it has nothing to do with the key variables.

23
00:01:57,570 --> 00:01:58,770
Both are two different things.

24
00:02:00,030 --> 00:02:07,440
Anyway, at last, this application is so that 0.2 0.0 put it in nothing complex in it, right?

25
00:02:09,150 --> 00:02:14,130
Now, before running it using GitLab, if I could test this app by running it locally on my system,

26
00:02:15,000 --> 00:02:16,980
I have the same version of it president locally.

27
00:02:26,510 --> 00:02:30,020
You should have Python three and flask installed on your system to run this.

28
00:02:30,350 --> 00:02:33,680
If not, then you can install them using these commands, respectively.

29
00:02:49,870 --> 00:02:50,140
See?

30
00:02:50,380 --> 00:02:54,940
Since as of now, the environment variable is not set, so it has been turned to default.

31
00:02:54,970 --> 00:02:55,870
Happy birthday, John.

32
00:02:56,500 --> 00:02:56,800
Good.

33
00:02:57,250 --> 00:03:00,700
So that was all about the flask application running on my local system.

34
00:03:01,030 --> 00:03:04,690
Note that on local system and not using get left local, run it.

35
00:03:07,290 --> 00:03:11,610
So we have a booking by Tom Flask application and a local installer.

36
00:03:12,000 --> 00:03:17,730
The next thing you would need is an environment and environment on which this flask application can

37
00:03:17,730 --> 00:03:20,100
run using your local runners resources.

38
00:03:20,670 --> 00:03:26,460
Remember, while we are executing the pipeline on shareowners in the execution details we were seeing

39
00:03:26,460 --> 00:03:31,950
a Docker ruby image was getting created as environment here, so we would need something like that.

40
00:03:33,180 --> 00:03:39,060
Now, if you may know, apart from using existing Docker images, Docker also gives us the capability

41
00:03:39,060 --> 00:03:45,270
to create our own Docker images with the help of Docker fight, and our confie is a text document that

42
00:03:45,270 --> 00:03:47,700
contains commands a user could call on.

43
00:03:47,700 --> 00:03:49,980
The Docker come online to assemble an image.

44
00:03:50,880 --> 00:03:53,070
So next thing I will create a Docker fight.

45
00:03:53,520 --> 00:03:54,420
Let me commit this.

46
00:03:59,540 --> 00:04:00,530
This fight is committed.

47
00:04:10,210 --> 00:04:12,700
Docker file and paste code.

48
00:04:15,800 --> 00:04:22,340
Our Docker file statement from Kmart from Gilbert tells Docker, which basically means it has to use

49
00:04:22,460 --> 00:04:29,030
to build this new image in this case, since we are working with Python, so its Python image with its

50
00:04:29,030 --> 00:04:30,620
tag separated by colored.

51
00:04:32,110 --> 00:04:37,990
Then within the base image, we are defining the current working directory of a Docker container that

52
00:04:37,990 --> 00:04:41,860
is this AB directory using the work diagram on.

53
00:04:42,430 --> 00:04:48,760
So by setting this book directory, every subsequent commands like ad run copy will be executed in this

54
00:04:48,760 --> 00:04:49,520
directory only.

55
00:04:49,540 --> 00:04:52,330
That is this arbitrary noise.

56
00:04:52,540 --> 00:04:55,360
Since this above created a work directory would be empty.

57
00:04:55,960 --> 00:05:01,390
So next, with this ad directive, we are copying everything plays that the current directory.

58
00:05:01,720 --> 00:05:07,630
That is the location where this Docker file would be placed into this app folder that we just created.

59
00:05:08,110 --> 00:05:14,710
So all the files I've got be my Docker file and I get lepeska file that I would create next or any other

60
00:05:14,710 --> 00:05:21,220
file in this directory will be copied to the app directory of the newly built image after copying.

61
00:05:22,270 --> 00:05:25,840
If you remember in our application, we require the flash module.

62
00:05:26,110 --> 00:05:32,080
So with this line of code using a document, we are installing Flask on top of that base Python image.

63
00:05:33,780 --> 00:05:38,100
Next is what this application is based on with this EMV directive.

64
00:05:38,430 --> 00:05:40,230
We are setting an environment variable.

65
00:05:40,650 --> 00:05:44,190
And if you know, environment variables are defined as in key value.

66
00:05:44,850 --> 00:05:51,120
So here also we have an environment variable with key as name and have set this value to mark.

67
00:05:51,300 --> 00:05:51,670
Look at.

68
00:05:51,840 --> 00:05:53,520
Happy birthday mark in the output.

69
00:05:54,900 --> 00:06:02,130
After citing environment variables at last, we have seemed command this CMT command specifies the instruction

70
00:06:02,430 --> 00:06:07,200
that you want to execute when a Docker container is started from this image in here.

71
00:06:07,500 --> 00:06:12,280
But see, we are just running an application code and not by with Python command.

72
00:06:13,110 --> 00:06:18,450
So basically, when a container for this Docker image will start, it will start running the abort by

73
00:06:18,450 --> 00:06:19,770
file on the given port.

74
00:06:21,210 --> 00:06:27,870
All right, so Docker file is also complete and is since this time we would be leveraging local run

75
00:06:27,870 --> 00:06:33,450
it, so it would expect Docker installed on your system to build the image written in this Docker fight.

76
00:06:33,960 --> 00:06:35,970
I already have Docker installed in my bundle.

77
00:06:36,450 --> 00:06:40,950
If you don't have, then have a Docker installation lecture in the bonus section of this course.

78
00:06:41,310 --> 00:06:44,820
Nothing complex init just seven to eight commands to be done sequentially.

79
00:06:45,860 --> 00:06:50,060
Anyways, know what's next with Abbott, Billy and Docker file ready.

80
00:06:50,330 --> 00:06:55,970
The last thing to write is get lepeska dot via email file to define a pipeline that you're trying to

81
00:06:55,970 --> 00:07:01,970
build and deploy the Abbott Billy on a local system that has this Linux machine using the Git LeBron

82
00:07:02,110 --> 00:07:02,810
we have installed.

83
00:07:04,660 --> 00:07:09,190
Thank you, and I will see in the next lecture and come at this as well.
