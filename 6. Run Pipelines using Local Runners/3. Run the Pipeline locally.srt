1
00:00:01,070 --> 00:00:08,120
Now, before I commit the changes, take you to the local terminal before you can run that pipeline

2
00:00:08,120 --> 00:00:08,570
locally.

3
00:00:08,810 --> 00:00:14,190
You have to make sure a few things because this time this is your local system, which has to provide

4
00:00:14,190 --> 00:00:14,840
the resources.

5
00:00:15,650 --> 00:00:21,680
So first, you need to ensure that it is put on the host operating system is free to check it and the

6
00:00:21,680 --> 00:00:22,040
command.

7
00:00:25,880 --> 00:00:27,640
That sets should be installed on a system.

8
00:00:34,850 --> 00:00:37,890
See, my airport is busy with some a budget.

9
00:00:38,630 --> 00:00:42,500
So either I need to kill this task or assign a new port number in the pipeline.

10
00:00:43,430 --> 00:00:49,580
Let this task run and the new board, which I will give, is 5000.

11
00:00:51,840 --> 00:00:52,530
I won't commit.

12
00:00:55,050 --> 00:00:56,370
Cheque for 5000.

13
00:00:59,170 --> 00:00:59,920
Yep, it's free.

14
00:01:01,000 --> 00:01:05,920
Next thing to check is if LeBron Night is up and running for that, the commanders.

15
00:01:10,870 --> 00:01:11,020
US.

16
00:01:13,230 --> 00:01:14,340
Yes, my runner is running.

17
00:01:15,210 --> 00:01:17,250
And since it would be using Docker also.

18
00:01:17,520 --> 00:01:22,560
So the second thing you need to make sure is if your Docker is running, which Docker status we have

19
00:01:22,560 --> 00:01:26,640
command sudo system, it'll.

20
00:01:33,180 --> 00:01:34,380
It's also up and running.

21
00:01:35,910 --> 00:01:36,600
And Rosie.

22
00:01:37,680 --> 00:01:43,290
Now, let me show you the Docker version, since I need to explain a point relating to it around a common

23
00:01:43,290 --> 00:01:44,820
Docker version.

24
00:01:45,810 --> 00:01:48,240
Its version is twenty 20.0 10.0.

25
00:01:49,200 --> 00:01:49,920
One last thing?

26
00:01:50,340 --> 00:01:53,190
Let me list the Docker images my system currently has.

27
00:01:54,420 --> 00:01:56,040
For that we have come on Docker images.

28
00:01:57,000 --> 00:01:57,500
It returned.

29
00:01:57,510 --> 00:02:00,330
Nothing means currently there is no Docker image on my system.

30
00:02:01,080 --> 00:02:01,920
Everything is checked.

31
00:02:02,340 --> 00:02:03,660
Let's not commit those changes.

32
00:02:21,530 --> 00:02:22,430
Oops, it failed.

33
00:02:23,090 --> 00:02:25,340
Let's check out which our pipeline was weak.

34
00:02:28,450 --> 00:02:29,350
In the job.

35
00:02:33,770 --> 00:02:39,530
So in the logs in the start, it says running with GitLab version thirteen point twelve point zero on

36
00:02:39,530 --> 00:02:39,890
Linux.

37
00:02:40,430 --> 00:02:43,070
This is a similar note that we installed locally on our system.

38
00:02:43,730 --> 00:02:49,220
It means even though the pipeline was filled, but we managed to create a successful connection between

39
00:02:49,220 --> 00:02:51,050
local runner and GitLab instance.

40
00:02:52,040 --> 00:02:55,400
Then it is preparing the shell executed the executed.

41
00:02:55,460 --> 00:02:59,300
We registered with the specific runner, then at Line seven.

42
00:02:59,870 --> 00:03:01,670
It's running on my user VirtualBox.

43
00:03:01,910 --> 00:03:03,680
This is my Linux machine's username.

44
00:03:04,870 --> 00:03:05,350
And.

45
00:03:06,390 --> 00:03:14,100
At this line, it has executed the local version, come on, see, it has done the same local version,

46
00:03:14,100 --> 00:03:15,300
which my local system has.

47
00:03:15,990 --> 00:03:24,210
But when it tried to build the image from Docker fight this permission, Araki got permission denied

48
00:03:24,210 --> 00:03:25,920
while trying to connect with the Docker element.

49
00:03:26,550 --> 00:03:31,280
Well, guys, since in this case we are trying to run Docker engine from our local Git library.

50
00:03:31,590 --> 00:03:37,470
So this error came up because the GitLab runner by default can't access that Docker engine as it lacks

51
00:03:37,470 --> 00:03:40,800
the permissions to access the socket to communicate with the engine.

52
00:03:41,700 --> 00:03:45,450
So to fix this issue, we have to add get LeBron up to the Docker group.

53
00:03:46,300 --> 00:03:49,930
For that, you have to run this command and terminal.

54
00:03:53,490 --> 00:03:54,720
Sudo user mode.

55
00:03:58,900 --> 00:04:00,880
Dr got LeBron.

56
00:04:04,370 --> 00:04:05,810
And then restart the doctor.

57
00:04:15,420 --> 00:04:19,480
So basically at the Get LeBron Docker group and then restart Docker.

58
00:04:20,430 --> 00:04:24,280
Let's retry our pipeline again manually, this time we have to do.

59
00:04:24,300 --> 00:04:30,000
Since there is no shortage either, you can directly do it from here or for more options.

60
00:04:30,480 --> 00:04:32,610
Navigate to the project, usually pipelines.

61
00:04:34,570 --> 00:04:36,490
From here, click on the pipeline button.

62
00:04:40,830 --> 00:04:46,200
It will ask you to choose the branch name or tag under which you want to run the pipeline for Monster

63
00:04:46,200 --> 00:04:48,980
is the only branch I have from this variable field.

64
00:04:48,990 --> 00:04:52,650
You can enter any environment variables that are required for this pipeline to run.

65
00:04:53,010 --> 00:04:54,750
Variables, however, will cover letter.

66
00:04:55,660 --> 00:04:55,960
I don't.

67
00:05:02,450 --> 00:05:04,910
OK, so the bill stages successfully completed.

68
00:05:08,770 --> 00:05:15,070
As you can see, while building the image from Docker fight first, it has pulled the Python image from

69
00:05:15,070 --> 00:05:15,610
Docker hub.

70
00:05:16,920 --> 00:05:17,310
Then.

71
00:05:20,110 --> 00:05:24,550
Then it executed step two and step three from the local fight.

72
00:05:26,640 --> 00:05:29,970
At step four, it installed a flash module on that image.

73
00:05:32,550 --> 00:05:34,470
Don't worry, this is just a warning.

74
00:05:35,910 --> 00:05:42,420
And and then it here at step five, it has had the environment variable name to mark value.

75
00:05:43,810 --> 00:05:48,400
Then last at step six, it executed the command python abnormally.

76
00:05:50,080 --> 00:05:51,520
And finally, the job succeeded.

77
00:05:55,380 --> 00:05:56,100
Or to deploy.

78
00:06:00,220 --> 00:06:01,450
It is still in pending state.

79
00:06:01,810 --> 00:06:03,010
I'll wait for its completion.

80
00:06:07,120 --> 00:06:09,550
Deploy also passed in this job.

81
00:06:09,820 --> 00:06:16,360
We have only one command to run Image's container, so it did the same now to see the running gap open

82
00:06:16,360 --> 00:06:16,990
up the browser.

83
00:06:18,510 --> 00:06:19,830
And visit address.

84
00:06:23,920 --> 00:06:25,840
Remember both five thousand?

85
00:06:26,560 --> 00:06:27,130
Here we go.

86
00:06:27,400 --> 00:06:28,420
Happy birthday, Mark.

87
00:06:28,690 --> 00:06:30,830
We had Marcus coming from the environment variable.

88
00:06:30,850 --> 00:06:33,810
We sat in fight right now, guys.

89
00:06:33,830 --> 00:06:38,650
You might be wondering that why this time that deployed jobs succeeded even when there were no artifacts

90
00:06:38,650 --> 00:06:44,230
used in the previous gnawed example, the deployed job was failing until and unless we were not storing

91
00:06:44,230 --> 00:06:46,270
the artifacts from built up, right?

92
00:06:46,660 --> 00:06:47,560
So what differentiate?

93
00:06:48,160 --> 00:06:53,740
The difference is that this time we have used local donut, which is managed by us since we are not

94
00:06:53,740 --> 00:06:55,150
using any GitLab resources.

95
00:06:55,420 --> 00:06:57,430
So the results produced by the better job.

96
00:06:57,640 --> 00:07:02,260
That is, a Docker image is not automatically deleted after the bridge of completion.

97
00:07:02,650 --> 00:07:05,440
They remain intact and can be used by subsequent jobs.

98
00:07:05,920 --> 00:07:09,010
This is the reason why deploy job did not fail for this pipeline.

99
00:07:09,520 --> 00:07:10,600
I can even show it to you.

100
00:07:11,260 --> 00:07:17,770
The belly job has completed, but still that bright image would be present in my system, and the terminal

101
00:07:18,460 --> 00:07:21,220
now run Docker images.

102
00:07:23,040 --> 00:07:29,220
See, even after the job completion, we have to Docker images, the Python three Dot 8.0 slim and are

103
00:07:29,220 --> 00:07:30,210
all built by a.

104
00:07:31,230 --> 00:07:36,330
And similarly, if I list the running containers who list it, we have brokered peace.

105
00:07:37,860 --> 00:07:43,710
We have one running container, but this container ID image by Bieb and names buy up container.

106
00:07:44,610 --> 00:07:49,550
Even if you kill this application, the artifacts will still be present in our system until you go ahead

107
00:07:49,560 --> 00:07:50,640
and Manila delete it.

108
00:07:50,940 --> 00:07:52,200
I hope I made my point clear.

109
00:07:53,040 --> 00:07:54,840
So, guys, this is the power of GitLab.

110
00:07:55,230 --> 00:08:00,330
We just rolled the pipeline and it's self-build and deployed app on a local server automatically.

111
00:08:00,960 --> 00:08:03,270
Let's continue discussing more on it in the next class.
