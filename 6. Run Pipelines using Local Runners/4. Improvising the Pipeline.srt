1
00:00:00,720 --> 00:00:03,150
Welcome, guys, to this last lecture of local donors.

2
00:00:03,750 --> 00:00:09,180
I will give you an additional piece of information while learning pipelines in and managed donors to

3
00:00:09,180 --> 00:00:10,050
go with this lecture.

4
00:00:10,170 --> 00:00:14,940
I'll make a change in some code and try to read in the pipeline and see what happens.

5
00:00:16,290 --> 00:00:17,970
I will change the environment variable.

6
00:00:24,820 --> 00:00:26,650
Change this to summer committed.

7
00:00:41,630 --> 00:00:46,820
The pipeline filled build job was a success, but there was something wrong and the deployed job.

8
00:00:47,600 --> 00:00:48,020
Let's see.

9
00:00:50,430 --> 00:00:55,410
So the error says there was a conflict as a container nearby container is already in use.

10
00:00:56,100 --> 00:00:58,080
You have to remove or rename that container.

11
00:00:58,140 --> 00:01:02,970
It will be able to reuse that name as it is pretty much clear from the error description itself.

12
00:01:03,450 --> 00:01:09,330
This time, Docker was unable to run and launched a container with the name by app container as the

13
00:01:09,330 --> 00:01:13,290
same container name is already in use by a container with this I.D..

14
00:01:14,760 --> 00:01:16,980
If I show you on terminal.

15
00:01:21,560 --> 00:01:22,010
Sorry.

16
00:01:25,520 --> 00:01:29,900
See, we have one container with container rated this and named by a container.

17
00:01:30,500 --> 00:01:35,990
So that's why when we ran our pipeline again, the Docker anchorman tried to run the container with

18
00:01:35,990 --> 00:01:40,940
the pipe container and hence the conflict arises not to solve this conflict.

19
00:01:41,270 --> 00:01:46,610
What we need to do is we need to first stop and then remove this container, Macao.

20
00:01:47,360 --> 00:01:52,940
Either way, you can manually go to Docker Terminal and on some Docker commands and remove it, or you

21
00:01:52,940 --> 00:01:56,300
can even write some additional steps in the pipeline itself to automate it.

22
00:01:56,720 --> 00:02:01,670
I'll show it both ways to stop the container manually in the terminal, right?

23
00:02:01,670 --> 00:02:07,790
The Docker command Docker stop and then pass the container right.

24
00:02:07,930 --> 00:02:08,720
You want to stop.

25
00:02:15,690 --> 00:02:21,480
It stopped and then removed this container for that, and Dr. Adam.

26
00:02:25,430 --> 00:02:28,610
Containers removed next to Docker piece again.

27
00:02:29,880 --> 00:02:33,150
We have no more running containers container being removed.

28
00:02:33,270 --> 00:02:34,770
Let us run the pipeline manually.

29
00:02:54,200 --> 00:02:54,710
Excellent.

30
00:02:55,130 --> 00:02:58,250
Our pipeline is marked as bust means it was a success.

31
00:02:59,030 --> 00:03:03,080
This was a manual way of doing it, but obviously it's not a real time approach.

32
00:03:03,680 --> 00:03:10,310
Collapse is all about automation, so we need to avoid this manual activity to solve this problem permanently

33
00:03:10,310 --> 00:03:11,120
and automatically.

34
00:03:11,450 --> 00:03:15,020
We need to put the Docker stop and remove command in our pipeline.

35
00:03:29,330 --> 00:03:31,940
Let me first dive the whole command, then I will explain.

36
00:03:50,620 --> 00:03:57,160
So let me first explain the working of these two things double pipeline and double ampersand novel pipeline

37
00:03:57,160 --> 00:04:03,130
means if the first command before the double pipe symbol succeeds, the second command after the double

38
00:04:03,130 --> 00:04:04,960
pipe will never be executed.

39
00:04:05,350 --> 00:04:08,830
But in case the first fails, then only the second command will run.

40
00:04:09,490 --> 00:04:15,340
For example, if I have these two processes silly named electric as first command and the second command

41
00:04:15,340 --> 00:04:19,480
after a double pipe is m m directly this command.

42
00:04:19,660 --> 00:04:25,960
When done first, it will try to go to America if the directory is already dead and the system will

43
00:04:25,960 --> 00:04:27,370
simply go into that directory.

44
00:04:27,760 --> 00:04:34,390
But if the directory is not already created means the first command will fail, then the second command

45
00:04:34,390 --> 00:04:38,230
of the rhetoric will come into action and attempt to retrieve will be created.

46
00:04:39,990 --> 00:04:46,980
Talking about the billion percent percent are in short and operator is used to separate a list of commands

47
00:04:47,220 --> 00:04:51,000
that you want to run sequentially if used in between two commands.

48
00:04:51,330 --> 00:04:54,570
The second command will run only if the last did not fail.

49
00:04:55,140 --> 00:05:00,000
So, for example, if I have this to command, unzip a file within a particular directly.

50
00:05:00,450 --> 00:05:05,880
Obviously, you should be present in that directory to unzip the files to the commands can be arranged

51
00:05:05,880 --> 00:05:07,950
in this manner using double ampersand.

52
00:05:08,550 --> 00:05:15,480
This means the second dark command to unzip the files will only run once the first Zilli command to

53
00:05:15,480 --> 00:05:17,640
go to that directory has run successfully.

54
00:05:17,970 --> 00:05:19,620
I hope you understood these two operators.

55
00:05:20,430 --> 00:05:26,160
Now, if we relate this understanding to our application as part of our application requirements, remove

56
00:05:26,160 --> 00:05:28,620
command should only run if the container is stopped.

57
00:05:29,100 --> 00:05:35,310
That is why you should use double ampersand operator between stop and remove Docker commands with stop

58
00:05:35,310 --> 00:05:36,570
obviously being written first.

59
00:05:37,620 --> 00:05:42,940
Now, why I used this double pipe operator, this is to align with the first run of this pipeline.

60
00:05:43,650 --> 00:05:49,680
If this double pipe route would not have been there, then for the first time when this pipeline would

61
00:05:49,680 --> 00:05:52,980
run, there is no container running at that time.

62
00:05:53,220 --> 00:05:58,290
Fighting this stop container command is definitely going to throw an error, saying that there is no

63
00:05:58,290 --> 00:06:01,510
pipe container to prevent this unwanted error.

64
00:06:01,800 --> 00:06:05,370
Double pipe operator withdrawal is used in the first run.

65
00:06:05,640 --> 00:06:08,970
This document will fail and a would be written.

66
00:06:09,940 --> 00:06:16,660
After true, due to this double 10 percent, this second command would be tried here until they remove

67
00:06:16,660 --> 00:06:18,010
container command will fail.

68
00:06:18,520 --> 00:06:23,530
So again, a truce would be returned here as well, which eventually means that for the first run,

69
00:06:23,890 --> 00:06:28,720
this whole command is going to return us through and will pass on the control to the next commander.

70
00:06:29,530 --> 00:06:34,960
So you see, playing smartly with double pipe and double ampersand, we have covered all the scenarios

71
00:06:34,960 --> 00:06:37,060
of stopping and removing the containers.

72
00:06:38,590 --> 00:06:39,010
Great.

73
00:06:39,520 --> 00:06:43,660
Now, before I commit, let's list the running Docker container.

74
00:06:49,450 --> 00:06:51,690
Editor's note That Container Radio.

75
00:06:52,980 --> 00:06:53,580
No comment.

76
00:07:10,880 --> 00:07:15,170
So, Bob, the pipeline is a success, and if we check the containers.

77
00:07:21,700 --> 00:07:26,380
The earlier container was killed and a new Docker container is launched with this container, right?

78
00:07:27,680 --> 00:07:32,960
Not wrapping up this lecture, if you want to check this code for the first round of a pipeline, then.

79
00:07:44,420 --> 00:07:46,250
Let me change the container name here.

80
00:07:53,210 --> 00:07:56,030
This is a new container with no running instance currently.

81
00:07:57,720 --> 00:08:02,940
I also need to change this boat as 5000 is already occupied by Bieb container.

82
00:08:03,840 --> 00:08:05,730
Let me make it appear the only.

83
00:08:25,900 --> 00:08:31,120
Good record is perfectly running fine for the first one is that all the senators got cover.

84
00:08:31,420 --> 00:08:33,880
And with this, this action also comes to an end.

85
00:08:34,120 --> 00:08:34,540
Thank you.
