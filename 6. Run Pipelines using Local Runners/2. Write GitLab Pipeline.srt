1
00:00:02,780 --> 00:00:03,890
Creative fight.

2
00:00:10,180 --> 00:00:15,790
Starting with, we will have a better job that will run the Docker file we have written and would prepare

3
00:00:15,790 --> 00:00:18,340
the required environment for the application to run.

4
00:00:22,850 --> 00:00:24,020
And inside script.

5
00:00:25,230 --> 00:00:27,150
Let's first print out the Docker version.

6
00:00:27,510 --> 00:00:30,750
This is optional, though, for that we have a Docker command.

7
00:00:33,870 --> 00:00:34,620
No question.

8
00:00:35,130 --> 00:00:40,800
And in the next command, we will actually build the image from the Docker fight to build an image out

9
00:00:40,800 --> 00:00:41,580
of Docker file.

10
00:00:41,670 --> 00:00:43,230
We have command Docker build.

11
00:00:48,290 --> 00:00:53,900
And then pass the flag to tag this new image with the name, which I will keep say.

12
00:00:55,280 --> 00:00:55,880
Bieb.

13
00:00:58,340 --> 00:01:04,310
And it does specify the part of Docker file with respect to this file, calling it since in this case,

14
00:01:05,210 --> 00:01:05,930
if I show you.

15
00:01:14,350 --> 00:01:16,780
All these files are placed at the same location.

16
00:01:17,230 --> 00:01:19,930
So for part, I will simply type.

17
00:01:20,090 --> 00:01:23,740
Dot Dot means the current directory you are in.

18
00:01:24,870 --> 00:01:30,930
OK, so when this job, well done, it will first print out the local doctor version, then build an

19
00:01:30,930 --> 00:01:34,050
image from the doctor fight and store it locally on our system.

20
00:01:35,140 --> 00:01:40,660
No, by default, as we know, if we do not specify that tax explicitly, gut level two is the default

21
00:01:40,660 --> 00:01:43,360
on it now to make it pick the local runner.

22
00:01:43,390 --> 00:01:44,440
We need to add tags.

23
00:01:50,050 --> 00:01:55,880
So tax would be if you remember, I have just heard of my local run it with local shell and local runner

24
00:01:55,900 --> 00:01:56,350
bags.

25
00:01:56,740 --> 00:01:57,730
I will add both of them.

26
00:02:04,480 --> 00:02:09,100
Good with this are Belgium is complete after the images are created.

27
00:02:09,220 --> 00:02:09,910
What's next?

28
00:02:10,750 --> 00:02:13,150
The next step is to run that image as a container.

29
00:02:13,390 --> 00:02:15,160
That is the deploy phase, right?

30
00:02:15,850 --> 00:02:17,260
Let's add that job as well.

31
00:02:20,590 --> 00:02:21,310
Deploy.

32
00:02:24,110 --> 00:02:25,010
Under the script.

33
00:02:27,520 --> 00:02:33,220
Duran Docker image as a container, I will write a command to create an instance of the image, or,

34
00:02:33,220 --> 00:02:37,450
you can say, to create a container, which is same runnable instance of an image.

35
00:02:38,170 --> 00:02:38,900
So Docker.

36
00:02:42,830 --> 00:02:48,920
Then a flag d d is for detached mode to run this command in background and feed that terminal.

37
00:02:49,310 --> 00:02:52,910
Remember, we have run that no Dodgers application in background.

38
00:02:53,180 --> 00:02:58,460
By rerouting the execution to null directory, Docker has simply given us a D flag for the same.

39
00:02:58,910 --> 00:03:02,420
You can run the applications in background by running them in detached mode.

40
00:03:04,010 --> 00:03:08,480
Continue writing the command post the name of the container using name option.

41
00:03:10,600 --> 00:03:15,730
You can provide any name to it, let's say, by, yep, container.

42
00:03:17,340 --> 00:03:18,540
Now an important thing.

43
00:03:19,050 --> 00:03:24,690
How are you going to make sure that the final output received on the browser of our Horsely Nic machine

44
00:03:24,870 --> 00:03:27,450
and not on something which is inside the Docker container?

45
00:03:28,170 --> 00:03:32,550
See, since flask and everything is installed and going to run inside a container.

46
00:03:33,030 --> 00:03:38,430
So you need to make sure that the output of the code should somehow reflect back to your host browser

47
00:03:38,970 --> 00:03:40,590
and not something within that container.

48
00:03:41,160 --> 00:03:43,830
For that, I will add one more flag a.

49
00:03:45,720 --> 00:03:46,620
At the port, no.

50
00:03:49,090 --> 00:03:55,690
So with this be flag and port number, we are exposing the containers, put it on the host, would it

51
00:03:55,690 --> 00:03:55,900
be?

52
00:03:57,600 --> 00:04:03,390
So with this selective port forwarding, the application, which was set to run on Port 880, would

53
00:04:03,390 --> 00:04:05,520
be now exposed to host Port 80.

54
00:04:05,910 --> 00:04:08,280
And we would be able to see it at this address.

55
00:04:08,490 --> 00:04:08,970
Understood.

56
00:04:10,140 --> 00:04:15,320
Finishing the command at last, we will have the image name that we want to run as container, which

57
00:04:15,390 --> 00:04:16,130
is by a.

58
00:04:18,710 --> 00:04:23,990
Next, since this job would also require a local honor, so I'll paste the above tags here as well.

59
00:04:31,320 --> 00:04:31,740
All right.

60
00:04:32,100 --> 00:04:33,300
Lou Dobbs being defined.

61
00:04:33,690 --> 00:04:35,170
We have still left with one last.

62
00:04:35,790 --> 00:04:36,390
Any guesses?

63
00:04:40,020 --> 00:04:43,110
We are left with stages to specify the order of jobs.

64
00:04:43,470 --> 00:04:44,130
Let me add it.

65
00:04:58,520 --> 00:04:59,170
With ordering.

66
00:05:04,960 --> 00:05:06,850
Obviously, big job, well done first.

67
00:05:10,800 --> 00:05:11,610
Then the deployed.

68
00:05:15,530 --> 00:05:16,040
Wonderful.

69
00:05:16,400 --> 00:05:18,080
So with this, our pipeline is ready.

70
00:05:18,380 --> 00:05:20,090
Let us try to run it in next lecture.
