1
00:00:01,380 --> 00:00:06,750
Let's start installing runner on local machine, which of course, as we just learned, would be a separate

2
00:00:06,750 --> 00:00:12,730
infrastructure from where GitLab instance is installed to install a runner in the browser.

3
00:00:12,750 --> 00:00:13,830
Simply search for.

4
00:00:18,400 --> 00:00:19,380
His target not.

5
00:00:21,150 --> 00:00:23,190
Open this first link from Wet Lab.

6
00:00:24,540 --> 00:00:26,940
This link is already attached in the Resources tab.

7
00:00:28,850 --> 00:00:29,750
See, it says here.

8
00:00:30,140 --> 00:00:36,320
Get LeBron, it can be installed and he was jianyu, Linux, Mac, OS, FreeBSD and Windows.

9
00:00:37,490 --> 00:00:39,710
Minus Linux system, so I will open.

10
00:00:43,710 --> 00:00:48,630
This guide for Linux, in case if you have different host, then you can visit the guide accordingly.

11
00:00:49,720 --> 00:00:52,480
Installation for all systems is pretty easy and similar.

12
00:00:53,880 --> 00:00:56,760
What installation will follow the steps described here?

13
00:00:58,420 --> 00:01:03,400
First, we need to download the package for our system to download, we are given this command.

14
00:01:04,210 --> 00:01:07,150
This call command may vary according to system architecture.

15
00:01:07,540 --> 00:01:11,140
So first, you need to know the architecture of your system and then.

16
00:01:12,650 --> 00:01:16,100
Replace this with the architecture value.

17
00:01:16,880 --> 00:01:17,930
Let's copy this command.

18
00:01:19,740 --> 00:01:24,300
And in the terminal window, let me first find the architecture.

19
00:01:25,270 --> 00:01:26,560
For that, we have a command.

20
00:01:34,820 --> 00:01:38,210
Minus AMD64 not based the command.

21
00:01:40,630 --> 00:01:42,340
And replace this R-value.

22
00:01:44,980 --> 00:01:46,480
It AMD64.

23
00:01:47,820 --> 00:01:52,380
Nice in case if Golkar Command is not installed in your system, then you can install it by running

24
00:01:52,380 --> 00:01:53,580
this command in terminal.

25
00:01:54,640 --> 00:01:55,600
Mine is already dead.

26
00:01:58,280 --> 00:02:00,200
The package is around 135.

27
00:02:02,980 --> 00:02:03,790
Downloads done.

28
00:02:04,180 --> 00:02:04,780
If I do.

29
00:02:04,890 --> 00:02:12,580
Let's hear yup, we got to get LeBron a package, download on our system and go back to documentation.

30
00:02:14,840 --> 00:02:16,400
Next step is to install it.

31
00:02:16,790 --> 00:02:19,520
For that, we have this installation command of it.

32
00:02:24,030 --> 00:02:26,550
Replace the FCA with.

33
00:02:31,730 --> 00:02:35,060
Edit the requested operation requires somebody who's a privilege.

34
00:02:35,270 --> 00:02:35,660
OK?

35
00:02:36,200 --> 00:02:37,220
Simply type sudo.

36
00:02:45,120 --> 00:02:45,810
Installing.

37
00:02:48,550 --> 00:02:48,850
Don't.

38
00:02:50,430 --> 00:02:53,250
If LeBron is installed, you can check version of it.

39
00:02:54,630 --> 00:02:55,470
Don't get lab.

40
00:03:00,510 --> 00:03:03,210
It's volume thirteen point twelve point zero.

41
00:03:04,350 --> 00:03:05,970
Look at the list of available commands.

42
00:03:06,120 --> 00:03:07,950
You can also get LeBron or help.

43
00:03:11,110 --> 00:03:13,390
These are the list of available internet commands.

44
00:03:14,260 --> 00:03:14,670
All right.

45
00:03:14,710 --> 00:03:20,350
So the runner being installed at this point does in many ways are both GitLab runner and GitLab server

46
00:03:20,530 --> 00:03:22,310
are located at two different locations.

47
00:03:22,660 --> 00:03:27,430
But GitLab runner installed on your local system and GitLab instance on GitLab servers.

48
00:03:27,850 --> 00:03:32,260
Now the question is how do we build a connection between them so they can work cohesively?

49
00:03:32,770 --> 00:03:37,300
Well, the answer is by registering that installed on or GitLab server instance.

50
00:03:37,810 --> 00:03:43,360
When we register a runner, we are setting up communication between our GitLab instance and the machine

51
00:03:43,360 --> 00:03:46,570
where GitLab runner is installed in terminal.

52
00:03:46,990 --> 00:03:50,170
In this list of commands, you can see to register a runner.

53
00:03:50,770 --> 00:03:52,360
We have this command register.

54
00:03:53,170 --> 00:03:59,140
Registering a runner would require somebody was privileges so that this.

55
00:04:07,160 --> 00:04:09,420
It says enter the GitLab, you're.

56
00:04:10,010 --> 00:04:13,640
You need to provide a URL where your GitLab server instance is installed.

57
00:04:14,360 --> 00:04:15,170
Is this your role in?

58
00:04:15,170 --> 00:04:17,600
Most of the cases will remain the address of GitLab.

59
00:04:17,600 --> 00:04:23,150
Not commonly, but in case if you would have installed GitLab instance on some specific server, then

60
00:04:23,150 --> 00:04:25,010
you have to specify this URL accordingly.

61
00:04:25,430 --> 00:04:28,280
Since right now on GitHub, it is GitLab dot com.

62
00:04:28,640 --> 00:04:29,660
So it will specify.

63
00:04:36,080 --> 00:04:38,630
Then it's asking to enter the registration token.

64
00:04:39,170 --> 00:04:41,690
This token you will get from the Getler project settings.

65
00:04:49,260 --> 00:04:49,770
Under the.

66
00:04:51,680 --> 00:04:52,880
Under that A.S..

67
00:04:58,430 --> 00:04:58,940
Here it is.

68
00:05:00,080 --> 00:05:04,670
Guys, this token is the thing that will connect your GitLab instance with whatsoever, another you

69
00:05:04,680 --> 00:05:08,940
want and if you want, you can also decide this token from this button.

70
00:05:10,510 --> 00:05:11,040
Copyright.

71
00:05:12,980 --> 00:05:13,370
Based.

72
00:05:17,610 --> 00:05:19,950
It's asking us to enter some random description.

73
00:05:20,280 --> 00:05:26,250
Let's say Linux, maxed out bags, bags we have already seen in the last lecture.

74
00:05:26,850 --> 00:05:27,720
Let the tax be.

75
00:05:29,920 --> 00:05:33,670
Local runner, multiple tags are separated by a comma.

76
00:05:36,320 --> 00:05:37,190
Local shell.

77
00:05:38,700 --> 00:05:40,740
You're going to want to sign this tax letter as well.

78
00:05:45,560 --> 00:05:51,410
Registering or not succeeded last, it is asking us to enter and execute the registered owner.

79
00:05:51,500 --> 00:05:52,730
You must use and execute.

80
00:05:53,150 --> 00:05:54,560
And what is this executor?

81
00:05:54,920 --> 00:06:01,160
An executor determines the environment in which each job runs in GitLab or not implements a number of

82
00:06:01,160 --> 00:06:05,000
executors that can be used to deliver bills in different scenarios.

83
00:06:05,540 --> 00:06:11,270
For example, if you want your Cassilly job to run PowerShell commands, you might install GitLab,

84
00:06:11,270 --> 00:06:17,270
run it on a Windows server and then register a runner that uses the shell executable on in case.

85
00:06:17,540 --> 00:06:23,230
If you want your Kayseri job to run commands in a custom Docker container, you might install GitLab

86
00:06:23,360 --> 00:06:27,850
not on a Linux server and a registered owner that uses that Docker execute.

87
00:06:28,610 --> 00:06:34,250
In our case, for the example that I'm going to create next, I will simply choose a basic executor

88
00:06:34,250 --> 00:06:34,970
to configure.

89
00:06:35,180 --> 00:06:36,800
That is the shall execute that.

90
00:06:37,250 --> 00:06:38,990
So type shall.

91
00:06:42,320 --> 00:06:42,800
Excellent.

92
00:06:43,070 --> 00:06:44,870
A runner is registered successfully.

93
00:06:45,590 --> 00:06:50,390
Finally, we need to restart to get LeBron of service to restart, get LeBron up.

94
00:06:50,420 --> 00:06:51,320
We have a command.

95
00:06:52,040 --> 00:06:53,150
Remember with Soto?

96
00:07:00,510 --> 00:07:03,960
Now, let's go back to get away and see if our runoff shows up there.

97
00:07:07,470 --> 00:07:08,040
Refresh.

98
00:07:15,700 --> 00:07:16,360
Here we go.

99
00:07:16,600 --> 00:07:24,460
We got a runner in the available analyst with its description tags and all of the things you can pause

100
00:07:24,460 --> 00:07:27,010
and remove this runner from these two buttons, respectively.

101
00:07:27,850 --> 00:07:32,530
And in case if you want to change its configuration, then you can do it from this edited speech.

102
00:07:35,620 --> 00:07:38,230
You can change its description and some tags.

103
00:07:39,340 --> 00:07:44,200
All right, so we have Randy installed and have successfully registered it with GitLab instance.

104
00:07:44,620 --> 00:07:47,980
Now it's time to bring this are not in use and run a pipeline on it.
