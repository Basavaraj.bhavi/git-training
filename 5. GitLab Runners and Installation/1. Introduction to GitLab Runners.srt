1
00:00:00,630 --> 00:00:01,320
Welcome again.

2
00:00:01,830 --> 00:00:07,500
In the previous lectures, I was talking about runners and GitLab GitLab server itself does not do the

3
00:00:07,500 --> 00:00:08,130
execution.

4
00:00:08,490 --> 00:00:13,950
Rather, it has some dedicated agents called GitLab runners, and in the next few lectures, I'm going

5
00:00:13,950 --> 00:00:16,340
to discuss about those runners in detail.

6
00:00:17,220 --> 00:00:20,910
If you have questions like what happens behind the scenes when a pipeline runs?

7
00:00:21,180 --> 00:00:22,290
Who runs your pipeline?

8
00:00:22,620 --> 00:00:24,000
What does that actually perform?

9
00:00:24,000 --> 00:00:26,220
The tasks or jobs do you find in the pipeline?

10
00:00:26,520 --> 00:00:28,530
All your questions will be answered in these lectures.

11
00:00:29,280 --> 00:00:30,750
So what is Get LeBron?

12
00:00:31,650 --> 00:00:38,840
Get LeBron is a lightweight, highly scalable agent that works, but get left to pick and execute specialty

13
00:00:38,850 --> 00:00:39,240
jobs.

14
00:00:39,810 --> 00:00:43,770
Like I said, the jobs defined in our pipeline are not executed by GitLab.

15
00:00:43,770 --> 00:00:44,190
So what?

16
00:00:44,550 --> 00:00:50,760
It more likely manages to get GitLab related stuff like database, project repo, etc. to execute the

17
00:00:50,760 --> 00:00:54,270
jobs it has deployed a special agent, the GitLab or not.

18
00:00:54,900 --> 00:00:58,830
So basically, when necessary, pipeline is run behind the scenes.

19
00:00:58,950 --> 00:01:02,310
Get LeBron a close up project you read that get lepeska.

20
00:01:02,610 --> 00:01:08,380
My email fight performs the task accordingly and finally send the results back to GitLab instance.

21
00:01:09,360 --> 00:01:15,030
Again, guys, with this happening, don't underestimate the power of GitLab server as it remains the

22
00:01:15,030 --> 00:01:19,350
boss, though it does not execute the jobs defined by its own.

23
00:01:19,650 --> 00:01:22,740
But still, it is who manages this entire process.

24
00:01:23,130 --> 00:01:28,020
It makes sure that runners are working right, coordinate the things and all the management stuff.

25
00:01:29,760 --> 00:01:35,550
Get LeBron, it is open sourced and written in good language, so consider it as a grueling process

26
00:01:35,550 --> 00:01:37,650
that executes some instructor task.

27
00:01:39,500 --> 00:01:44,960
Depending upon your pipeline needs, GitLab offers you to scale by adding more owners into your GitLab

28
00:01:44,960 --> 00:01:45,590
architecture.

29
00:01:46,040 --> 00:01:49,820
So depending upon the situation, you can add or remove get cleverness.

30
00:01:51,340 --> 00:01:57,130
To start with and get you going fast, GitLab offers us a number of shareowners that are available to

31
00:01:57,130 --> 00:01:59,110
every project in a GitLab instance.

32
00:01:59,650 --> 00:02:05,530
These shareowners are maintained by GitLab team and since they are shared, so the availability of them

33
00:02:05,530 --> 00:02:06,670
varies from time to time.

34
00:02:07,330 --> 00:02:09,400
And please understand that below.

35
00:02:09,610 --> 00:02:14,380
In previous lectures, whenever we ran our pipeline, it was run with shareowners only.

36
00:02:16,070 --> 00:02:22,340
Other than shareowners, GitLab also offers us to install get Lebanon on application on the infrastructure

37
00:02:22,370 --> 00:02:24,080
that we own or manage.

38
00:02:24,560 --> 00:02:28,400
That infrastructure can be a server, desktop or anything like that.

39
00:02:29,180 --> 00:02:33,260
Well, coming in the next lecture, I'm also going to do the same with an example.

40
00:02:33,620 --> 00:02:37,760
I will install a liberal note on my local system and try to run a pipeline on it.

41
00:02:38,990 --> 00:02:42,140
OK, so these were a few introductory points about us.

42
00:02:42,500 --> 00:02:45,710
In the next lecture, we will interact with these learners from UAE.
