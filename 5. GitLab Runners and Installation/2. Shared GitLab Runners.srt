1
00:00:00,750 --> 00:00:04,770
Let us now hop up to get Libya and see from where we can access it on us.

2
00:00:06,250 --> 00:00:10,860
I have created a new empty project for this demo under the project settings.

3
00:00:12,320 --> 00:00:12,830
Good to see.

4
00:00:16,340 --> 00:00:18,110
Here we have that intersection.

5
00:00:19,690 --> 00:00:20,200
Expanded.

6
00:00:24,820 --> 00:00:28,360
We have specific donors and share donors at this moment.

7
00:00:30,480 --> 00:00:32,850
Fifteen shareholders are available to us.

8
00:00:34,290 --> 00:00:37,560
We have GitLab doctor shareholder manager.

9
00:00:37,620 --> 00:00:41,790
Zero two is one such run out, then manager.

10
00:00:41,790 --> 00:00:42,800
Zero three is one more.

11
00:00:42,930 --> 00:00:43,110
Our.

12
00:00:44,140 --> 00:00:47,510
We have windows here on us manager to seem like this.

13
00:00:47,530 --> 00:00:51,430
We have 15 runners available as of now with each runner.

14
00:00:51,460 --> 00:00:53,300
You can see some tags are associated.

15
00:00:53,680 --> 00:00:59,450
Bags are important as which job from a pipeline goes to visit or not depends on this deck.

16
00:01:00,020 --> 00:01:04,540
Actually, when you define a job, you can add one more directive a tag in it.

17
00:01:05,020 --> 00:01:09,820
For example, while defining the belt job, you're going to add a tag with the value built in it.

18
00:01:10,150 --> 00:01:15,490
Then what will happen is while using the runners for better job get level, check your foot on it with

19
00:01:15,490 --> 00:01:17,590
a tag value build is available.

20
00:01:17,920 --> 00:01:24,340
If yes, that that runner will pick up that belt job so you can see bags are used to select a specific

21
00:01:24,340 --> 00:01:27,970
runner from the list of all runners that are available for the project.

22
00:01:28,900 --> 00:01:36,850
Talking about the tag values, we have tags like Docker, GCE, Linux, Mongo, Bicycle and so on.

23
00:01:37,360 --> 00:01:43,990
The tag name itself specifies what type of job or not can handle, and if you do not specify any tags

24
00:01:43,990 --> 00:01:48,670
in the job, then GitLab picks up a up from the pool of available runners at that time.

25
00:01:49,300 --> 00:01:49,940
One more thing?

26
00:01:50,260 --> 00:01:54,670
Sometimes, while running your pipeline, you will notice that your pipeline is in pending state.

27
00:01:55,150 --> 00:01:59,800
If that happens, it means at that moment the queue for shareowners is long.

28
00:02:00,010 --> 00:02:03,970
So your job may take some extra time to get complete and is.

29
00:02:05,200 --> 00:02:10,390
In case if you don't want get left to pick from shareowners, then you can disable it from here.

30
00:02:11,900 --> 00:02:14,450
Let us know quickly ran a pipeline that will use.

31
00:02:16,520 --> 00:02:21,080
This window staggering it notice it has given windows associated with.

32
00:02:22,440 --> 00:02:23,880
Back to project repository.

33
00:02:29,530 --> 00:02:32,440
Since it is a new one, I need to create a Gettler file.

34
00:02:39,870 --> 00:02:43,950
Inside the Yemen file, let's create a new job, save windows in for.

35
00:02:47,350 --> 00:02:49,120
And inside the script.

36
00:02:51,300 --> 00:02:55,800
Let's print out the detailed configuration of Windows OS four that we have a command.

37
00:02:57,940 --> 00:02:59,020
System in full.

38
00:03:00,040 --> 00:03:04,990
This system, in fact, will output the system configuration in four of the I know on which this job

39
00:03:04,990 --> 00:03:07,540
is going to run since it will be a shared on it.

40
00:03:07,750 --> 00:03:14,170
So the system configurations of that on is what we will see in the output and now use that window than

41
00:03:14,170 --> 00:03:16,870
it would need to specify its tag like this.

42
00:03:22,820 --> 00:03:28,820
The same deck as we saw in there, guys inside this DAGS configuration, you can specify multiple tags

43
00:03:28,910 --> 00:03:30,200
to distinguish between Dennis.

44
00:03:31,260 --> 00:03:31,740
That's it.

45
00:03:32,740 --> 00:03:33,240
The genius.

46
00:03:41,450 --> 00:03:42,470
Our pipeline is running.

47
00:03:42,890 --> 00:03:44,150
I'll wait till its completion.

48
00:03:47,500 --> 00:03:48,640
Pipeline got completed.

49
00:03:55,420 --> 00:03:57,010
So from the logs, you can see.

50
00:03:58,370 --> 00:04:04,520
This time, it says on windows shared on US manager that this I.D. copy this.

51
00:04:06,070 --> 00:04:08,380
And search it from the available schedule analyst.

52
00:04:21,970 --> 00:04:22,660
Controller.

53
00:04:26,070 --> 00:04:28,080
Here we go to perform that job.

54
00:04:28,560 --> 00:04:33,570
This get LeBron out was used, which was expected and the system in four Windows Command.

55
00:04:37,500 --> 00:04:40,590
Has returned us this much information about that Windows host.

56
00:04:41,850 --> 00:04:45,450
All right, so that was about the shared good Labradors and their dogs.

57
00:04:45,750 --> 00:04:50,970
And like I said in the coming lectures, I will try to create my own local runner and run a pipeline

58
00:04:50,970 --> 00:04:51,270
on it.

59
00:04:51,750 --> 00:04:51,980
To.
