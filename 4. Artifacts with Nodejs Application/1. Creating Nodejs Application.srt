1
00:00:00,500 --> 00:00:01,130
Hello, guys.

2
00:00:01,280 --> 00:00:05,780
In this lecture, we will learn about artifacts in that lab, we'll learn artifacts.

3
00:00:05,780 --> 00:00:11,390
We will directly start with an example where not only artifacts going through this lecture, we will

4
00:00:11,390 --> 00:00:13,460
hopefully learn some new things about pipelines.

5
00:00:14,720 --> 00:00:16,370
So I'll switch to a new project.

6
00:00:20,890 --> 00:00:26,630
I've already created a fresh project artifacts demo, how to create a new project.

7
00:00:26,650 --> 00:00:30,730
You already know let's clone this project to a local system.

8
00:00:36,910 --> 00:00:38,050
Go to my folder.

9
00:00:40,260 --> 00:00:40,800
Now run.

10
00:00:58,270 --> 00:01:00,220
It's done now, go to this project.

11
00:01:04,640 --> 00:01:06,280
Now, check out to the branch.

12
00:01:13,670 --> 00:01:16,400
So I'm in the branch of Artifacts Demo Project.

13
00:01:18,050 --> 00:01:18,500
Unless.

14
00:01:19,620 --> 00:01:20,570
We got nothing on it.

15
00:01:20,760 --> 00:01:23,400
We fight for better visualization of God.

16
00:01:23,430 --> 00:01:26,790
Let's take this project to Visual Studio code.

17
00:01:33,030 --> 00:01:35,850
This time we will create a very basic no application.

18
00:01:36,480 --> 00:01:42,870
I believe many of you would already be familiar with Node.js, but for others who don't know, Node.js

19
00:01:42,870 --> 00:01:48,750
is an open source cross-platform JavaScript runtime environment that executes JavaScript code outside

20
00:01:48,750 --> 00:01:49,440
a web browser.

21
00:01:50,250 --> 00:01:55,380
It is used for developing server side and networking applications that required a persistent connection

22
00:01:55,380 --> 00:01:56,880
from the browser to the server.

23
00:01:57,810 --> 00:02:03,750
No, this is often used for realtime applications such as chat, newsfeeds and web push notifications.

24
00:02:04,560 --> 00:02:10,880
Furthermore, in context to Node.js, there is a term npm actually, since I'm going to use this and

25
00:02:10,920 --> 00:02:12,240
pointing later in the code.

26
00:02:12,450 --> 00:02:17,910
So I thought clubmates explanation hit and BIM is acronym for Node Package Manager.

27
00:02:18,900 --> 00:02:25,470
NPM is a default package manager for the JavaScript runtime environment Node.js that lets you install

28
00:02:25,500 --> 00:02:30,330
third party libraries, plugins, frameworks and applications by using the command line.

29
00:02:31,050 --> 00:02:36,930
For example, to install X-Press Framework, you will run the Command NPM Install Express in command

30
00:02:36,930 --> 00:02:37,140
line.

31
00:02:38,230 --> 00:02:44,590
So these were very basic information of Node.js and being in case if you want to explore more, you

32
00:02:44,590 --> 00:02:48,910
can visit to their official website to the link given in the source just off this lecture.

33
00:02:49,870 --> 00:02:51,760
Anyway, moving back to the example.

34
00:02:53,390 --> 00:02:55,610
Let's create a new file index, gorgeous.

35
00:03:01,420 --> 00:03:07,030
And guys, since we are not in, you know, just development goals, so I won't deviate the course by

36
00:03:07,030 --> 00:03:09,220
creating that Node.js application from scratch.

37
00:03:09,700 --> 00:03:13,540
Rather, I will simply paste the code for this application and then explain it.

38
00:03:16,640 --> 00:03:17,810
The application is nothing.

39
00:03:18,110 --> 00:03:23,930
It simply uses the express or express gorgeous framework, which is a standard web application of a

40
00:03:23,930 --> 00:03:28,100
framework for Node.js applications who send back some response to request.

41
00:03:28,940 --> 00:03:30,650
Now giving you the overview of this code.

42
00:03:31,710 --> 00:03:34,770
As I said, this application will be using express framework.

43
00:03:35,310 --> 00:03:41,100
So at the very first line of code, we are declaring a variable express to contain the module express

44
00:03:41,100 --> 00:03:46,320
in it after requesting as this express module is actually a function.

45
00:03:46,770 --> 00:03:52,650
So in the next line of code, we are assigning the function call to another variable app, which will

46
00:03:52,650 --> 00:03:57,000
call it and will give us the access to that function just like an object us.

47
00:03:58,450 --> 00:04:04,000
Then in the next line of code, I am declaring a variable port that defines the port number on which

48
00:04:04,000 --> 00:04:05,110
the application will run.

49
00:04:05,540 --> 00:04:06,320
Export it.

50
00:04:08,500 --> 00:04:15,130
In this next block of code, we are routing the applications end point, so basically by listening for

51
00:04:15,130 --> 00:04:20,320
a get request, our application will send this line of message as a response.

52
00:04:21,490 --> 00:04:28,510
And at last, with this little method, it starts the server and listens on port defined above.

53
00:04:28,900 --> 00:04:30,940
With this starting message.

54
00:04:32,840 --> 00:04:33,330
All right.

55
00:04:33,380 --> 00:04:38,750
So with this are very basics of basic Node.js application, using express framework is ready.

56
00:04:39,940 --> 00:04:44,920
Now, guys looking at the code, it is obvious that our application requires some dependencies to run

57
00:04:45,280 --> 00:04:52,120
like an express framework as a prerequisite, so to handle and list down those product dependencies.

58
00:04:52,480 --> 00:04:58,810
The next thing we need to do is to create and define a package or disinvite actually in any note project,

59
00:04:59,050 --> 00:05:05,320
all in packages contain a file, usually in project, route or package decent adjacent fight.

60
00:05:05,860 --> 00:05:11,410
This fight holds all the metadata information about the project and is used for managing the project's

61
00:05:11,410 --> 00:05:13,330
dependencies, scripts, virgin.

62
00:05:13,330 --> 00:05:15,970
And so let's create that fight.

63
00:05:21,240 --> 00:05:22,350
Packaging or design?

64
00:05:23,740 --> 00:05:25,090
I'll paste those dependencies.

65
00:05:26,620 --> 00:05:32,980
OK, so here we have listed the product dependencies, which is expressed with version four point sixteen

66
00:05:32,980 --> 00:05:34,270
point one or greater.

67
00:05:35,050 --> 00:05:39,010
And in case if you have multiple dependencies, then you can list all of them here.

68
00:05:39,850 --> 00:05:46,090
See if both the files now the codes of our Node.js applications are ready.

69
00:05:46,540 --> 00:05:50,080
We have the application next fight and the dependencies backers got this and.

70
00:05:50,920 --> 00:05:55,060
Now let's be clear on this application manually in my local system to see the results.

71
00:05:59,190 --> 00:05:59,650
Claret.

72
00:06:00,540 --> 00:06:03,660
I'm already in my project to run the application locally.

73
00:06:03,690 --> 00:06:05,910
Plus, you need to install node on the system.

74
00:06:07,180 --> 00:06:11,110
Nice, if you're planning to start writing that little upcycled pipeline straight off.

75
00:06:11,380 --> 00:06:12,200
Yes, you can.

76
00:06:12,220 --> 00:06:17,740
You can skip this local park, but I am running this application in my local system purposely in order

77
00:06:17,740 --> 00:06:20,080
to demonstrate the transition from local to GitLab.

78
00:06:20,590 --> 00:06:25,750
Moreover, running this app locally would give us a hint of commands that we will need to specify in

79
00:06:25,750 --> 00:06:26,320
our pipeline.

80
00:06:26,560 --> 00:06:32,680
That is, the lepeska got my email file to build and deploy this app on the server as and when the error

81
00:06:32,680 --> 00:06:34,420
comes while running the application here.

82
00:06:34,810 --> 00:06:38,110
The solution to it, like it may be additional installation commands.

83
00:06:38,290 --> 00:06:43,990
Download external libraries all of this later, we will add all those commands into the Get Labs, the

84
00:06:43,990 --> 00:06:44,500
8.00am.

85
00:06:44,500 --> 00:06:51,670
And if I know before I install Node.js on my system for, say, Forsyte, let's run the update command.

86
00:07:09,990 --> 00:07:11,520
Now I will install an audience.

87
00:07:26,700 --> 00:07:27,060
Great.

88
00:07:27,450 --> 00:07:28,680
No, just being installed.

89
00:07:28,830 --> 00:07:30,210
Let's try to run the index flight.

90
00:07:36,700 --> 00:07:37,060
Oops!

91
00:07:37,240 --> 00:07:39,310
One added This was expected.

92
00:07:39,880 --> 00:07:45,580
Remember, we had used Ex-best Framework Index Dodgers code and since there is no module named Express

93
00:07:45,580 --> 00:07:46,780
as of now in this project.

94
00:07:47,080 --> 00:07:50,440
So it has thrown us at it and how to get the express framework.

95
00:07:50,860 --> 00:07:52,070
Well, run the package.

96
00:07:52,240 --> 00:07:55,300
Decent file in which all the dependencies are already defined.

97
00:07:59,280 --> 00:08:05,670
And being installed, you don't have to mention baggage charges and file them in the command as npm

98
00:08:05,670 --> 00:08:07,890
install automatically picks it up by default.

99
00:08:10,020 --> 00:08:10,280
Great.

100
00:08:10,590 --> 00:08:11,200
One, Moreira.

101
00:08:11,490 --> 00:08:16,680
Go on and beam is not found before we can actually use and beam tool, we need to install it first.

102
00:08:17,220 --> 00:08:22,560
For that, we have a command sudo apt get install npm.

103
00:08:32,210 --> 00:08:33,650
Note package manager installed.

104
00:08:34,580 --> 00:08:35,360
Let's try again.

105
00:08:45,450 --> 00:08:47,580
These are warnings only, but it's done.

106
00:08:48,620 --> 00:08:52,310
Let me make some space if I do less

107
00:08:55,220 --> 00:09:00,200
running package dodges and file has installed the requisite modules into this normally historically,

108
00:09:01,340 --> 00:09:03,410
OK, so I guess we are done with the prerequisites.

109
00:09:03,680 --> 00:09:05,450
If I try to run our application now.

110
00:09:11,880 --> 00:09:12,420
Excellent.

111
00:09:12,600 --> 00:09:15,270
Our application is listening at localhost 880.

112
00:09:15,750 --> 00:09:16,680
Let's open this link.

113
00:09:16,920 --> 00:09:17,820
Control Click.

114
00:09:21,730 --> 00:09:22,390
Here we go.

115
00:09:23,200 --> 00:09:25,690
It has returned us the response message success.

116
00:09:26,590 --> 00:09:29,710
So we have managed to run Node.js project on our local server.

117
00:09:30,160 --> 00:09:35,920
Now what we are left with is taking this project to GitLab repository and creating a pipeline by listing

118
00:09:35,920 --> 00:09:40,270
all the necessary steps and commands that we learn from here into the get labs.

119
00:09:40,270 --> 00:09:41,290
The 8.00am will fight.

120
00:09:41,680 --> 00:09:42,880
Let's do it in the next class.
