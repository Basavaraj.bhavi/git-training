1
00:00:00,680 --> 00:00:06,260
This is a continuation lecture of previous one where we will optimize this get lepeska the via will

2
00:00:06,260 --> 00:00:07,490
fight that we created.

3
00:00:08,120 --> 00:00:16,280
Let me take you to the logs of previous pipeline, run into the logs of the job and we'll have this

4
00:00:16,280 --> 00:00:17,150
long list of logs.

5
00:00:17,990 --> 00:00:21,140
Let me first tell you the problem in the way we created the pipeline.

6
00:00:22,010 --> 00:00:26,570
Now, as we have seen in the earlier lectures, we know current jobs get LeBron.

7
00:00:26,570 --> 00:00:29,000
US by default, uses the Docker ruby image.

8
00:00:29,390 --> 00:00:32,390
And here also you can see who executed this build job.

9
00:00:32,870 --> 00:00:38,990
GitLab How to use Docker Image Ruby Version 2.5, which of course, is capable to perform a ruby task

10
00:00:38,990 --> 00:00:40,280
and not Node.js.

11
00:00:40,790 --> 00:00:47,720
And with the default configuration in this ruby image, there is no such service as npm or Node.js preinstalled

12
00:00:47,720 --> 00:00:47,990
on it.

13
00:00:48,560 --> 00:00:52,040
But in this example, since we were dealing with node application.

14
00:00:52,340 --> 00:00:57,500
So anyhow, we needed npm and Node.js installations, that is way in pipeline.

15
00:00:59,760 --> 00:01:03,870
I had to explicitly install these services with these commands.

16
00:01:04,530 --> 00:01:05,670
Well, not a big deal.

17
00:01:05,700 --> 00:01:12,180
This can be done, but still you would feel that this is an overhead of first installing the basic services

18
00:01:12,390 --> 00:01:14,190
and then running the actual application.

19
00:01:14,760 --> 00:01:18,000
This is not an optimal way to run applications in real projects.

20
00:01:18,510 --> 00:01:24,450
Hence, we have to change the pipeline code in a way so that we can focus more on our application development

21
00:01:24,720 --> 00:01:27,060
rather than installing the basic environments for it.

22
00:01:27,540 --> 00:01:33,480
Imagine how easy would be if, instead of Ruby, we get an environment where all the required libraries

23
00:01:33,480 --> 00:01:36,780
and packages to run a Node.js application are pre-installed.

24
00:01:37,080 --> 00:01:41,940
In that case, we don't have to worry about the npm node installations and all the other stuff.

25
00:01:42,210 --> 00:01:43,890
You just focus on the application.

26
00:01:44,790 --> 00:01:49,710
Indeed, it would be of great help, and this is what I'm going to do by pulling a Docker node image

27
00:01:50,610 --> 00:01:52,120
as a solution to our problem.

28
00:01:52,170 --> 00:01:58,620
We will leverage a Docker image that is built specifically for Node.js applications and to say over

29
00:01:58,620 --> 00:02:01,110
time, I will directly edited this file in GitLab editor.

30
00:02:16,920 --> 00:02:17,610
And this.

31
00:02:20,010 --> 00:02:25,920
To instruct labor or not to use any specific Docker image, another job for which you want to use specific

32
00:02:25,920 --> 00:02:26,280
image.

33
00:02:26,490 --> 00:02:28,050
Add a new tag image.

34
00:02:34,530 --> 00:02:39,360
And then specify that image name for note, Dodge is the name would be not.

35
00:02:41,580 --> 00:02:42,060
That's it.

36
00:02:42,690 --> 00:02:45,930
And since we've shown the same image to be used for deploying job as well.

37
00:02:46,140 --> 00:02:47,820
So we need to edit there to.

38
00:02:52,090 --> 00:02:58,180
And now that is not good, image note that services and IBM a.j.'s are already installed so we can delete

39
00:02:58,210 --> 00:02:59,380
these installation tasks.

40
00:03:07,670 --> 00:03:13,410
So this way, when these jobs will run now, get LeBron will use Docker, not image than the default,

41
00:03:13,410 --> 00:03:15,380
Ruby won't commit the changes.

42
00:03:21,550 --> 00:03:22,480
Go to biplanes.

43
00:03:36,290 --> 00:03:37,100
Let's go to build.

44
00:03:41,410 --> 00:03:47,410
Great, as you can see for our military job this time that are not has started using Docker image node

45
00:03:47,560 --> 00:03:48,400
and not Ruby.

46
00:03:48,880 --> 00:03:50,920
Similarly, if we switch to deploy job.

47
00:03:57,240 --> 00:04:01,450
He had also Docker image, not environment and application is still running.

48
00:04:02,530 --> 00:04:07,690
So guys, depending upon your interest of job, you can choose a specific Docker image that is tailor

49
00:04:07,690 --> 00:04:08,710
fit for your application.

50
00:04:09,250 --> 00:04:10,630
Seemed like Docker, not image.

51
00:04:10,810 --> 00:04:12,940
There are a lot of images available on Docker hub.

52
00:04:25,050 --> 00:04:30,930
Dzokhar is a service provided by doctor for finding and sharing container images from Docker hub.

53
00:04:30,960 --> 00:04:36,450
You can download and leverage from thousands of Docker images that are buried officially or by any third

54
00:04:36,450 --> 00:04:37,350
party contributors.

55
00:04:38,360 --> 00:04:42,620
For example, for your Python use case, you can simply search for Python.

56
00:04:47,800 --> 00:04:50,590
And can use this official Python image.

57
00:04:51,250 --> 00:04:56,440
The good thing is, in case if you don't find any image, then Nokia also gives you the freedom to create

58
00:04:56,440 --> 00:04:57,490
your own image as well.

59
00:04:58,030 --> 00:05:03,340
And yeah, if you're very new to this Docker term itself, then I have added a basic Docker introduction

60
00:05:03,340 --> 00:05:05,220
lecture in the bonus section of this course.

61
00:05:05,540 --> 00:05:08,770
You can get a good knowledge of what is Docker and how it works from there.

62
00:05:09,070 --> 00:05:12,010
This was that lecture, and it's only recorded for newbies.

63
00:05:12,940 --> 00:05:16,630
And before I wrap up this lecture, let me give you an additional piece of information.

64
00:05:29,370 --> 00:05:30,540
No esperado use case.

65
00:05:30,780 --> 00:05:37,050
Both of our jobs require a Docker image node, so rather than specifying it separately for each job,

66
00:05:37,290 --> 00:05:40,160
you can also set it as global configuration like this.

67
00:05:42,130 --> 00:05:43,270
Let me comment on this.

68
00:05:45,180 --> 00:05:47,580
And this added globally.

69
00:05:53,440 --> 00:05:58,540
This configuration will also look the same, all the jobs in this pipeline would now be using Docker

70
00:05:58,540 --> 00:05:59,080
node image.

71
00:05:59,440 --> 00:06:02,170
There is no need to run it and that's it for this lecture.

72
00:06:02,320 --> 00:06:02,740
Thank you.
