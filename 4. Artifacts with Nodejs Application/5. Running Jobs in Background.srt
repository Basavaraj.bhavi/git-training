1
00:00:00,520 --> 00:00:05,650
Welcome, guys, to this good lecture, Reed, I'm going to tell you a little bit about running application

2
00:00:05,650 --> 00:00:11,890
in background, setting up the context of this lecture while you run the previous Node.js application.

3
00:00:12,250 --> 00:00:18,550
You will see that this application will continue listening on this localhost A-380 board as we have

4
00:00:18,550 --> 00:00:21,100
deployed the Node.js application on this book.

5
00:00:22,120 --> 00:00:25,480
So this deployed job would always be in the running state.

6
00:00:26,300 --> 00:00:28,570
Oh well, it will not run forever, actually.

7
00:00:28,960 --> 00:00:31,450
Rather, this job will run until it's time of time.

8
00:00:32,110 --> 00:00:36,550
As per the current settings, the current timeout is set to default of OneNote.

9
00:00:36,970 --> 00:00:42,560
So whether this deployed job will get automatically filled when it's duration, time exceeds the time

10
00:00:42,580 --> 00:00:42,940
of time.

11
00:00:43,540 --> 00:00:49,720
With this behavior, GitLab will not only kill this job, but also if there are any subsequent jobs

12
00:00:49,720 --> 00:00:56,170
defined after this job than those jobs or commands will never execute as the terminal is already occupied

13
00:00:56,170 --> 00:00:59,170
and stuck with this node application run command.

14
00:00:59,770 --> 00:01:06,130
And moreover, this pipeline is never going to be marked as success by because the deployed job is always

15
00:01:06,130 --> 00:01:10,570
going to get filled after a timeout leading the whole pipeline there just to be filled.

16
00:01:11,080 --> 00:01:11,860
This is a problem.

17
00:01:12,460 --> 00:01:18,010
Obviously, we cannot let the pipeline fail, as in our case, we know that the job perfectly worked

18
00:01:18,220 --> 00:01:20,260
and that application was successfully deployed.

19
00:01:21,010 --> 00:01:24,100
So how can we correct this behavior to correct this?

20
00:01:24,310 --> 00:01:29,830
What we can do is we can instruct who run this node application in background and output nothing.

21
00:01:30,700 --> 00:01:36,160
So basically, while running a processing background, we're detached our job from the terminal so that

22
00:01:36,160 --> 00:01:38,260
the console can be used for other commands.

23
00:01:39,530 --> 00:01:43,850
And for running this Node.js application in background, we have to modify.

24
00:01:58,810 --> 00:02:01,090
This node, different command.

25
00:02:02,930 --> 00:02:03,530
So here.

26
00:02:11,540 --> 00:02:19,250
So here this dev null means standard output goes to Dev NULL, which is a dummy device that does not

27
00:02:19,250 --> 00:02:20,180
record any notebook.

28
00:02:21,590 --> 00:02:27,680
And this thing means standard error also goes to the standard output, which we already do director

29
00:02:27,680 --> 00:02:33,980
to definite at last December's at the end means I run this command as a background task.

30
00:02:34,790 --> 00:02:40,040
Now what will happen is when the control comes to this node application run command, the application

31
00:02:40,040 --> 00:02:43,730
will run as an independent task and the terminal is freed up immediately.

32
00:02:44,210 --> 00:02:47,600
So we will see the deploy stage just as soon as I get left.

33
00:02:47,600 --> 00:02:48,330
Triggers Darren.

34
00:02:48,350 --> 00:02:50,930
Come on, let me demonstrate.

35
00:03:00,410 --> 00:03:04,160
See, the old one is still running and the latest one.

36
00:03:11,310 --> 00:03:13,800
See, this time the deploy stage is past.

37
00:03:14,220 --> 00:03:19,530
And if at all, there would have been more jobs after that deployed job, this time they would not stuck

38
00:03:19,680 --> 00:03:22,530
and will run immediately after that deployed job is bust.

39
00:03:23,250 --> 00:03:25,710
And this also makes the pipeline as bust.

40
00:03:26,740 --> 00:03:28,930
So, yeah, that was the only motive of this lecture.

41
00:03:29,050 --> 00:03:29,770
See you next.
