1
00:00:01,020 --> 00:00:07,470
Welcome, guys, in this class, we will discuss a few points on stage and stages and get lepeska as

2
00:00:07,470 --> 00:00:09,600
they are the fundamental of any car pipeline.

3
00:00:10,230 --> 00:00:14,250
Keep in mind, the quiet stages and stage are two different keywords.

4
00:00:15,970 --> 00:00:23,110
The singular form stage is used in a job to define which stage that job is part of stages, on the other

5
00:00:23,110 --> 00:00:28,210
hand, is used to define the order of execution of jobs or group of jobs in a pipeline.

6
00:00:29,080 --> 00:00:31,000
Well, this will be more clear with an example.

7
00:00:33,810 --> 00:00:36,270
OK, so I have a very simple pipeline to find here.

8
00:00:36,660 --> 00:00:41,580
This pipeline has three jobs built a job, desk job and deployed job.

9
00:00:42,270 --> 00:00:46,020
Each job is simply echoing out a message to which job they belong to.

10
00:00:46,740 --> 00:00:47,910
Build your blueprint.

11
00:00:48,090 --> 00:00:54,120
This message is from Build Job Desk Job will print this messages from desk job and same as deployed

12
00:00:54,120 --> 00:00:54,910
job, right?

13
00:00:55,380 --> 00:00:57,600
No state or stages is defined as of now.

14
00:00:58,230 --> 00:01:00,570
Now, if I commit and show you the pipeline run.

15
00:01:19,090 --> 00:01:25,150
We have all these three jobs, and like I told by default, if you don't specify any stages, all are

16
00:01:25,150 --> 00:01:27,010
kept under the dust stage.

17
00:01:27,880 --> 00:01:28,870
They bought anything to see.

18
00:01:28,870 --> 00:01:30,190
Here is the job execution.

19
00:01:30,700 --> 00:01:32,470
All the jobs are executed badly.

20
00:01:32,860 --> 00:01:37,530
It means the order in which you define the jobs environmental file does not matter.

21
00:01:37,540 --> 00:01:38,950
I don't get lapsed.

22
00:01:38,950 --> 00:01:41,740
Default behavior is to execute our jobs badly.

23
00:01:42,190 --> 00:01:44,800
If order not explicitly defined by some given.

24
00:01:46,320 --> 00:01:47,280
I like this job done.

25
00:01:48,780 --> 00:01:54,690
This pipeline got completed with all the jobs, and I think badly now in order to define the jobs execution

26
00:01:54,690 --> 00:01:55,000
order.

27
00:01:55,050 --> 00:01:56,400
We need to define stages.

28
00:01:56,850 --> 00:01:59,790
Remember, we have to give us stage and stages.

29
00:02:01,200 --> 00:02:02,280
I'll go back to the code.

30
00:02:10,850 --> 00:02:11,370
And it.

31
00:02:15,240 --> 00:02:19,770
So let's first organize each job into some stage, starting with better job.

32
00:02:24,330 --> 00:02:28,290
Let's say this, Billy Joel will run in a build stage.

33
00:02:32,230 --> 00:02:32,920
Desk job.

34
00:02:35,780 --> 00:02:39,800
Is assigned this job is assigned to test stage

35
00:02:42,610 --> 00:02:43,940
last, just deploying.

36
00:02:45,040 --> 00:02:49,960
Nice door, you can name a stage whatever you want, but it's a good practice to keep the stage name

37
00:02:50,170 --> 00:02:51,700
relevant to the job it performs.

38
00:02:52,600 --> 00:02:55,000
Alright, so stage for each job being defined.

39
00:02:55,330 --> 00:03:01,630
Next thing we need to define the execution order of each stage for that at the start of pipeline.

40
00:03:02,380 --> 00:03:08,560
At what stages these stages keyboard is applied at global level at pipeline.

41
00:03:10,320 --> 00:03:13,530
Now, at each stage, in the order you want the execution to happen.

42
00:03:13,890 --> 00:03:19,230
I want to build stage to run first, then test and at last deploy stage, so write them in that order.

43
00:03:23,340 --> 00:03:26,100
This stage order, which I'm writing here, is is what matters.

44
00:03:31,920 --> 00:03:34,890
Everything being set commented and see the results.

45
00:03:52,610 --> 00:03:53,120
Excellent.

46
00:03:53,480 --> 00:03:57,760
This time yet, love has executed the jobs in the order we explicitly define.

47
00:03:58,340 --> 00:04:05,570
You can see on top of each job their stage name is mentioned, build your nest and deploy stage surveys.

48
00:04:05,870 --> 00:04:11,210
That was how you can define stage and stages for a pipeline that collectively defined the execution

49
00:04:11,210 --> 00:04:11,930
order of jobs.

50
00:04:12,690 --> 00:04:14,420
Now adding some more points into it.

51
00:04:16,820 --> 00:04:21,100
Jobs with no state defined for them by default are allocated to two-stage.

52
00:04:21,320 --> 00:04:22,430
It was already shown to you.

53
00:04:24,210 --> 00:04:29,160
Next, if there are two or more jobs sharing the same stage name, then they will run in parallel.

54
00:04:29,550 --> 00:04:30,180
Now let's check.

55
00:04:33,190 --> 00:04:34,250
In the same pipeline.

56
00:04:34,750 --> 00:04:36,190
Let's add another desk job.

57
00:04:40,410 --> 00:04:41,100
Copy this.

58
00:04:46,900 --> 00:04:48,700
Desk job to.

59
00:04:54,480 --> 00:04:57,960
And stage name is kept them best committed.

60
00:05:17,860 --> 00:05:23,050
As you can see, this time, both best job and best job, too will run in parallel.

61
00:05:23,680 --> 00:05:29,050
Usually, multiple tests are made to run parallel in a pipeline to save overall pipeline execution time.

62
00:05:30,400 --> 00:05:31,600
And I will discuss more points.

63
00:05:33,250 --> 00:05:38,410
Jobs in the next stage will run after all of the jobs from previous stage complete successfully.

64
00:05:38,860 --> 00:05:44,530
For example, in this pipeline, let's say one of your next job fails for some reason, then that applied

65
00:05:44,530 --> 00:05:46,000
job in the next stage will not run.

66
00:05:46,540 --> 00:05:51,610
But yes, if there are multiple jobs under a stage, then they are independent of each other's failure

67
00:05:51,610 --> 00:05:52,240
or success.

68
00:05:52,690 --> 00:05:57,400
Failing one job in a stage will not stop the execution of other jobs of the same state.

69
00:05:57,670 --> 00:05:59,890
They will continue to run in the screenshot.

70
00:05:59,890 --> 00:06:02,530
You can see that even best job too fails.

71
00:06:02,950 --> 00:06:05,240
This test job still continue to run.

72
00:06:05,950 --> 00:06:07,120
No doubt in this case.

73
00:06:07,120 --> 00:06:09,610
Also, the pipeline will be magnetic field only.

74
00:06:09,820 --> 00:06:12,490
But still, the independent job execution will happen.

75
00:06:13,880 --> 00:06:19,700
Then GitLab has, by default, five pre-defined stages which execute in this particular order.

76
00:06:20,090 --> 00:06:28,790
They are pre stage build, test, deploy and last post stage executing from pre to post order guys build,

77
00:06:28,790 --> 00:06:31,280
test and deploy stage you're already familiar with.

78
00:06:31,880 --> 00:06:37,940
Pre and post are just two more stages in the list, where pre is guaranteed to always be the first stage

79
00:06:37,940 --> 00:06:42,110
in pipeline and post is guaranteed to always be the last stage in pipeline.

80
00:06:43,160 --> 00:06:47,150
Order of build, test and deploy stages can be changed explicitly.

81
00:06:47,540 --> 00:06:53,210
The order of pre and post stage is always fixed, and you can't change that execution order by any means.

82
00:06:54,560 --> 00:07:00,170
And by the time default status, I mean, if in your pipeline you are keeping the stages name from these

83
00:07:00,170 --> 00:07:05,900
five names, then you don't have to specify the order after execution, as GitLab already knows how

84
00:07:05,900 --> 00:07:06,650
to execute them.

85
00:07:07,160 --> 00:07:13,250
It will automatically start running up, restage first, then build, then test, deploy to postage.

86
00:07:13,790 --> 00:07:15,380
Let me even demonstrate this for you.

87
00:07:24,220 --> 00:07:27,760
This is just an extension of the old code here in the code.

88
00:07:27,790 --> 00:07:31,870
I have five jobs named Rebuild Best Deploy Post.

89
00:07:32,500 --> 00:07:37,390
Notice that I haven't defined the stages or the simply jobs with default stage names.

90
00:07:37,900 --> 00:07:39,490
Now if I commit this code.

91
00:07:59,720 --> 00:08:05,750
See, even though I haven't defined the stages on a GitLab with this default, behavior has automatically

92
00:08:05,750 --> 00:08:06,710
set the job's order.

93
00:08:07,010 --> 00:08:09,890
With three jobs starting first proposed at last.

94
00:08:10,960 --> 00:08:11,860
I hope this is clear.

95
00:08:12,490 --> 00:08:13,870
Now moving to next scenario.

96
00:08:19,710 --> 00:08:22,650
Let's say you wish to override this default execution order.

97
00:08:22,950 --> 00:08:25,350
Then you can explicitly define the stages given.

98
00:08:26,610 --> 00:08:31,230
So this time, if you want the job to run first, then I might like this.

99
00:08:45,280 --> 00:08:51,580
But remember, the pre and post jobs will still have their execution order fixed at first and last of

100
00:08:51,580 --> 00:08:52,550
my plan, respectively.

101
00:08:52,570 --> 00:08:53,470
You can't change them.

102
00:09:09,370 --> 00:09:12,750
Yep, the autotuned test has now come before the bill.

103
00:09:14,260 --> 00:09:15,040
Last scenario.

104
00:09:17,200 --> 00:09:21,560
What if you have defined a stage in a job and forgot to add it in two stages?

105
00:09:22,180 --> 00:09:22,810
For example?

106
00:09:26,490 --> 00:09:29,170
Let us comment out this test stage from stages.

107
00:09:43,010 --> 00:09:48,740
Then an execution get level three was an edit, as it cannot find that best stage in available list

108
00:09:48,740 --> 00:09:49,280
of stages.

109
00:09:49,610 --> 00:09:54,920
So it means if you are explicitly defining the order, you have to specify all the stage names in it,

110
00:09:55,190 --> 00:09:58,220
accept pre and post it because they are anyhow fixed.

111
00:09:58,400 --> 00:09:59,150
You can skip them.

112
00:09:59,450 --> 00:10:03,140
But other than these two, all the stage names should be there in the stages order.

113
00:10:03,980 --> 00:10:04,760
I hope it is clear.

114
00:10:05,690 --> 00:10:06,140
All right.

115
00:10:06,170 --> 00:10:08,360
So that's pretty much about stage and stages.

116
00:10:08,810 --> 00:10:10,340
Let's continue with artifact selector.

117
00:10:10,520 --> 00:10:11,030
See you then.
