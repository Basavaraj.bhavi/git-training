1
00:00:00,880 --> 00:00:05,950
Before I start now that you all are familiar with the command and flow of those commands, stood on

2
00:00:05,950 --> 00:00:06,820
a Getler pipeline.

3
00:00:07,180 --> 00:00:10,030
So I would highly recommend you guys to start building the lepeska.

4
00:00:10,030 --> 00:00:12,600
I got my email file of this application on your own.

5
00:00:12,910 --> 00:00:19,300
This will surely help you learn it much better, or you can create it with me and follow the same CIA

6
00:00:19,300 --> 00:00:20,650
approach that is clear.

7
00:00:20,650 --> 00:00:25,180
The files locally were shipped to my branch and our net debt and then merged with Master.

8
00:00:26,150 --> 00:00:27,650
So that's great that.

9
00:00:34,560 --> 00:00:37,050
We know every pipeline starts with writing a job.

10
00:00:37,380 --> 00:00:39,570
And in this case, we would require two jobs.

11
00:00:40,050 --> 00:00:43,650
Their first job build will install all the required dependencies.

12
00:00:43,920 --> 00:00:47,640
And second job deploy will have the arrangements of application.

13
00:00:48,060 --> 00:00:49,740
Let's start with Bill's job first.

14
00:00:52,750 --> 00:00:57,610
Remember, in the last lecture before we could actually use npm, we need to first install it.

15
00:00:58,030 --> 00:01:02,530
So in our pipeline as well, we need to first write down instruction to install npm.

16
00:01:03,880 --> 00:01:06,070
Under this script tag.

17
00:01:07,550 --> 00:01:09,320
I will write the APD update command.

18
00:01:15,010 --> 00:01:19,450
And then the subsequent task should be to install npm the node package manager.

19
00:01:27,350 --> 00:01:29,180
And men and women stalled.

20
00:01:29,660 --> 00:01:33,530
We have to install the package dependencies defined in packages and fight.

21
00:01:34,010 --> 00:01:41,420
So for the next task, right, the command and beam installed since the package file is in the route

22
00:01:41,420 --> 00:01:41,750
itself.

23
00:01:41,900 --> 00:01:43,340
So simply type and being installed.

24
00:01:44,500 --> 00:01:50,050
After Bill is complete, we need to find a second job that will run the index, not just fight, so

25
00:01:50,050 --> 00:01:50,830
create a new job.

26
00:01:55,520 --> 00:01:56,180
Deploy.

27
00:01:57,210 --> 00:02:03,640
Another script tag put on in door this fight, right?

28
00:02:03,960 --> 00:02:05,220
No index, yes, come on.

29
00:02:10,160 --> 00:02:16,010
But we remember from the last lecture we have installed, nor the first hearing, so since default,

30
00:02:16,010 --> 00:02:18,710
get GitLab environment would not have nor installed on it.

31
00:02:19,040 --> 00:02:22,190
So we need to install it explicitly before we can actually use it.

32
00:02:23,000 --> 00:02:27,110
So before this task, right, the prerequisite tasks.

33
00:02:31,380 --> 00:02:32,330
A repeat of the.

34
00:02:34,870 --> 00:02:35,320
By.

35
00:02:36,810 --> 00:02:36,960
And.

36
00:02:39,490 --> 00:02:44,090
Install notice, David.

37
00:02:45,160 --> 00:02:45,640
That's it.

38
00:02:46,120 --> 00:02:51,430
Now you guys may ask that why not love this installation things and write the note installation commands

39
00:02:51,430 --> 00:02:52,570
in ability job itself?

40
00:02:53,080 --> 00:02:55,490
Well, your question is valid and answer to it.

41
00:02:55,510 --> 00:02:56,140
You'll get it.

42
00:02:56,920 --> 00:02:59,600
OK, so both build and deploy jobs are good to go.

43
00:03:00,130 --> 00:03:01,450
Job definition is complete.

44
00:03:01,930 --> 00:03:05,560
But before we directly feed this pipeline to GitLab, that is one issue here.

45
00:03:06,160 --> 00:03:07,510
How will get LeBron or no?

46
00:03:07,540 --> 00:03:09,010
Which job to perform first?

47
00:03:09,190 --> 00:03:12,700
You know, the order of jobs gets this order.

48
00:03:13,420 --> 00:03:16,270
The way you define jobs in this file does not matter.

49
00:03:16,660 --> 00:03:17,350
Like in here?

50
00:03:17,710 --> 00:03:22,090
I think the builder job above deployed job will not make the job to run fast.

51
00:03:22,510 --> 00:03:22,800
No.

52
00:03:23,200 --> 00:03:29,170
If we keep the jobs as it is as per the default behavior of GitLab, it will try to run these jobs in

53
00:03:29,170 --> 00:03:29,590
parallel.

54
00:03:30,160 --> 00:03:32,920
Let me show you how let's commit the pipeline.

55
00:03:37,530 --> 00:03:40,440
We have already gone through this process once, so will quickly do it.

56
00:03:41,660 --> 00:03:42,500
Let me do a less.

57
00:03:43,660 --> 00:03:50,530
Good, and since we will be building this up through pipeline, so we need not to push the installed

58
00:03:50,530 --> 00:03:52,450
packages of node modules directly.

59
00:03:52,930 --> 00:03:56,740
So add everything except the normal use for the for that.

60
00:04:00,060 --> 00:04:03,650
Do not include normals.

61
00:04:08,840 --> 00:04:11,390
See, everything is stage, except when it was.

62
00:04:12,800 --> 00:04:14,360
Now, get commit.

63
00:04:22,090 --> 00:04:23,710
Now set upstream and push.

64
00:04:36,730 --> 00:04:38,830
The pipeline would have got automatically triggered.

65
00:04:44,240 --> 00:04:46,040
The death branch is running.

66
00:04:47,400 --> 00:04:47,850
Be open.

67
00:04:50,450 --> 00:04:55,580
As you can see, since we haven't defined any order of the jobs, get left by default has chosen the

68
00:04:55,580 --> 00:04:59,390
payroll execution and there is no interdependency between these two jobs.

69
00:05:00,170 --> 00:05:02,870
If this thing happens, most probably the pipeline will fail.

70
00:05:04,250 --> 00:05:09,980
See, the pipeline failed as it tried to deploy the app even before the prerequisites are met from the

71
00:05:09,980 --> 00:05:12,890
build stage, not to prevent this behavior.

72
00:05:13,190 --> 00:05:19,220
We need to tell GitLab the order in which it should run the jobs, and for that we define stages.

73
00:05:19,970 --> 00:05:22,610
Let's explore what are these stages in the next lecture.
