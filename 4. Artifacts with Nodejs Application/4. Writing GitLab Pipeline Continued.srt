1
00:00:00,570 --> 00:00:05,280
Now that you have good understanding of stages, we can easily include them in this application.

2
00:00:06,690 --> 00:00:10,260
So under the bill, the job at a new directive stage.

3
00:00:15,680 --> 00:00:18,950
And similarly, the deployed job would have the stage name deploy.

4
00:00:25,800 --> 00:00:31,110
Now, since I have provided the default stage names, it's up to you, whether you want to explicitly

5
00:00:31,110 --> 00:00:34,680
specify the stage or not, even though it is not required.

6
00:00:34,740 --> 00:00:35,820
But still, I will provide.

7
00:00:47,820 --> 00:00:49,320
But this our pipeline is ready.

8
00:00:49,470 --> 00:00:54,440
We have to find the jobs that there order and are all set to push this branch to get laps of.

9
00:00:55,610 --> 00:00:56,150
Save it.

10
00:00:59,950 --> 00:01:02,230
Again, push the index not to remote.

11
00:01:06,710 --> 00:01:10,430
Sorry, we have to get loveseat, audio, not index, not use.

12
00:01:38,760 --> 00:01:40,360
The pipeline would have automatically start.

13
00:01:41,010 --> 00:01:42,270
Let's see what we got now.

14
00:01:54,180 --> 00:01:55,860
Since this time we have two jobs.

15
00:01:56,160 --> 00:02:01,410
We got two blocks here, and visually the idea presented in the same order as we have defined in our

16
00:02:01,410 --> 00:02:05,400
pipeline that is was build stage, then deploy stage.

17
00:02:06,000 --> 00:02:07,560
Let's wait for the job completion.

18
00:02:11,590 --> 00:02:17,290
The pipeline executed, the job has successfully passed, but deployer job was failed.

19
00:02:17,710 --> 00:02:18,160
Why?

20
00:02:18,280 --> 00:02:18,910
Let's find out.

21
00:02:19,570 --> 00:02:20,590
First open, better job.

22
00:02:24,460 --> 00:02:26,690
In her first APD update command.

23
00:02:27,730 --> 00:02:35,500
Then after that, it started installing the note package manager and then scrolling down.

24
00:02:39,680 --> 00:02:44,240
At last, it installed the package dependencies specified in I and Fight.

25
00:02:44,690 --> 00:02:46,070
And finally, the deal was succeeded.

26
00:02:46,760 --> 00:02:49,920
This is OK now moving on to a second job deploy.

27
00:02:50,240 --> 00:02:51,740
Let us check what went wrong there.

28
00:02:52,310 --> 00:02:53,090
Just a quick tip.

29
00:02:53,630 --> 00:02:56,870
You can switch in between stage and jobs from here also.

30
00:03:02,550 --> 00:03:05,010
In here, the update command was a success.

31
00:03:05,730 --> 00:03:07,120
Then it has installed more.

32
00:03:08,400 --> 00:03:15,540
But at last, when it tried to run the index gorgeous file, the error came the error says cannot find

33
00:03:15,540 --> 00:03:20,010
module express and this job exited with code one, meaning failed.

34
00:03:20,610 --> 00:03:24,930
If you remember, this is the same error we got locally when we hadn't run the packages.

35
00:03:24,930 --> 00:03:26,940
Gorgeous fight weird, isn't it?

36
00:03:27,390 --> 00:03:31,200
Because just now we checked that and install command four packages.

37
00:03:31,200 --> 00:03:34,200
No decent file has successfully ran in the build job.

38
00:03:34,440 --> 00:03:35,010
Then why this?

39
00:03:36,000 --> 00:03:36,690
Let me explain.

40
00:03:46,980 --> 00:03:52,140
Actually, the default behavior of GitLab, the jobs in your pipeline are independent of each other,

41
00:03:52,410 --> 00:03:55,110
and they do not exchange any data among themselves.

42
00:03:55,650 --> 00:04:01,260
By default, all the jobs are done in their own silo without hitting anything with other jobs in the

43
00:04:01,260 --> 00:04:01,740
pipeline.

44
00:04:02,340 --> 00:04:05,670
They don't care what jobs are there in the pipeline and what they do.

45
00:04:06,210 --> 00:04:12,350
Whatever is written within a particular job, you're just complete those tasks in its own instance and

46
00:04:12,780 --> 00:04:15,420
before exiting cleanups, everything understood.

47
00:04:16,080 --> 00:04:20,850
And now, if we implement this default behavior into our pipeline, why not first job?

48
00:04:20,940 --> 00:04:22,030
That is the better the job?

49
00:04:22,060 --> 00:04:25,500
Then it installs the express framework in its own instance.

50
00:04:25,860 --> 00:04:32,280
And as soon as the bill's job got completed, all the files or packages that are downloaded got automatically

51
00:04:32,280 --> 00:04:36,330
cleaned and all the environment in which bills job was executed.

52
00:04:36,570 --> 00:04:39,270
And all those things, what a downloader got destroyed.

53
00:04:39,270 --> 00:04:45,300
And that is why after bad job completion, why not a second job that deployed job started.

54
00:04:45,660 --> 00:04:47,970
It started as a fresh in its own instance.

55
00:04:48,450 --> 00:04:51,990
It did not get that express framework from the previous job.

56
00:04:52,590 --> 00:04:57,570
When that applied up started, it was around that the update command then installed more dodges.

57
00:04:57,870 --> 00:05:02,880
And finally, when it tried to run, the index got this file, it failed as that index scored.

58
00:05:02,880 --> 00:05:04,840
This file requires express framework.

59
00:05:05,580 --> 00:05:11,610
But since there is no such package in node modules folder, for this instance, it failed to run that

60
00:05:11,610 --> 00:05:15,020
index file and a job got Exeter with a failed status.

61
00:05:15,660 --> 00:05:16,770
I hope you got this point.

62
00:05:17,910 --> 00:05:19,920
Now we have a clear picture of the problem.

63
00:05:20,280 --> 00:05:21,780
But how to handle this problem?

64
00:05:22,350 --> 00:05:24,840
This can be handled with the concept of artifacts.

65
00:05:25,530 --> 00:05:30,480
Artifacts, as the name itself suggest, is you can say it the byproduct produced by a job.

66
00:05:31,080 --> 00:05:34,680
The output entity of a job then can be considered as an artifact.

67
00:05:35,130 --> 00:05:40,500
Now that output can be the project source code, dependencies, binaries or other resources.

68
00:05:40,950 --> 00:05:42,990
And these are usually stored in a repository.

69
00:05:44,060 --> 00:05:49,880
So in our case, the express framework downloader is what you can say is the artifact produced by a

70
00:05:49,900 --> 00:05:53,600
job, but it is not useful for the deployed job as of now.

71
00:05:54,380 --> 00:06:00,770
To solve this issue, somehow we need to instruct GitLab, not destroy the produced artifacts, rather

72
00:06:00,770 --> 00:06:02,030
store them for later use.

73
00:06:02,780 --> 00:06:07,280
And how does other jobs access these artifacts internally handled by GitLab?

74
00:06:07,760 --> 00:06:12,590
Once the artifacts are produced by the owner of a job, in our case it is very job.

75
00:06:13,170 --> 00:06:19,010
Results are sent back from that run to the GitLab server, which in turn made it available for all the

76
00:06:19,010 --> 00:06:19,880
subsequent jobs.

77
00:06:20,660 --> 00:06:26,750
By default, jobs in later stages automatically download all the artifacts created by jobs in earlier

78
00:06:26,750 --> 00:06:27,170
stages.

79
00:06:27,710 --> 00:06:33,650
We just have to write some configuration in our YAML file to instruct GitLab to store artifacts and

80
00:06:33,650 --> 00:06:34,800
rest all his management.

81
00:06:34,850 --> 00:06:35,600
Get Leverkusen.

82
00:06:36,940 --> 00:06:38,920
So now implement artifacts in our code.

83
00:06:41,330 --> 00:06:43,850
Since artifacts are produced by better job.

84
00:06:44,920 --> 00:06:47,050
So under the job, right artifacts.

85
00:06:55,070 --> 00:06:57,830
Then under the artifact, specify the key word parts.

86
00:07:02,480 --> 00:07:06,890
Bots will determine which files or directories to be added to the job artifacts.

87
00:07:07,220 --> 00:07:12,920
Keep in mind, all the path to files and directories are relative to the repository that was cloned

88
00:07:12,920 --> 00:07:15,700
during the build and esperar requirements.

89
00:07:15,710 --> 00:07:20,720
We have to store the entire node modules directory and the package locked or disinvite.

90
00:07:31,190 --> 00:07:36,590
But this little block of code, when Bill's job will exit Hitler will not destroy these two artifacts

91
00:07:36,770 --> 00:07:39,500
node modules, folder and package locked or file.

92
00:07:39,890 --> 00:07:42,470
Hence, the deployed job will be able to leverage it.

93
00:07:43,490 --> 00:07:48,260
Optionally, you can also specify one more directive here, not expiry time of the artifacts.

94
00:07:48,560 --> 00:07:49,970
Using expired underscore in.

95
00:07:54,170 --> 00:08:00,620
This giver is used to specify for how long job effects are stored before they expire and are deleted

96
00:08:01,160 --> 00:08:01,820
if they expire.

97
00:08:01,850 --> 00:08:03,530
Time is not defined explicitly.

98
00:08:03,830 --> 00:08:09,620
A default GitLab instant by setting, which is of 30 days, will be applicable, and this expiration

99
00:08:09,620 --> 00:08:15,380
time period begins when the artifact generated by a job is uploaded and stored on to get less of it.

100
00:08:17,570 --> 00:08:22,790
You can keep this time to one week, two weeks or any other suitable time as per the requirements.

101
00:08:23,570 --> 00:08:26,540
I won't be needing it in my application, so commanding it out.

102
00:08:28,880 --> 00:08:30,590
And also in this.

103
00:08:33,230 --> 00:08:35,090
Save it and commit the changes.

104
00:09:03,750 --> 00:09:05,640
I will wait for this pipeline to execute.

105
00:09:08,230 --> 00:09:12,520
So the job is completed, and if I open the deployed job.

106
00:09:16,490 --> 00:09:16,850
Great.

107
00:09:17,210 --> 00:09:22,040
And you can see this node in next gorgeous dusk, this time by using artifacts.

108
00:09:22,280 --> 00:09:26,500
Our job did not fail and the application is listening at localhost 880.

109
00:09:27,020 --> 00:09:32,360
Well, though this local laws address will not open here in GitLab, but the good thing is blind job

110
00:09:32,360 --> 00:09:37,550
is not free, you know, by picking up the express framework from the artifacts produced by better job.

111
00:09:38,640 --> 00:09:43,560
Obasi, what artifacts were produced by Billy Job go to build up terminal?

112
00:09:51,000 --> 00:09:57,240
And here we are, given three buttons to interact with those artifacts we have deep download and browse

113
00:09:58,140 --> 00:10:03,420
key button will let you keep the artifacts available in case if you have said the artifacts expiry date

114
00:10:04,320 --> 00:10:09,360
from the download button, you can download artifacts to your local system and from Browse Button,

115
00:10:09,570 --> 00:10:12,390
you can visit and browse the artifacts contents in browser.

116
00:10:14,870 --> 00:10:18,080
See, we got more modules and package a lot more decent.

117
00:10:19,220 --> 00:10:24,710
So, guys, that was how you can store and use artifacts in Great Lab after learning from this lecture.

118
00:10:24,980 --> 00:10:30,380
I hope you people would now be able to create independent jobs that can actually exchange data in your

119
00:10:30,380 --> 00:10:31,370
career pipeline.

120
00:10:31,700 --> 00:10:32,570
See you in next class.
