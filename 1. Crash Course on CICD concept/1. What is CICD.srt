1
00:00:00,580 --> 00:00:05,620
Welcome, guys, to this very first lecture of this course, where we will lay the foundation of our

2
00:00:05,620 --> 00:00:07,600
learnings by understanding what is the.

3
00:00:09,020 --> 00:00:15,530
Starting off with these key words quickly are two acronyms where K stands for continuous integration,

4
00:00:15,890 --> 00:00:21,380
and the three part again refers to two conduct terms continuous delivery and continuous deployment.

5
00:00:22,040 --> 00:00:27,110
Sicily is relatively a new concept and has gained a lot of popularity in the past few years.

6
00:00:27,560 --> 00:00:31,910
So let's see what the reaction is and why companies are going to adopt it.

7
00:00:32,840 --> 00:00:40,190
What is the Sicily is a way or method of developing software with which you are able to deliver software

8
00:00:40,190 --> 00:00:42,800
applications to your customers very frequently.

9
00:00:43,250 --> 00:00:48,260
And how do we do that is by introducing automation into the stages of software development.

10
00:00:49,680 --> 00:00:55,320
Those ancillary terms hold different meanings, but internally, if you peel off the terms, continuous

11
00:00:55,320 --> 00:01:00,960
integration, delivery and deployment, you will find it's all about streamlined automation, so you

12
00:01:00,960 --> 00:01:02,310
can see that implementing.

13
00:01:03,210 --> 00:01:08,880
We try to automate every possible stage of a software development cycle, stages like integrating the

14
00:01:08,880 --> 00:01:11,340
code, testing, building, deploying.

15
00:01:11,340 --> 00:01:16,890
And so and by doing this, we try to minimize the need for human intervention in these stages.

16
00:01:17,370 --> 00:01:23,580
Minimizing human intervention in done means minimizing the risk of human errors, which eventually increases

17
00:01:23,580 --> 00:01:26,820
the overall development speed and how this automation happens.

18
00:01:27,150 --> 00:01:30,510
Well, you will get to know every bit of its details throughout this course.

19
00:01:31,630 --> 00:01:36,700
When properly implemented, quickly makes the building and deploying of a software so easy.

20
00:01:36,970 --> 00:01:42,430
And it holds that much potential that a developer can make a cogent living to the production directly.

21
00:01:42,700 --> 00:01:43,570
Yes, it is true.

22
00:01:43,960 --> 00:01:48,100
People hear coming from non-statutory teams would find it difficult to digest.

23
00:01:48,280 --> 00:01:50,320
But this is the power of the process.

24
00:01:50,770 --> 00:01:55,870
Developers can make a code change live in production, and the application will be smoothly deployed

25
00:01:56,560 --> 00:01:57,350
with CCD.

26
00:01:57,550 --> 00:02:02,050
Gone are those days when you have to do a proper production planning, get a release window and all

27
00:02:02,290 --> 00:02:03,730
but even a small production, please.

28
00:02:04,090 --> 00:02:05,470
It's all automated nowadays.

29
00:02:05,830 --> 00:02:11,620
You just put your one time effort in creating a secret pipeline and then do frequent releases by frequent,

30
00:02:11,620 --> 00:02:13,300
I mean, even multiple times a day.

31
00:02:14,420 --> 00:02:15,830
The next point is quite important.

32
00:02:16,500 --> 00:02:19,400
This is not some tool or particular technology.

33
00:02:19,930 --> 00:02:25,340
Rather, it is a methodology or you can see some set of operating principles that need to be adopted

34
00:02:25,580 --> 00:02:28,520
to automate and speed up the development and deployment cycle.

35
00:02:29,330 --> 00:02:34,820
People new to this dump, often confused to be some technology tool or even coding language.

36
00:02:35,060 --> 00:02:36,500
No, it's not even close.

37
00:02:37,070 --> 00:02:43,190
Sicily is just a collection of standards and practices that needs to be followed by teams that enables

38
00:02:43,190 --> 00:02:46,490
them to deliver code changes more frequently and reliably.

39
00:02:47,990 --> 00:02:50,910
OK, so that was just a comprehensive definition of specialty.

40
00:02:51,410 --> 00:02:54,230
Let us know discuss its formation terms individually.

41
00:02:54,410 --> 00:02:56,750
That is sweet and silly and next lecture.
