1
00:00:01,710 --> 00:00:07,110
An increasingly vocal cheer starts with continuous integration first, adopting continuous integration

2
00:00:07,110 --> 00:00:11,910
practice is your first move towards delivering a high quality software with adopting.

3
00:00:12,420 --> 00:00:17,640
We try to establish a consistent and automated way to build package and best applications.

4
00:00:18,510 --> 00:00:20,070
So what is continuous integration?

5
00:00:20,580 --> 00:00:26,310
As the name itself suggests, grammatical if we see continuous integration means to integrate continuously

6
00:00:26,820 --> 00:00:28,180
in software development as well.

7
00:00:28,200 --> 00:00:29,040
It means the same.

8
00:00:29,640 --> 00:00:35,070
Continuous integration is a development practice that requires developers to integrate their code into

9
00:00:35,070 --> 00:00:40,920
a central shared repository, typically a version control system such as get multiple times a day and

10
00:00:40,920 --> 00:00:45,540
doing this can even lead to multiple commits merging into the mainland code in a day.

11
00:00:46,170 --> 00:00:50,760
This is one major practice that has to be followed in CI team members by Lily.

12
00:00:50,760 --> 00:00:56,430
Working on a same project or even on the same codes are required to integrate their codes frequently

13
00:00:56,550 --> 00:00:58,350
into some central shared repository.

14
00:00:59,240 --> 00:01:05,090
This you do, because if you keep on postponing the integration and do it after a few weeks or months

15
00:01:05,090 --> 00:01:10,640
of development, then every developer would have its own version of code and integrating at that time

16
00:01:10,640 --> 00:01:11,630
would be a real mess.

17
00:01:11,870 --> 00:01:17,360
And believe me, after a month of development, developers would forget even its own code, let alone

18
00:01:17,360 --> 00:01:18,260
the integration part.

19
00:01:20,550 --> 00:01:23,340
Eat chicken developer makes to the central source code.

20
00:01:23,670 --> 00:01:30,060
It is verified by automated builds and tests that runs on some cassava, which immediately surface any

21
00:01:30,060 --> 00:01:34,680
error in the code, which is just mud and notified to the respective team or developer.

22
00:01:35,250 --> 00:01:36,510
So you can see this.

23
00:01:36,510 --> 00:01:42,150
The principle allows the teams to detect problems very early, early, as in a few minutes after the

24
00:01:42,150 --> 00:01:46,770
commit, welcoming towards the goal from the last two principles.

25
00:01:46,920 --> 00:01:53,160
You yourself can deduce that the key goals of continuous integration are to find an address books quicker,

26
00:01:53,640 --> 00:01:58,860
improve software quality and reduce the time it takes to validate and release new software updates.

27
00:02:00,430 --> 00:02:05,650
After continuous integration, the next step towards a successful software deployment is continuous

28
00:02:05,650 --> 00:02:06,100
delivery.

29
00:02:06,760 --> 00:02:08,080
So what is continuous delivery?

30
00:02:08,620 --> 00:02:12,820
Again, grammatically, if we see it is to deliver something continuously, right?

31
00:02:13,210 --> 00:02:15,430
Well, it is with continuous delivery.

32
00:02:15,640 --> 00:02:21,400
The ultimate goal is to make developing process in a fast and automated so that developers will always

33
00:02:21,400 --> 00:02:23,770
have a deployment ready built software in their hands.

34
00:02:24,190 --> 00:02:26,470
Let us pass through a standardized test process.

35
00:02:27,800 --> 00:02:33,830
With continuous delivery, the artifacts are the executables produced by your car are deployed to the

36
00:02:33,830 --> 00:02:39,470
test or staging server, or you can see delivered to the doorsteps of production deployment activity.

37
00:02:40,040 --> 00:02:45,680
This means that on top of continuous integration with continuous delivery, we deploy an automated release

38
00:02:45,680 --> 00:02:46,130
process.

39
00:02:47,850 --> 00:02:53,910
Continuous delivery in that building, testing and releasing software with greater speed and frequency,

40
00:02:54,150 --> 00:02:58,740
and thus helps teams deliver update to customers faster and more frequently.

41
00:03:00,480 --> 00:03:05,970
Last continuous delivery gives the answer to your problem of poor visibility and communication between

42
00:03:05,970 --> 00:03:11,280
development and business teams, and it makes it possible to continuously adapt software in line with

43
00:03:11,280 --> 00:03:11,970
user feedback.

44
00:03:13,030 --> 00:03:17,740
Nice, once again, each point stated in this lecture, we will explore them in detail in the upcoming

45
00:03:17,740 --> 00:03:18,130
lectures.

46
00:03:18,340 --> 00:03:21,580
And you will get a clear picture of all the technical terms used here.

47
00:03:22,810 --> 00:03:23,170
OK.

48
00:03:24,420 --> 00:03:28,740
Now, the final stage of automating your software development process is continuous deployment.

49
00:03:29,190 --> 00:03:31,890
This is the last goal post for any Cassilly process.

50
00:03:33,030 --> 00:03:38,400
Continuous deployment is a software engineering approach in which software functionalities are delivered

51
00:03:38,400 --> 00:03:41,850
frequently to the market through automated deployment systems.

52
00:03:42,510 --> 00:03:47,970
The deployment phase is responsible for automatically launching and distributing the software artifacts

53
00:03:47,970 --> 00:03:54,660
to end users see a delivered artifact from the previous phase is just a piece of code, which is of

54
00:03:54,660 --> 00:03:55,980
no use to any customers.

55
00:03:56,370 --> 00:04:01,710
You have to run it, or you can deploy it on some infrastructure so that your customers who are the

56
00:04:01,710 --> 00:04:04,320
end users can interact with what you have developed.

57
00:04:05,040 --> 00:04:09,180
So you know, you can say this is the phase we, the actual business value resides.

58
00:04:10,410 --> 00:04:15,420
That's there is a very thin line between continuous delivery and its deployment, but the only difference

59
00:04:15,420 --> 00:04:19,140
between these two is the presence of manual approval to update to production.

60
00:04:19,980 --> 00:04:25,500
In continuous delivery software, functionalities are frequently delivered and holds the potential to

61
00:04:25,500 --> 00:04:27,870
get released and deployed into market anytime.

62
00:04:28,620 --> 00:04:34,260
But in this delivery, though, the software holds the potential, but doing so requires a green flag,

63
00:04:34,260 --> 00:04:35,490
an intervention of human.

64
00:04:35,790 --> 00:04:38,490
It could be any person from top management or whatever.

65
00:04:39,060 --> 00:04:43,920
So it's like a package saying that, Hey, I'm all ready to provide a new utility to your customers.

66
00:04:44,130 --> 00:04:45,000
Allow me to do so.

67
00:04:45,990 --> 00:04:51,870
And now, if you go one level up in the automation with continuous deployment, reduction happens automatically

68
00:04:51,900 --> 00:04:53,400
without the explicit approval.

69
00:04:53,730 --> 00:04:55,050
No need of manual approvals.

70
00:04:55,620 --> 00:05:01,200
It's like as soon as a new executable package is delivered, deploy it to production with this level

71
00:05:01,200 --> 00:05:01,860
of automation.

72
00:05:02,130 --> 00:05:07,800
The SDLC lifecycle becomes so powerful that developers are capable of pushing that change into the production

73
00:05:08,130 --> 00:05:12,390
and make their change life with just a single command of code in the code repository.

74
00:05:12,990 --> 00:05:17,940
He or she will make a change in the code repository, and you will see the new code getting tested,

75
00:05:17,940 --> 00:05:22,410
automatically built, automatically delivered and then deployed automatically.

76
00:05:22,830 --> 00:05:26,880
And all of this happens through various scripts or tools used in an orderly fashion.

77
00:05:27,390 --> 00:05:30,270
In fact, even I'm going to show you this practically in the course.

78
00:05:30,630 --> 00:05:35,440
I'll make a small change in my application code, and you will see that change live in production within

79
00:05:35,440 --> 00:05:35,970
a few minutes.

80
00:05:37,100 --> 00:05:37,580
Excellent.

81
00:05:38,030 --> 00:05:43,340
So that was a basic introduction to Sicily or what you can say, the crust of the Israeli media in the

82
00:05:43,340 --> 00:05:43,850
next lecture.
