1
00:00:01,130 --> 00:00:06,650
Welcome students to this lecture, where we will find out the downsides in activities performed in each

2
00:00:06,650 --> 00:00:12,590
race of the software lifecycle that we saw in previous selected, starting with when there are multiple

3
00:00:12,590 --> 00:00:17,930
teams writing their set of goals in their own branches, the first problem comes out by integrating

4
00:00:17,930 --> 00:00:18,320
the code.

5
00:00:18,740 --> 00:00:21,950
Let me explain how not to integrate and build the code.

6
00:00:22,250 --> 00:00:25,880
We saw codes from different teams is then to build an integration team.

7
00:00:26,450 --> 00:00:29,990
Now, since Build, an integration team knows very little about this code.

8
00:00:30,680 --> 00:00:36,500
The developers from each team like front and back and in our case, joins and sits with the build and

9
00:00:36,500 --> 00:00:39,260
integration team to help them in code integration.

10
00:00:39,890 --> 00:00:44,960
Now with this convention software development practice, usually what developers do while writing the

11
00:00:44,960 --> 00:00:50,930
code is the key bonding the code in their local machines and do not checking the code frequently grapple

12
00:00:50,930 --> 00:00:52,130
for weeks or even months.

13
00:00:52,700 --> 00:00:58,010
It is more than possible that multiple developers would be working on the same code quite badly in their

14
00:00:58,010 --> 00:00:58,640
own systems.

15
00:00:59,090 --> 00:01:02,990
So do you see the problem here after a few weeks or months of development?

16
00:01:03,380 --> 00:01:08,150
Developer himself would be unable to recognize his own code from very short and very tense.

17
00:01:08,570 --> 00:01:14,420
He himself can get confused in his own code and the worst cases when he leaves the organization in-between.

18
00:01:14,870 --> 00:01:20,570
If a proper handover of codes is not done, then all of his weeks of code development needs to be understood

19
00:01:20,570 --> 00:01:23,870
by some new developer and then integrated into the final code.

20
00:01:25,050 --> 00:01:30,720
In short, you can say that when multiple gigantic codes from different teams come to build an integration

21
00:01:30,720 --> 00:01:33,990
team, the integration of such courts becomes a total mess.

22
00:01:34,710 --> 00:01:39,990
Moreover, since this integration is itself a manual and lengthy task, so takes a good amount of time

23
00:01:40,140 --> 00:01:43,560
from several days to weeks to integrate just one phase of code.

24
00:01:44,160 --> 00:01:45,930
Imagine how Africa releases.

25
00:01:45,930 --> 00:01:49,140
You can make one for a small list of change takes this much time.

26
00:01:49,740 --> 00:01:54,870
Unable to add new features in this fast moving and digital world is, of course, considered inefficiency.

27
00:01:56,510 --> 00:02:02,180
In a nutshell, if I would say the first drawback is that it takes a huge amount of time, effort and

28
00:02:02,180 --> 00:02:03,860
human resources to integrate the.

29
00:02:04,580 --> 00:02:10,130
And yes, I mean, don't forget that to integrate code, it requires developers from all the teams who

30
00:02:10,130 --> 00:02:13,160
sit and join, which definitely base their development does.

31
00:02:14,480 --> 00:02:19,610
Moving ahead, let's just say the court has been integrated successfully, though after several days

32
00:02:20,140 --> 00:02:23,360
now, I suppose after integration, the integrated code does not work.

33
00:02:24,050 --> 00:02:28,970
A red flag is raised from Build, an integration team, which means the code requires more time for

34
00:02:28,970 --> 00:02:29,900
developers to work.

35
00:02:30,410 --> 00:02:34,190
The fun part here is since the integration defect services after a long time.

36
00:02:34,490 --> 00:02:39,860
At this moment, it becomes a little difficult for developers to recall that huge code, which they

37
00:02:39,860 --> 00:02:41,840
would have written long back and then corrected.

38
00:02:42,590 --> 00:02:48,680
So the takeaway is with conventional work culture, the integration defects take time to get on surface,

39
00:02:48,890 --> 00:02:50,450
and so is its rectification.

40
00:02:52,110 --> 00:02:54,900
Next problem is in this area, the operational area.

41
00:02:55,650 --> 00:03:01,410
So by somehow, after a number of iterations, your ARE package is ready for deployment and is handed

42
00:03:01,410 --> 00:03:06,420
over to the operations team with the requirement node, which is nothing but a list of instructions

43
00:03:06,420 --> 00:03:12,090
and steps required to run the application to deploy that software package to support the operations

44
00:03:12,090 --> 00:03:17,520
team first, have to read those instructions, prepare the appropriate environment accordingly and then

45
00:03:17,520 --> 00:03:22,950
deploy that package onto his desk server or further on to production server, according to those instructions.

46
00:03:23,750 --> 00:03:29,550
Now course this medal reading and implementing method is highly error prone, as there could be miscommunication,

47
00:03:29,790 --> 00:03:33,300
misunderstanding, gap anywhere in reading and deploying those instructions.

48
00:03:33,480 --> 00:03:36,930
You know, the operation team is a human only and to air is human.

49
00:03:38,040 --> 00:03:43,620
So you can conclude this has a drawback that this process is highly error prone due to miscommunication

50
00:03:43,620 --> 00:03:45,940
and misunderstanding in understanding the documents.

51
00:03:47,190 --> 00:03:52,650
Next is not suppose that software package has been successfully created and is deployed on the testing

52
00:03:52,650 --> 00:03:55,080
environment for quality assurance and other testing.

53
00:03:55,620 --> 00:04:00,810
At this stage, when the team finds out any functional defects in the package, they communicate those

54
00:04:00,810 --> 00:04:06,750
errors to the developers and developers start reworking on that code again after it once corrected.

55
00:04:06,900 --> 00:04:12,180
That new code has to again go through this long time and effort consuming process for the next review.

56
00:04:13,050 --> 00:04:14,160
So the downside here is.

57
00:04:15,450 --> 00:04:16,710
The long feedback cycle.

58
00:04:17,130 --> 00:04:22,260
It takes a lot many days for the developer to get notified about the functional defects in the code.

59
00:04:22,590 --> 00:04:29,340
Ideally, developers should be notified about these defects as early as possible now without pointing

60
00:04:29,340 --> 00:04:30,750
out many other small drawbacks.

61
00:04:31,140 --> 00:04:35,430
One last major drawback I would like to mention here is the lack of streamlined automation.

62
00:04:36,180 --> 00:04:41,250
Like, I have already mentioned that whatever the outcomes you get from office, it has to be manually

63
00:04:41,250 --> 00:04:42,510
taken to that next phase.

64
00:04:43,050 --> 00:04:47,880
There are no standard defined as in how to pass on the code to business an integration team.

65
00:04:48,180 --> 00:04:54,030
No templates on the documentation to be provided to operations team, nor biplanes know best practices.

66
00:04:54,030 --> 00:04:55,770
Do you find whatever suits them?

67
00:04:55,770 --> 00:05:01,320
Best teams are following that, but to carry out those activities and also there is a lot of room for

68
00:05:01,320 --> 00:05:02,730
automation in this lifecycle.

69
00:05:04,320 --> 00:05:09,480
OK, so guys, these are some of the major drawbacks of developing a software with conventional methodologies

70
00:05:09,990 --> 00:05:14,430
when clubbed, these drawbacks make the entire cycle cumbersome and time consuming.

71
00:05:15,120 --> 00:05:20,520
In the next lecture, we will see how about adding Cassilly into your work culture and health software

72
00:05:20,520 --> 00:05:22,470
development, eliminating these drawbacks?

73
00:05:22,860 --> 00:05:23,820
See you in the next class.
