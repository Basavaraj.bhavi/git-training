1
00:00:01,210 --> 00:00:06,910
Now that we have seen the drawbacks of adopting conventional philosophies in what culture in this lecture,

2
00:00:06,910 --> 00:00:12,610
we will see how by adopting Kasady practice into any software development culture help you eradicate

3
00:00:12,610 --> 00:00:17,650
those drawbacks and can help you achieve faster deliveries with higher software quality possible.

4
00:00:18,130 --> 00:00:23,420
And trust me, the deliveries would be so fast that on every single commit, you would be able to send

5
00:00:23,420 --> 00:00:25,480
a new software at into production.

6
00:00:26,840 --> 00:00:30,980
Having said that, let's start integrating stated practice into our old model.

7
00:00:31,520 --> 00:00:31,790
Nice.

8
00:00:32,030 --> 00:00:37,130
Since this is not a tool technology or a coding language, it's mirror work culture.

9
00:00:37,520 --> 00:00:40,010
So to evolve Kayseri culture into a company.

10
00:00:40,310 --> 00:00:46,880
Your developers do not need to learn any new skill rather from now on the desktop to change and improvise

11
00:00:46,880 --> 00:00:48,050
the way they were working.

12
00:00:48,050 --> 00:00:53,870
Till now, they only have to adapt a certain set of operating principles that will help them to deliver

13
00:00:53,870 --> 00:00:59,780
code changes more frequently and reliably, and try to add those principles in a previous mobile app

14
00:00:59,780 --> 00:01:03,560
example and will help build steam to adopt clever culture.

15
00:01:04,950 --> 00:01:09,000
The first change achieving casualty is to use a shared common repository.

16
00:01:09,270 --> 00:01:12,360
We had all the codes of any team or team members is present.

17
00:01:13,170 --> 00:01:19,920
See, as of now, developers in conventional work culture worry using task specific branches in which

18
00:01:19,920 --> 00:01:22,350
the final code integration was done occasionally.

19
00:01:22,710 --> 00:01:23,820
After weeks or months.

20
00:01:24,390 --> 00:01:29,910
And but that specific branch, I mean, like frontend team has their own branch to work on and backend

21
00:01:29,910 --> 00:01:34,620
database team created their own branch and they would merge their branches at last.

22
00:01:35,430 --> 00:01:42,480
But now there would be a one code repository in which all the codes are present and all the edits or

23
00:01:42,480 --> 00:01:45,720
additions to the code should be committed back to this repository.

24
00:01:46,170 --> 00:01:46,920
You're not example.

25
00:01:46,920 --> 00:01:52,650
You can say that there would be no longer frontend team branch or backend team branch containing exclusive

26
00:01:52,650 --> 00:01:53,910
code of their own area.

27
00:01:54,270 --> 00:02:00,000
Rather, you would have one code repository with all the frontend backend or whatever is needed for

28
00:02:00,000 --> 00:02:05,130
a full fledged application to run are present and developers will work on that shared goal.

29
00:02:05,850 --> 00:02:10,920
Developers can still have their own individual branches for their development, but all of their final

30
00:02:10,920 --> 00:02:16,710
code should be then committed back to the shared code repository so that it always remains updated with

31
00:02:16,710 --> 00:02:17,880
the latest code changes.

32
00:02:18,540 --> 00:02:21,420
Also, this should happen frequently.

33
00:02:21,720 --> 00:02:25,620
It can be multiple times a day, or at the very least, at the end of day.

34
00:02:26,100 --> 00:02:31,260
Whatever changes the developers are making into their own branches, they should make a habit to commit

35
00:02:31,260 --> 00:02:35,400
them in the master repository at least once a day, not by doing this.

36
00:02:35,580 --> 00:02:41,010
We are very much tackling the problem of integrating those gigantic codes at the very end of development.

37
00:02:41,700 --> 00:02:47,130
If the developers are committing their code daily, then you see the integration of the whole code is

38
00:02:47,130 --> 00:02:49,980
automatically getting open at any point of time.

39
00:02:50,250 --> 00:02:55,470
You get the holistic view of all the codes getting prepared across various theme of the project, right?

40
00:02:57,160 --> 00:03:01,630
Well, once this integration starts happening, you will start thinking of to remove this build and

41
00:03:01,630 --> 00:03:02,170
integration.

42
00:03:02,800 --> 00:03:08,530
But we build an integration team also does the task of compiling the code and building the software

43
00:03:08,530 --> 00:03:08,950
package.

44
00:03:09,280 --> 00:03:14,380
So before we can roll them off first, we need to assign those jobs to someone who could perform them.

45
00:03:15,190 --> 00:03:17,380
This is where very server come into picture.

46
00:03:17,650 --> 00:03:23,950
Here we deploy a dedicated bill server, which itself can automatically compiled a code, build up packages

47
00:03:23,950 --> 00:03:27,190
out of them and specify what performed that unit.

48
00:03:27,190 --> 00:03:31,060
And you are testing for the software so it builds off of being deployed.

49
00:03:31,090 --> 00:03:33,940
There will be no need of human to compile or build.

50
00:03:34,210 --> 00:03:35,980
Everything will be handled automatically.

51
00:03:36,790 --> 00:03:42,220
So the next continuous integration principle coming out of this is that whenever a developer commits

52
00:03:42,220 --> 00:03:48,250
the code that commit will automatically trigger the build server to run various test cases, compiled

53
00:03:48,280 --> 00:03:50,080
code and build a package out of it.

54
00:03:50,560 --> 00:03:56,230
And in case if there is any compilation error and the build fails, that data is immediately sent out

55
00:03:56,230 --> 00:04:01,060
to the developer who has just triggered that build upon getting that error notification.

56
00:04:01,390 --> 00:04:04,150
He will be resolving that issue after minutes of its creation.

57
00:04:04,840 --> 00:04:08,890
And if you go back, this was a big drawback in the traditional work culture.

58
00:04:09,520 --> 00:04:14,080
Earlier, the code defects, which were taking days to surface, are now coming up in a few minutes

59
00:04:14,260 --> 00:04:15,220
after the code committee.

60
00:04:16,210 --> 00:04:21,610
From now on, no matter how many commits are being done by developers within a day, built server will

61
00:04:21,610 --> 00:04:25,090
automatically build them, test them within a few minutes or so.

62
00:04:25,960 --> 00:04:29,500
Now moving the next to make deployment ready software quickly available.

63
00:04:29,890 --> 00:04:32,680
One thing that can be changed is this operational process.

64
00:04:33,520 --> 00:04:38,740
As of now, the operations team is manually reading the instructions and then create environments accordingly.

65
00:04:39,220 --> 00:04:44,470
But if you have ever seen that instructions file, you will notice that those are basic instructions

66
00:04:44,470 --> 00:04:49,060
only and can be carried out by executable scripts like Unix scripts.

67
00:04:49,690 --> 00:04:55,240
Steps like command to download any requisite libraries, set environment, variables, parts and other

68
00:04:55,240 --> 00:04:58,870
things can be easily done via Unix scripts or new advanced techniques.

69
00:04:59,230 --> 00:05:00,040
And these scripts.

70
00:05:00,190 --> 00:05:01,540
Developers themselves can build.

71
00:05:02,260 --> 00:05:06,790
So the next principle is to automate the operations part by some scripting language.

72
00:05:07,980 --> 00:05:12,630
I guess you don't really assume that a person who is creating that mobile application code can also

73
00:05:12,630 --> 00:05:13,440
write these scripts.

74
00:05:13,710 --> 00:05:15,180
No, it is not expected.

75
00:05:15,660 --> 00:05:21,810
The development team here in DevOps, as the name suggests, is in the mix of both developers and operations

76
00:05:21,810 --> 00:05:27,360
team, with the roles clearly defined to them where app developers will create the application code,

77
00:05:27,720 --> 00:05:32,850
the operations people now being included in that development team only would be politely writing those

78
00:05:32,850 --> 00:05:33,600
unique scripts.

79
00:05:34,020 --> 00:05:39,510
It's just their manual efforts are eliminated, not the operations team itself, but yeah, they have

80
00:05:39,510 --> 00:05:42,660
to upgrade themselves and learn a bit of more coding than before.

81
00:05:43,780 --> 00:05:48,880
Now, as far as a third rollback is concerned, while writing the unit cases for the application, you

82
00:05:48,880 --> 00:05:54,190
can also write some functional test cases and automated quality assurance scripts to shorten these feedback

83
00:05:54,190 --> 00:05:54,640
cycles.

84
00:05:55,890 --> 00:06:00,940
I'm very easily saying this to write some automated quality assurance test scripts to remove any other

85
00:06:00,940 --> 00:06:03,010
human effort from this complete lifecycle.

86
00:06:03,520 --> 00:06:08,860
But this requires a high level of experience and confidence in what you are doing as automating.

87
00:06:08,860 --> 00:06:14,110
The quality assurance test will make the developers to release a new build into live market with just

88
00:06:14,110 --> 00:06:17,860
a single click on Commit, which definitely has huge risk involved in it.

89
00:06:18,400 --> 00:06:23,140
So that is why this automated acceptance testing, I would say, is subjective to adopt or not.

90
00:06:23,980 --> 00:06:27,820
Anyways, the last drawback was non streamlined and automated process.

91
00:06:28,780 --> 00:06:33,010
Undoubtably, in case you recall, you lose a lot of automation in the whole process.

92
00:06:33,460 --> 00:06:38,470
We have already seen the manual efforts of build an integration team are now replaced by an automated

93
00:06:38,470 --> 00:06:39,040
mail server.

94
00:06:39,550 --> 00:06:42,550
Operations teams manual efforts can be replaced by some scripts.

95
00:06:42,910 --> 00:06:47,260
And believe me, this is just the trailer of automation and the highest level of automation.

96
00:06:47,560 --> 00:06:52,390
As I'm saying all along, in this course, you will just make a single code commit in the repository

97
00:06:52,570 --> 00:06:54,370
and your change will be live in production.

98
00:06:54,550 --> 00:06:58,870
After going through the automated tasks of compiling, build, test and then deploy.

99
00:07:00,370 --> 00:07:06,400
Recalling all that we have learned so far and clubbing them together, we can deduce that increasingly

100
00:07:06,550 --> 00:07:12,460
we tend to create a pipeline of test, build, deliver and released, and all these steps work in a

101
00:07:12,460 --> 00:07:15,730
seamless manner for foster and error free software deliveries.

102
00:07:16,450 --> 00:07:20,920
And how and where to write this individual steps is further there in the course with full details.

103
00:07:21,900 --> 00:07:22,380
Excellent.

104
00:07:22,950 --> 00:07:27,900
So this is how if your developers are adopting this working culture, then you can say that you are

105
00:07:27,900 --> 00:07:34,320
implementing Cassilly into your project and undoubtedly adopting S.H.I.E.L.D culture can help an organization

106
00:07:34,320 --> 00:07:38,100
to deliver changes and new releases more frequently and reliably.

107
00:07:38,730 --> 00:07:40,920
And with this comes the end of this action here.

108
00:07:41,250 --> 00:07:46,050
Starting from the next section, you will be introduced to GitLab and progressively you will learn how

109
00:07:46,050 --> 00:07:49,340
to smoothly plant it in development cycle to get lifecycle.

110
00:07:49,710 --> 00:07:50,160
Thank you.
