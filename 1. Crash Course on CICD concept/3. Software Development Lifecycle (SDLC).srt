1
00:00:00,660 --> 00:00:03,360
My voice, every invention has a motive behind it.

2
00:00:03,690 --> 00:00:05,830
You don't just adopt anything without a reason.

3
00:00:05,850 --> 00:00:06,180
Right?

4
00:00:06,810 --> 00:00:12,960
So to understand CD from root level, we have to first understand why there was the need of the principles

5
00:00:13,500 --> 00:00:18,360
one the traditional SDLC standards not good enough to production ideology and just frequently.

6
00:00:18,720 --> 00:00:19,890
Were there any flaws in it?

7
00:00:20,010 --> 00:00:21,090
Any drawbacks in them?

8
00:00:21,300 --> 00:00:22,410
All of this we will discuss.

9
00:00:23,070 --> 00:00:23,610
Well, on that.

10
00:00:23,850 --> 00:00:29,370
Fourth, let's discover how companies used to develop software products conventionally before the adoption

11
00:00:29,370 --> 00:00:31,230
of the SLA into their development culture.

12
00:00:31,710 --> 00:00:33,240
And break this lecture into two parts.

13
00:00:33,690 --> 00:00:37,680
But first, we will see a high level picture of what a software development cycle is.

14
00:00:37,950 --> 00:00:40,380
And then we will try to fit this cycle in an example.

15
00:00:41,040 --> 00:00:46,440
As a result of it, at the end of the section, you will be able to identify what were the missing aspects

16
00:00:46,440 --> 00:00:51,960
from the conventional SDLC methodologies that lead to the beginning of a new series culture.

17
00:00:53,250 --> 00:00:55,740
Some of you may find these lectures to be of critical.

18
00:00:56,130 --> 00:01:01,410
And if you are already having the basic knowledge and are here to only learn that, get less specific.

19
00:01:01,410 --> 00:01:06,450
But then you can skip this section and jump on to the section from where the Getler portion starts.

20
00:01:06,810 --> 00:01:07,770
Rest, all can continue.

21
00:01:08,850 --> 00:01:09,210
OK.

22
00:01:09,690 --> 00:01:14,430
So for any software application, whether it may be an e-commerce website, analytical application,

23
00:01:14,430 --> 00:01:19,530
mobile application or whatever to produce a high quality software, the development teams have to do

24
00:01:19,530 --> 00:01:25,530
a lot of work for creating and maintaining that application application that meets or exceeds the customer

25
00:01:25,530 --> 00:01:32,280
expectations while remaining inside that time and costly means that work here does not only refer to

26
00:01:32,280 --> 00:01:37,590
writing an optimized code for the application, writing code is just one phase of software development.

27
00:01:38,100 --> 00:01:43,170
Along with that, there are a lot of other phases that any software has to go through to make it a high

28
00:01:43,170 --> 00:01:49,440
quality software, and all these phases in conjunct forms a complete software development lifecycle.

29
00:01:50,440 --> 00:01:53,710
So putting this dumb software development lifecycle in a formal way.

30
00:01:55,210 --> 00:02:01,360
Software development lifecycle acronym SDLC is a systematic process used by software industry for building

31
00:02:01,360 --> 00:02:06,820
the high quality software that didn't use to meet customer expectations with lowest time and cost as

32
00:02:06,820 --> 00:02:07,300
possible.

33
00:02:08,560 --> 00:02:15,010
This is a graphical picture of an SDLC process, as DLC consists of these phases on stages one through

34
00:02:15,010 --> 00:02:17,830
which a high quality software is produced as the end result.

35
00:02:18,610 --> 00:02:24,880
These phases include planning, defining, designing, building, testing and finally deployment.

36
00:02:25,510 --> 00:02:31,030
Let me just briefly explain each development stage, starting with the first phase requirement.

37
00:02:31,030 --> 00:02:35,890
Gathering and analysis is the most important and crucial part of any software development lifecycle.

38
00:02:36,340 --> 00:02:41,950
At this stage, all the relevant information from the customers is collected as per their expectations.

39
00:02:42,670 --> 00:02:47,410
This step is performed by a senior members of the team by taking inputs from the customer.

40
00:02:48,220 --> 00:02:53,680
Planning for the quality assurance requirements and identification of the risk associated with the project

41
00:02:53,890 --> 00:02:55,270
is also done at this stage.

42
00:02:56,850 --> 00:03:03,390
After the requirements are clear and are planned in the next phase, defining all those product requirements

43
00:03:03,390 --> 00:03:08,220
are laid down onto a document or SARS software requirement specification document.

44
00:03:08,820 --> 00:03:14,250
This document is a formal document that consists of all the product requirements to be designed and

45
00:03:14,250 --> 00:03:16,110
developed during that project lifecycle.

46
00:03:17,390 --> 00:03:23,090
Then after defining the next phase design and comes in now based on the requirements specified in the

47
00:03:23,210 --> 00:03:27,710
status document, in this stage, various design approaches will complete.

48
00:03:27,710 --> 00:03:34,700
The product are architected and documented in the design document specification details after design

49
00:03:34,700 --> 00:03:35,690
documents are created.

50
00:03:35,900 --> 00:03:42,260
They are compared and reviewed from all aspects like robustness, time, cost and others, and the best

51
00:03:42,260 --> 00:03:43,250
approach is selected.

52
00:03:44,720 --> 00:03:50,840
Now that we have product requirements, clear design document in hand building is the stage where actual

53
00:03:50,840 --> 00:03:52,610
development of a software begins.

54
00:03:53,090 --> 00:03:58,460
This is where engineers will come in and start writing the code while sticking to the agreed blueprint.

55
00:03:59,960 --> 00:04:04,970
After the court is built, the generator code is tested against the requirements to make sure that the

56
00:04:04,970 --> 00:04:08,220
product is solving the needs gathered during the requirement stage.

57
00:04:08,570 --> 00:04:12,110
And it also meets the quality standards defined in the SRS document.

58
00:04:13,190 --> 00:04:19,130
Once the software testing phase is over and no books or errors are left in the system, then the final

59
00:04:19,130 --> 00:04:24,020
deployment process starts with the software is formally released into the market.

60
00:04:24,900 --> 00:04:30,480
And once released, the maintenance of the product is taken care by the developers or some support team

61
00:04:30,510 --> 00:04:32,070
for any future address or issues.

62
00:04:33,930 --> 00:04:37,320
Once this cycle is completed, it's generally called an arbitration.

63
00:04:37,980 --> 00:04:43,710
So after one successful iteration, the same cycle is followed again to release any new features or

64
00:04:43,800 --> 00:04:46,170
update existing ones in the running software.

65
00:04:46,500 --> 00:04:49,140
So know the cycle just goes and goes and goes.

66
00:04:50,490 --> 00:04:50,950
All right.

67
00:04:50,970 --> 00:04:56,370
So these what few stages that every software development project follows for the generation of a high

68
00:04:56,370 --> 00:04:57,090
quality product.

69
00:04:57,900 --> 00:05:03,750
And yes, before we move to the next class guys, one thing worth mentioning here would be the stages

70
00:05:03,750 --> 00:05:07,590
that we just saw in this DLC are never conventional or modern.

71
00:05:07,890 --> 00:05:11,880
They remain the same no matter what new standard or principles you adopt.

72
00:05:12,180 --> 00:05:14,940
These as DLC steps are going to be there forever.

73
00:05:15,630 --> 00:05:21,390
What really makes a difference is how the stages, preferably starting from development phase, are

74
00:05:21,390 --> 00:05:22,470
carried out internally.

75
00:05:23,070 --> 00:05:28,050
In short, if I have to say in conventional models, these stages were carried out manually.

76
00:05:28,440 --> 00:05:33,420
That is, to carry any internal operation or to move the product from one phase to next phase in the

77
00:05:33,420 --> 00:05:34,050
lifecycle.

78
00:05:34,320 --> 00:05:37,920
We need a person or set of teams like after the development.

79
00:05:38,160 --> 00:05:41,520
You will manually provide the CO two testing team testing team.

80
00:05:41,520 --> 00:05:46,410
What test the code manually and then the final code is manually taken to production servers and not,

81
00:05:46,770 --> 00:05:50,040
you know, a lot of manual activities are involved in the whole lifecycle.

82
00:05:50,520 --> 00:05:56,520
But in modern models without option of continuous integration and continuous deployment culture, we

83
00:05:56,530 --> 00:06:01,770
try to foster this transition from one place to another by introducing automation within the underlying

84
00:06:01,770 --> 00:06:02,130
steps.

85
00:06:02,550 --> 00:06:05,880
And how exactly we automated it will see it in the coming lectures.

86
00:06:06,690 --> 00:06:08,430
So that's pretty much it for this lecture.

87
00:06:08,610 --> 00:06:09,450
See you next one.
