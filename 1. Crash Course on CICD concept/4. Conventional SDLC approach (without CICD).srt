1
00:00:00,820 --> 00:00:06,820
Now that we know how a software development cycle looks like and what are various phases in this lecture,

2
00:00:06,820 --> 00:00:12,040
we will figure out how applications were developed conventionally without embodying the culture.

3
00:00:12,550 --> 00:00:17,170
And for example, I guess a food ordering mobile application would be easy to correlate with.

4
00:00:17,680 --> 00:00:23,230
Just assume that the mobile application has to be developed as project requirements now to develop this

5
00:00:23,230 --> 00:00:23,860
application.

6
00:00:24,010 --> 00:00:29,530
As I saw in previous lecture, the development process will go around this incremental cycle from one

7
00:00:29,530 --> 00:00:30,400
stage to the next.

8
00:00:31,090 --> 00:00:31,420
Nice.

9
00:00:31,660 --> 00:00:36,730
Since we would be more concerned about the development and deployment stages so we will not go deep

10
00:00:36,730 --> 00:00:38,140
into the first three stages.

11
00:00:38,470 --> 00:00:43,990
So let's just assume that the first three phases of requirements gathering, defining, designing are

12
00:00:43,990 --> 00:00:46,180
already done and the building phase has started.

13
00:00:46,420 --> 00:00:47,500
That is a developer.

14
00:00:47,500 --> 00:00:48,850
Teams have come into picture.

15
00:00:49,540 --> 00:00:50,260
Now let's start.

16
00:00:51,530 --> 00:00:57,020
Generally, for a large scale development, the software development team itself is divided into multiple

17
00:00:57,020 --> 00:00:59,840
different teams, with each team performing different roles.

18
00:01:00,260 --> 00:01:04,610
For this example, we will consider there are two teams the frontend development.

19
00:01:04,790 --> 00:01:09,730
And second, the backend team, and they all maintain their code onto a source code repository.

20
00:01:09,740 --> 00:01:10,070
Get.

21
00:01:11,000 --> 00:01:11,780
What does this get?

22
00:01:12,020 --> 00:01:18,230
I believe you all may know it is a free and open source distributed version control system designed

23
00:01:18,230 --> 00:01:20,750
to keep track of projects as they change over time.

24
00:01:21,320 --> 00:01:26,810
You have multiple platforms available in the market that provide you this functionality like GitHub,

25
00:01:26,810 --> 00:01:29,000
GitLab, Bitbucket, anyway.

26
00:01:29,540 --> 00:01:34,670
Now, since we have multiple teams working together and also multiple team members apparently working

27
00:01:35,000 --> 00:01:40,460
so to keep their code separate and isolate from each other, you may have multiple branches of the code.

28
00:01:40,910 --> 00:01:43,310
Each branch having a unique version of the code.

29
00:01:44,330 --> 00:01:49,850
So, for example, one engineer can be working on the payment gateway to engineers are working on many.

30
00:01:50,060 --> 00:01:52,280
Some are working on databases like this.

31
00:01:52,400 --> 00:01:57,740
Many engineers would be working on different parts and writing their own set of codes in their own branch

32
00:01:57,890 --> 00:02:01,370
and barely testing it, writing more codes and testing it.

33
00:02:01,850 --> 00:02:07,280
All of this in their own branch or in the worst case, which is very rare these days.

34
00:02:07,610 --> 00:02:09,230
They don't even use a good platform.

35
00:02:09,560 --> 00:02:12,680
Rather, all the codes are kept locally in their own systems.

36
00:02:13,620 --> 00:02:17,730
Now, when a certain phase of code is completed, let's say, after a few days or weeks.

37
00:02:18,120 --> 00:02:23,490
And since that team members were working on different branches, the code from each branch needs to

38
00:02:23,490 --> 00:02:29,340
be compiled into one set of code onto a master branch in order to get a holistic code for the application

39
00:02:29,910 --> 00:02:30,450
to do so.

40
00:02:30,750 --> 00:02:36,450
The code from all the teams is sent to a build and integration team whose responsibility is to integrate

41
00:02:36,450 --> 00:02:37,920
the code into one entity.

42
00:02:38,730 --> 00:02:44,160
After the integration, the code is compiled and a software package is built out of that compiled code.

43
00:02:45,060 --> 00:02:50,190
The dissatisfactions building phase gets completed and the development cycle is moved on to the next

44
00:02:50,190 --> 00:02:51,160
phase move.

45
00:02:51,180 --> 00:02:57,410
How manually the compile software package is then handed over to the operations team for deployments.

46
00:02:58,440 --> 00:03:04,020
Along with the package, the Build an integration team also provide them with the instructions on how

47
00:03:04,020 --> 00:03:09,630
that software will deploy and run instructions, like which tools are required and should be installed

48
00:03:09,630 --> 00:03:10,500
for the application.

49
00:03:10,830 --> 00:03:12,150
Which libraries are needed.

50
00:03:12,600 --> 00:03:16,860
Or It can be a list of variables and configurations like for test environment.

51
00:03:16,920 --> 00:03:20,250
Use these set of configurations and for production use used order.

52
00:03:20,670 --> 00:03:25,980
So basically, all the prerequisites and instructions on how that software will run are written in those

53
00:03:25,980 --> 00:03:26,340
files.

54
00:03:27,300 --> 00:03:32,940
Now, the operations team reading from those files first deploys the software onto a test environment

55
00:03:33,330 --> 00:03:35,490
so that an end during testing can be done.

56
00:03:35,850 --> 00:03:40,950
And this is where the test and quality assurance team comes into action during this phase.

57
00:03:41,310 --> 00:03:47,370
Security checks for any bugs and defects in the code and test if the product matches the original specifications,

58
00:03:48,030 --> 00:03:53,160
if at all, any defects are found, which obviously they will find nothing can be perfectly made in

59
00:03:53,160 --> 00:03:53,820
a single shot.

60
00:03:54,330 --> 00:03:57,630
Then those defects are communicated back to developers for correction.

61
00:03:58,660 --> 00:04:03,970
Now, development team has to fix the bug, which means the whole phases of development unit testing,

62
00:04:03,970 --> 00:04:07,900
integration, compiling are repeated back with that modified code.

63
00:04:08,140 --> 00:04:11,410
And finally, the modified code is sent back to Kuwait for a retest.

64
00:04:12,010 --> 00:04:17,560
Now, this process is highly iterative, and it continues until the software is bug free, stable and

65
00:04:17,560 --> 00:04:19,840
working, according to the business needs of that system.

66
00:04:21,280 --> 00:04:26,420
Once the code is passed from testing to the operations, team then deploys the latest software code

67
00:04:26,470 --> 00:04:31,030
package on the live production server and is made available to its appropriate market.

68
00:04:31,720 --> 00:04:36,970
So, you know, to correlate with a mobile application, you can see after deployment, the application

69
00:04:36,970 --> 00:04:38,410
is available on Playstore.

70
00:04:39,730 --> 00:04:45,580
Please note that all of these rounds are just for one time release once released, whenever a new feature

71
00:04:45,610 --> 00:04:49,840
needs to be added into this application or the existing ones needs to be updated.

72
00:04:50,140 --> 00:04:52,450
The whole development cycle has to be repeated.

73
00:04:53,770 --> 00:04:59,560
This one development cycle that we just saw is generally known as an iteration, and each iteration

74
00:04:59,560 --> 00:05:05,410
in this conventional software development lifecycle can take from weeks to even a few months anyways.

75
00:05:05,890 --> 00:05:11,440
So this is how people use to develop software conventionally without embodying S.A.T. methodology into

76
00:05:11,440 --> 00:05:12,640
their development culture.

77
00:05:13,390 --> 00:05:18,490
You would have already got the essence that this is a very time consuming and error prone process in

78
00:05:18,490 --> 00:05:19,150
the next lecture.

79
00:05:19,390 --> 00:05:25,470
We will find out what are the drawbacks of this approach and why there is a need to adopt a modern work

80
00:05:25,480 --> 00:05:25,900
culture.

81
00:05:26,020 --> 00:05:26,470
Thank you.
