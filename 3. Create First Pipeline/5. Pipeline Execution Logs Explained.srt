1
00:00:02,160 --> 00:00:02,940
Click on the job.

2
00:00:06,940 --> 00:00:12,250
These are the logs showing us what happened during their job execution in this terminal window.

3
00:00:13,210 --> 00:00:16,960
You can see the the job logs are divided into many sections.

4
00:00:17,350 --> 00:00:20,020
Each of the section depicts a different background process.

5
00:00:20,210 --> 00:00:22,060
Gettler performed while running this job.

6
00:00:26,610 --> 00:00:28,760
You might be wondering that in my email fight.

7
00:00:29,010 --> 00:00:34,530
I did not find any Python version or the environment to run this Python code and in Docker image also

8
00:00:34,920 --> 00:00:35,850
nothing was defined.

9
00:00:36,120 --> 00:00:41,010
Still, the code run automatically write all of your notes will be cleared from this execution log,

10
00:00:41,700 --> 00:00:44,790
starting from when the job was not done in terminal.

11
00:00:44,790 --> 00:00:49,440
Let's it's running with GitLab run at thirteen point one, which is some Docker based on that.

12
00:00:50,010 --> 00:00:55,250
Actually, guys in GitLab, the scripts defined in jobs are not run by the GitLab somebody itself.

13
00:00:55,710 --> 00:01:00,290
Rather, they are run by a dedicated application called GitLab runner and head.

14
00:01:00,300 --> 00:01:06,420
Although it is showing us the stats of the runner that was used to run this job since inadvisable code,

15
00:01:06,430 --> 00:01:09,120
we had not specified any particular runner it should use.

16
00:01:09,480 --> 00:01:12,870
So by default, GitLab has auto selected some shared on it.

17
00:01:13,590 --> 00:01:15,450
This is how the codes are run on GitLab.

18
00:01:15,780 --> 00:01:21,240
In case you do not specify any donor explicitly in your pipeline, GitLab automatically selects some

19
00:01:21,240 --> 00:01:23,010
shared runner to perform those jobs.

20
00:01:23,760 --> 00:01:24,140
I don't know.

21
00:01:24,180 --> 00:01:30,360
In short, is basically a lightweight and highly scalable agent that works with GitLab Casilli to pick

22
00:01:30,360 --> 00:01:34,980
up a C a job, another job and send those results back to GitLab instance.

23
00:01:35,640 --> 00:01:41,220
I won't go deep into RA at this stage, as it is explained in detail somewhere next in the course for

24
00:01:41,220 --> 00:01:41,790
the time being.

25
00:01:42,030 --> 00:01:45,270
Just keep in mind that GitLab itself do not perform any jobs.

26
00:01:45,660 --> 00:01:51,510
Rather, it has some dedicated agent, the GitLab runner, which by default runs the jobs over some

27
00:01:51,510 --> 00:01:52,410
extraordinary machines.

28
00:01:52,710 --> 00:01:55,580
And after performing the jobs, it sends back the results.

29
00:01:55,590 --> 00:01:56,460
You get less what?

30
00:01:57,450 --> 00:02:03,090
Anyways, the next step, it is preparing the Docker machine executor, obviously a code would need

31
00:02:03,090 --> 00:02:04,170
an environment to run.

32
00:02:04,650 --> 00:02:10,350
That is why even though you don't define it in YAML Fight, GitLab will automatically spin up an environment

33
00:02:10,350 --> 00:02:12,990
for your code, which is nothing but a Docker image.

34
00:02:13,440 --> 00:02:17,070
And here it is Docker with Ruby version 2.5 image on it.

35
00:02:18,240 --> 00:02:23,100
Then it's preparing some environment next after the environment is created.

36
00:02:23,430 --> 00:02:26,760
At this level, it's getting the source code from our repository.

37
00:02:27,540 --> 00:02:32,730
And finally, at this step that are not started running that job script, which is to execute a Python

38
00:02:32,730 --> 00:02:38,310
script, how to not be right after running the output of the Python code is returned, which was a print

39
00:02:38,310 --> 00:02:44,010
statement offloads after the execution is completed, cleanup activities are performed.

40
00:02:44,130 --> 00:02:50,390
You know, like whatever was assigned to this job this year or any other resources are taken back temporarily,

41
00:02:50,400 --> 00:02:51,930
not this job will be deleted.

42
00:02:52,260 --> 00:02:54,180
And finally, the job is actually done.

43
00:02:55,370 --> 00:02:59,300
So these were the steps that are performed automatically to get the job done.

44
00:03:00,440 --> 00:03:05,120
And on the right side of this log statement, you get that time spent in each step.

45
00:03:06,580 --> 00:03:13,030
And here in the right, Ben, we have a replay button, if you wish to man he in this job, note that

46
00:03:13,150 --> 00:03:18,940
it on this job only and not the whole pipeline this little button is to rerun this particular job only.

47
00:03:19,910 --> 00:03:22,320
Then we have that total duration in seconds.

48
00:03:22,340 --> 00:03:28,400
This job took to execute again, keep in mind that these logs and details are for the one job in the

49
00:03:28,400 --> 00:03:30,560
pipeline and not for the whole pipeline.

50
00:03:31,130 --> 00:03:33,620
So these 34 seconds is that time.

51
00:03:33,810 --> 00:03:36,740
Look for this job to complete and not for the whole pipeline.

52
00:03:37,970 --> 00:03:41,060
Then we have timeout, which is set to default.

53
00:03:41,180 --> 00:03:46,190
One hour value timeout for a job defines maximum job time out for each run.

54
00:03:46,850 --> 00:03:52,610
This feature can be used to prevent your children from being overwhelmed by a project that has jobs

55
00:03:52,610 --> 00:03:53,300
with a long time.

56
00:03:53,570 --> 00:03:54,650
For example, weeks.

57
00:03:56,210 --> 00:04:00,980
This being explained, let's retry this job and see the live execution of these logs.

58
00:04:10,700 --> 00:04:14,150
Our job is starting again, and it's performing the steps one by one.

59
00:04:15,260 --> 00:04:17,320
It is preparing the Docker machine, executing.

60
00:04:27,540 --> 00:04:29,730
Pulling the trigger image of Ruby 2.5.

61
00:04:37,810 --> 00:04:39,940
It has complete operating environment.

62
00:04:42,170 --> 00:04:44,030
And next, all the steps are done within a moment.

63
00:04:44,810 --> 00:04:46,280
And finally, the job succeeding.

64
00:04:47,240 --> 00:04:53,300
And guys, if you want to check the status of any of your jobs, you can check them from this jobs page

65
00:04:53,300 --> 00:04:53,600
at here.

66
00:04:56,060 --> 00:04:59,390
As we have run out of job good times, so we got two jobs here.

67
00:04:59,960 --> 00:05:06,410
Great, good so far now that we have defined a pipeline for hello, what might an application from now

68
00:05:06,410 --> 00:05:09,760
on whenever you make any change in the Python code might change.

69
00:05:09,770 --> 00:05:14,510
I mean, you're doing committing the repository your pipeline will get triggered automatically means

70
00:05:14,510 --> 00:05:15,980
the lower application will run.

71
00:05:16,520 --> 00:05:17,990
Let me just demonstrated for you.

72
00:05:29,190 --> 00:05:33,420
For saving us some time, I will simply add those changes directly from get a.

73
00:05:38,210 --> 00:05:39,650
Let us change the prince statement.

74
00:05:43,670 --> 00:05:47,090
Committed commitments message just fine.

75
00:05:53,030 --> 00:05:55,610
Kennedys are committed, not open society by plane.

76
00:06:00,900 --> 00:06:01,440
Excellent.

77
00:06:01,830 --> 00:06:07,290
As you can see, as soon as there was a commitment into Devon Grant, the Wyman file was automatically

78
00:06:07,290 --> 00:06:10,860
triggered and it has automatically started running the pipeline for us.

79
00:06:11,520 --> 00:06:16,890
Just try to correlate this small example with any real time project if you would have to find a proper

80
00:06:16,890 --> 00:06:20,100
pipeline with steps to build, test and deploy the code.

81
00:06:20,310 --> 00:06:23,370
What a full fledged large scale application in that case.

82
00:06:23,370 --> 00:06:28,350
Also, anytime a developer will commit has change, the full pipeline would automatically perform those

83
00:06:28,350 --> 00:06:30,210
steps and if at all.

84
00:06:30,240 --> 00:06:31,920
All the things go as per plan.

85
00:06:32,250 --> 00:06:36,540
That change will probably go live within a few minutes of comet that's up out of the.

86
00:06:39,090 --> 00:06:40,500
This run is also succeeded.

87
00:06:40,930 --> 00:06:41,640
Now what's next?

88
00:06:42,510 --> 00:06:45,780
Follow the S.H.I.E.L.D principles as part of the Thessaly Principles.

89
00:06:46,140 --> 00:06:51,180
Once you have mastered the code into your own branch and have confidence on it, you go ahead and merge

90
00:06:51,180 --> 00:06:54,150
this branch into master branch so as to keep it updated.

91
00:06:54,690 --> 00:06:55,530
Let's do it quickly.

92
00:06:57,370 --> 00:06:59,010
Repeat the same magic steps.

93
00:07:01,840 --> 00:07:02,770
You have two options.

94
00:07:04,670 --> 00:07:05,630
You might request.

95
00:07:08,330 --> 00:07:12,200
Source branch is Devon Margaret is master.

96
00:07:12,800 --> 00:07:13,970
This is the latest comment.

97
00:07:40,390 --> 00:07:42,460
Approve his option and directly how much it.

98
00:07:48,340 --> 00:07:49,060
It is most.

99
00:07:50,040 --> 00:07:53,170
Go to Sicily now, guys.

100
00:07:53,190 --> 00:07:55,500
The same logic applies to my branch as well.

101
00:07:56,010 --> 00:08:00,600
That is, whenever changes encounter in the master branch, the pipeline is automatically triggered.

102
00:08:00,800 --> 00:08:02,040
See, you can even see it here.

103
00:08:02,310 --> 00:08:04,260
This run is thought master branch.

104
00:08:05,310 --> 00:08:08,790
But it's up to you if you want to let it run automatically or not.

105
00:08:09,360 --> 00:08:09,600
Why?

106
00:08:09,600 --> 00:08:14,940
Because Monster Branch is usually associated with production environment, so you would definitely not

107
00:08:14,940 --> 00:08:20,160
allow all the developers to merge the branches into production, but every single commit and let them

108
00:08:20,160 --> 00:08:20,790
play with it.

109
00:08:21,180 --> 00:08:26,250
There would be a lot many developers working on a project, so running the production pipeline for each

110
00:08:26,250 --> 00:08:27,900
of their commit is not a good idea.

111
00:08:28,320 --> 00:08:32,010
So you usually set some criteria on when to run the master branch.

112
00:08:32,550 --> 00:08:37,920
That is why you usually exclude the master branch from automatic pipeline trigger upon code commit.

113
00:08:38,370 --> 00:08:43,410
In fact, for every other branch, you can set this property that whether or not you run the pipeline

114
00:08:43,410 --> 00:08:49,110
automatically upon receiving a change in the code, how to do this is how I would later point of discussion.

115
00:08:49,620 --> 00:08:55,350
But since as of now, I have not made those settings, so the pipeline got automatically triggered and

116
00:08:55,350 --> 00:08:56,760
ran for Master Branch too.

117
00:08:57,870 --> 00:08:58,620
It is completed.

118
00:09:00,090 --> 00:09:02,100
OK, so here comes the end of this lecture.

119
00:09:02,520 --> 00:09:07,500
Before we move to the next class again, I would like to tell you that this was a very basics of basic

120
00:09:07,500 --> 00:09:07,980
example.

121
00:09:08,430 --> 00:09:13,710
My motto of this lecture was just to introduce you with the great lepeska file and show you how great

122
00:09:13,710 --> 00:09:16,200
lepeska books I know from our pipeline.

123
00:09:16,210 --> 00:09:21,120
There were so many things missing like we have not written any test cases, the better the metals and

124
00:09:21,120 --> 00:09:24,000
other things, but nobody's as we go along.

125
00:09:24,000 --> 00:09:27,120
In this course, we will add more and more options in our pipeline.

126
00:09:27,720 --> 00:09:32,340
Your takeaway from this example should be that whenever there is a code committing to the repository

127
00:09:32,700 --> 00:09:36,090
that it will automatically trigger your defines the pipeline.

128
00:09:36,510 --> 00:09:37,350
See your next lecture.
