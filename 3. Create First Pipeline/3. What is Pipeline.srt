1
00:00:00,610 --> 00:00:02,080
The application code is built.

2
00:00:02,560 --> 00:00:07,480
Now we should try to run it on GitLab and create first, though very basic pipeline.

3
00:00:08,370 --> 00:00:12,430
I will write a very simple pipeline that run the code present in a report.

4
00:00:13,090 --> 00:00:19,000
My purpose as of now is just to let you know how pipelines are defined in GitLab and hence will not

5
00:00:19,000 --> 00:00:21,150
bring any testing building phases as of now.

6
00:00:21,460 --> 00:00:26,830
It will unnecessarily complicate things at this stage, and it is now that since this pipeline term

7
00:00:26,830 --> 00:00:27,370
has come up.

8
00:00:27,670 --> 00:00:32,220
So before we start defining a pipeline, let me explain a few former points about it.

9
00:00:33,490 --> 00:00:37,990
Pipelines are that top level component of continuous integration, delivery and deployment.

10
00:00:38,440 --> 00:00:44,230
Basically, it is a set of some elements connected in series where each set of elements perform some

11
00:00:44,230 --> 00:00:44,590
tasks.

12
00:00:45,340 --> 00:00:50,840
For example, this is a pipeline that it starts from Process A, which performs a few tasks.

13
00:00:50,840 --> 00:00:55,950
One task to Process B has its own list of tasks seem like this.

14
00:00:55,960 --> 00:00:59,500
The pipeline continues performing tasks and ends that process D.

15
00:01:00,630 --> 00:01:06,360
We're talking about pipeline in the context with GitLab sketchily, the pipeline comprises of two things.

16
00:01:06,600 --> 00:01:07,860
Jobs and stages.

17
00:01:09,460 --> 00:01:14,320
Jobs are the most fundamental element of a pipeline, and it defines what to do.

18
00:01:14,830 --> 00:01:19,300
For example, it can be a job to test the code or a job to compile the code.

19
00:01:20,690 --> 00:01:27,920
Second stages are collection of jobs, few jobs, when combined together comprises a stage stage, basically

20
00:01:27,920 --> 00:01:33,950
defines the order of jobs as in which job within a stage to run, after which job of that stage.

21
00:01:35,870 --> 00:01:36,680
In a pipeline.

22
00:01:36,800 --> 00:01:44,030
If all the jobs in a stage succeed, the pipeline moves on to the next stage or if any job in a stage

23
00:01:44,030 --> 00:01:48,620
fails, the next stage is not usually executed and the pipeline ends early.

24
00:01:49,920 --> 00:01:55,200
Talking in general, less normally by planes are executed automatically and require no intervention

25
00:01:55,200 --> 00:01:55,920
once created.

26
00:01:57,020 --> 00:02:01,910
But yeah, in case, if needed, GitLab allows you to manually interact with a pipeline that we will

27
00:02:01,910 --> 00:02:02,360
see later.

28
00:02:03,640 --> 00:02:09,440
In most usual cases, in the complete casualty pipeline, a pipeline might consist of four stages in

29
00:02:09,460 --> 00:02:10,090
a given order.

30
00:02:10,600 --> 00:02:13,510
The test build, staging and production stage.

31
00:02:14,110 --> 00:02:17,650
However, it is not a standard depending on the product requirements.

32
00:02:17,650 --> 00:02:20,680
You can create any number of stages and arrange them accordingly.

33
00:02:21,790 --> 00:02:26,950
Last, there are multiple types of pipelines that you can create according to your project requirements

34
00:02:27,190 --> 00:02:32,830
like basic pipelines, which are good for straightforward projects that actually is actually graph pipelines,

35
00:02:32,860 --> 00:02:38,770
what for large and complex projects, parent-child pipeline, multi project pipeline and so on, the

36
00:02:38,770 --> 00:02:42,520
names and complexity can differ among us them, but the core idea would be seem.

37
00:02:43,750 --> 00:02:46,600
All right, so these are a few introductory points about pipelines.

38
00:02:47,020 --> 00:02:50,650
That's number one to create a very simple one using it lepeska feature.
