1
00:00:00,360 --> 00:00:02,070
But you get the feature.

2
00:00:02,340 --> 00:00:03,720
There are two prerequisites.

3
00:00:04,230 --> 00:00:09,240
First, you need the application code to be hosted in a repository which we already have.

4
00:00:09,690 --> 00:00:16,710
And second, you should define a file called Dot GitLab Nash thi8 dot via email in the Rudolfo repository

5
00:00:16,710 --> 00:00:18,030
that will define your pipeline.

6
00:00:18,720 --> 00:00:19,380
This is via email.

7
00:00:19,380 --> 00:00:25,410
File is what basically creates a pipeline that runs for the changes to the code in the repository in

8
00:00:25,410 --> 00:00:25,860
this file.

9
00:00:26,070 --> 00:00:31,200
You basically write structure and order off your jobs with configurations like the scripts you want

10
00:00:31,200 --> 00:00:31,620
to run.

11
00:00:31,860 --> 00:00:37,660
Any dependencies it will have tests that should be done before deploying or the location where to deploy

12
00:00:37,770 --> 00:00:39,270
application disaster.

13
00:00:39,630 --> 00:00:45,150
So in short, this final file is the one that will define your complete and burn pipeline with step

14
00:00:45,150 --> 00:00:46,170
by step instructions.

15
00:00:47,160 --> 00:00:51,470
Now, the court being already present in get, let's create that, get let's the will fight.

16
00:00:52,500 --> 00:00:54,270
Now we're going to create this viral file.

17
00:00:54,300 --> 00:00:55,230
You have two options.

18
00:00:55,740 --> 00:01:00,170
Innovate, you can create it in your local system and then push it on to get less of it.

19
00:01:00,270 --> 00:01:05,970
You know, the way we did it in the last lecture for hollowed out by using some good commands or the

20
00:01:05,970 --> 00:01:09,180
other way you can create it directly from GitLab, you might.

21
00:01:09,990 --> 00:01:14,010
Well, I guess the second option will be easy for now and a new learning for you as well.

22
00:01:14,880 --> 00:01:20,460
So in the GitLab project dashboard, first I will select Branch in which this file should be created.

23
00:01:20,890 --> 00:01:21,390
Devon.

24
00:01:26,460 --> 00:01:29,550
Then from here, click on new file button.

25
00:01:33,290 --> 00:01:39,010
And another file name carefully to get CIA dot by email.

26
00:01:40,760 --> 00:01:43,310
And see, it has automatically picked up the format.

27
00:01:44,270 --> 00:01:49,910
Guys, this filing is important because GitLab will search only for this particular file name to find

28
00:01:49,910 --> 00:01:54,440
your query pipelines unless you have not changed its default behavior in settings.

29
00:01:55,570 --> 00:02:00,820
And from this dropdowns, you can choose from some default templates to make your job a little easy,

30
00:02:01,450 --> 00:02:02,830
but we learned from scratch.

31
00:02:05,120 --> 00:02:11,300
Now creating this pipeline, any pipeline configuration begins with defining jobs, which is mandatory.

32
00:02:11,840 --> 00:02:15,320
And again, defining a job starts with giving its name first.

33
00:02:15,860 --> 00:02:17,240
So let our job name be

34
00:02:21,440 --> 00:02:29,090
job to run the code and then under the job name, you can even specify to which stage it belongs to.

35
00:02:29,390 --> 00:02:33,140
Using duck stage, given mentioning the speech clause is optional.

36
00:02:33,380 --> 00:02:37,370
If you don't specify any stage, GitLab defaults to test stage.

37
00:02:38,240 --> 00:02:43,280
There is a full lecture coming up next in the course where you will get to know full details of how

38
00:02:43,280 --> 00:02:47,390
to define stage, what is a default behavior of it, and how to order them.

39
00:02:47,420 --> 00:02:49,310
These are things will come in that lecture.

40
00:02:50,280 --> 00:02:55,080
Containing the pipeline next, I redefined a script that is to be done under this job.

41
00:02:55,620 --> 00:02:57,600
This script is mandatory to write.

42
00:03:01,600 --> 00:03:08,110
Under this script tag, we will write a script name to write, you can write multiple task here like

43
00:03:08,110 --> 00:03:11,980
first balsam Docker image, then run this command and then this and so on.

44
00:03:12,460 --> 00:03:16,040
But in this case, as we simply want to run the hallowed be thy file.

45
00:03:16,390 --> 00:03:21,520
So I type the same python command that is used to run any Python fight, which is.

46
00:03:22,750 --> 00:03:25,260
Biton, hallowed be thy.

47
00:03:25,990 --> 00:03:30,580
And you can also see that get ED is automatically intending that Yemen fight.

48
00:03:31,930 --> 00:03:32,860
And yeah, that's it.

49
00:03:33,280 --> 00:03:34,750
First, Getler Pipeline is ready.

50
00:03:35,200 --> 00:03:39,190
We have successfully defined a job that will run up, bite and fight Motoko.

51
00:03:40,300 --> 00:03:44,440
Guys, please understand that for this pipeline, we have defined just a single job.

52
00:03:44,860 --> 00:03:48,040
But this is not the case in rail project in your pipeline.

53
00:03:48,250 --> 00:03:53,440
You define a good number of jobs and in fact, you can practically create a limited number of jobs if

54
00:03:53,440 --> 00:03:53,920
you want to.

55
00:03:54,610 --> 00:03:58,240
Even we will define multiple of them in the upcoming section of a real case study.

56
00:03:59,500 --> 00:04:02,800
All right, so the pipeline being defined, let's commit the changes we have done.

57
00:04:03,550 --> 00:04:04,120
Go right.

58
00:04:04,270 --> 00:04:07,150
Scroll down at the commit message.

59
00:04:14,990 --> 00:04:16,340
Target Branch, 51.

60
00:04:17,460 --> 00:04:18,750
Coming changes into one.

61
00:04:23,630 --> 00:04:25,490
Your changes have been successfully committed.

62
00:04:25,850 --> 00:04:27,500
And if we go to see this action.

63
00:04:31,130 --> 00:04:37,010
Good, after committing the moment, get notices that there is a pipeline for you to finding a rappel,

64
00:04:37,400 --> 00:04:39,560
GitLab has started running this pipeline for us.

65
00:04:40,340 --> 00:04:44,600
As you can see, the pipeline status is running and we have an electric grid.

66
00:04:45,110 --> 00:04:50,000
As soon as I made a comment in the repository and the GitLab S.A. Dot via email file is present.

67
00:04:50,580 --> 00:04:52,430
Level automatically bring up the pipeline.

68
00:04:53,600 --> 00:04:59,240
A quick side note notes from this page, you can view all of your pipelines, their statuses, pipeline

69
00:04:59,240 --> 00:05:05,750
and who has triggered that pipeline, etc. From this page, all of your pipelines will be present in

70
00:05:05,750 --> 00:05:12,530
this list, and from this search bar, you can filter your pipelines with options like author name,

71
00:05:12,800 --> 00:05:14,780
branch name, tag, name and status.

72
00:05:15,290 --> 00:05:20,540
And on the right side, if you see we have some time which denotes when this pipeline was triggered

73
00:05:21,200 --> 00:05:22,340
for how long it ran.

74
00:05:23,180 --> 00:05:25,100
And I'll come to these options later.

75
00:05:26,530 --> 00:05:26,890
OK.

76
00:05:26,950 --> 00:05:30,670
The pipeline has passed and green, it means it is success.

77
00:05:31,660 --> 00:05:33,220
To see more details, click on it.

78
00:05:37,000 --> 00:05:41,050
In this box, we got the same highlights that we got in the last page.

79
00:05:41,680 --> 00:05:48,370
Now the important thing which I want to show is here, OK, since pipelines can be complex structures

80
00:05:48,370 --> 00:05:50,350
with many sequential and payroll jobs.

81
00:05:50,830 --> 00:05:56,740
That is why to make it easier to understand the flow of a pipeline, GitLab has pipeline graphs for

82
00:05:56,740 --> 00:05:58,570
viewing pipelines and their statuses.

83
00:05:59,200 --> 00:06:05,050
This is a graphical representation of my first pipeline since we had only one job in our pipeline.

84
00:06:05,350 --> 00:06:11,290
So currently we have only one job to run the code block header under the default test stage.

85
00:06:11,710 --> 00:06:17,230
But as the number of jobs will increase, all those jobs will get listed here in the same order as you

86
00:06:17,230 --> 00:06:18,520
would have defined in our pipeline.

87
00:06:19,230 --> 00:06:23,590
And now let's open this job and see how the execution happens in next lecture.
