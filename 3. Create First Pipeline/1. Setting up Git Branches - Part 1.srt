1
00:00:00,600 --> 00:00:01,260
Welcome again.

2
00:00:01,800 --> 00:00:07,110
Now that we are a little bit familiar with jetlag, you and I have created our first project starting

3
00:00:07,110 --> 00:00:07,880
from this lecture.

4
00:00:07,890 --> 00:00:14,310
We will lean towards this lapse feature and we learn how one can write their pipelines to instruct GitLab

5
00:00:14,310 --> 00:00:14,850
what to do.

6
00:00:15,540 --> 00:00:20,340
Guys, in this class, I will try to mimic the compressed version of how development happens in real

7
00:00:20,340 --> 00:00:23,510
time projects and how does this whole pipeline run?

8
00:00:24,270 --> 00:00:29,510
So in any real name, project developers do not write their code directly onto this award.

9
00:00:29,970 --> 00:00:33,120
Of course, you know you won't start writing the code here in this panel.

10
00:00:33,690 --> 00:00:39,000
Rather, you first write your code in a local system and after some considerable amount of code is written,

11
00:00:39,270 --> 00:00:41,730
then you push that code from local to the supplier.

12
00:00:42,120 --> 00:00:47,460
And after that code is pushed to over, the next process is like build, test, deploy will start.

13
00:00:48,120 --> 00:00:49,770
So here also we will mimic the same.

14
00:00:50,430 --> 00:00:55,560
First, we will take this GitLab project to a local system, will do some changes into the code locally

15
00:00:55,830 --> 00:01:01,500
and then will push no changes to the GitLab support after GitLab notices some change and committed to

16
00:01:01,500 --> 00:01:01,950
the code.

17
00:01:02,280 --> 00:01:07,200
It will run that pipeline and would possibly test, build and deploy the code into different environments

18
00:01:07,200 --> 00:01:07,710
specified.

19
00:01:09,000 --> 00:01:11,940
Not to take this project local into my system, I will clone it.

20
00:01:14,980 --> 00:01:15,580
A.B..

21
00:01:16,300 --> 00:01:17,410
Open a new terminal window.

22
00:01:20,260 --> 00:01:21,790
Let me first install get.

23
00:01:46,000 --> 00:01:49,180
Now, let me go to the folder I have created.

24
00:01:50,610 --> 00:01:58,290
And to clone simply type get blown and then the URL where the original repository is located.

25
00:02:02,690 --> 00:02:03,410
Username.

26
00:02:07,500 --> 00:02:08,070
Password.

27
00:02:14,060 --> 00:02:19,850
Cloning a repository pulls down a full copy of all the project files and folders Git Repo has at that

28
00:02:19,850 --> 00:02:25,310
point in time, and one important thing is the operating system doesn't matter much when you are working

29
00:02:25,310 --> 00:02:28,280
on a cloud environment as it is accessed mostly from browser.

30
00:02:28,790 --> 00:02:32,360
So if you want to use some other operating system for it, you are free to choose.

31
00:02:32,690 --> 00:02:37,610
You can install get tools on windows for which I have either a good installation of Windows lecture

32
00:02:37,640 --> 00:02:42,620
in the bonus section of this course, you can take help from it working through the code, even though

33
00:02:42,620 --> 00:02:48,050
there is no need for any extra tools to run any application in GitLab since it is a fully managed cloud

34
00:02:48,050 --> 00:02:52,580
solution, but particularly for this code to make it understand better.

35
00:02:53,240 --> 00:02:57,770
We need some tools in a local system like Python, Docker, some runners also.

36
00:02:58,250 --> 00:03:03,620
All these installations are easy only on any of the operating systems, but still, since I would be

37
00:03:03,620 --> 00:03:09,860
using Ubuntu OS in the videos, so I would be attaching the documents, videos or references for installation

38
00:03:09,860 --> 00:03:11,360
on other systems side by side.

39
00:03:11,570 --> 00:03:12,470
You can take help from it.

40
00:03:13,680 --> 00:03:18,930
Anyways, going back to her original thing, a copy of First Project has been created locally into my

41
00:03:18,930 --> 00:03:21,510
system, if I would open the same.

42
00:03:29,610 --> 00:03:33,770
We have this first project and we got the same read me file as what?

43
00:03:34,800 --> 00:03:35,190
All right.

44
00:03:35,340 --> 00:03:36,960
We have a copy of Project in Local.

45
00:03:37,050 --> 00:03:37,680
What's next?

46
00:03:38,280 --> 00:03:39,120
Start working on it.

47
00:03:39,540 --> 00:03:39,990
But wait.

48
00:03:40,440 --> 00:03:42,690
Let's first check in which branch I'm currently in.

49
00:03:47,190 --> 00:03:48,780
Let's first go to the project.

50
00:03:51,240 --> 00:03:54,150
Now, get branch is the command.

51
00:03:56,500 --> 00:04:02,440
So we have only one branch right now, the master branch and green color means currently we are inside

52
00:04:02,440 --> 00:04:06,760
this branch now looking directly on the master branch could be disastrous.

53
00:04:07,120 --> 00:04:07,330
Why?

54
00:04:07,330 --> 00:04:12,550
Because this is a shared branch, so every other developer in the organization would be taking a cut

55
00:04:12,550 --> 00:04:18,370
from this master branch only if at all wrong or just push you into this master branch, then all the

56
00:04:18,370 --> 00:04:22,870
subsequent checkouts done by the team, they all will be working on top of that code.

57
00:04:23,440 --> 00:04:28,990
That is why, as a best practice, rather than making changes directly into master branch, people create

58
00:04:28,990 --> 00:04:34,150
a new branch from this original code, make their additions or changes to that branch, and then merge

59
00:04:34,150 --> 00:04:35,620
that code with the master branch.

60
00:04:36,040 --> 00:04:40,930
After testing it and when they have confidence that it's a valid change, in fact, you won't even need

61
00:04:40,960 --> 00:04:42,280
master branches or not.

62
00:04:42,280 --> 00:04:47,770
Approval to match your branch into master honor, which is some senior member of your team, will review

63
00:04:47,770 --> 00:04:50,170
your change and then only approves the request.

64
00:04:51,340 --> 00:04:55,960
OK, now to create a new branch Typekit branch.

65
00:04:57,210 --> 00:04:58,020
And the branch name.

66
00:04:59,270 --> 00:05:02,180
Devon means a developer one is working on something.

67
00:05:04,230 --> 00:05:04,980
It has created.

68
00:05:06,210 --> 00:05:09,420
Now, if I can get pregnant again.

69
00:05:11,720 --> 00:05:14,500
We have two branches now, Devon and Muster.

70
00:05:14,930 --> 00:05:16,940
But as you can see, most of them are still green.

71
00:05:17,270 --> 00:05:23,210
It means we are in muster only, so we need to navigate to a new branch, Devon, to navigate from one

72
00:05:23,210 --> 00:05:30,500
branch to another type, get checked out and the branch name which you want to navigate.

73
00:05:33,580 --> 00:05:38,830
It is switched to Branch Devon if I ran the branch again.

74
00:05:39,580 --> 00:05:41,770
See, Devon is in green.

75
00:05:42,490 --> 00:05:47,020
So from now on, any change you make, it will have their scope to this branch only.

76
00:05:47,940 --> 00:05:49,080
And if I do.

77
00:05:49,230 --> 00:05:54,510
Let's hear we got the same exact file contents of Master Branch.

78
00:05:55,350 --> 00:05:57,000
Everything is good now.

79
00:05:57,000 --> 00:05:59,790
You're all set to make any changes or additions in this project.

80
00:05:59,970 --> 00:06:03,360
We didn't have one branch and we will continue doing so in the next lecture.
