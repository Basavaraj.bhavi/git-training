1
00:00:00,330 --> 00:00:01,320
To keep it simple.

2
00:00:01,350 --> 00:00:04,110
I have a lot of very basic Python file only in my project.

3
00:00:04,650 --> 00:00:06,830
So in Terminal First, go to your project directory.

4
00:00:06,840 --> 00:00:12,030
I'm already in and open this project into Visual Studio Code, which is an I.D. from Microsoft.

5
00:00:12,660 --> 00:00:19,650
You open it in VSC, just type code space dot, or you can even directly imported from Visual Studio

6
00:00:19,650 --> 00:00:19,920
code.

7
00:00:21,200 --> 00:00:25,670
And in case if you don't have visual studio code installed, you can install it by running this command.

8
00:00:28,090 --> 00:00:28,360
OK.

9
00:00:28,420 --> 00:00:31,300
The project is important now to make a change in this project.

10
00:00:31,540 --> 00:00:33,010
Let's create a simple Python fight.

11
00:00:34,610 --> 00:00:35,870
Let's say hallowed, be thy.

12
00:00:40,800 --> 00:00:42,120
And right up in statement.

13
00:00:50,400 --> 00:00:50,970
Save it.

14
00:00:52,820 --> 00:00:57,380
Now, if I go back to terminal and run, get status here.

15
00:01:00,200 --> 00:01:07,100
Good status is showing us that there is one contract while hallowed be like we just created and is also

16
00:01:07,100 --> 00:01:10,670
showing us a hint to use, come get ad good racket.

17
00:01:11,300 --> 00:01:15,680
Let's get straight Eskom on displays the state of the working directory and the staging area.

18
00:01:16,370 --> 00:01:19,070
It shows us the changes that are being tracked by it.

19
00:01:19,550 --> 00:01:21,560
Let's now add this file to staging area.

20
00:01:23,480 --> 00:01:25,760
Get AD and then the filing.

21
00:01:26,570 --> 00:01:27,440
Hello, not be.

22
00:01:30,530 --> 00:01:34,910
Running get ad campaign ads, a change in the working directory to the staging area.

23
00:01:35,480 --> 00:01:40,010
It tells get that you want to include updates to a particular file in the next comment.

24
00:01:40,640 --> 00:01:43,160
Now let's again check good status one more time.

25
00:01:45,110 --> 00:01:46,520
There is nothing left to be stage.

26
00:01:46,910 --> 00:01:51,140
Hello, not be in green means it is staged and pending to be committed.

27
00:01:51,860 --> 00:01:55,670
Breaking a new fight doesn't really affect the repository in any significant way.

28
00:01:56,000 --> 00:02:01,850
It's just for reference purposes, and the changes you made are not actually recorded until you committed.

29
00:02:02,630 --> 00:02:07,160
So for changes to reflect in the project, we need to render good comment.

30
00:02:07,160 --> 00:02:10,280
Command Typekit commit.

31
00:02:11,450 --> 00:02:13,850
And then pass any comment message.

32
00:02:22,210 --> 00:02:24,310
This message will help you identify that change.

33
00:02:27,390 --> 00:02:29,470
OK, get tested on us and add it.

34
00:02:29,670 --> 00:02:32,250
And is asking to set some global username and e-mails.

35
00:02:32,670 --> 00:02:33,420
I will do it quickly.

36
00:02:34,020 --> 00:02:35,520
First set the global email.

37
00:02:51,050 --> 00:02:52,040
And said the name.

38
00:02:59,530 --> 00:03:00,230
Is being set.

39
00:03:00,250 --> 00:03:01,540
Let's commit the changes again.

40
00:03:05,230 --> 00:03:11,800
Done with this, all the staging files have been committed to my local repository to get status one

41
00:03:11,800 --> 00:03:12,250
more time.

42
00:03:14,100 --> 00:03:15,630
There is nothing to commit.

43
00:03:16,750 --> 00:03:21,700
Guys, one important thing to understand here is that the changes we have made till now are done locally

44
00:03:21,700 --> 00:03:25,420
in this system, and they won't be reflected in the survey as of now.

45
00:03:25,960 --> 00:03:27,400
So if I go back to GitLab.

46
00:03:29,560 --> 00:03:30,070
Refresh.

47
00:03:33,950 --> 00:03:36,050
And go to this branch's section.

48
00:03:37,920 --> 00:03:43,830
See, on GitLab server, we still have only one, the master branch Devon branch is still unknown to

49
00:03:43,830 --> 00:03:44,010
it.

50
00:03:46,250 --> 00:03:51,170
Now, that's part of the three principles, after some certain set of code is created or at least once

51
00:03:51,170 --> 00:03:57,260
a day, you upload those changes onto the remote repository that is the GitLab server and afterwards

52
00:03:57,260 --> 00:03:58,980
integrate them into the master branch.

53
00:03:59,000 --> 00:03:59,330
Right?

54
00:04:00,260 --> 00:04:02,090
And yes, before uploading the code.

55
00:04:02,090 --> 00:04:07,100
Indeed, you can definitely test your code locally so as to reduce the chances of defective merge into

56
00:04:07,100 --> 00:04:07,760
the master code.

57
00:04:08,210 --> 00:04:13,010
But for this example, I will simply push the code to get less of it and will run it directly there

58
00:04:13,910 --> 00:04:16,940
to push it to remote run, get push command.

59
00:04:17,360 --> 00:04:18,350
Let me first clear this.

60
00:04:20,960 --> 00:04:22,040
Now don't get push.

61
00:04:24,970 --> 00:04:25,330
OK.

62
00:04:25,510 --> 00:04:28,250
This was expected as a branch we are pushing.

63
00:04:28,270 --> 00:04:33,340
Devon is just a local branch and GitLab has no entry of it and don't oppose it on somebody.

64
00:04:33,700 --> 00:04:37,030
That's why the push failed, so we have to set upstream for that.

65
00:04:38,410 --> 00:04:39,310
Simply copy this.

66
00:04:43,880 --> 00:04:44,450
And based.

67
00:04:47,810 --> 00:04:52,310
I need to provide my username and password to authenticate myself and then push to change.

68
00:05:04,480 --> 00:05:05,020
It's done.

69
00:05:05,600 --> 00:05:07,480
Now if I check good status again.

70
00:05:12,630 --> 00:05:13,170
Excellent.

71
00:05:13,710 --> 00:05:15,270
Your branch is up to date.

72
00:05:16,200 --> 00:05:18,900
If I go back to you and refresh.

73
00:05:26,360 --> 00:05:27,020
Here we go.

74
00:05:27,290 --> 00:05:29,600
Now our project has two branches in silver as well.

75
00:05:32,820 --> 00:05:37,230
Since we haven't touched a master branch, so in here we have only read me fight.

76
00:05:37,770 --> 00:05:39,600
And if I switch to one branch.

77
00:05:44,600 --> 00:05:48,700
In Devon, there are two files that need me and hello, not be very great.

78
00:05:49,550 --> 00:05:53,330
Now the next step would be to merge these branches so as to integrate the court.

79
00:05:54,660 --> 00:05:59,850
A quick note, nice idea, you should not be watching the feature code into Master Branch without testing.

80
00:06:00,150 --> 00:06:04,980
You should test it first with some test scripts in a pipeline and then only it is a budget request.

81
00:06:05,310 --> 00:06:08,760
But since in here we are at a very basic level of CSI the pipeline.

82
00:06:09,000 --> 00:06:10,380
So escaping it for the time being.

83
00:06:10,620 --> 00:06:15,930
But as you progress in the testing part, is also cut back to create a budget request.

84
00:06:16,110 --> 00:06:21,900
As you can see from here, after push a new option game here, great multi request, click on it.

85
00:06:25,400 --> 00:06:31,070
We got this much details from that is one way of creating much request or the other way.

86
00:06:31,100 --> 00:06:34,790
You can also create a budget request is from this budget request.

87
00:06:40,730 --> 00:06:44,300
That former major cost option is only for the change that we made.

88
00:06:44,840 --> 00:06:49,580
But this is a generic option from where you can generate much request for other projects as well.

89
00:06:50,630 --> 00:06:54,560
So from here to the source branch, you want too much.

90
00:06:55,250 --> 00:06:55,850
Devon.

91
00:06:57,260 --> 00:07:00,650
And then the target branch in which you want too much that is mustered only.

92
00:07:03,350 --> 00:07:05,420
And click compare branches and continue.

93
00:07:07,790 --> 00:07:10,130
Here you will get the same form we already saw.

94
00:07:11,330 --> 00:07:13,400
Let's keep this title as it.

95
00:07:14,090 --> 00:07:16,680
And that description be added.

96
00:07:17,570 --> 00:07:18,650
Hello, not by.

97
00:07:20,880 --> 00:07:22,710
I'll keep the remaining options untouched.

98
00:07:23,700 --> 00:07:30,330
And last, let me uncheck this delete option, which will otherwise delete the Source Devon branch automatically

99
00:07:30,510 --> 00:07:31,620
once the Mod is approved.

100
00:07:31,950 --> 00:07:35,190
I want Devon Grant still be there after the match, so uncheck this.

101
00:07:40,250 --> 00:07:45,350
Much of the question is created since I'm logged in as a maintainer off this project, so I have the

102
00:07:45,350 --> 00:07:46,220
authority to much.

103
00:07:48,770 --> 00:07:49,730
Much of this change.

104
00:07:53,870 --> 00:07:55,330
The changes were much to muster.

105
00:07:55,430 --> 00:07:56,930
Let's go back to project details.

106
00:08:07,440 --> 00:08:07,890
Great.

107
00:08:08,550 --> 00:08:12,420
Hello, not be late is there in Master Branch and the master branch is up to it.

108
00:08:13,180 --> 00:08:19,590
So this is the first principle we have followed that is keep a shared code repository and regularly

109
00:08:19,590 --> 00:08:21,660
integrate the changes into that repository.

110
00:08:22,320 --> 00:08:22,740
Thank you.
