1
00:00:00,330 --> 00:00:06,060
Project requirements being clear in this lecture, we will list down the sequence of orchestrator task

2
00:00:06,300 --> 00:00:12,030
and try to create a workflow that will give us a holistic view of how the project requirements can be

3
00:00:12,030 --> 00:00:12,300
met.

4
00:00:13,110 --> 00:00:17,280
Not only the holistic view, we will also figure it out throughout the pipeline.

5
00:00:17,430 --> 00:00:21,600
What our tools would be required and at what phase to help achieving our goal.

6
00:00:22,350 --> 00:00:24,960
Broadly thing, we need to achieve two major goals here.

7
00:00:25,930 --> 00:00:32,110
First is to create an employer portal where the claims can view, edit or deleted employer details.

8
00:00:32,620 --> 00:00:39,220
And second, for what we all are here in this course, that is right to get lepeska pipeline for frequent

9
00:00:39,220 --> 00:00:45,520
and seamless deployments, we will enable the client to do modifications, add new features in the application

10
00:00:45,940 --> 00:00:50,920
and release the changes to live market with just one single commit without any downtime.

11
00:00:51,520 --> 00:00:55,660
Thus, in this way, they always have a production ready application with them.

12
00:00:56,950 --> 00:00:58,930
OK, so let's start designing our workflow.

13
00:00:59,840 --> 00:01:05,000
So if you remember, this is the complete, the pipeline that we got from the first section of this

14
00:01:05,000 --> 00:01:08,870
course, that's since this is a generic CSG pipe in Waterloo.

15
00:01:09,200 --> 00:01:15,050
So for this project as well, I will follow the same guidelines from this pipeline and will the project

16
00:01:15,050 --> 00:01:15,530
around it.

17
00:01:16,730 --> 00:01:21,800
Obviously, there will be multiple team members involved in this project with their own set of skills

18
00:01:21,800 --> 00:01:22,760
and defined task.

19
00:01:23,950 --> 00:01:28,830
So the first task would be to develop the employer portal application application.

20
00:01:29,080 --> 00:01:32,620
I'm talking about the core application without any involved.

21
00:01:33,430 --> 00:01:39,460
So for the app development language, we will use the popular programming language Python to code and

22
00:01:39,460 --> 00:01:42,670
within Python since it would be a web application.

23
00:01:43,000 --> 00:01:49,120
So will use its popular web framework flask that will help us develop an app easily and fast.

24
00:01:49,930 --> 00:01:52,990
Then comes the UI and backend stuff for you.

25
00:01:53,440 --> 00:02:00,010
We will develop it with simple HDMI access and will also take leverage from the Python template to make

26
00:02:00,010 --> 00:02:01,750
the frontend development a bit fast.

27
00:02:02,700 --> 00:02:08,700
Next, as part of database, we will use Simple SQLite, which is a high quality relational database

28
00:02:08,700 --> 00:02:09,540
management system.

29
00:02:10,880 --> 00:02:14,300
So you see multiple teams would be doing multiple task badly.

30
00:02:14,660 --> 00:02:18,170
Meanwhile, maintaining the code in one single shared repository.

31
00:02:19,420 --> 00:02:25,060
This would be enough for developing the employer application, and with this, our first goal of developing

32
00:02:25,060 --> 00:02:26,710
the application would be achieved.

33
00:02:27,520 --> 00:02:33,730
Now coming to the second segment of our goal, the Gettler pipeline part for that we will try to get

34
00:02:33,730 --> 00:02:35,200
Lepeska Dot via email file.

35
00:02:35,440 --> 00:02:42,370
The pipeline fight in where we will automate things, things like testing, building, staging, publishing

36
00:02:42,370 --> 00:02:43,910
and so and I spoke on order.

37
00:02:44,800 --> 00:02:50,440
Inside the pipeline, the first task would be to test the new code as soon as there is a new commit

38
00:02:50,440 --> 00:02:51,280
in the code rappel.

39
00:02:51,880 --> 00:02:54,970
This will allow us to catch any flaws at the earliest.

40
00:02:55,690 --> 00:03:00,760
We will do a few types of testing on the code Lynch testing, smoke testing and unit testing.

41
00:03:01,480 --> 00:03:05,740
Lynch testing to analyze the code for any programmatic or stylistic errors.

42
00:03:06,250 --> 00:03:12,310
And since it is a Python application, so limited test, we will do it with flickered then in smoke

43
00:03:12,310 --> 00:03:12,760
testing.

44
00:03:12,790 --> 00:03:19,720
We will see if that set page is returning as the expected response and statistical then unit testing

45
00:03:19,720 --> 00:03:23,080
will be included to test each individual method of the application.

46
00:03:24,040 --> 00:03:27,850
For unit and smoke testing, we will use a Python tool called by test.

47
00:03:28,540 --> 00:03:30,810
I will use these flick it and bite tools.

48
00:03:31,210 --> 00:03:34,180
But of course, you can play with other tools available in the market.

49
00:03:35,650 --> 00:03:41,620
OK, so once the tests are passed, the pipeline will translate to build stage, where we will build

50
00:03:41,620 --> 00:03:47,170
a package out of our quote, we will use Docker here and build a Docker image of the application.

51
00:03:47,860 --> 00:03:52,300
Basically, we have to write a Docker file and specify the application requirements in it.

52
00:03:53,480 --> 00:03:58,130
Now, after the application has passed, the initial test executable package is built.

53
00:03:58,910 --> 00:04:04,160
The next thing we will do is we will deploy our application in a staging environment first, which is

54
00:04:04,160 --> 00:04:10,490
sort of test environment and as a service provider or the host, we will use Heroku platform here.

55
00:04:11,270 --> 00:04:18,710
Heroku is a platform as a service that enables developers to build, run and operate applications entirely

56
00:04:18,710 --> 00:04:19,310
in the cloud.

57
00:04:20,580 --> 00:04:25,560
After the app is released in a test environment, we have two options for acceptance testing.

58
00:04:26,340 --> 00:04:33,060
Either way, we could employ the test team to do a total acceptance testing for all the UI, logic,

59
00:04:33,060 --> 00:04:37,830
etc. and then they take a decision to release that feature to public or not.

60
00:04:38,460 --> 00:04:44,310
Or the second way could be to automate this step as well by introducing automated test scripts to take

61
00:04:44,310 --> 00:04:44,940
this decision.

62
00:04:45,870 --> 00:04:50,070
I will do kind of both these testings and after this acceptance testing.

63
00:04:50,640 --> 00:04:55,280
Finally, the application will be deployed to live production environment for production.

64
00:04:55,290 --> 00:04:57,900
Also, I'm going to use the Iroko cloud services.

65
00:04:59,560 --> 00:05:05,350
After this application is live, if you want to make any changes to your application, like if you want

66
00:05:05,350 --> 00:05:08,950
to change it, you'll see some logics or whatever under the Sun.

67
00:05:09,350 --> 00:05:15,190
With this pipeline being set, you will be able to list that feature to public with just a single click.

68
00:05:15,580 --> 00:05:16,660
That's the issue before you.

69
00:05:17,850 --> 00:05:23,760
But so far, so now with this road map in our hand from the next lecture, let's start building an application.

70
00:05:23,940 --> 00:05:24,450
See you there!
