1
00:00:00,820 --> 00:00:02,060
Database being defined.

2
00:00:02,290 --> 00:00:04,780
Let's start creating the front end of our application.

3
00:00:05,820 --> 00:00:06,600
For Front-End.

4
00:00:11,000 --> 00:00:12,650
I would create a separate front and branch.

5
00:00:18,650 --> 00:00:20,240
Models and requirements is there.

6
00:00:21,050 --> 00:00:24,560
Let's hope up to visual studio code and start writing the front end quote.

7
00:00:33,640 --> 00:00:36,910
Guys, for developing their frontline environment, we will use template.

8
00:00:37,390 --> 00:00:43,600
As with templating variables and programming, logic can be easily embedded into the code for templating.

9
00:00:43,630 --> 00:00:46,690
We would require two folders the static and templates.

10
00:00:51,020 --> 00:00:51,710
Static.

11
00:00:59,680 --> 00:01:05,200
Instead, Nick Folder, we generally keep the content like she says, JavaScript files and in templates

12
00:01:05,200 --> 00:01:09,490
folder, the estimer templates are kept, they're stocked with the templates.

13
00:01:10,870 --> 00:01:16,510
First, I will create a base template for our application, which is the template or estimable file.

14
00:01:21,060 --> 00:01:22,050
And based on here.

15
00:01:25,400 --> 00:01:30,470
This is a basic steel mill code that every address G.M. page in our application will inherit.

16
00:01:31,070 --> 00:01:32,270
There is nothing special in it.

17
00:01:32,510 --> 00:01:36,860
Just the basic SDL code importing the font, the styles, etc..

18
00:01:37,810 --> 00:01:42,040
Next, let me based on CSIS, not redefine the styling of this application.

19
00:01:43,010 --> 00:01:43,940
Inside the static.

20
00:01:46,610 --> 00:01:48,710
Create a template or CSV file.

21
00:01:50,970 --> 00:01:51,630
Base support.

22
00:01:55,050 --> 00:02:00,990
This ceasefire handles all the styling features of our application, you know, like color, borders,

23
00:02:00,990 --> 00:02:01,890
fonts, etc..

24
00:02:02,670 --> 00:02:09,270
Save it and just be assured that each and every code or file used in this project is provided to you

25
00:02:09,270 --> 00:02:11,910
in the resources that you don't have to write anything for it.

26
00:02:12,940 --> 00:02:15,760
Moving next, this application will have three different pages.

27
00:02:16,210 --> 00:02:20,260
First is that index, which is the main page where all the employees will be listed.

28
00:02:20,830 --> 00:02:27,250
Second is the ad page from where we can add a new employee and a third page is edit page to edit them.

29
00:02:27,250 --> 00:02:29,180
Implied it up for the detection.

30
00:02:29,200 --> 00:02:31,450
There is no need of any separate H.M. page.

31
00:02:31,720 --> 00:02:33,100
It will be a simple button only.

32
00:02:33,730 --> 00:02:35,770
Let's not create these three pages.

33
00:02:36,810 --> 00:02:37,710
I'll create and paste.

34
00:02:42,600 --> 00:02:43,860
And next door testimony.

35
00:02:52,040 --> 00:02:52,490
And.

36
00:02:57,140 --> 00:02:58,220
Additional testing.

37
00:03:18,510 --> 00:03:19,830
Starting with the next fight.

38
00:03:22,140 --> 00:03:28,470
But the first line would declare Templegate a.m. as the parent of this current page index or estimate,

39
00:03:28,770 --> 00:03:33,270
which means index don't HDMI will inherit from empty dot at Stamford pitch.

40
00:03:34,870 --> 00:03:40,570
Then this block even specifies the head section of this index page and within this section.

41
00:03:42,270 --> 00:03:47,040
You can add or replace the original code that this file is inheriting from its parent template.

42
00:03:48,140 --> 00:03:52,070
And if I scroll down more at Pitt in the body section.

43
00:03:54,640 --> 00:04:00,820
We are listing the employers data in some clean visual form see here, the culprit is the employee data

44
00:04:00,820 --> 00:04:01,990
will come from a database.

45
00:04:02,740 --> 00:04:04,750
Then we have the edit and delete buttons.

46
00:04:05,850 --> 00:04:07,350
Now coming to our page.

47
00:04:09,980 --> 00:04:11,240
In brief, if I would say.

48
00:04:12,390 --> 00:04:19,260
In A.T.M., we have a foam with metal post that will take input from a user and see if that input to

49
00:04:19,260 --> 00:04:19,890
the database.

50
00:04:21,250 --> 00:04:28,270
Similarly, we have the edit page here, also we have a form that forced method using which one can

51
00:04:28,270 --> 00:04:29,050
edit the employed.

52
00:04:29,090 --> 00:04:33,370
It's nice if you're not a front end developer, you don't need to worry about all these codes.

53
00:04:33,820 --> 00:04:38,140
Just copy and paste them if you are doing hands on and you concentrate only on the metal apart.

54
00:04:38,650 --> 00:04:39,010
All right.

55
00:04:39,220 --> 00:04:42,730
So as HTML pages are being done, if I show you the output, no.

56
00:04:50,450 --> 00:04:52,100
Starting with that in next page.

57
00:04:55,350 --> 00:04:59,400
It seems quite weird, actually, since as of now, this is just a nets.

58
00:05:00,120 --> 00:05:04,920
The application hasn't been set with Python, so it's showing us the code inside calibrates.

59
00:05:05,190 --> 00:05:10,140
But once we use the render underscore template from Python, everything will be replaced with the actual

60
00:05:10,140 --> 00:05:10,530
values.

61
00:05:10,680 --> 00:05:11,940
And this will render properly.

62
00:05:13,380 --> 00:05:14,100
Similarly.

63
00:05:17,370 --> 00:05:18,300
We're going to add.

64
00:05:22,330 --> 00:05:23,290
And Elliot Page.

65
00:05:24,500 --> 00:05:27,200
What so far, I guess enough of frontend work is done.

66
00:05:27,470 --> 00:05:30,140
We have prepared the bare skeleton of our application.

67
00:05:30,500 --> 00:05:31,490
Let's commit the changes.

68
00:05:32,450 --> 00:05:33,920
Let me check if everything is saved.

69
00:05:38,410 --> 00:05:39,070
This is spending.

70
00:05:59,750 --> 00:06:00,560
Back to you, White.

71
00:06:09,330 --> 00:06:09,930
Have we got.

72
00:06:10,260 --> 00:06:13,680
We have a third front and branch and all the files present here.

73
00:06:14,840 --> 00:06:16,070
Let's watch this as well.

74
00:06:40,340 --> 00:06:40,650
Done.

75
00:06:41,300 --> 00:06:42,740
That's pretty much it for end.

76
00:06:42,950 --> 00:06:45,320
See you in the next lecture with abducTee Michael's.
