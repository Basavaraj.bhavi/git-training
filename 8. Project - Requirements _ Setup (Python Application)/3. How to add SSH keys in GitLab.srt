1
00:00:00,760 --> 00:00:02,290
Welcome, guys, to this small lecture.

2
00:00:02,830 --> 00:00:07,960
Before we start developing the application, a small prerequisite, we have to complete once and for

3
00:00:07,960 --> 00:00:12,100
all, which is setting up offices searches for GitLab on Ubuntu.

4
00:00:12,730 --> 00:00:18,610
If you remember on the start of GitLab, we got a popup saying you won't be able to pull or push repositories.

5
00:00:18,790 --> 00:00:24,550
Why, as I search until you add as I search key to your profile, actually, GitLab uses the search

6
00:00:24,550 --> 00:00:29,860
protocol was securely communicate with Git, and since we haven't said as I search for our account,

7
00:00:30,160 --> 00:00:31,540
that's why that error was there.

8
00:00:32,050 --> 00:00:38,110
And if anyone if you don't know about this, such as this, it is an acronym for a secure shell or secure

9
00:00:38,140 --> 00:00:38,770
socket shell.

10
00:00:39,310 --> 00:00:45,250
It is a cryptographic network communication protocol that enables two computers to communicate and share

11
00:00:45,250 --> 00:00:48,220
data over an insecure network such as internet.

12
00:00:49,060 --> 00:00:54,340
It is often used to log in to a remote server and perform operations on remote computers, and also

13
00:00:54,340 --> 00:01:00,130
transfers data from one machine to another, basically by adding such keys to your GitLab account.

14
00:01:00,400 --> 00:01:05,470
You don't need to supply your username and password each time for authentication to the GitLab remote

15
00:01:05,470 --> 00:01:05,830
server.

16
00:01:06,910 --> 00:01:10,920
All right, so let's set up as the search goes for GitLab on Ubuntu system.

17
00:01:12,620 --> 00:01:17,990
Guys, before you set up, as I said, to communicate with GitLab, you need to make sure of a few prerequisites.

18
00:01:18,530 --> 00:01:21,380
First, you need open such client on your system.

19
00:01:21,860 --> 00:01:23,600
Well, it comes pre-installed on Linux.

20
00:01:24,140 --> 00:01:30,620
Second, as this situation should be 6.5 or later, earlier versions use an MD5 signature, which is

21
00:01:30,620 --> 00:01:36,290
not secure, less susceptible, you know, in my system, in the terminal type.

22
00:01:36,510 --> 00:01:38,720
As such, we gaps.

23
00:01:41,270 --> 00:01:41,780
It's good.

24
00:01:42,790 --> 00:01:47,650
Nice to communicate with GitLab, GitLab allows you to choose from these type of assets, such keys,

25
00:01:48,070 --> 00:01:54,890
we will use RSA and to generate a new pair of such key in the terminal type as such.

26
00:01:56,540 --> 00:02:02,260
Gwich'in I fonti and then pass the archetype RSA.

27
00:02:03,680 --> 00:02:04,580
That is for.

28
00:02:05,880 --> 00:02:07,770
To zero for it better, I say.

29
00:02:08,460 --> 00:02:11,460
And you can also pass an optional comment using see?

30
00:02:16,370 --> 00:02:21,260
This government is included in that dot the Eubie file that will be created as part of this command

31
00:02:21,710 --> 00:02:22,270
hereunder.

32
00:02:24,050 --> 00:02:25,730
I guess see is in gaps.

33
00:02:29,700 --> 00:02:35,700
It says and a file in which to save the key here, either you can specify the specific part where you

34
00:02:35,700 --> 00:02:39,030
want to store your keys or can simply accept the default path.

35
00:02:39,540 --> 00:02:41,160
I will go with default and end up.

36
00:02:43,410 --> 00:02:47,400
Since I already have one set of Keys generator, so it is asking me to overwrite, yes.

37
00:02:49,780 --> 00:02:55,990
And their passphrase, this passphrase is similar to a password and generally refers to a secret use

38
00:02:56,000 --> 00:02:57,370
to protect an encryption key.

39
00:02:57,730 --> 00:03:01,440
Skip it For now, just enter into.

40
00:03:02,760 --> 00:03:03,390
Here we go.

41
00:03:03,600 --> 00:03:06,640
As I switch gears, I generator he's being generated.

42
00:03:06,660 --> 00:03:09,050
Let's now use these keys into our GitLab account.

43
00:03:10,080 --> 00:03:16,050
To use such bad GitLab, we need to copy the publicly from this system and need to place them into a

44
00:03:16,050 --> 00:03:20,130
GitLab account to view the public keys first copied a key part.

45
00:03:26,170 --> 00:03:27,280
And then got.

46
00:03:30,190 --> 00:03:30,790
Here it is.

47
00:03:31,660 --> 00:03:35,770
These are the contents of my public if I copy this whole content.

48
00:03:37,600 --> 00:03:42,220
While copying, make sure you copy the entire key, which starts with a set hyphen odyssey.

49
00:03:46,650 --> 00:03:47,970
Now hope to get a.

50
00:03:49,890 --> 00:03:53,040
On the top bar in the right corner, go to Preferences.

51
00:03:54,870 --> 00:03:57,060
On the left sidebar, as I said, she's.

52
00:03:58,640 --> 00:04:02,330
And this text box, we need to page the public, as I said, Keys.

53
00:04:03,390 --> 00:04:08,820
In the title box, you can type any description like walk back practice or whatever, and next in that

54
00:04:08,820 --> 00:04:09,870
expires at Box.

55
00:04:10,110 --> 00:04:11,580
You can select an expiration date.

56
00:04:11,760 --> 00:04:12,630
This is optional, though.

57
00:04:12,960 --> 00:04:14,640
And finally, click on Add Button.

58
00:04:17,060 --> 00:04:17,810
Congratulations.

59
00:04:18,180 --> 00:04:23,400
The such guys are added into GitLab from now on authenticate to the GitLab remote server.

60
00:04:23,420 --> 00:04:27,890
You don't need to supply your username and password each time, as has its skills are there for you.

61
00:04:28,220 --> 00:04:32,210
And yes, in case if you want to delete this case, you can simply click on this delete button.

62
00:04:33,110 --> 00:04:38,660
So keys being added to verify that your SSL skis were added correctly or not, go back to terminal.

63
00:04:41,980 --> 00:04:48,190
And run this command, so such high fund capital to get it.

64
00:04:50,290 --> 00:04:51,170
The clear protocol.

65
00:04:55,690 --> 00:04:56,020
Great.

66
00:04:56,500 --> 00:05:01,270
We got a welcome message with my username at GitLab means a successful connection was made.

67
00:05:02,050 --> 00:05:06,610
Alright, so guys, this was how you can add such keys into your get Lebogang on Ubuntu.

68
00:05:06,940 --> 00:05:09,430
Let's continue with the project development from next lecture.
