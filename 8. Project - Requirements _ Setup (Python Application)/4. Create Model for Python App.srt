1
00:00:00,630 --> 00:00:04,140
Let's put ourselves in développés shoes and start building the project.

2
00:00:04,780 --> 00:00:09,960
We will lay the foundation of our Catholic culture by creating the Python application first and then

3
00:00:09,960 --> 00:00:17,090
gradually move towards writing Casilli workflow by adding desks built in the YAML fight in this video,

4
00:00:17,130 --> 00:00:18,270
or maybe in the next one.

5
00:00:18,270 --> 00:00:24,240
Also, we will fast moving create the employer portal Python application and will lean a little towards

6
00:00:24,240 --> 00:00:25,020
the Python code.

7
00:00:25,380 --> 00:00:31,170
So in case if you are here to learn only the pipeline stuff, then you can skip this build application

8
00:00:31,170 --> 00:00:36,750
related Python code lectures and can join from the next lectures where I have started writing the Yamal

9
00:00:36,750 --> 00:00:37,080
file.

10
00:00:37,470 --> 00:00:43,170
But personally, I would recommend you do watch these lectures and stick to the floor for better understanding.

11
00:00:43,920 --> 00:00:45,840
Anyway, let's start from scratch.

12
00:00:46,560 --> 00:00:49,080
I will be creating a new fresh GitLab project.

13
00:00:53,590 --> 00:00:54,490
Project Be.

14
00:00:57,700 --> 00:00:58,690
Employee portal.

15
00:01:01,770 --> 00:01:02,450
Description.

16
00:01:10,910 --> 00:01:12,020
Check this file.

17
00:01:18,780 --> 00:01:25,350
Project been created now as we are discussing it from the start of the course, to adopt a river culture.

18
00:01:25,770 --> 00:01:30,810
The first requisite of the act is we will never work in the master, the main branch of coal.

19
00:01:31,230 --> 00:01:36,600
Rather, you, your teams should create different branches for themselves to make sure that the master

20
00:01:36,600 --> 00:01:39,030
branch is always bug free and deployable.

21
00:01:39,510 --> 00:01:40,980
So here also we will do the same.

22
00:01:41,640 --> 00:01:47,340
I may create several branches for Python developers, frontend developers, backend developers, etc,

23
00:01:47,640 --> 00:01:53,820
and well time to time most of the changes from their individual branches into the main code so as to

24
00:01:53,820 --> 00:01:57,660
keep the master branch up to date back to terminal.

25
00:01:59,080 --> 00:02:01,390
First of all, create a new directory for the project.

26
00:02:02,260 --> 00:02:03,460
I mean, to get to the folder.

27
00:02:10,870 --> 00:02:11,470
Go to it.

28
00:02:14,690 --> 00:02:19,400
Now inside this project, create a blanket repository with good and IT command.

29
00:02:21,890 --> 00:02:29,690
Now we need to create a connection to a remote repository for that good remote and a region.

30
00:02:42,730 --> 00:02:48,010
But I think the connection I'm making sure that whatever Bush polls I do, I would be doing it on the

31
00:02:48,010 --> 00:02:51,490
employer portal project, everything set up now.

32
00:02:51,490 --> 00:02:56,470
The first quote I will write is to create a database and database model class for employers.

33
00:02:59,200 --> 00:03:00,640
So create a new branch.

34
00:03:03,220 --> 00:03:03,790
Let's say.

35
00:03:06,040 --> 00:03:11,890
Guys, usually in life projects, was that a feature branch name as identified by a story number or

36
00:03:11,890 --> 00:03:12,430
story name?

37
00:03:13,090 --> 00:03:19,840
But here I will simply use a relevant name back in models and remember that such keys should be added

38
00:03:19,840 --> 00:03:22,570
into your GitLab account, as explained in previous lecture.

39
00:03:24,740 --> 00:03:32,270
Then pulled from that region, that is the remote main branch, get pole origin.

40
00:03:35,910 --> 00:03:41,310
Do let's just read me if I listed not taking this project to Visual Studio.

41
00:03:48,800 --> 00:03:55,100
I will start with defining the model for class that will represent the table in our database, so create

42
00:03:55,100 --> 00:03:55,760
a new folder.

43
00:03:58,960 --> 00:03:59,500
Models.

44
00:04:01,510 --> 00:04:02,260
And inside it.

45
00:04:03,300 --> 00:04:04,140
Create a new file.

46
00:04:07,410 --> 00:04:08,460
Models don't be like.

47
00:04:10,440 --> 00:04:16,320
Now, guys, since we are in to get lepeska records, so writing the Python code line by line and explaining

48
00:04:16,320 --> 00:04:20,220
them in detail would go out of their scope and we will be off the track.

49
00:04:20,610 --> 00:04:26,160
So keeping in mind the court's relevancy and side by side making sure that we understand the overall

50
00:04:26,160 --> 00:04:31,500
idea of Python code, I will paste the Python code in block and explain them in a concise manner.

51
00:04:32,970 --> 00:04:35,130
So paste the model's code.

52
00:04:36,500 --> 00:04:43,630
And giving you a good idea of this code, at first, we have imported the sequel Alchemy to perform

53
00:04:43,640 --> 00:04:48,710
grand operations on database using it all sequel in flask web applications can be clumsy.

54
00:04:49,130 --> 00:04:55,130
So instead of using Recycle Equal, we generally use Cycle Alchemy, which is the Python sequel toolkit

55
00:04:55,370 --> 00:05:01,250
and object relation method that gives application developers the full power and flexibility of sequel.

56
00:05:01,910 --> 00:05:08,150
It is basically a library that facilitates the communication between Python programs and databases that

57
00:05:08,150 --> 00:05:14,510
translates Python classes to tables on relational databases and automatically convert function calls

58
00:05:14,510 --> 00:05:15,470
to SQL statements.

59
00:05:17,120 --> 00:05:19,730
And this flask cycle alchemy.

60
00:05:20,210 --> 00:05:25,430
It is an extension for Flask that adds support for sequel alchemy to Water Flask application.

61
00:05:26,090 --> 00:05:28,400
Python developers would definitely know about this.

62
00:05:29,570 --> 00:05:33,830
Then this DB, the object of sequel Alchemy class, is created.

63
00:05:34,250 --> 00:05:38,600
This objective will contain the helper functions for autumn operations.

64
00:05:39,710 --> 00:05:46,610
And next, we have a class named imply that inherits from the base model class provided by sequel alchemy

65
00:05:47,300 --> 00:05:48,440
in this class.

66
00:05:48,830 --> 00:05:52,760
We are basically creating a table named employee with the required columns.

67
00:05:53,450 --> 00:06:00,170
So an employed table we have the ID, name, gender address, phone, salary and department.

68
00:06:00,920 --> 00:06:04,640
Each column is defined with data type and constraints, if any.

69
00:06:05,240 --> 00:06:08,390
For example, this ID is an integer.

70
00:06:09,450 --> 00:06:12,540
And it will also act as a primary key for the stable.

71
00:06:13,930 --> 00:06:19,050
OK, so you can say with this model's not be quite the database object and people are different.

72
00:06:19,630 --> 00:06:20,470
No, what's next?

73
00:06:21,510 --> 00:06:27,480
Next, we need to define requirements, not the extra fight requirements fight you, all we know is

74
00:06:27,480 --> 00:06:32,190
a file used for specifying what Python packages you need to run the project.

75
00:06:32,640 --> 00:06:37,050
It basically contains the list of dependencies needed to run a Python application.

76
00:06:37,710 --> 00:06:42,780
So whenever you write a Python application, it is always recommended that do a side by side keep on

77
00:06:42,780 --> 00:06:44,690
updating their requirements, not be fight.

78
00:06:45,570 --> 00:06:50,250
Now, as of now for our application, we have just created this model what by.

79
00:06:50,640 --> 00:06:52,860
And with respect to this model, don't be late.

80
00:06:53,160 --> 00:06:59,400
If you see here in this file, we have used flasks equal alchemy, which by default does not come with

81
00:06:59,400 --> 00:06:59,880
Python.

82
00:07:00,300 --> 00:07:05,910
So before we can use it, we need to explicitly install this package in whatever system you would be

83
00:07:05,910 --> 00:07:07,110
running this application on.

84
00:07:07,890 --> 00:07:11,850
That is why we need to add this dependency in the requirements, not the ex to fight.

85
00:07:12,390 --> 00:07:19,140
So first of all, you created typically the requirement or the extra file is located in the rule directories

86
00:07:19,140 --> 00:07:19,830
of your project.

87
00:07:20,910 --> 00:07:22,050
So in the project.

88
00:07:30,060 --> 00:07:31,380
And mentioned the requirement.

89
00:07:35,390 --> 00:07:40,880
Now, while the Docker file starts executing to create an environment for our application, this file

90
00:07:40,880 --> 00:07:44,750
is automatically picked by it if placed in the root directory of project.

91
00:07:44,930 --> 00:07:47,140
So you need not to worry about the locations and not.

92
00:07:48,580 --> 00:07:50,260
OK, now enough of work is done.

93
00:07:50,710 --> 00:07:51,880
Time to commit the changes.

94
00:07:52,780 --> 00:07:53,710
Let me save this.

95
00:07:59,140 --> 00:08:00,130
Get status.

96
00:08:03,750 --> 00:08:09,090
Two untracked files do add them get add dot dot means everything.

97
00:08:14,910 --> 00:08:17,640
Yep, they are into staging now comment.

98
00:08:20,200 --> 00:08:21,370
Models and.

99
00:08:24,190 --> 00:08:28,300
And since we are in Beacon branch, we're just not known to a repository.

100
00:08:28,960 --> 00:08:31,750
So do Upstream get push?

101
00:08:42,030 --> 00:08:42,390
Great.

102
00:08:42,630 --> 00:08:43,140
It's done.

103
00:08:44,330 --> 00:08:45,290
Let's check in the U.S..

104
00:08:58,700 --> 00:08:59,300
Here we go.

105
00:08:59,930 --> 00:09:02,420
Models folder and requirements, not the extra fight.

106
00:09:03,390 --> 00:09:09,780
Next is what it's time to step back and branch to the main master branch so that when a new pull request

107
00:09:09,780 --> 00:09:13,350
is made by some other developer, he should be getting the latest quarter.

108
00:09:14,010 --> 00:09:15,270
So create a budget request.

109
00:09:21,760 --> 00:09:23,770
Source branch back models.

110
00:09:24,840 --> 00:09:25,710
Margaret is mean.

111
00:09:30,460 --> 00:09:34,420
Description, be models, clothes.

112
00:09:40,870 --> 00:09:45,820
This is if you would have a signed you can choose and select from other members as a signing or the

113
00:09:45,820 --> 00:09:46,080
order.

114
00:09:47,680 --> 00:09:48,940
I will uncheck this delete.

115
00:09:50,630 --> 00:09:50,920
Great.

116
00:09:54,860 --> 00:09:57,950
It says no pipeline, no problem, we will add it very soon.

117
00:09:59,190 --> 00:10:04,800
One important thing, guys, since I'm the maintainer and owner of this project, so I can approve and

118
00:10:04,800 --> 00:10:06,600
most this request by myself.

119
00:10:06,960 --> 00:10:11,160
But in real time projects, there will be a lot of people working on the same project.

120
00:10:11,670 --> 00:10:14,190
They all will never share the same role in a project.

121
00:10:14,730 --> 00:10:19,680
Admin will add the teams into Getler project, and they will be assigned different, different roles

122
00:10:19,800 --> 00:10:22,710
and permissions according to their work and designation.

123
00:10:23,250 --> 00:10:28,500
Roles vary from guest reporter, developer, maintainer and owner.

124
00:10:28,860 --> 00:10:32,340
Everyone will not have the power to approve or moister request.

125
00:10:32,880 --> 00:10:36,330
Their permissions will be defined according to the roles assigned to them.

126
00:10:36,870 --> 00:10:41,310
Hence, in large projects, when you create a budget request, it will go to a reviewer.

127
00:10:41,760 --> 00:10:48,000
He or she will review the changes made by you and then can approve or reject it only when the budget

128
00:10:48,000 --> 00:10:48,840
request is approved.

129
00:10:48,990 --> 00:10:51,540
You will be able to match your branch with a master branch.

130
00:10:52,600 --> 00:10:58,660
And the details of what our roles and permissions are there and GitLab how to define them is how over

131
00:10:58,660 --> 00:11:04,660
a later point of discussion for the time I will go ahead and approve this budget request by myself.

132
00:11:13,910 --> 00:11:14,570
It smudged.

133
00:11:15,290 --> 00:11:15,770
Let's see.

134
00:11:20,190 --> 00:11:20,460
Yeah.

135
00:11:21,000 --> 00:11:23,850
Main Branch is up to date with the back end database courts.

136
00:11:24,450 --> 00:11:27,510
Let's proceed with developing our application in the next lecture.
