1
00:00:00,580 --> 00:00:05,440
So, people, we have come a long journey in GitLab, we are now, well, familiar with the basics of

2
00:00:05,440 --> 00:00:06,460
GitLab Sicily.

3
00:00:06,760 --> 00:00:11,410
We have seen how a basic Gettler pipeline look like, what are its component and other things.

4
00:00:11,860 --> 00:00:14,950
So I believe this is the best point from where we can project.

5
00:00:14,950 --> 00:00:20,560
This goes by taking a good lepeska, learning into building some real time application.

6
00:00:21,130 --> 00:00:22,750
So welcome guys to this new section.

7
00:00:23,140 --> 00:00:28,930
Starting from this section, we will push to get lopsidedly learnings into some good use by taking the

8
00:00:29,020 --> 00:00:35,200
learned concepts all together into building a project that will mimic any real world DevOps project.

9
00:00:35,920 --> 00:00:40,600
OK, so first things first, let's get a picture of what other application requirements.

10
00:00:41,440 --> 00:00:48,250
So we got a client asking us to make an employer portal web application in Python that is capable of

11
00:00:48,250 --> 00:00:53,800
storing that employer data like name, salary department and all from that Python application.

12
00:00:54,100 --> 00:00:58,660
They should be able to view the employee data edited or can also delete the employee.

13
00:00:59,290 --> 00:01:01,510
Let me show you the glimpse of Python application.

14
00:01:02,470 --> 00:01:06,940
The Python part is out of the context, considering this is a pure GitLab lopsidedly course.

15
00:01:07,270 --> 00:01:10,900
But still, since it is a part of project, we cannot completely ignore it.

16
00:01:12,580 --> 00:01:15,130
So this is a Web application developed in Python.

17
00:01:15,430 --> 00:01:16,210
Very simple one.

18
00:01:17,230 --> 00:01:21,670
We have the app's main page showing the data of all employers who add a new one.

19
00:01:21,880 --> 00:01:24,460
Click on here and fill in the details.

20
00:01:42,080 --> 00:01:42,890
Click on Submit.

21
00:01:47,820 --> 00:01:49,170
And I imply will be added.

22
00:01:49,650 --> 00:01:51,810
And similarly, clicking on this edit button.

23
00:01:53,070 --> 00:01:53,670
You can edit.

24
00:01:56,920 --> 00:01:57,670
The details.

25
00:02:04,270 --> 00:02:10,270
See, the dangers are reflecting and last, clicking on this delete button will delete the corresponding

26
00:02:10,270 --> 00:02:11,290
employer from database.

27
00:02:15,310 --> 00:02:15,670
Yes.

28
00:02:16,180 --> 00:02:20,080
So that the employer portal application for you now along with the application.

29
00:02:21,700 --> 00:02:27,220
Our client also foresees that there will be frequent additions and modifications in application, so

30
00:02:27,220 --> 00:02:30,250
it expects that there can be multiple deployments within a month.

31
00:02:30,430 --> 00:02:32,770
You know, just like we have an agile methodology.

32
00:02:33,010 --> 00:02:37,510
So in short, Client wants us to adopt the security culture for this application development.

33
00:02:38,630 --> 00:02:43,330
OK, so if we segregate the client requirements, that requirements can be classified into two vertical

34
00:02:43,330 --> 00:02:43,870
categories.

35
00:02:44,170 --> 00:02:47,740
First, we need to develop the employer portal application in Python.

36
00:02:48,070 --> 00:02:53,470
You know, it can include the FDA, Miles East and central things for frontend and some database to

37
00:02:53,470 --> 00:02:54,490
store the employee data.

38
00:02:55,270 --> 00:03:01,150
And the second thing for deliveries and deployments, we need to take help from GitLab creating a CSC

39
00:03:01,270 --> 00:03:06,870
pipeline, and that pipeline will be created in a way that will help developers to deploy that change

40
00:03:06,880 --> 00:03:09,180
to life environment with just a single commit.

41
00:03:09,900 --> 00:03:10,400
Guys again.

42
00:03:10,600 --> 00:03:16,660
The magic is not in the app, and either the magic lies in the pipeline that we will design and develop

43
00:03:16,660 --> 00:03:17,740
as part of this project.

44
00:03:18,280 --> 00:03:23,830
Trust me, the final season, the pipeline would be so much capable that whenever there would be a code

45
00:03:23,830 --> 00:03:29,200
commit, that change can automatically go into production within minutes after going through a series

46
00:03:29,200 --> 00:03:31,390
of build testing and staging stages.

47
00:03:32,590 --> 00:03:34,960
So let's buckle up and start building this project.

48
00:03:35,290 --> 00:03:40,360
Believe me, going through the next few sections of this project, you will understand and get a feel

49
00:03:40,360 --> 00:03:46,690
of how real time lapse the project is carried out in companies while learning numerous GitLab S.H.I.E.L.D.

50
00:03:46,690 --> 00:03:48,160
features and its concepts.
