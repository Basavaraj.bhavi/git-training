1
00:00:01,110 --> 00:00:04,980
Next comes right down to right application logic for that I will create.

2
00:00:07,770 --> 00:00:08,670
I brought by.

3
00:00:11,230 --> 00:00:14,890
This is the file that will actually run when we deploy this application.

4
00:00:16,110 --> 00:00:17,580
Now again, create a new branch.

5
00:00:19,190 --> 00:00:21,820
Because this Python part will be done by some other developer.

6
00:00:26,510 --> 00:00:29,900
You don't really assume that all the codes are written in a sequential manner.

7
00:00:29,930 --> 00:00:35,540
One after the other, just like I'm doing it, you know, for the back and codes were greater than juggling

8
00:00:35,550 --> 00:00:39,320
to master than Apple was taken from master to write the frontend codes.

9
00:00:39,710 --> 00:00:43,970
Then they were merged and now taking a new code from Master Branch Rewrite Python codes.

10
00:00:44,420 --> 00:00:47,210
Things don't go in sequential manner in any of the whole projects.

11
00:00:47,660 --> 00:00:53,240
All the frontend backend or any other team members involved in the project would be working concurrently

12
00:00:53,240 --> 00:00:53,900
on their code.

13
00:00:54,290 --> 00:00:57,860
But yeah, with one condition that they have to frequently must do the master.

14
00:01:01,840 --> 00:01:04,000
Now, this Abbott is a standard name.

15
00:01:04,780 --> 00:01:09,820
This is where you can see all the logic for our application is written logics like how to fetch employer

16
00:01:09,820 --> 00:01:10,810
data from database.

17
00:01:11,020 --> 00:01:12,460
How to add them in the DB.

18
00:01:12,580 --> 00:01:15,730
And all of the logics that replace the whole code.

19
00:01:18,230 --> 00:01:20,270
Now, giving you a brief idea about this could.

20
00:01:22,340 --> 00:01:25,130
At first, models from models directly are important.

21
00:01:25,550 --> 00:01:31,760
This will basically give us the access to that VB object and employ class that we created earlier and

22
00:01:31,760 --> 00:01:34,640
to make this process of importing from other directory work.

23
00:01:35,000 --> 00:01:38,360
We need to create an empty IT file inside the model's electric.

24
00:01:39,640 --> 00:01:40,090
So.

25
00:01:50,430 --> 00:01:57,090
The next line of code contained few more imports from Flask Import Flask Flask is a lightweight web

26
00:01:57,090 --> 00:02:01,950
server gateway interface, which is a popular Python Web framework, developed web applications.

27
00:02:02,920 --> 00:02:10,030
Next to a different extradition request, request is also imported from Flask and again, since we are

28
00:02:10,030 --> 00:02:10,990
working with templating.

29
00:02:11,260 --> 00:02:14,590
So to render the template import render underscore template.

30
00:02:15,190 --> 00:02:21,480
This method will help us rendering the SDL template very easily lost from flask import.

31
00:02:21,490 --> 00:02:27,160
The Redirect Function Redirect as an introduced will help us redirecting the user to another target

32
00:02:27,160 --> 00:02:29,080
location with any custom response.

33
00:02:30,330 --> 00:02:36,030
Then in this block of code, I'm basically creating the instance of application and setting that securely

34
00:02:36,030 --> 00:02:41,700
database and underscore DB, not DB for this application going in the block.

35
00:02:43,100 --> 00:02:49,100
At first, the database file name and bundles called Debbie Dot Debbie is defined as a variable database.

36
00:02:49,820 --> 00:02:51,650
This database file we will create shortly.

37
00:02:52,910 --> 00:02:55,400
Next, the flask instance app is created.

38
00:02:56,450 --> 00:03:03,680
Then in this line, I'm telling Flask, where the database is located as psychologist, getting used

39
00:03:03,680 --> 00:03:11,360
for database so circulate and these three forward slashes means the relative, but not absolute, plus

40
00:03:11,630 --> 00:03:14,960
the database name coming from the database variable created above.

41
00:03:16,630 --> 00:03:21,280
In the next lane, we had updating the application configuration values from the given object.

42
00:03:21,850 --> 00:03:27,090
And finally, the application is initialized while setting the configuration for database.

43
00:03:28,580 --> 00:03:32,960
All right, so database and application being set next in the code.

44
00:03:35,020 --> 00:03:41,230
We have few methods, each defining a specific page in the application, starting with the first matter.

45
00:03:42,120 --> 00:03:50,490
This index method is the entry point of the application, which is bound to slash all this root function

46
00:03:50,490 --> 00:03:56,970
of the flask, Glass is a decorator and it tells application which you are should trigger the associated

47
00:03:56,970 --> 00:03:57,360
function.

48
00:03:57,990 --> 00:04:01,110
So basically a slash will trigger this index function.

49
00:04:02,430 --> 00:04:03,870
Coming inside the index method.

50
00:04:05,610 --> 00:04:12,210
First employees artifacts from database and then using an older template, we render the template indexed

51
00:04:12,210 --> 00:04:15,600
or decimal while passing the fetched employed in it.

52
00:04:16,470 --> 00:04:21,750
So you can see with these two lines of code, when we will open the index page of the application,

53
00:04:22,110 --> 00:04:27,180
it will first fetch the employer list from database and then display it individually formatted page.

54
00:04:28,990 --> 00:04:29,680
Coming next.

55
00:04:31,180 --> 00:04:33,340
There is a method defined for ad page.

56
00:04:34,430 --> 00:04:44,540
This Adewale is limited to accept two methods post and get, so if the request type is off, post all

57
00:04:44,540 --> 00:04:51,470
the employed data like name, gender address and other details, they are added in the database.

58
00:04:52,190 --> 00:04:56,270
But in case, if it is not post, then in health condition.

59
00:04:57,910 --> 00:05:01,450
It will simply display the ad, not HD M. Page, where design previously.

60
00:05:02,470 --> 00:05:02,830
Got it.

61
00:05:04,140 --> 00:05:04,920
Now scrolling down.

62
00:05:07,510 --> 00:05:11,680
There is a method to delete an employee when control comes to this method.

63
00:05:12,250 --> 00:05:14,950
It will simply delete the employee data based on the employee.

64
00:05:16,460 --> 00:05:17,570
The logic to the leaders.

65
00:05:18,710 --> 00:05:26,210
If the request method is of both type and based on the implied past, first look out for the employees

66
00:05:26,210 --> 00:05:28,640
with that ID and database and.

67
00:05:29,540 --> 00:05:31,790
And if the employee exists in the database.

68
00:05:33,180 --> 00:05:34,710
Delete that record from database.

69
00:05:35,920 --> 00:05:36,550
Otherwise.

70
00:05:37,990 --> 00:05:43,240
If they employ with that bastard idea is not there, then simply pass and display the error message

71
00:05:43,570 --> 00:05:45,640
saying, sorry, the employer does not exist.

72
00:05:47,890 --> 00:05:49,510
Similarly, at last.

73
00:05:51,170 --> 00:05:57,350
We have an ID method to edit the details of employment, the working here is also the same here.

74
00:05:57,350 --> 00:06:02,690
Also, the editing will be based on employed and getting passed along with the URL as get request.

75
00:06:03,410 --> 00:06:09,230
If they employ it with the pass, data is present facts that imply and fill the form with all details

76
00:06:09,590 --> 00:06:13,340
while allowing the user to edit otherwise told the same edit.

77
00:06:14,840 --> 00:06:16,250
And at the end of the cold.

78
00:06:18,020 --> 00:06:19,430
But this doesn't matter, flask.

79
00:06:19,730 --> 00:06:26,540
We are simply running this application on host address 0.0.0.0 and by default, the port it will run

80
00:06:26,540 --> 00:06:27,920
on his boat five times in.

81
00:06:29,720 --> 00:06:32,330
Well, so with this, the Abbot Point coal is ready.

82
00:06:33,050 --> 00:06:38,210
One last thing pending the application is to create the initial database file amp underscore debate

83
00:06:38,450 --> 00:06:41,780
debate for that in terminal.

84
00:06:42,830 --> 00:06:44,570
I'm already an employer portal directory.

85
00:06:46,780 --> 00:06:51,460
Abroad is here now to start an interactive bite entry, shall.

86
00:06:56,170 --> 00:06:57,290
I mean, Python, Clay.

87
00:06:58,260 --> 00:07:02,930
Right from app import Debbie.

88
00:07:04,130 --> 00:07:07,070
This will import the DBI object from abroad, be verify.

89
00:07:08,510 --> 00:07:09,230
And then right.

90
00:07:13,660 --> 00:07:18,790
This command will create the employed level, and yeah, before you can run these commands in a local

91
00:07:18,790 --> 00:07:23,710
system, make sure you have Python three and flasks equal alchemy installed on your system.

92
00:07:24,130 --> 00:07:27,750
If you don't have, then you can install them by running these commands in terminal.

93
00:07:29,880 --> 00:07:30,260
On this.

94
00:07:36,060 --> 00:07:39,270
Yep, we got an initial empty employment database fight.

95
00:07:40,610 --> 00:07:45,290
All right, so at this stage, I believe our application is ready and we can have its first look in

96
00:07:45,290 --> 00:07:50,690
a local system, even though it is not required, and we usually don't run on local systems in rail

97
00:07:50,690 --> 00:07:51,170
projects.

98
00:07:51,650 --> 00:07:56,870
But since I already have the dependency required for this application installed on my system, so it

99
00:07:56,870 --> 00:07:57,530
will be a quick one.

100
00:08:09,430 --> 00:08:11,410
Burrup is running up to the browser.

101
00:08:19,530 --> 00:08:20,370
Congratulations.

102
00:08:20,760 --> 00:08:21,840
Application is hit.

103
00:08:22,530 --> 00:08:27,220
This is the index page we had, all the employees will be listed since there is no employer as of now.

104
00:08:27,240 --> 00:08:27,840
So explain.

105
00:08:28,530 --> 00:08:29,760
Let me quickly add a new employee.

106
00:08:51,430 --> 00:08:53,740
Employers added, Now try editing it.

107
00:09:05,960 --> 00:09:09,650
Yep, everything is also working now to check the lead operation.

108
00:09:09,680 --> 00:09:11,300
Let me first add a dummy play.

109
00:09:23,530 --> 00:09:24,520
Not right, leading it.

110
00:09:27,530 --> 00:09:28,670
Boom, it's gone.

111
00:09:29,480 --> 00:09:31,490
Excellent ah, application is looking fine.

112
00:09:32,000 --> 00:09:34,040
Now let's commit the changes to a remote repository.

113
00:09:49,210 --> 00:09:52,300
I will also create a modest request for this discord out of this video.

114
00:09:52,630 --> 00:09:53,110
Thank you.
