1
00:00:00,980 --> 00:00:05,960
OK, so after the changes being reviewed in future branch deployment, it's time to merge the feature

2
00:00:05,960 --> 00:00:07,260
test branch into main.

3
00:00:25,860 --> 00:00:28,710
It says by plane past for future test branch.

4
00:00:28,770 --> 00:00:29,490
Three minutes ago.

5
00:00:30,420 --> 00:00:36,360
Not before approving or merging the branch into mean the maintainer slash approval can review the future

6
00:00:36,360 --> 00:00:38,310
branch deployment from this link.

7
00:00:42,010 --> 00:00:43,150
This is the same application.

8
00:00:45,150 --> 00:00:49,020
Then the last commit to that future branch can also be seen from here.

9
00:00:49,990 --> 00:00:53,650
Every pipeline run status for that branch is also available.

10
00:00:54,700 --> 00:00:56,830
Any code changes can be seen from here.

11
00:00:57,580 --> 00:01:01,480
If everything looks fine, then the maintainer will go ahead and moist branch.

12
00:01:04,660 --> 00:01:09,040
As soon as the marriage happens, a new pipeline for Main Branch has started running.

13
00:01:10,680 --> 00:01:11,730
Open up the pipeline.

14
00:01:15,280 --> 00:01:22,600
OK, I see as called the deploy feature and automated test, which stage is not present here because

15
00:01:22,600 --> 00:01:23,350
it is a main branch.

16
00:01:25,100 --> 00:01:26,210
And it's a success.

17
00:01:26,390 --> 00:01:30,450
Her application would have gone live in production environment, guys.

18
00:01:30,470 --> 00:01:36,410
The current model of our pipeline is fully automated, as we just saw once dust test stage of his bust

19
00:01:36,800 --> 00:01:42,020
deployed to production, job is automatically done and GitLab automatically deploys the new application

20
00:01:42,110 --> 00:01:43,190
agent who lives of us.

21
00:01:43,820 --> 00:01:47,540
Well, this was just a demo app and we can let it go live by itself.

22
00:01:47,990 --> 00:01:53,210
However, for the real world applications for automated acceptance to production, the company needs

23
00:01:53,210 --> 00:01:57,800
to reach at a certain maturity level with sophisticated automated acceptance testing.

24
00:01:58,310 --> 00:02:03,530
Only then, you can allow the scripts to test and leave them to decide whether to go live with the new

25
00:02:03,530 --> 00:02:04,520
app version or not.

26
00:02:05,210 --> 00:02:10,370
But Adobe as a safe site, what you can do is we can leave this decision to humans.

27
00:02:10,910 --> 00:02:16,460
We can ride the pipeline in a way that when, for example, in this case after the test, Steve Jobs

28
00:02:16,460 --> 00:02:16,820
paused.

29
00:02:17,090 --> 00:02:19,610
It won't automatically render the production job.

30
00:02:19,880 --> 00:02:26,570
Rather, after the successful execution of this stage, the Deploy two production task will be skipped

31
00:02:26,570 --> 00:02:32,060
for manual trigger, and it will be a human being who, after checking the new was an application from

32
00:02:32,060 --> 00:02:37,160
all logic or business or whatever aspects will decide to run that deploy production job.

33
00:02:37,610 --> 00:02:42,350
And this thing is done with adding a keyword when manual in Python configuration.

34
00:02:42,620 --> 00:02:43,280
So let's add it.

35
00:02:44,550 --> 00:02:47,610
I'm directly doing it in main branch to save us some time.

36
00:02:50,860 --> 00:02:52,810
What do you don't do that ever in your project?

37
00:02:56,790 --> 00:02:58,740
So inside that deployed production job.

38
00:03:06,510 --> 00:03:10,590
After only mean had the one condition like this.

39
00:03:17,020 --> 00:03:23,050
This way, when the pipeline will run, the deployed production job will be shipped for automatic execution

40
00:03:23,290 --> 00:03:26,080
and will only run after a manual, click on it from you.

41
00:03:27,040 --> 00:03:27,850
Push this change.

42
00:03:39,090 --> 00:03:39,840
Despite being on.

43
00:03:44,670 --> 00:03:51,420
Great, as you can see this time after the test stages past the control was not automatically passed

44
00:03:51,420 --> 00:03:57,480
to deploy production stage, rather, pipeline is paused here and required a manual action to run the

45
00:03:57,480 --> 00:03:58,500
play production stage.

46
00:03:59,220 --> 00:04:00,720
Now to run, we have two options.

47
00:04:01,230 --> 00:04:05,760
First is at the state level, which basically will run all the jobs in this stage.

48
00:04:06,150 --> 00:04:12,660
The second option at job level, which will run this particular job on any since there is one job only

49
00:04:12,660 --> 00:04:14,460
in this stage, so both will be same.

50
00:04:17,750 --> 00:04:23,280
Clicking on the play button has run the job normally, so that's all you can limit some jobs put on

51
00:04:23,300 --> 00:04:26,750
automatically and make the manual human intervention compulsory.
