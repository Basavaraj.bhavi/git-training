1
00:00:02,160 --> 00:00:04,190
Let me check in which branch we are in.

2
00:00:08,710 --> 00:00:10,990
It's just good.

3
00:00:11,890 --> 00:00:16,120
Now, before I commit the changes, first see the current state of GitLab and Hiroko.

4
00:00:17,760 --> 00:00:18,960
Go to environments.

5
00:00:25,750 --> 00:00:32,080
Environments, there are only two environments staging and production hop up to Iroko dashboard here

6
00:00:32,080 --> 00:00:32,420
as well.

7
00:00:32,440 --> 00:00:34,120
We have only two running applications.

8
00:00:34,150 --> 00:00:38,070
The staging and production now commit and push the changes to rappel.

9
00:01:01,190 --> 00:01:02,090
Pipeline is running.

10
00:01:05,320 --> 00:01:07,900
So for the future test branch, we have these many jobs.

11
00:01:08,350 --> 00:01:10,780
The first two are the testing, then build.

12
00:01:11,290 --> 00:01:16,660
Then since we use the regular expression that children that apply feature job with any branch whose

13
00:01:16,660 --> 00:01:22,930
name starts with Vejjajiva, that condition holds true for this branch and therefore job deploy underscore

14
00:01:22,930 --> 00:01:23,650
feature is here.

15
00:01:23,890 --> 00:01:29,170
And the same condition is true for best feature as well, and rest staging and production jobs will

16
00:01:29,170 --> 00:01:30,310
be listed for Main Branch.

17
00:01:31,300 --> 00:01:34,270
I now pause this video here and will meet you after the completion.

18
00:01:38,070 --> 00:01:38,390
Great.

19
00:01:38,460 --> 00:01:39,600
The pipeline was a success.

20
00:01:40,050 --> 00:01:42,210
I'm going through the logs of deploy for each job.

21
00:01:47,490 --> 00:01:53,250
First, from the before script tag, the variables were exported, then the logging was done.

22
00:01:55,200 --> 00:02:01,740
Then the control was given to script tag, where first the artifact file was created, then Docker image

23
00:02:01,740 --> 00:02:02,190
command.

24
00:02:04,580 --> 00:02:06,920
Docker tag, Docker run.

25
00:02:10,150 --> 00:02:16,510
Then log in to Heroku, post to Heroku and finally hit Oakwood and command.

26
00:02:17,520 --> 00:02:21,060
Which generates as that Hiroko application at this point, it.

27
00:02:25,260 --> 00:02:29,580
Here we go after application is available at this dynamic address.

28
00:02:31,560 --> 00:02:33,690
Now if you move on to the environmental section.

29
00:02:41,230 --> 00:02:41,560
Great.

30
00:02:42,040 --> 00:02:49,030
We have an environment with the name review, slash branch name, feature list, the job that made this

31
00:02:49,030 --> 00:02:50,410
environment is deploy feature.

32
00:02:51,050 --> 00:02:52,540
Let's open the live environment.

33
00:02:55,480 --> 00:02:56,860
At the same as our application.

34
00:02:57,670 --> 00:02:59,560
And now if we go to the Heroku dashboard.

35
00:03:03,890 --> 00:03:04,380
Here we go.

36
00:03:05,060 --> 00:03:12,050
A new dynamic application was created, but this dynamic name automatically from a code slice that was

37
00:03:12,050 --> 00:03:17,810
how you can create and deploy the review apps to dynamic environments automatically and GitLab by playing

38
00:03:17,810 --> 00:03:19,160
with its predefined variables.

39
00:03:19,640 --> 00:03:24,860
This way, from now on, when a developer creates a new feature branch to add new feature into the current

40
00:03:24,860 --> 00:03:29,810
application, a new dynamic environment will be created for that particular deployment.

41
00:03:30,170 --> 00:03:35,450
And because of dynamic environments from now on, there will be no race between developers who get hold

42
00:03:35,450 --> 00:03:39,710
of staging environment, and every feature is reviewed and tested isolated.
