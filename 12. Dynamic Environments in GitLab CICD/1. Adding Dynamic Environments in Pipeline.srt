1
00:00:00,300 --> 00:00:05,910
Static environment being created in this lecture, we will get back on track who aligned the project

2
00:00:06,000 --> 00:00:10,680
according to latest workflow, and create dynamic environments for each of the featured branch.

3
00:00:11,340 --> 00:00:17,250
As of now, every developer reviews his bizarre feature directly onto a single static and pre-defined

4
00:00:17,250 --> 00:00:18,090
staging environment.

5
00:00:18,600 --> 00:00:24,000
But since this approach is ineffective for already discussed reasons, we need to add a few modifications

6
00:00:24,000 --> 00:00:30,180
in the current pipeline flow modifications that each developer will deploy its feature to a separate

7
00:00:30,180 --> 00:00:33,270
and dynamic environment, which is created automatically.

8
00:00:33,660 --> 00:00:39,500
I repeat that environment would be a dynamic and will be created automatically automatically, as in

9
00:00:39,940 --> 00:00:45,660
unlike deploy stage or deploy production jobs before we could deploy to hit all servers.

10
00:00:45,990 --> 00:00:50,580
First, we created Oracle Projects, one for staging and one for production manually.

11
00:00:51,300 --> 00:00:56,340
But as part of new workflow, since there can be many feature branches getting created in a project

12
00:00:56,700 --> 00:00:59,610
with each branch deployed onto a separate environment.

13
00:00:59,850 --> 00:01:03,180
So obviously we can't handle the project creation, but manually.

14
00:01:03,660 --> 00:01:09,900
That is why from now on the environment for review apps, that is the Heroku project and your creation

15
00:01:10,140 --> 00:01:12,660
will be automatically created by our pipeline.

16
00:01:13,230 --> 00:01:18,930
And then the same repeater stuff like uploading Docker image to Heroku, releasing it and so on will

17
00:01:18,930 --> 00:01:19,260
happen.

18
00:01:19,770 --> 00:01:23,880
And once the change is approved, that code will be pushed to master branch from there.

19
00:01:24,090 --> 00:01:28,830
First deployment two staging will happen and again after the staging gets the green flag.

20
00:01:29,010 --> 00:01:30,900
The same is deployed to production environment.

21
00:01:31,870 --> 00:01:32,260
All right.

22
00:01:32,470 --> 00:01:37,330
So this being said in this lecture or maybe in the next few as well, we will be changing the current

23
00:01:37,330 --> 00:01:37,840
workflow.

24
00:01:37,990 --> 00:01:40,390
According to the one we discussed, let's get started.

25
00:01:42,430 --> 00:01:46,780
Say that we have to add a new feature in application, so we'll create a new branch.

26
00:01:51,180 --> 00:01:55,410
Whose name would start from feature given feature Dash.

27
00:01:56,390 --> 00:02:01,670
Actually, this is a naming convention which GitLab users use for every development when you check out

28
00:02:01,670 --> 00:02:07,610
a branch or to add a new feature or anything, the name of that branch should start with any feature,

29
00:02:07,610 --> 00:02:13,070
keyword review, keyword you know, a standard one keyword, though this naming convention is not mandatory,

30
00:02:13,310 --> 00:02:18,530
but it is recommended and standard approach that people go with and is very helpful at times.

31
00:02:18,830 --> 00:02:23,750
You know, like if we follow this naming convention, then we can protect all the development branches

32
00:02:23,750 --> 00:02:26,900
in one shot using feature in regular expression here.

33
00:02:28,430 --> 00:02:30,960
So it's good to name the branch with some agreed suffix.

34
00:02:30,980 --> 00:02:32,900
It can be that feature or review.

35
00:02:33,290 --> 00:02:34,670
I'm keeping it as a feature.

36
00:02:35,570 --> 00:02:38,990
So in Terminal Jakarta, a new branch say feature test.

37
00:02:49,050 --> 00:02:56,220
So after a new Docker image, images built in this job, or you can say after a review app is build,

38
00:02:56,730 --> 00:02:59,850
the next task would be to deploy it on a dynamic environment.

39
00:03:00,270 --> 00:03:01,350
So after the job?

40
00:03:06,030 --> 00:03:09,960
Create a new job deploy feature.

41
00:03:13,490 --> 00:03:15,800
Or you can name it as deploy it, if you as well.

42
00:03:16,940 --> 00:03:22,680
To save some time since the next few lines of code like image and all would be same as off deployed

43
00:03:22,700 --> 00:03:24,770
staging job, so I will copy it from there.

44
00:03:33,970 --> 00:03:35,710
Now, I'll change the values accordingly.

45
00:03:36,520 --> 00:03:38,080
First is the stage name.

46
00:03:39,790 --> 00:03:41,320
It would be deploy feature.

47
00:03:44,680 --> 00:03:46,240
Now, this is same.

48
00:03:47,050 --> 00:03:49,010
This environmental thing, we will change later.

49
00:03:49,810 --> 00:03:56,050
Now coming to the script, but the first step would be to pull the new Docker image from GitHub repository.

50
00:03:56,740 --> 00:03:57,100
Then.

51
00:03:58,160 --> 00:04:04,400
Back that image, according to Hiroko, naming conventions, the new image name create a new variable

52
00:04:04,940 --> 00:04:07,130
Heroku feature.

53
00:04:09,140 --> 00:04:10,670
And define this variable about.

54
00:04:24,050 --> 00:04:25,180
Change this to feature.

55
00:04:29,510 --> 00:04:31,670
This feature will be again defined here.

56
00:04:34,550 --> 00:04:40,390
Nice, unlike the staging up production app names, which were static ones for future applications.

57
00:04:40,700 --> 00:04:46,220
We need to define a dynamic name that will be automatically assigned to it to assign the app name.

58
00:04:46,250 --> 00:04:52,160
We will leverage from GitLab Predefined Environment Variables, CIA Environment Select, which outputs

59
00:04:52,160 --> 00:04:57,410
the simplified version of the environment team and best suits for inclusion in DNS and URLs.

60
00:05:04,380 --> 00:05:07,230
Variables got defined, but there is a problem.

61
00:05:07,650 --> 00:05:09,000
Actually not from our site.

62
00:05:09,300 --> 00:05:11,130
It's an ongoing issue with jet lag only.

63
00:05:12,390 --> 00:05:17,640
The thing is, if you keep this variable configuration as is and run the pipeline, it won't succeed.

64
00:05:18,240 --> 00:05:24,360
The issue is that a Gettler variable is not resolved properly when you use and variable inside another

65
00:05:24,360 --> 00:05:26,520
variable containing an environment variable.

66
00:05:27,560 --> 00:05:28,070
Confused.

67
00:05:28,190 --> 00:05:29,540
Let me explain it with an example.

68
00:05:30,260 --> 00:05:36,830
So here we have two variables Varun and VAT, when everyone is a variable pointing to a predefined environment,

69
00:05:36,830 --> 00:05:40,130
variable CAA job name that basically outputs the job name.

70
00:05:40,640 --> 00:05:43,430
And the second variable value outputs a string.

71
00:05:43,610 --> 00:05:46,360
The job name is and then the value from that one.

72
00:05:46,910 --> 00:05:50,210
Now, if you're on this call, ideally it should output the result like this.

73
00:05:50,960 --> 00:05:56,870
The job name is an actual job name, but guys, due to this open issue, GitLab is unable to resolve

74
00:05:56,870 --> 00:05:57,980
variable value.

75
00:05:58,490 --> 00:06:00,950
And if you run it, it will output something like this.

76
00:06:01,670 --> 00:06:07,970
As you can see, that one is output correctly with the job name new job, but variable value has some

77
00:06:07,970 --> 00:06:08,570
weird output.

78
00:06:09,020 --> 00:06:11,720
The job name is variable name the age of name.

79
00:06:12,080 --> 00:06:17,480
Ideally, it should have returned as the job name, new job and the same thing is happening in our pipeline

80
00:06:17,480 --> 00:06:19,430
as well in our configuration.

81
00:06:19,880 --> 00:06:27,650
Since this Heroku feature depends on a variable feature app which again contains environment variable

82
00:06:27,800 --> 00:06:34,970
K environments like so on running, this variable Heroku feature will resolve all to this, which is

83
00:06:34,970 --> 00:06:37,010
not a valid URL, and our pipeline will fail.

84
00:06:38,480 --> 00:06:44,030
This usage of variable inside another variable is an open issue from quite a long time and an official

85
00:06:44,030 --> 00:06:47,390
link to the same thread is attached in the Resources tab of this lecture.

86
00:06:47,720 --> 00:06:49,550
You can visit and go through the discussion.

87
00:06:50,990 --> 00:06:55,580
OK, so the ongoing issue is there, but anyhow, we need to run the pipeline with proper you at it.

88
00:06:56,090 --> 00:06:59,630
So from that discussion itself, there is a workaround that we can do.

89
00:07:00,080 --> 00:07:01,040
What's that workaround?

90
00:07:01,310 --> 00:07:02,990
Let's discuss that in next lecture.
