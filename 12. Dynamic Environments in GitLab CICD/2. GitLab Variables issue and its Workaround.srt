1
00:00:00,830 --> 00:00:06,530
The workaround is that rather than defining the variable in variable section, we have to export these

2
00:00:06,530 --> 00:00:12,980
variables in the before script that we first the variables will be resolved and then will be made available

3
00:00:12,980 --> 00:00:13,940
to the following script.

4
00:00:14,780 --> 00:00:16,820
So cut these two variables from here.

5
00:00:18,010 --> 00:00:18,700
Feature up.

6
00:00:23,300 --> 00:00:24,800
In that deploy feature job.

7
00:00:26,150 --> 00:00:26,960
Before script.

8
00:00:30,210 --> 00:00:32,370
Export based it.

9
00:00:34,290 --> 00:00:36,680
A little change here in courts.

10
00:00:38,460 --> 00:00:39,750
Similarly, the second variable.

11
00:00:59,720 --> 00:01:03,590
With this set of variable definition, there would be no variable resolution problem.

12
00:01:04,460 --> 00:01:12,020
OK, so tagging is done moving next night before we can directly upload this image to Heroku and launch

13
00:01:12,020 --> 00:01:12,230
it.

14
00:01:12,560 --> 00:01:17,990
We have to do one more step, which is to create a Heroku application just like we did for the static

15
00:01:17,990 --> 00:01:18,590
environment.

16
00:01:18,920 --> 00:01:22,340
But at that time, it was manually done in Heroku dashboard from you.

17
00:01:22,820 --> 00:01:24,410
But this time it has to be done.

18
00:01:24,410 --> 00:01:32,420
From this time again, we have to leverage the Alpine Heroku Clia image, so I'll copy this command.

19
00:01:36,390 --> 00:01:42,660
I'm using the same image, this doctored image comes very handy when you have to write Hiroko UCLA commands

20
00:01:42,660 --> 00:01:43,560
from Docker itself.

21
00:01:44,460 --> 00:01:47,970
So the same command Docker ran it okay back.

22
00:01:49,260 --> 00:01:56,820
Just the command will be changed to create a new app command is very simple, create and then specify

23
00:01:56,820 --> 00:01:57,660
the application name.

24
00:02:04,210 --> 00:02:08,710
So when the control will come to this command using this Docker image.

25
00:02:10,260 --> 00:02:16,050
It will first authenticate us and after authentication will render Heroku create command, which creates

26
00:02:16,050 --> 00:02:21,450
a new Heroku application with the name as that of branch name from which this pipeline would be running.

27
00:02:22,140 --> 00:02:27,870
And yeah, do note that after the successful application creation from this pipeline, you will also

28
00:02:27,870 --> 00:02:32,850
be able to access that app from Heroku dashboard and start moving next.

29
00:02:33,450 --> 00:02:38,160
Once the app is created in Hirokazu, other things will be similar to the process we followed for the

30
00:02:38,160 --> 00:02:38,820
static apps.

31
00:02:39,660 --> 00:02:43,680
That is, we can push the app Docker image to Heroku and release it.

32
00:02:44,700 --> 00:02:50,490
I'll change this to feature up, and here also I will change.

33
00:03:00,550 --> 00:03:01,090
Excellent.

34
00:03:01,600 --> 00:03:05,810
So with this, the script, but to create an deployed dynamic review up is done.

35
00:03:06,340 --> 00:03:12,430
We are just left with one thing that is to specify the condition for which branch this job should run.

36
00:03:12,850 --> 00:03:14,170
I'm giving you a minute to do it.

37
00:03:14,380 --> 00:03:15,940
Watch this video and think about it.

38
00:03:21,630 --> 00:03:27,510
OK, so guys, since this job is just dealing with the review apps, so this job should only run for

39
00:03:27,510 --> 00:03:33,660
the branches whose names start from the keyboard feature because as we've discussed from now on, developers

40
00:03:33,660 --> 00:03:38,040
working on their different features would create a branch name, starting with keyboard feature.

41
00:03:38,370 --> 00:03:41,160
Has this job would also run for those branches only?

42
00:03:41,550 --> 00:03:42,750
Let's add that condition.

43
00:03:45,330 --> 00:03:50,910
You already know it only given in so that we will use a regular expression.

44
00:03:56,830 --> 00:04:01,540
This regular expression will match any branch name, starting with the keyword feature hyphen.

45
00:04:01,960 --> 00:04:04,090
And after that dash, it could be anything.

46
00:04:05,020 --> 00:04:05,560
Save it.

47
00:04:07,520 --> 00:04:13,160
Good so far that they apply for a job is almost complete, but the last thing we are left with is the

48
00:04:13,160 --> 00:04:18,720
mapping of a branch with environment that is the creation of dynamic environment and GitLab.

49
00:04:19,550 --> 00:04:21,110
Let's gagliardini make environments.

50
00:04:21,590 --> 00:04:25,730
So after that stage, tag environment name is.

51
00:04:29,600 --> 00:04:35,420
Since this would be a dynamic environment, so unlike giving it a static name and your role, we will

52
00:04:35,420 --> 00:04:41,870
use predefined GitLab basically variables, so the environment name would be a review slash, then use

53
00:04:41,870 --> 00:04:45,080
a variable CAA commit ref name.

54
00:04:47,860 --> 00:04:49,870
This variable is also the branch name.

55
00:04:50,770 --> 00:04:56,290
The environment name is prevented with given a review so that from the environment's list in way, you

56
00:04:56,290 --> 00:04:57,430
can easily distinguish it.

57
00:04:58,210 --> 00:04:59,190
Next is your.

58
00:05:01,020 --> 00:05:05,640
For the dynamic you are generally you can use the same security definition variable.

59
00:05:06,060 --> 00:05:11,460
But because this value may contain slashes or any other characters that would not be well in a domain

60
00:05:11,460 --> 00:05:12,360
name or a hotel.

61
00:05:13,050 --> 00:05:17,190
So it's recommended to use the variable CIA environment slug instead.

62
00:05:24,490 --> 00:05:27,400
That's the environment select variable is guaranteed to be unique.

63
00:05:28,530 --> 00:05:34,290
So, yes, with this dynamic environment they successfully created each time a new feature branch gets

64
00:05:34,290 --> 00:05:40,440
created, this deploy feature job will deploy the application to a dynamic, isolated environment which

65
00:05:40,440 --> 00:05:41,160
is created on.

66
00:05:43,610 --> 00:05:47,870
Deployed with the job is completed here, let's add the best feature job to.

67
00:05:49,660 --> 00:05:54,850
Well, I recommend you to do it yourself or otherwise, I will simply copy and make some changes.

68
00:06:25,090 --> 00:06:25,810
This is OK.

69
00:06:26,720 --> 00:06:34,670
But if you explored the feature, a variable with the environment slug in this best feature job, unfortunately

70
00:06:34,670 --> 00:06:38,690
it won't work as this best feature job has no environment defined.

71
00:06:39,020 --> 00:06:45,170
So the question is how do we pass this feature up variable containing the CIA environment select value?

72
00:06:45,620 --> 00:06:51,590
The answer is using artifacts, we can pass environment variables from one job to another, job with

73
00:06:51,590 --> 00:06:55,850
artifacts and not to pass an environment variable from one job to another.

74
00:06:56,150 --> 00:07:01,640
The general idea is first, we saved a variable to be passed into a special dot in Wi-Fi.

75
00:07:02,090 --> 00:07:03,170
Then we saved that.

76
00:07:03,170 --> 00:07:09,200
Not even we file as an artifact, and in the next job where that variable is required, we set the job

77
00:07:09,200 --> 00:07:12,740
dependencies on that stored artifact and then later use it.

78
00:07:13,070 --> 00:07:13,580
That's right.

79
00:07:15,080 --> 00:07:20,090
First things first seemed a variable into a dot in Wi-Fi.

80
00:07:20,420 --> 00:07:24,560
So under the script tag as the first dusk equals the variable.

81
00:07:37,240 --> 00:07:39,940
And stored it and do got in a fight like this.

82
00:07:45,650 --> 00:07:54,770
Not in this way, we are storing variable feature up whose value would be this into a file named deploy

83
00:07:54,770 --> 00:07:55,870
underscore feature not in.

84
00:07:56,720 --> 00:08:02,740
Let's keep in mind the maximum size of this data and Wi-Fi is five GB file being created generate the

85
00:08:02,750 --> 00:08:03,770
artifacts out of it.

86
00:08:06,680 --> 00:08:08,480
You already know how to create artifacts.

87
00:08:23,510 --> 00:08:29,570
And GitLab will use the reports given to collect the test reports, code quality reports and security

88
00:08:29,570 --> 00:08:34,430
reports from jobs and expose them in GitLab Way using our direct reports.

89
00:08:34,670 --> 00:08:38,600
Test reports are collected regardless of the job results, success or failure.

90
00:08:39,140 --> 00:08:44,510
Basically, with this, the artifacts will be generated within this job and would be available to use

91
00:08:44,510 --> 00:08:45,440
for subsequent jobs.

92
00:08:46,400 --> 00:08:51,350
Now, guys, do we learn from the artifacts lecture that all the artifacts from previous stages are

93
00:08:51,350 --> 00:08:52,460
passed to each job.

94
00:08:53,030 --> 00:08:58,700
However, we can use dependencies given to define a limited list of jobs to fetch artifacts from.

95
00:08:59,390 --> 00:09:03,830
And in that best feature job, we will do the same after script.

96
00:09:14,700 --> 00:09:20,070
Basically, by setting dependencies, we pass a list of all the previous jobs the artifacts should be

97
00:09:20,070 --> 00:09:22,660
downloaded from, you know, to avoid any confusion.

98
00:09:23,220 --> 00:09:25,650
But yeah, while defining dependencies for any job.

99
00:09:25,980 --> 00:09:31,380
Keep in mind under the dependencies, you're going to find jobs from stages that were executed before

100
00:09:31,380 --> 00:09:31,950
the current one.

101
00:09:32,300 --> 00:09:36,900
Advice and recourse if you do find jobs from the current or an upcoming stage.

102
00:09:37,830 --> 00:09:43,230
Also, if the artifacts of the job that is set as dependencies unexpired or deleted, then that dependent

103
00:09:43,230 --> 00:09:43,800
job fails.

104
00:09:44,210 --> 00:09:51,000
And anyways, in our code, I'm creating the test, which your job dependency on deploy feature job.

105
00:09:51,810 --> 00:09:56,950
So with this piece of code, the environment variable from Deploy feature would be available in that

106
00:09:56,950 --> 00:09:57,780
test with job.

107
00:09:59,490 --> 00:10:00,600
Accord is almost done.

108
00:10:03,860 --> 00:10:07,370
Just run these stages for only main branch.

109
00:10:14,670 --> 00:10:15,450
This one as well.

110
00:10:21,100 --> 00:10:21,910
And communities.

111
00:10:26,920 --> 00:10:28,330
Also add hostages.

112
00:10:46,320 --> 00:10:47,040
God looks good.

113
00:10:48,300 --> 00:10:53,190
One last thing I need to do is make the feature branch protected so that the secret variables can be

114
00:10:53,190 --> 00:10:53,490
used.

115
00:10:54,060 --> 00:10:55,170
Let me save this one.

116
00:10:56,510 --> 00:10:57,470
And the branch.

117
00:11:08,570 --> 00:11:10,460
Now it is good to use wildcard.

118
00:11:13,700 --> 00:11:16,850
And protect all the branches with feature given as prefix.

119
00:11:30,430 --> 00:11:31,030
That's all.

120
00:11:31,120 --> 00:11:34,690
And we are going to run the pipeline and test it in the next lecture.
