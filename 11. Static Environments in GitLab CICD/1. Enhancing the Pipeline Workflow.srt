1
00:00:00,910 --> 00:00:02,230
Welcome to this new section.

2
00:00:02,650 --> 00:00:07,870
In this section, we will learn about environments in GitLab and using environments with some other

3
00:00:07,870 --> 00:00:08,440
concepts.

4
00:00:08,650 --> 00:00:11,320
We will optimize the project and take it to the next level of.

5
00:00:12,760 --> 00:00:13,630
OK, flashback.

6
00:00:14,660 --> 00:00:20,090
Nice, the workflow using which we developed our project, if you remember, was that in our project

7
00:00:20,090 --> 00:00:22,460
repo, we had basically two types of branches.

8
00:00:22,940 --> 00:00:29,240
The Mean or the master branch and some feature branches where when a developer had to add a new feature,

9
00:00:29,750 --> 00:00:33,200
he used to create his own feature branch and I did code in it.

10
00:00:33,590 --> 00:00:38,660
And finally, when the feature code was complete, he would push that branch to the remote repository,

11
00:00:39,260 --> 00:00:45,890
and RCA was configured in a way that it would deploy that pushed code on to a static staging environment.

12
00:00:46,070 --> 00:00:50,810
You know, the AMP Portal region got Heroku, ABC.com and post staging environment.

13
00:00:50,960 --> 00:00:52,640
Now for that acceptance test warrant.

14
00:00:53,670 --> 00:00:59,520
And after all, this was the application from staging environment was accepted, the code from future

15
00:00:59,520 --> 00:01:06,060
branch was then most into the main branch, part of it as part of the CAA set up from the main branch.

16
00:01:06,090 --> 00:01:08,610
The application was launched under the production environment.

17
00:01:08,790 --> 00:01:09,150
Right?

18
00:01:10,050 --> 00:01:16,470
Well, this kind of workflow in its own is not an invalid approach, as it is a very simple and straightforward

19
00:01:16,470 --> 00:01:17,250
approach to work.

20
00:01:17,880 --> 00:01:21,600
But if you dig deep down into this workflow, you will notice some issues in it.

21
00:01:22,710 --> 00:01:24,540
See, if you boil down the workflow.

22
00:01:24,930 --> 00:01:31,560
The general idea in this kind of workflow is that whenever a developer pushes his branch into a repository,

23
00:01:31,770 --> 00:01:36,150
that branch is deployed to one common static staging environment for preview.

24
00:01:36,900 --> 00:01:42,600
Now this kind of approach that is to deploy every developer's branch onto a single staging environment

25
00:01:42,990 --> 00:01:43,620
works well.

26
00:01:43,770 --> 00:01:47,910
If you have a very few developers working in your team like one or two, very few.

27
00:01:48,480 --> 00:01:54,530
But imagine if you have tens of developers working publicly and each adding a new feature branch daily,

28
00:01:55,110 --> 00:01:56,610
then in that such situation.

29
00:01:57,120 --> 00:02:02,850
Suppose a developer has pushed his future branch and has deployed his application onto the staging environment

30
00:02:03,480 --> 00:02:08,850
and the moment while he was previewing his application, some other developer also pushed his own feature

31
00:02:08,850 --> 00:02:09,930
branch to a repository.

32
00:02:10,380 --> 00:02:14,610
Then what will happen is the new code will get deployed onto the steam staging environment.

33
00:02:15,240 --> 00:02:19,380
Thus, the moment developer one was brewing his change on staging environment.

34
00:02:19,590 --> 00:02:25,500
At the same moment, that application got overridden by developer 2's code and developer, one would

35
00:02:25,500 --> 00:02:27,960
not be able to see his version of application.

36
00:02:29,040 --> 00:02:33,990
And depending upon the number of pushes done each day, this change could continue to infinity.

37
00:02:34,770 --> 00:02:40,200
So guys, in this type of configuration, maybe we have a single static environment under which each

38
00:02:40,200 --> 00:02:42,960
developer is testing or previewing his future branch.

39
00:02:43,380 --> 00:02:48,180
The staging environment will, most of the times, will be congested to review their new apps.

40
00:02:48,510 --> 00:02:53,490
Developers will have to wait for long hours for their turn to get control over staging environment.

41
00:02:53,880 --> 00:03:00,180
Otherwise, they will be playing and overwriting someone's running application, and thus this approach

42
00:03:00,180 --> 00:03:03,000
to deploy every branch to a staging will stop working.

43
00:03:03,420 --> 00:03:06,990
And at that point of time, you would fail to change your CAA configuration.

44
00:03:07,660 --> 00:03:09,330
But the question is what would change?

45
00:03:10,080 --> 00:03:15,780
But change would be, of course, the problem creating part of this workflow, which is this approach

46
00:03:15,810 --> 00:03:19,830
of deploying everyone's code to with common static staging environment.

47
00:03:20,790 --> 00:03:26,280
So to resolve this issue, we have to create a new workflow inverter configuration will be like.

48
00:03:26,580 --> 00:03:31,860
Apart from deploying the feature branch application onto a single common staging environment in this

49
00:03:31,860 --> 00:03:38,280
new workflow, the application from Future Branch would first deploy it on to future branches own personal

50
00:03:38,280 --> 00:03:41,820
dynamic environment dynamic design for each new branch.

51
00:03:41,970 --> 00:03:48,120
A temporary instance of the application perfect branch will be automatically created by OCR configuration.

52
00:03:48,840 --> 00:03:54,180
After a temporary dynamic environment is created, GitLab will deploy your application on that temporary

53
00:03:54,180 --> 00:03:55,140
instance for preview.

54
00:03:55,920 --> 00:04:02,040
So here, as you can see, when developer one pushes his future branch, it is deployed onto some dynamic

55
00:04:02,040 --> 00:04:03,750
environment with this address.

56
00:04:04,320 --> 00:04:09,480
This Xbox One dot com and when developer to push its feature branch.

57
00:04:09,720 --> 00:04:16,290
Even at the same time, a new dynamic environment x x two dot com is created for his application preview.

58
00:04:17,280 --> 00:04:22,650
So this way, both the developers are now independent of each other and not overwriting the work of

59
00:04:22,650 --> 00:04:22,950
others.

60
00:04:23,430 --> 00:04:28,620
So no matter how many developers your team would have from now on, they will not have to wait in a

61
00:04:28,620 --> 00:04:30,420
few days to preview their application.

62
00:04:31,560 --> 00:04:36,930
OK, I guess the important thing to keep in mind here is so after the application is deployed to individual

63
00:04:36,930 --> 00:04:43,200
dynamic environments for whichever developer, the testing and review is past, that branch is then

64
00:04:43,200 --> 00:04:47,820
merged to Main Branch, and the other one will keep on rectifying its caught in its own branch.

65
00:04:48,870 --> 00:04:53,700
After the code is received by main branch, again, there would be a change in how CAA will work.

66
00:04:54,660 --> 00:05:00,570
So this time, when a merger is made to Main Branch, it will first deploy the application to a static

67
00:05:00,570 --> 00:05:01,500
staging environment.

68
00:05:01,860 --> 00:05:06,930
We just kept exclusively only for admin branch, you know, a last stage before the production.

69
00:05:08,140 --> 00:05:13,420
After the application gets approval in staging, either by automated acceptance or manual testing,

70
00:05:13,900 --> 00:05:18,370
it is deployed to its final stage the production environment where it is live to audience.

71
00:05:19,420 --> 00:05:25,300
And yes, as in this floor seems for every feature branch, a separate instance is booted up, and it

72
00:05:25,300 --> 00:05:30,400
is quite obvious that after a few days, there could be tens or even hundreds of temporary instances

73
00:05:30,610 --> 00:05:31,660
consuming resources.

74
00:05:32,170 --> 00:05:37,540
So once the feature branch is merged into mean ducks, the configuration of your project automatically

75
00:05:37,540 --> 00:05:40,810
destroys that temporary instance and frees up the resources.

76
00:05:41,140 --> 00:05:41,500
Got it!

77
00:05:42,290 --> 00:05:46,780
Now, guys, seeing from this new workflow, you will still think that in this workflow.

78
00:05:46,780 --> 00:05:52,480
Also, there are chances with application deployed in this staging environment could be overridden by

79
00:05:52,480 --> 00:05:54,280
the subsequent modules to the main branch.

80
00:05:54,790 --> 00:05:56,350
Well, yes, this could happen.

81
00:05:56,620 --> 00:06:00,550
But in this case, its rate would be very low as compared to the earlier workflow.

82
00:06:01,750 --> 00:06:04,360
So, yeah, that's a more optimized Cassilly workflow.

83
00:06:04,810 --> 00:06:10,300
And in the next few videos, we will trigger project based on this new workflow while learning about

84
00:06:10,300 --> 00:06:12,040
environments in get labs easily.
