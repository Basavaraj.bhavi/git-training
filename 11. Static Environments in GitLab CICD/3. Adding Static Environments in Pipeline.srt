1
00:00:00,640 --> 00:00:03,700
To keep the changes discreet, let's check out the new branch.

2
00:00:10,630 --> 00:00:12,730
He ended up for environment deployments.

3
00:00:14,190 --> 00:00:15,210
Open the quality, it'll.

4
00:00:19,920 --> 00:00:25,350
OK, so as in this pipeline, we are deploying an application to two environments, the staging and

5
00:00:25,350 --> 00:00:26,220
production environment.

6
00:00:26,670 --> 00:00:31,650
So here we will create two static environments, one for each staging and production area.

7
00:00:32,310 --> 00:00:33,780
So under this stage job?

8
00:00:38,080 --> 00:00:39,610
Right, the giver environment.

9
00:00:41,560 --> 00:00:44,110
And under it, give this environment a name.

10
00:00:47,160 --> 00:00:52,390
Since this job will be deployed to a staging environment and give it the relevant static environment

11
00:00:52,390 --> 00:00:53,730
team that is staging.

12
00:00:56,190 --> 00:00:59,100
Another second, though optional, but very important option.

13
00:00:59,430 --> 00:01:06,480
You can pass the ball, which determines the deployment to your hotel for this environment and copy

14
00:01:06,480 --> 00:01:07,140
it from here.

15
00:01:13,810 --> 00:01:17,960
Guys, this mural we pass here is exposed in various places within the gate.

16
00:01:18,300 --> 00:01:23,320
You're looking on which it will directly take you to the specified you wouldn't touch showing you the

17
00:01:23,320 --> 00:01:24,580
current position of a deployment.

18
00:01:25,590 --> 00:01:31,440
OK, so just this piece of code, we have successfully created a staging environment for this deploy

19
00:01:31,440 --> 00:01:34,180
stage job now for the production environment.

20
00:01:34,240 --> 00:01:35,670
The idea to create what we see.

21
00:01:36,360 --> 00:01:38,190
So I'll simply copy paste it.

22
00:01:48,680 --> 00:01:50,000
Change the name.

23
00:01:51,610 --> 00:01:52,240
Production.

24
00:01:55,630 --> 00:01:56,260
Production at.

25
00:02:04,950 --> 00:02:08,310
Do static environments been created like save and push the changes?

26
00:02:32,650 --> 00:02:33,580
Pipeline is running.

27
00:02:41,170 --> 00:02:44,560
In U.S. environments are available under the deployments.

28
00:02:50,180 --> 00:02:51,380
Why only staging environment?

29
00:02:52,160 --> 00:02:53,060
Oh, OK, got it.

30
00:02:54,320 --> 00:02:56,930
There is only men given in the production stage.

31
00:03:00,270 --> 00:03:02,310
Let me quickly remove it and reopen the pipeline.

32
00:03:13,020 --> 00:03:13,710
Scott, this.

33
00:03:41,510 --> 00:03:47,000
Yeah, now both the staging and production environments are hit, a bunch of information is available

34
00:03:47,000 --> 00:03:47,210
here.

35
00:03:47,660 --> 00:03:51,020
Let us wait for the pipeline to complete and the information will get filled here.

36
00:03:54,550 --> 00:04:00,760
Pipeline is completed, and then Waterman Scott created a bunch of information is available here, the

37
00:04:00,760 --> 00:04:03,190
environment name that's linked to its deployment.

38
00:04:04,090 --> 00:04:10,840
The last deployment I number and who performed it, it's me only that job name of the last deployment

39
00:04:10,960 --> 00:04:12,770
with respective job I.D..

40
00:04:14,810 --> 00:04:20,330
Next, what we have is the committee information of the last deployment, like which branch document

41
00:04:20,340 --> 00:04:28,130
was made, who committed and did etc. of document updated the exact time the last deployment was performed.

42
00:04:29,420 --> 00:04:35,690
And here goes this Arrow button will take you to the URL that you have defined under the environment

43
00:04:35,690 --> 00:04:37,680
keyword and get lapped, not by a fight.

44
00:04:38,150 --> 00:04:39,110
So if I click here.

45
00:04:41,200 --> 00:04:45,880
It will take me to the live environment, which is an important staging, Hiroko ABC.com.

46
00:04:46,600 --> 00:04:50,470
And similarly, it will go for that production environment as well.

47
00:04:52,060 --> 00:04:58,300
This retry button clicking on this button will redeploy the latest deployment, meaning it runs a job

48
00:04:58,360 --> 00:05:04,420
defined by the environment name for that specific commit, for example, redeployed to a staging environment.

49
00:05:11,070 --> 00:05:15,840
On this page, you won't see any significant changes, as the information shown in that environment

50
00:05:15,840 --> 00:05:18,180
page is limited to the latest deployments.

51
00:05:18,600 --> 00:05:21,600
But if I open that deployment history of an environment.

52
00:05:26,660 --> 00:05:32,300
Here you will see the running environment, as you can see, we have a running job with the same committee

53
00:05:32,450 --> 00:05:33,920
for which we are deploying it.

54
00:05:34,640 --> 00:05:36,800
If I opened the complete pipeline.

55
00:05:46,670 --> 00:05:52,490
From here, also, you can see redeploying to environment only triggers that job, and other jobs are

56
00:05:52,490 --> 00:05:54,770
not touched back to environments.

57
00:06:01,880 --> 00:06:07,610
Guys, like I said in the previous lecture that GitLab keeps track of your deployments from this page.

58
00:06:09,230 --> 00:06:15,590
You can confirm the same each time a job that has an environment specified in it, it succeeds.

59
00:06:15,890 --> 00:06:17,180
A deployment is recorded.

60
00:06:17,540 --> 00:06:23,900
Remembering the safety and environment name GitLab gives the deployment history of an environment so

61
00:06:23,900 --> 00:06:27,290
that you always know what is currently being deployed on your service.

62
00:06:27,650 --> 00:06:30,410
And this way you can have the full history of deployments.

63
00:06:30,650 --> 00:06:33,950
But environment right in your browser for staging.

64
00:06:34,140 --> 00:06:38,870
There are these few deployments with the first rule, which is currently active.

65
00:06:39,170 --> 00:06:42,280
And the second one is historical on the extreme right.

66
00:06:42,290 --> 00:06:45,680
If you notice, these icons for two rows are different.

67
00:06:46,460 --> 00:06:50,900
The first one for active deployment, it says, redeployed to environment.

68
00:06:51,260 --> 00:06:56,780
This thing we already saw, but for the old deployment, it says roll back environment.

69
00:06:57,590 --> 00:07:00,200
Well, during development, you can't control everything.

70
00:07:00,500 --> 00:07:04,940
So it's a natural thing that sometimes things go wrong and not in a way you want it.

71
00:07:05,420 --> 00:07:10,010
Hence, wrong deployments could happen to get you covered from these long deployments.

72
00:07:10,310 --> 00:07:16,820
GitLab gives you the power to roll back to any previous successful deployment when you roll back a deployment

73
00:07:16,820 --> 00:07:17,930
on a specific commit.

74
00:07:18,320 --> 00:07:21,350
A new deployment of that old snapshot is created.

75
00:07:22,070 --> 00:07:26,830
This deployment has its own unique job idea, and it points to the commit you are rolling back.

76
00:07:27,680 --> 00:07:30,950
But again, though, GitLab gives you this rollback feature.

77
00:07:31,340 --> 00:07:37,250
Please keep in mind that the final status of rollback it is a success or failure would entirely depend

78
00:07:37,250 --> 00:07:38,780
on the scripts that you have written.

79
00:07:46,610 --> 00:07:52,460
And the last button we have had is Stop Environment button by stopping any environment, you are effectively

80
00:07:52,460 --> 00:07:55,880
terminating its recording of that deployment that happened in it.

81
00:07:56,510 --> 00:08:01,550
Suppose there are multiple developers working on a project at the same time, each of them pushing to

82
00:08:01,550 --> 00:08:05,060
their own branches, causing many dynamic environments to be created.

83
00:08:05,540 --> 00:08:10,520
So in that sort of situation, you can stop that environment making to stop its deployment occurring.

84
00:08:11,270 --> 00:08:16,850
Stopping any environment moves the environment from this available environmentalist to this stopped

85
00:08:16,850 --> 00:08:17,660
environment speech.

86
00:08:20,140 --> 00:08:23,930
If you still have doubts in this button, don't worry, things will get clear.

87
00:08:24,190 --> 00:08:26,380
When will you stop environment for our project?

88
00:08:27,400 --> 00:08:27,790
All right.

89
00:08:28,060 --> 00:08:31,480
So that was how you can create and define your own environment.

90
00:08:31,870 --> 00:08:32,860
That's it for this lecture.

91
00:08:33,160 --> 00:08:37,270
See you guys in the next class where I'm going to explain how a rollback, folks.
