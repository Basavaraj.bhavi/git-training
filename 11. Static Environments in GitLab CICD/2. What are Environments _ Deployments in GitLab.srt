1
00:00:00,590 --> 00:00:06,890
Well, come on now that we have our new road map that is a workflow already starting from this lecture,

2
00:00:07,250 --> 00:00:10,700
we will rectify the project so as to align it with the new workflow.

3
00:00:11,150 --> 00:00:16,610
And as a first move, we will start by learning the concept environments and deployments and GitLab

4
00:00:17,060 --> 00:00:22,820
and specifically will start with creating these new static environments and then move on to these dynamic

5
00:00:22,820 --> 00:00:23,090
ones.

6
00:00:23,840 --> 00:00:27,080
OK, so let's start the team environment in general.

7
00:00:27,080 --> 00:00:32,630
English, obviously with contextual development and get lepeska board, symbolizes the same meaning

8
00:00:33,020 --> 00:00:37,040
it is the conditions or ecosystem in which we deploy the application.

9
00:00:38,200 --> 00:00:43,990
So the first point to finding both environments and deployments in one line, if I see environments

10
00:00:43,990 --> 00:00:46,150
describe where the code is deployed.

11
00:00:47,020 --> 00:00:54,910
And each time GitLab deploys a version of code to an environment, it is set to be a deployment with

12
00:00:54,910 --> 00:00:55,570
environments.

13
00:00:55,750 --> 00:00:59,680
You can control the continuous deployment of your software, all within GitLab.

14
00:01:00,010 --> 00:01:02,560
All you need to do is define them in your projects.

15
00:01:02,570 --> 00:01:04,090
GitLab 0.5 mill fight.

16
00:01:05,390 --> 00:01:10,670
Environments are like bags for you, the jobs describing the environment where gold get deployed.

17
00:01:11,210 --> 00:01:18,200
And each time when jobs deploy versions of go to environments, deployments are created so every environment

18
00:01:18,410 --> 00:01:20,360
can have one or more deployments.

19
00:01:21,050 --> 00:01:23,150
I know these sentences can be quite confusing.

20
00:01:23,380 --> 00:01:23,900
No is.

21
00:01:24,050 --> 00:01:25,730
It will be clear when we do practical.

22
00:01:26,900 --> 00:01:29,090
Next point is with adding environments.

23
00:01:29,450 --> 00:01:34,940
You can always know what is currently being deployed or has been deployed on your service, like which

24
00:01:34,940 --> 00:01:37,220
application budget is running on which environment.

25
00:01:38,710 --> 00:01:42,220
Gettler provides a full history of your deployments, but environment.

26
00:01:43,610 --> 00:01:48,710
To create and define environments, you can choose from two options, either you can create environments

27
00:01:48,710 --> 00:01:54,710
from GitLab UI or the other way the best way you can define them in your projects get LeBron environmental

28
00:01:54,710 --> 00:01:54,980
fight.

29
00:01:56,030 --> 00:02:01,610
There are two types of environments static environments having static names like staging or production.

30
00:02:02,270 --> 00:02:04,160
Dynamic environments have dynamic names.

31
00:02:04,640 --> 00:02:06,860
These two will theme detail in the later video.

32
00:02:08,440 --> 00:02:14,190
OK, so these were few points about environments and deployments to better understand how environments

33
00:02:14,190 --> 00:02:15,120
and deployments work.

34
00:02:15,360 --> 00:02:16,710
Let's start with an example.

35
00:02:16,980 --> 00:02:20,700
And for example, we will continue with the project code from where we left.
