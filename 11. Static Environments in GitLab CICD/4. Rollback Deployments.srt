1
00:00:00,330 --> 00:00:05,340
Contending with the rollback in the last elected, like I said, a successful rollback would depend

2
00:00:05,340 --> 00:00:06,420
upon your job scripts.

3
00:00:06,900 --> 00:00:12,210
You cannot just simply click on this rollback button and expect that previous deployment you get left

4
00:00:12,210 --> 00:00:15,330
by a mail file should be designed in a way for a proper rollback.

5
00:00:15,720 --> 00:00:18,450
Let me explain it out well, in our case.

6
00:00:18,450 --> 00:00:21,120
Also, I have multiple deployments available in this list.

7
00:00:21,840 --> 00:00:25,560
This current deployment is the one with welcome to imply portal.

8
00:00:27,710 --> 00:00:34,110
With this deployment and the second deployment, it was without the doubt.

9
00:00:34,970 --> 00:00:40,940
Now if I want to roll back to this previous version of application, what I will do is I will click

10
00:00:40,940 --> 00:00:42,410
on this rollback button and see.

11
00:00:50,860 --> 00:00:53,170
My plane is running safe to.

12
00:00:53,860 --> 00:00:58,360
This job is creating a deployment to staging, this will override the latest deployment.

13
00:01:00,750 --> 00:01:02,550
This rollback is a success.

14
00:01:02,790 --> 00:01:03,900
And let's check the way.

15
00:01:08,260 --> 00:01:08,620
Oops!

16
00:01:08,770 --> 00:01:10,060
Still, the harder is hit.

17
00:01:10,750 --> 00:01:13,570
It means even though the roll back shows a success status.

18
00:01:13,750 --> 00:01:17,110
But unfortunately, we are not seeing any change in the application.

19
00:01:17,680 --> 00:01:23,300
Well, this was expected and that is why I think that an effective rollback depends on the job scripts.

20
00:01:23,860 --> 00:01:25,420
Let's now find out what happened here.

21
00:01:27,040 --> 00:01:28,790
First things first, many do roll back.

22
00:01:28,810 --> 00:01:31,270
It does not mean that the whole pipeline is run again.

23
00:01:31,720 --> 00:01:35,740
No rollback and only that stage in which the environment is defined.

24
00:01:36,220 --> 00:01:37,420
For example, in our case.

25
00:01:45,670 --> 00:01:48,580
This is the pipeline which was just run as part of rollback.

26
00:01:51,950 --> 00:01:57,650
In our case, going rollback means we are running this deploy stage again of that commodity.

27
00:01:57,950 --> 00:02:01,640
It means this portion of coal is going to get run on rollback.

28
00:02:05,050 --> 00:02:06,610
You can even confirm it from here.

29
00:02:06,820 --> 00:02:10,570
This stage around two times, and this is the latest one we just ran.

30
00:02:11,840 --> 00:02:17,150
This was one thing, and another thing to watch out is how Docker images are getting stored in the repository

31
00:02:17,630 --> 00:02:19,130
for that go to build job.

32
00:02:23,840 --> 00:02:24,770
See in here.

33
00:02:25,040 --> 00:02:29,540
Check the way we are tagging and studying the Docker image while building the project Docker image,

34
00:02:29,840 --> 00:02:35,420
we are tagging that image name with bag value coming from this image that variable the value of which.

35
00:02:37,420 --> 00:02:40,630
Is this a registry name slash employer image?

36
00:02:41,270 --> 00:02:42,850
Colin, this is important.

37
00:02:43,360 --> 00:02:48,580
They commit an f slug, which resolves to the branch name that push that change for that right right?

38
00:02:49,540 --> 00:02:53,830
So means the final image name will be stored as the branch name for which it was run.

39
00:02:54,520 --> 00:02:58,660
Also in Docker, each image is distinguished from the other image from its deck.

40
00:02:58,990 --> 00:03:04,170
It means for every pipeline run which is done from the same branch, each of those runs would create

41
00:03:04,180 --> 00:03:05,890
a Docker image with the same name.

42
00:03:06,340 --> 00:03:10,990
It's just the latest Docker image will overwrite the previous image, but the name will be seen.

43
00:03:11,380 --> 00:03:12,400
I can't even show it to you.

44
00:03:13,150 --> 00:03:15,520
Let's go back to the repository.

45
00:03:17,160 --> 00:03:18,420
Go to container part your screen.

46
00:03:22,950 --> 00:03:23,520
Open this.

47
00:03:26,260 --> 00:03:31,720
We were running the pipelines from Ian Branch, and I had run the pipeline multiple times from this

48
00:03:31,720 --> 00:03:34,650
branch, but in the repository, only one images there.

49
00:03:35,170 --> 00:03:39,340
This image contains the code with welcome to Imply Portal in the index file.

50
00:03:39,640 --> 00:03:42,370
And this is the reason why the roll back did not go right.

51
00:03:42,970 --> 00:03:43,600
You explain it.

52
00:03:43,780 --> 00:03:45,340
I will come back to my previous point.

53
00:03:45,670 --> 00:03:50,140
That rollback means running off only that stage in which the environment is defined.

54
00:03:50,650 --> 00:03:51,130
That is.

55
00:03:55,900 --> 00:03:57,190
This deploy stage.

56
00:03:58,970 --> 00:04:01,910
So let's see what would have happened when I clicked on that roll back button.

57
00:04:02,750 --> 00:04:09,770
This stage got triggered upon rollback, which says to pull this image, which it is all in with the

58
00:04:09,790 --> 00:04:10,460
bag image.

59
00:04:10,880 --> 00:04:15,980
And like I showed, you just know that image contains the code, but welcome to imply portal in next

60
00:04:15,980 --> 00:04:16,220
flight.

61
00:04:16,760 --> 00:04:21,920
So basically with rolling back, GitLab is trying to deploy the same version of Docker image, and hence

62
00:04:22,040 --> 00:04:24,800
we did not see any change in the application problem.

63
00:04:24,800 --> 00:04:25,100
It is.

64
00:04:25,920 --> 00:04:26,720
What's the solution?

65
00:04:27,290 --> 00:04:32,450
The easiest solution to this problem is to tag the image in some different way so that we are able to

66
00:04:32,450 --> 00:04:35,120
keep the history of images Bachmann tidy.

67
00:04:35,750 --> 00:04:37,910
We need to modify this.

68
00:04:40,770 --> 00:04:46,890
Image that convention in such a way that for each committed, it should generate a new image deck.

69
00:04:47,490 --> 00:04:49,260
That is why I will change this.

70
00:04:49,320 --> 00:04:52,800
CIA commit refs look with another built in environment variable.

71
00:04:54,510 --> 00:04:55,440
See, I commit.

72
00:04:58,400 --> 00:05:03,080
Short asset that gives the first aid characters off the witness.

73
00:05:03,980 --> 00:05:09,870
So this way, for each moment, it will build a different image with a new tack, thus making the actual

74
00:05:09,870 --> 00:05:10,670
rollback possible.

75
00:05:11,180 --> 00:05:16,010
From now on, for every build, a Docker image will be created with the names committed.

76
00:05:16,520 --> 00:05:22,040
And for any subsequent pipeline runs, the images will not get overwritten and a history of images park

77
00:05:22,040 --> 00:05:25,500
community will be maintained not owing to the same reason.

78
00:05:25,760 --> 00:05:31,190
When you have an image available corresponding to a committed rolling back will now pull that particular

79
00:05:31,190 --> 00:05:32,360
image from the repository.

80
00:05:33,380 --> 00:05:40,070
Save it So for this new pipeline, first, let me deploy with header.

81
00:05:58,280 --> 00:05:59,420
Pipeline is a success.

82
00:06:03,320 --> 00:06:07,160
And an application has come to imply Portal Aspen, you get lab configuration.

83
00:06:08,230 --> 00:06:08,650
Now.

84
00:06:11,690 --> 00:06:14,630
With the same viral code, I will run it without the header.

85
00:06:18,470 --> 00:06:19,400
Again, the same thing.

86
00:06:36,800 --> 00:06:38,660
Meanwhile, the second pipeline is running.

87
00:06:39,140 --> 00:06:41,120
Let's check the image stored in the repository.

88
00:06:50,280 --> 00:06:55,020
As you can see, this time, the Docker image Doug specified with the command reference.

89
00:06:58,400 --> 00:07:01,460
This pipeline also ran successfully and.

90
00:07:04,960 --> 00:07:07,840
The new application without the headline is deployed.

91
00:07:09,000 --> 00:07:10,980
Now, if I again open the container industry.

92
00:07:14,510 --> 00:07:18,170
You can see for this comment, we go to different Docker image.

93
00:07:18,350 --> 00:07:22,610
And this comment hasn't overwritten the Docker image buried from previous comment.

94
00:07:23,330 --> 00:07:28,070
Now if we try to roll back to this old Docker image, hopefully it is going to be a successful roll

95
00:07:28,070 --> 00:07:28,670
back this time.

96
00:07:29,810 --> 00:07:31,010
Go back to environments.

97
00:07:37,630 --> 00:07:40,010
This is Ron England without.

98
00:07:40,390 --> 00:07:46,870
And this is a previous deployment, whether I want to roll back the application, whether this action

99
00:07:46,870 --> 00:07:52,450
will render job defined by staging for this commodity, putting the environment in a previous version,

100
00:07:52,570 --> 00:07:54,400
OK, conform to rollback.

101
00:07:59,970 --> 00:08:01,740
This job is creating a deployment or staging.

102
00:08:04,090 --> 00:08:07,510
GitLab has started creating an all deployment for this staging environment.

103
00:08:08,680 --> 00:08:09,730
If I showed you the pipeline.

104
00:08:13,430 --> 00:08:18,830
See, it's just running the concert on stage only in which environment is defined and will create a

105
00:08:18,830 --> 00:08:20,500
new deployment for that all coming.

106
00:08:22,130 --> 00:08:24,140
Anyways, I will let the roll back to complete.

107
00:08:26,320 --> 00:08:32,020
Great rollback for previous comment is completed, and hopefully a new deployment should have been created,

108
00:08:33,010 --> 00:08:34,360
refreshing the application page.

109
00:08:36,210 --> 00:08:36,780
Here we go.

110
00:08:37,260 --> 00:08:43,140
Welcome to employ Portal is back again, GitLab has successfully rolled back and have done a new deployment

111
00:08:43,140 --> 00:08:43,930
for Automate.

112
00:08:44,710 --> 00:08:46,710
So that was how you can do deployment.

113
00:08:46,710 --> 00:08:50,790
Rollbacks and GitLab rollbacks are important as you can't control everything.

114
00:08:51,000 --> 00:08:54,510
So sometimes things go wrong when that unfortunate time comes.

115
00:08:54,720 --> 00:08:59,700
You can simply relaunch the previous deployment by clicking the roll back button if the pipeline is

116
00:08:59,700 --> 00:09:01,110
properly defined, obviously.

117
00:09:01,770 --> 00:09:02,610
So that's it for now.

118
00:09:02,640 --> 00:09:03,540
So your next class?
