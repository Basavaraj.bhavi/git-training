1
00:00:00,880 --> 00:00:06,470
Welcome to the small lecture in where we will learn about a very useful tool that can lead to.

2
00:00:06,970 --> 00:00:13,510
OK, so what is lead as already seen in previous lectures, lint or litter in softer language is a static

3
00:00:13,510 --> 00:00:18,370
code analysis tool used for checking the code for any programmatic and stylistic errors.

4
00:00:19,060 --> 00:00:21,520
Lending in any softer language is important.

5
00:00:21,820 --> 00:00:27,730
As with lending, we test the validity of our code at very early stage so as to accelerate the development

6
00:00:27,730 --> 00:00:31,700
process and reduce the overall cost lending for Python code.

7
00:00:31,720 --> 00:00:37,600
We have already done with Flicka tool, and now I'm talking about performing lint testing on the GitLab

8
00:00:37,600 --> 00:00:43,510
CASELY configuration that is the Yamal code for validating the GitLab 6.5 Animals Index.

9
00:00:43,840 --> 00:00:46,390
GitLab has provided us with a calendar tool.

10
00:00:46,720 --> 00:00:48,970
And in this lecture, we're going to work with it.

11
00:00:49,660 --> 00:00:52,750
OK, now to access it, first, go to the pipelines.

12
00:00:59,170 --> 00:01:01,870
And on the top right, we have this silent option.

13
00:01:04,610 --> 00:01:10,810
Here it is, as you can see, the tool is nothing, just an editor where you will simply paste or YAML

14
00:01:10,820 --> 00:01:12,230
code and validate that.

15
00:01:12,980 --> 00:01:14,330
I'll paste a demo code for it.

16
00:01:18,280 --> 00:01:20,620
You can pass the video and take a glance at this demo.

17
00:01:22,060 --> 00:01:22,870
No validate.

18
00:01:26,970 --> 00:01:27,320
Great.

19
00:01:28,230 --> 00:01:29,610
It says the syntax is correct.

20
00:01:31,340 --> 00:01:36,980
Guys, if I scroll down along with the syntax validation results, this tool has also printed us above

21
00:01:36,980 --> 00:01:42,980
pipeline into tabular form and has shown us that jobs with that execution order, whatever stages are

22
00:01:42,980 --> 00:01:48,680
defined in the YAML code and in what order are nicely represented here, like in our pipeline.

23
00:01:50,900 --> 00:01:57,320
First ability of children, then the best job, well done and lost the deployed job on the right side

24
00:01:57,320 --> 00:01:59,420
of this table in front of each job.

25
00:01:59,990 --> 00:02:02,990
What their job is doing and when the job will run is short.

26
00:02:03,530 --> 00:02:05,840
So the military job will equal out this message.

27
00:02:06,850 --> 00:02:13,960
And by default, when Kiva is set to own success on success means execute job only when all the jobs

28
00:02:13,960 --> 00:02:15,460
in earlier stages succeed.

29
00:02:16,360 --> 00:02:19,600
And similarly, the same data is shown for other jobs as well.

30
00:02:20,750 --> 00:02:22,640
OK, so this time our caller was correct.

31
00:02:23,120 --> 00:02:26,330
Now, if I add some mistake in this court, let's see what happens.

32
00:02:27,980 --> 00:02:31,820
Now, let's see if I forgot to add this to stage in instead U.S..

33
00:02:35,240 --> 00:02:35,990
No validate.

34
00:02:41,280 --> 00:02:45,000
We know it is a wrong configuration, and hence it is showing us an error.

35
00:02:45,630 --> 00:02:47,760
It says to then stage does not exist.

36
00:02:48,060 --> 00:02:51,030
Available stages are free test, deploy and post.

37
00:02:52,020 --> 00:02:53,460
Now, let's hear it again.

38
00:02:56,530 --> 00:02:56,890
He did.

39
00:02:59,150 --> 00:02:59,630
It's good.

40
00:03:00,230 --> 00:03:04,760
So by this way, you can use the island to validate your original syntax and.

41
00:03:05,480 --> 00:03:10,170
This was a very short lecture as my purpose of this lecture was just to introduce with salient.

42
00:03:10,940 --> 00:03:15,890
I would recommend you people to use this tool before running any of your pipelines, as this tool will

43
00:03:15,890 --> 00:03:20,540
help you rectify the code without even running it, which will definitely save your cost in time.

44
00:03:20,810 --> 00:03:21,230
Thank you.
