1
00:00:01,530 --> 00:00:05,910
Welcome to this lecture, where we will learn how to set climate settings in GitLab.

2
00:00:06,570 --> 00:00:08,520
OK, so first of all, what is timeout?

3
00:00:09,240 --> 00:00:14,310
Timeout, as we already saw in previous lectures, defines the maximum amount of time in minutes.

4
00:00:14,550 --> 00:00:15,930
Not a job is able to run.

5
00:00:16,440 --> 00:00:20,340
Basically, it is the upper limit of time that a job can take to complete.

6
00:00:20,880 --> 00:00:26,310
But in case if your job execution time exceeds that timeout amount, GitLab automatically mocks that

7
00:00:26,310 --> 00:00:29,580
job as field not related to timeout error.

8
00:00:29,600 --> 00:00:35,580
Few pointers to highlight GitLab allows you to set three type of timeouts the job level timeout.

9
00:00:35,880 --> 00:00:41,610
Second one is an enough specific timeout and last project level timeout, starting with job level.

10
00:00:42,330 --> 00:00:46,050
As the name implies, job level timeout is up late at the job level.

11
00:00:46,710 --> 00:00:50,640
It defines the maximum amount of time a job can take to complete.

12
00:00:51,330 --> 00:00:57,120
This timeout can be used to forcefully stop the jobs that ideally you think should take few seconds

13
00:00:57,120 --> 00:01:02,460
or minutes to get complete, but due to some glitches or deadlocks are taking more time than your expectation.

14
00:01:03,120 --> 00:01:08,460
On the other hand, there are no specific timeout defines maximum job timeout for each runner.

15
00:01:09,290 --> 00:01:10,140
Run a timeout.

16
00:01:10,200 --> 00:01:15,180
Prevent your children from being overwhelmed by a project that has jobs with a long time.

17
00:01:15,510 --> 00:01:23,790
For example, when we last project no timeout as job level timeout is applied for specific or individual

18
00:01:23,790 --> 00:01:24,210
jobs.

19
00:01:24,600 --> 00:01:26,970
The project level timeout is applied at the root.

20
00:01:26,970 --> 00:01:32,940
Project level means every job in your particular project will inherently follow the project timeout.

21
00:01:33,210 --> 00:01:36,210
If you do not specify the job level timeout explicitly.

22
00:01:37,670 --> 00:01:45,020
Next, if both job level and project level are specified, then the jobs for which job level demoted

23
00:01:45,020 --> 00:01:50,180
specified will come under the job level timeline and the rest one will follow the project level timeline.

24
00:01:50,720 --> 00:01:53,840
Well, if you're a little confused, don't worry, we will see them in an example.

25
00:01:55,220 --> 00:01:59,840
The job level time out can exceed the project level timeline, but can't exceed that another specific

26
00:01:59,840 --> 00:02:00,050
time.

27
00:02:01,580 --> 00:02:06,800
You can decrease that time limit if you want to impose a hard limit on your jobs at any time or increase

28
00:02:06,800 --> 00:02:08,150
it otherwise again.

29
00:02:08,270 --> 00:02:14,480
In any case, if the job surpassed the threshold, it is marked as fate and the default timeout, as

30
00:02:14,480 --> 00:02:16,520
we already saw, is set to run out by GitLab.

31
00:02:17,660 --> 00:02:19,820
All right, so these were a few points about time out.

32
00:02:20,300 --> 00:02:22,210
Let's quickly see how we can set it in.

33
00:02:25,000 --> 00:02:30,160
Let's create a simple pipeline that does nothing but simply sleeps for a few minutes.

34
00:02:31,580 --> 00:02:37,160
OK, so if we have two jobs here with each job, first printing some message, then sleeps for two minutes

35
00:02:37,460 --> 00:02:39,500
and then again printing some exit message.

36
00:02:40,100 --> 00:02:45,260
Notice that there is no state just defined means both these jobs can run in parallel, committed.

37
00:03:05,840 --> 00:03:11,150
Guys, since we haven't said the time until now, so these jobs will fall under that default one time

38
00:03:11,150 --> 00:03:16,310
out of project level, and we'll of course, get a successful completion because the maximum sleep time

39
00:03:16,310 --> 00:03:16,940
is two minutes.

40
00:03:17,390 --> 00:03:19,520
And both jobs will get complete after two minutes.

41
00:03:20,360 --> 00:03:21,650
Let me wait for it to complete.

42
00:03:23,600 --> 00:03:24,860
See, it's a success.

43
00:03:25,820 --> 00:03:31,790
OK, now with the same pipeline, let's try to play with the project and dropped out first set the project

44
00:03:31,800 --> 00:03:34,700
level timeline for that from these settings.

45
00:03:35,570 --> 00:03:35,960
What was the.

46
00:03:40,380 --> 00:03:41,990
And expand general pipelines.

47
00:03:45,800 --> 00:03:49,310
Here is a timeout see, by default, it is set for one not.

48
00:03:50,830 --> 00:03:52,170
Inter Pipeline sleep is of.

49
00:03:53,050 --> 00:03:55,390
Let's decrease the time to say one minute.

50
00:03:57,750 --> 00:03:58,500
Safety news.

51
00:04:03,300 --> 00:04:03,670
OK.

52
00:04:03,690 --> 00:04:10,050
It says Baltimore needs to be between 10 minutes and one month, so it means the minimum we can set

53
00:04:10,050 --> 00:04:11,250
is 11 minutes.

54
00:04:20,960 --> 00:04:25,280
Remember, unless you override the job timeouts in pipeline this timeout.

55
00:04:28,170 --> 00:04:33,570
Will get automatically applied to all the jobs under this project now, project time being set.

56
00:04:33,620 --> 00:04:35,880
No, I need to change the sleep time.

57
00:04:45,860 --> 00:04:47,900
Let's change it to 30 minutes.

58
00:04:51,380 --> 00:04:55,970
So now the situation is that a project level timeout is 11 minutes.

59
00:04:56,300 --> 00:04:58,520
And both these jobs will sleep for 30 minutes.

60
00:05:05,590 --> 00:05:06,520
By blending started.

61
00:05:11,440 --> 00:05:11,890
When it.

62
00:05:16,780 --> 00:05:19,540
See, the time out is 11 minutes.

63
00:05:19,630 --> 00:05:25,540
We're just coming from the project level timeout, which we just said just confirm for Job two as well.

64
00:05:27,960 --> 00:05:28,200
Yeah.

65
00:05:28,290 --> 00:05:33,900
11 minutes is a timeout now esperar settings, both these jobs should feel after 11 minutes.

66
00:05:36,640 --> 00:05:43,930
Good motor jobs failed because of the demand better as soon as that duration of job exceeded the timeout

67
00:05:43,930 --> 00:05:45,490
value of 11 minutes.

68
00:05:45,820 --> 00:05:49,090
It was forcefully stopped and magnetic field job by GitLab.

69
00:05:50,230 --> 00:05:55,030
Let's now see what happens if we said the job level timeout for one job for that.

70
00:05:58,060 --> 00:06:01,780
Let's add the job out for, let's say, a second job.

71
00:06:09,630 --> 00:06:12,900
To specify the job level timeout after the script tag.

72
00:06:14,390 --> 00:06:16,970
Type timeout.

73
00:06:19,120 --> 00:06:21,700
And then specify that time, let's say, 20 minutes.

74
00:06:23,420 --> 00:06:29,240
With this by default behavior of GitLab, the timeout for job one should come from project level timeout

75
00:06:29,480 --> 00:06:31,940
that is 11 minutes and four job to.

76
00:06:32,120 --> 00:06:34,490
It should be overridden by job level timeout.

77
00:06:34,640 --> 00:06:35,660
That is minutes.

78
00:06:50,390 --> 00:06:52,040
So open the job one.

79
00:06:55,580 --> 00:06:59,990
See, payment for job one is coming from Project and 4.2.

80
00:07:05,210 --> 00:07:10,790
It's coming from job level timeout that we specified in our e-mail fight now, job one will fail if

81
00:07:10,790 --> 00:07:12,800
it does not get complete after 11 minutes.

82
00:07:13,100 --> 00:07:16,220
But Job two will fail if it doesn't get complete after 20 minutes.

83
00:07:17,210 --> 00:07:19,930
Okay, so that was how you can control the maximum time limit.

84
00:07:19,940 --> 00:07:24,090
A job can execute and save your pipelines from running long time in deadlock as.
