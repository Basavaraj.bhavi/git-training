1
00:00:00,940 --> 00:00:05,650
Welcome to this lecture, where I'm going to cover a very small topic on how to schedule pipelines in

2
00:00:05,650 --> 00:00:09,940
GitLab, generally, pipelines are run when some certain conditions are met.

3
00:00:10,210 --> 00:00:15,940
For example, when there is a committee on a branch or when a branch is pushed to repository upon merge

4
00:00:16,090 --> 00:00:17,290
or these are the factors.

5
00:00:17,950 --> 00:00:21,760
But running a pipeline in GitLab is not limited to these certain events.

6
00:00:21,760 --> 00:00:28,000
Only GitLab also provides us an option to schedule the pipelines to run repeatedly at specific intervals

7
00:00:28,270 --> 00:00:34,480
intervals like we can set the pipeline to run daily at, say, 11:00 a.m. or after every six hours or

8
00:00:34,510 --> 00:00:36,070
certain date of month and so on.

9
00:00:37,100 --> 00:00:41,030
Now to these settings and get lapses, we are given a section schedule.

10
00:00:41,450 --> 00:00:47,180
Let's explore it now and you go to schedules from Sicily Section two schedules.

11
00:00:51,510 --> 00:00:54,540
Currently, there are more scheduled said, let's create a new one.

12
00:00:57,030 --> 00:00:59,210
First, we need to send a description for this pipeline.

13
00:01:03,780 --> 00:01:09,780
Then is the main thing that will define when and at what intervals, despite being children hit out

14
00:01:10,290 --> 00:01:14,280
here we are given with few predefined intervals every day.

15
00:01:15,520 --> 00:01:22,750
Every week, every month, or you can set your own custom intervals with calls and text, and guys,

16
00:01:22,750 --> 00:01:28,510
if you don't know how to generate a content text, then you can take help from a very popular website

17
00:01:28,630 --> 00:01:29,680
crontab dot group.

18
00:01:30,980 --> 00:01:33,290
Scroll down examples.

19
00:01:34,840 --> 00:01:39,250
We have a number of commonly used grounds indexes here, let's say every two us.

20
00:01:45,120 --> 00:01:50,790
Next to the throne time zone, the default is at two UTC, minus Estey.

21
00:01:56,050 --> 00:01:58,550
Then choose the target branch for this pipeline.

22
00:01:58,570 --> 00:01:59,260
I will keep it.

23
00:01:59,410 --> 00:02:03,400
Men only may not muster last if needed.

24
00:02:03,430 --> 00:02:06,480
You can add any custom variables required activator.

25
00:02:06,520 --> 00:02:06,910
Yes.

26
00:02:07,300 --> 00:02:09,310
And finally, see if by Blanchette do.

27
00:02:10,500 --> 00:02:11,460
And there's one more thing.

28
00:02:13,950 --> 00:02:18,000
Scheduled pipelines cannot run more frequently than once per 60 minutes.

29
00:02:18,360 --> 00:02:24,180
A pipeline configured to run more frequently only starts after 60 Minutes have elapsed since the last

30
00:02:24,180 --> 00:02:24,720
time it ran.

31
00:02:25,050 --> 00:02:28,950
So it means you cannot schedule the pipeline after one minute or two minutes or so.

32
00:02:29,280 --> 00:02:32,910
Minimum interval between two pipeline runs should be 60 minutes, at least.

33
00:02:38,340 --> 00:02:39,540
So here we go.

34
00:02:39,990 --> 00:02:41,940
We got one entry in schedule pipelines.

35
00:02:42,360 --> 00:02:48,180
We have the pipeline description target under last pipeline currently entering Northern.

36
00:02:48,510 --> 00:02:54,420
But once your pipeline is run, it will show that I-90 here then is the next run means after how much

37
00:02:54,420 --> 00:02:56,760
time the next run for this pipeline will happen.

38
00:02:57,450 --> 00:03:01,380
The next run is automatically calculated by the server GitLab is installed on.

39
00:03:03,490 --> 00:03:09,270
Then we have the owner name play button, if you want to execute the pipeline right now, then edit

40
00:03:09,280 --> 00:03:10,910
the schedule or delete it.

41
00:03:12,410 --> 00:03:12,790
Great.

42
00:03:13,120 --> 00:03:14,110
So yeah, that much.

43
00:03:14,290 --> 00:03:20,290
The schedule pipeline will execute after 43 minutes and you will get to run it for the scene.

44
00:03:21,250 --> 00:03:23,590
So that was how you can set schedules for pipeline.

45
00:03:24,310 --> 00:03:25,150
So your next class.
